<?php

use Faker\Factory as Faker;
use App\Entities\ReviewJob;
use App\Repositories\ReviewJobRepository;

trait MakeReviewJobTrait
{
    /**
     * Create fake instance of ReviewJob and save it in database
     *
     * @param array $reviewJobFields
     * @return ReviewJob
     */
    public function makeReviewJob($reviewJobFields = [])
    {
        /** @var ReviewJobRepository $reviewJobRepo */
        $reviewJobRepo = App::make(ReviewJobRepository::class);
        $theme = $this->fakeReviewJobData($reviewJobFields);
        return $reviewJobRepo->create($theme);
    }

    /**
     * Get fake instance of ReviewJob
     *
     * @param array $reviewJobFields
     * @return ReviewJob
     */
    public function fakeReviewJob($reviewJobFields = [])
    {
        return new ReviewJob($this->fakeReviewJobData($reviewJobFields));
    }

    /**
     * Get fake data of ReviewJob
     *
     * @param array $postFields
     * @return array
     */
    public function fakeReviewJobData($reviewJobFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'job_id' => $fake->randomDigitNotNull,
            'start' => $fake->randomDigitNotNull,
            'review' => $fake->text,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $reviewJobFields);
    }
}
