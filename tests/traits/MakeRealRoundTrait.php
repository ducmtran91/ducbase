<?php

use Faker\Factory as Faker;
use App\Entities\RealRound;
use App\Repositories\RealRoundRepository;

trait MakeRealRoundTrait
{
    /**
     * Create fake instance of RealRound and save it in database
     *
     * @param array $realRoundFields
     * @return RealRound
     */
    public function makeRealRound($realRoundFields = [])
    {
        /** @var RealRoundRepository $realRoundRepo */
        $realRoundRepo = App::make(RealRoundRepository::class);
        $theme = $this->fakeRealRoundData($realRoundFields);
        return $realRoundRepo->create($theme);
    }

    /**
     * Get fake instance of RealRound
     *
     * @param array $realRoundFields
     * @return RealRound
     */
    public function fakeRealRound($realRoundFields = [])
    {
        return new RealRound($this->fakeRealRoundData($realRoundFields));
    }

    /**
     * Get fake data of RealRound
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRealRoundData($realRoundFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'season_id' => $fake->randomDigitNotNull,
            'round' => $fake->randomDigitNotNull,
            'start_at' => $fake->date('Y-m-d H:i:s'),
            'end_at' => $fake->date('Y-m-d H:i:s')
        ], $realRoundFields);
    }
}
