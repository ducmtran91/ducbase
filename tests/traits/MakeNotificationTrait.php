<?php

use Faker\Factory as Faker;
use App\Entities\WizepawzNotification;
use App\Repositories\NotificationRepository;

trait MakeNotificationTrait
{
    /**
     * Create fake instance of Notification and save it in database
     *
     * @param array $notificationFields
     * @return WizepawzNotification
     */
    public function makeNotification($notificationFields = [])
    {
        /** @var NotificationRepository $notificationRepo */
        $notificationRepo = App::make(NotificationRepository::class);
        $theme = $this->fakeNotificationData($notificationFields);
        return $notificationRepo->create($theme);
    }

    /**
     * Get fake instance of Notification
     *
     * @param array $notificationFields
     * @return WizepawzNotification
     */
    public function fakeNotification($notificationFields = [])
    {
        return new WizepawzNotification($this->fakeNotificationData($notificationFields));
    }

    /**
     * Get fake data of Notification
     *
     * @param array $postFields
     * @return array
     */
    public function fakeNotificationData($notificationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'object_id' => $fake->randomDigitNotNull,
            'type' => $fake->word,
            'from' => $fake->randomDigitNotNull,
            'to' => $fake->randomDigitNotNull,
            'status' => $fake->randomDigitNotNull,
            'read' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $notificationFields);
    }
}
