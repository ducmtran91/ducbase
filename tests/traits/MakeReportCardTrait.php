<?php

use Faker\Factory as Faker;
use App\Entities\ReportCard;
use App\Repositories\ReportCardRepository;

trait MakeReportCardTrait
{
    /**
     * Create fake instance of ReportCard and save it in database
     *
     * @param array $reportCardFields
     * @return ReportCard
     */
    public function makeReportCard($reportCardFields = [])
    {
        /** @var ReportCardRepository $reportCardRepo */
        $reportCardRepo = App::make(ReportCardRepository::class);
        $theme = $this->fakeReportCardData($reportCardFields);
        return $reportCardRepo->create($theme);
    }

    /**
     * Get fake instance of ReportCard
     *
     * @param array $reportCardFields
     * @return ReportCard
     */
    public function fakeReportCard($reportCardFields = [])
    {
        return new ReportCard($this->fakeReportCardData($reportCardFields));
    }

    /**
     * Get fake data of ReportCard
     *
     * @param array $postFields
     * @return array
     */
    public function fakeReportCardData($reportCardFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'job_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'description' => $fake->text,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $reportCardFields);
    }
}
