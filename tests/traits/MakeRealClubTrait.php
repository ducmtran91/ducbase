<?php

use Faker\Factory as Faker;
use App\Entities\RealClub;
use App\Repositories\RealClubRepository;

trait MakeRealClubTrait
{
    /**
     * Create fake instance of RealClub and save it in database
     *
     * @param array $realClubFields
     * @return RealClub
     */
    public function makeRealClub($realClubFields = [])
    {
        /** @var RealClubRepository $realClubRepo */
        $realClubRepo = App::make(RealClubRepository::class);
        $theme = $this->fakeRealClubData($realClubFields);
        return $realClubRepo->create($theme);
    }

    /**
     * Get fake instance of RealClub
     *
     * @param array $realClubFields
     * @return RealClub
     */
    public function fakeRealClub($realClubFields = [])
    {
        return new RealClub($this->fakeRealClubData($realClubFields));
    }

    /**
     * Get fake data of RealClub
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRealClubData($realClubFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'name' => $fake->word
        ], $realClubFields);
    }
}
