<?php

use Faker\Factory as Faker;
use App\Entities\Waste;
use App\Repositories\WasteRepository;

trait MakeWasteTrait
{
    /**
     * Create fake instance of Waste and save it in database
     *
     * @param array $wasteFields
     * @return Waste
     */
    public function makeWaste($wasteFields = [])
    {
        /** @var WasteRepository $wasteRepo */
        $wasteRepo = App::make(WasteRepository::class);
        $theme = $this->fakeWasteData($wasteFields);
        return $wasteRepo->create($theme);
    }

    /**
     * Get fake instance of Waste
     *
     * @param array $wasteFields
     * @return Waste
     */
    public function fakeWaste($wasteFields = [])
    {
        return new Waste($this->fakeWasteData($wasteFields));
    }

    /**
     * Get fake data of Waste
     *
     * @param array $postFields
     * @return array
     */
    public function fakeWasteData($wasteFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'owner_id' => $fake->randomDigitNotNull,
            'walker_id' => $fake->randomDigitNotNull,
            'status' => $fake->randomDigitNotNull,
            'payment_status' => $fake->word,
            'location_to' => $fake->word,
            'location_from' => $fake->word,
            'service_type_id' => $fake->randomDigitNotNull,
            'package_group_id' => $fake->randomDigitNotNull,
            'package_item_id' => $fake->randomDigitNotNull,
            'booked_date' => $fake->word,
            'number_of_dogs' => $fake->randomDigitNotNull,
            'walk_type' => $fake->word,
            'package_pet_id' => $fake->randomDigitNotNull,
            'repeat' => $fake->word,
            'to_do_list' => $fake->text,
            'additional_note' => $fake->text,
            'home_access' => $fake->text,
            'summary' => $fake->text,
            'price' => $fake->randomDigitNotNull,
            'currency_id' => $fake->randomDigitNotNull,
            'state_id' => $fake->randomDigitNotNull,
            'city_id' => $fake->randomDigitNotNull,
            'zip_code' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'booked_time_type' => $fake->word,
            'booked_time_from' => $fake->date('Y-m-d H:i:s'),
            'booked_time_to' => $fake->date('Y-m-d H:i:s'),
            'owner_charge_id' => $fake->word,
            'walker_payout_id' => $fake->word,
            'discount' => $fake->randomDigitNotNull,
            'tax' => $fake->randomDigitNotNull,
            'total' => $fake->randomDigitNotNull
        ], $wasteFields);
    }
}
