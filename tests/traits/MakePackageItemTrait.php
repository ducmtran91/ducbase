<?php

use Faker\Factory as Faker;
use App\Entities\PackageItem;
use App\Repositories\PackageItemRepository;

trait MakePackageItemTrait
{
    /**
     * Create fake instance of PackageItem and save it in database
     *
     * @param array $packageItemFields
     * @return PackageItem
     */
    public function makePackageItem($packageItemFields = [])
    {
        /** @var PackageItemRepository $packageItemRepo */
        $packageItemRepo = App::make(PackageItemRepository::class);
        $theme = $this->fakePackageItemData($packageItemFields);
        return $packageItemRepo->create($theme);
    }

    /**
     * Get fake instance of PackageItem
     *
     * @param array $packageItemFields
     * @return PackageItem
     */
    public function fakePackageItem($packageItemFields = [])
    {
        return new PackageItem($this->fakePackageItemData($packageItemFields));
    }

    /**
     * Get fake data of PackageItem
     *
     * @param array $postFields
     * @return array
     */
    public function fakePackageItemData($packageItemFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'package_group_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'type' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $packageItemFields);
    }
}
