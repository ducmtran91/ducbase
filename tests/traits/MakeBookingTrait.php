<?php

use Faker\Factory as Faker;
use App\Entities\Booking;
use App\Repositories\BookingRepository;

trait MakeBookingTrait
{
    /**
     * Create fake instance of Booking and save it in database
     *
     * @param array $bookingFields
     * @return Booking
     */
    public function makeBooking($bookingFields = [])
    {
        /** @var BookingRepository $bookingRepo */
        $bookingRepo = App::make(BookingRepository::class);
        $theme = $this->fakeBookingData($bookingFields);
        return $bookingRepo->create($theme);
    }

    /**
     * Get fake instance of Booking
     *
     * @param array $bookingFields
     * @return Booking
     */
    public function fakeBooking($bookingFields = [])
    {
        return new Booking($this->fakeBookingData($bookingFields));
    }

    /**
     * Get fake data of Booking
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBookingData($bookingFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'owner_id' => $fake->randomDigitNotNull,
            'walker_id' => $fake->randomDigitNotNull,
            'status' => $fake->randomDigitNotNull,
            'payment_status' => $fake->word,
            'location_to' => $fake->word,
            'location_from' => $fake->word,
            'service_type_id' => $fake->randomDigitNotNull,
            'package_group_id' => $fake->randomDigitNotNull,
            'package_item_id' => $fake->randomDigitNotNull,
            'date' => $fake->word,
            'number_of_dogs' => $fake->randomDigitNotNull,
            'walk_type' => $fake->word,
            'package_pet_id' => $fake->randomDigitNotNull,
            'repeat' => $fake->word,
            'to_do_list' => $fake->text,
            'additional_note' => $fake->text,
            'home_access' => $fake->text,
            'summary' => $fake->text,
            'price' => $fake->randomDigitNotNull,
            'currency_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $bookingFields);
    }
}
