<?php

use Faker\Factory as Faker;
use App\Entities\ServiceTypes;
use App\Repositories\ServiceTypesRepository;

trait MakeServiceTypesTrait
{
    /**
     * Create fake instance of ServiceTypes and save it in database
     *
     * @param array $serviceTypesFields
     * @return ServiceTypes
     */
    public function makeServiceTypes($serviceTypesFields = [])
    {
        /** @var ServiceTypesRepository $serviceTypesRepo */
        $serviceTypesRepo = App::make(ServiceTypesRepository::class);
        $theme = $this->fakeServiceTypesData($serviceTypesFields);
        return $serviceTypesRepo->create($theme);
    }

    /**
     * Get fake instance of ServiceTypes
     *
     * @param array $serviceTypesFields
     * @return ServiceTypes
     */
    public function fakeServiceTypes($serviceTypesFields = [])
    {
        return new ServiceTypes($this->fakeServiceTypesData($serviceTypesFields));
    }

    /**
     * Get fake data of ServiceTypes
     *
     * @param array $postFields
     * @return array
     */
    public function fakeServiceTypesData($serviceTypesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'type' => $fake->word,
            'name' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $serviceTypesFields);
    }
}
