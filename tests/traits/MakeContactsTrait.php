<?php

use Faker\Factory as Faker;
use App\Entities\Contacts;
use App\Repositories\ContactsRepository;

trait MakeContactsTrait
{
    /**
     * Create fake instance of Contacts and save it in database
     *
     * @param array $contactsFields
     * @return Contacts
     */
    public function makeContacts($contactsFields = [])
    {
        /** @var ContactsRepository $contactsRepo */
        $contactsRepo = App::make(ContactsRepository::class);
        $theme = $this->fakeContactsData($contactsFields);
        return $contactsRepo->create($theme);
    }

    /**
     * Get fake instance of Contacts
     *
     * @param array $contactsFields
     * @return Contacts
     */
    public function fakeContacts($contactsFields = [])
    {
        return new Contacts($this->fakeContactsData($contactsFields));
    }

    /**
     * Get fake data of Contacts
     *
     * @param array $postFields
     * @return array
     */
    public function fakeContactsData($contactsFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_id' => $fake->randomDigitNotNull,
            'email' => $fake->word,
            'content' => $fake->text,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $contactsFields);
    }
}
