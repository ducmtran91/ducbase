<?php

use Faker\Factory as Faker;
use App\Entities\PackagePet;
use App\Repositories\PackagePetRepository;

trait MakePackagePetTrait
{
    /**
     * Create fake instance of PackagePet and save it in database
     *
     * @param array $packagePetFields
     * @return PackagePet
     */
    public function makePackagePet($packagePetFields = [])
    {
        /** @var PackagePetRepository $packagePetRepo */
        $packagePetRepo = App::make(PackagePetRepository::class);
        $theme = $this->fakePackagePetData($packagePetFields);
        return $packagePetRepo->create($theme);
    }

    /**
     * Get fake instance of PackagePet
     *
     * @param array $packagePetFields
     * @return PackagePet
     */
    public function fakePackagePet($packagePetFields = [])
    {
        return new PackagePet($this->fakePackagePetData($packagePetFields));
    }

    /**
     * Get fake data of PackagePet
     *
     * @param array $postFields
     * @return array
     */
    public function fakePackagePetData($packagePetFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'number_of_dogs' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $packagePetFields);
    }
}
