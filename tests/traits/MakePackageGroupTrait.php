<?php

use Faker\Factory as Faker;
use App\Entities\PackageGroup;
use App\Repositories\PackageGroupRepository;

trait MakePackageGroupTrait
{
    /**
     * Create fake instance of PackageGroup and save it in database
     *
     * @param array $packageGroupFields
     * @return PackageGroup
     */
    public function makePackageGroup($packageGroupFields = [])
    {
        /** @var PackageGroupRepository $packageGroupRepo */
        $packageGroupRepo = App::make(PackageGroupRepository::class);
        $theme = $this->fakePackageGroupData($packageGroupFields);
        return $packageGroupRepo->create($theme);
    }

    /**
     * Get fake instance of PackageGroup
     *
     * @param array $packageGroupFields
     * @return PackageGroup
     */
    public function fakePackageGroup($packageGroupFields = [])
    {
        return new PackageGroup($this->fakePackageGroupData($packageGroupFields));
    }

    /**
     * Get fake data of PackageGroup
     *
     * @param array $postFields
     * @return array
     */
    public function fakePackageGroupData($packageGroupFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'service_type_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'type' => $fake->word,
            'val' => $fake->randomDigitNotNull
        ], $packageGroupFields);
    }
}
