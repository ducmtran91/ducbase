<?php

use Faker\Factory as Faker;
use App\Entities\Pet;
use App\Repositories\PetRepository;

trait MakePetTrait
{
    /**
     * Create fake instance of Pet and save it in database
     *
     * @param array $petFields
     * @return Pet
     */
    public function makePet($petFields = [])
    {
        /** @var PetRepository $petRepo */
        $petRepo = App::make(PetRepository::class);
        $theme = $this->fakePetData($petFields);
        return $petRepo->create($theme);
    }

    /**
     * Get fake instance of Pet
     *
     * @param array $petFields
     * @return Pet
     */
    public function fakePet($petFields = [])
    {
        return new Pet($this->fakePetData($petFields));
    }

    /**
     * Get fake data of Pet
     *
     * @param array $postFields
     * @return array
     */
    public function fakePetData($petFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'photo' => $fake->text,
            'birth_day' => $fake->date('Y-m-d H:i:s'),
            'breed' => $fake->word,
            'vet_name' => $fake->word,
            'vet_phone' => $fake->word,
            'neutered' => $fake->word,
            'sprayed' => $fake->word,
            'aggressive' => $fake->word,
            'note' => $fake->text,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $petFields);
    }
}
