<?php

use Faker\Factory as Faker;
use App\Entities\ZipCode;
use App\Repositories\ZipCodeRepository;

trait MakeZipCodeTrait
{
    /**
     * Create fake instance of ZipCode and save it in database
     *
     * @param array $zipCodeFields
     * @return ZipCode
     */
    public function makeZipCode($zipCodeFields = [])
    {
        /** @var ZipCodeRepository $zipCodeRepo */
        $zipCodeRepo = App::make(ZipCodeRepository::class);
        $theme = $this->fakeZipCodeData($zipCodeFields);
        return $zipCodeRepo->create($theme);
    }

    /**
     * Get fake instance of ZipCode
     *
     * @param array $zipCodeFields
     * @return ZipCode
     */
    public function fakeZipCode($zipCodeFields = [])
    {
        return new ZipCode($this->fakeZipCodeData($zipCodeFields));
    }

    /**
     * Get fake data of ZipCode
     *
     * @param array $postFields
     * @return array
     */
    public function fakeZipCodeData($zipCodeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'city_id' => $fake->randomDigitNotNull,
            'zip_code' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $zipCodeFields);
    }
}
