<?php

use Faker\Factory as Faker;
use App\Entities\PackagePricing;
use App\Repositories\PackagePricingRepository;

trait MakePackagePricingTrait
{
    /**
     * Create fake instance of PackagePricing and save it in database
     *
     * @param array $packagePricingFields
     * @return PackagePricing
     */
    public function makePackagePricing($packagePricingFields = [])
    {
        /** @var PackagePricingRepository $packagePricingRepo */
        $packagePricingRepo = App::make(PackagePricingRepository::class);
        $theme = $this->fakePackagePricingData($packagePricingFields);
        return $packagePricingRepo->create($theme);
    }

    /**
     * Get fake instance of PackagePricing
     *
     * @param array $packagePricingFields
     * @return PackagePricing
     */
    public function fakePackagePricing($packagePricingFields = [])
    {
        return new PackagePricing($this->fakePackagePricingData($packagePricingFields));
    }

    /**
     * Get fake data of PackagePricing
     *
     * @param array $postFields
     * @return array
     */
    public function fakePackagePricingData($packagePricingFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'package_item_id' => $fake->randomDigitNotNull,
            'package_pet_id' => $fake->randomDigitNotNull,
            'price' => $fake->randomDigitNotNull,
            'currency_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $packagePricingFields);
    }
}
