<?php

use Faker\Factory as Faker;
use App\Entities\PlayerUpdates;
use App\Repositories\PlayerUpdatesRepository;

trait MakePlayerUpdatesTrait
{
    /**
     * Create fake instance of PlayerUpdates and save it in database
     *
     * @param array $playerUpdatesFields
     * @return PlayerUpdates
     */
    public function makePlayerUpdates($playerUpdatesFields = [])
    {
        /** @var PlayerUpdatesRepository $playerUpdatesRepo */
        $playerUpdatesRepo = App::make(PlayerUpdatesRepository::class);
        $theme = $this->fakePlayerUpdatesData($playerUpdatesFields);
        return $playerUpdatesRepo->create($theme);
    }

    /**
     * Get fake instance of PlayerUpdates
     *
     * @param array $playerUpdatesFields
     * @return PlayerUpdates
     */
    public function fakePlayerUpdates($playerUpdatesFields = [])
    {
        return new PlayerUpdates($this->fakePlayerUpdatesData($playerUpdatesFields));
    }

    /**
     * Get fake data of PlayerUpdates
     *
     * @param array $postFields
     * @return array
     */
    public function fakePlayerUpdatesData($playerUpdatesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'player_id' => $fake->randomDigitNotNull,
            'player_round_id' => $fake->randomDigitNotNull,
            'user_id' => $fake->randomDigitNotNull,
            'key' => $fake->randomDigitNotNull,
            'value' => $fake->word,
            'value_display' => $fake->word
        ], $playerUpdatesFields);
    }
}
