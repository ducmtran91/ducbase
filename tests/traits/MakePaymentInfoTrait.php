<?php

use Faker\Factory as Faker;
use App\Entities\PaymentInfo;
use App\Repositories\PaymentInfoRepository;

trait MakePaymentInfoTrait
{
    /**
     * Create fake instance of PaymentInfo and save it in database
     *
     * @param array $paymentInfoFields
     * @return PaymentInfo
     */
    public function makePaymentInfo($paymentInfoFields = [])
    {
        /** @var PaymentInfoRepository $paymentInfoRepo */
        $paymentInfoRepo = App::make(PaymentInfoRepository::class);
        $theme = $this->fakePaymentInfoData($paymentInfoFields);
        return $paymentInfoRepo->create($theme);
    }

    /**
     * Get fake instance of PaymentInfo
     *
     * @param array $paymentInfoFields
     * @return PaymentInfo
     */
    public function fakePaymentInfo($paymentInfoFields = [])
    {
        return new PaymentInfo($this->fakePaymentInfoData($paymentInfoFields));
    }

    /**
     * Get fake data of PaymentInfo
     *
     * @param array $postFields
     * @return array
     */
    public function fakePaymentInfoData($paymentInfoFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_id' => $fake->randomDigitNotNull,
            'card_number' => $fake->word,
            'exp_date' => $fake->word,
            '9' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $paymentInfoFields);
    }
}
