<?php

use Faker\Factory as Faker;
use App\Entities\Users;
use App\Repositories\UsersRepository;

trait MakeUsersTrait
{
    /**
     * Create fake instance of Users and save it in database
     *
     * @param array $usersFields
     * @return Users
     */
    public function makeUsers($usersFields = [])
    {
        /** @var UsersRepository $usersRepo */
        $usersRepo = App::make(UsersRepository::class);
        $theme = $this->fakeUsersData($usersFields);
        return $usersRepo->create($theme);
    }

    /**
     * Get fake instance of Users
     *
     * @param array $usersFields
     * @return Users
     */
    public function fakeUsers($usersFields = [])
    {
        return new Users($this->fakeUsersData($usersFields));
    }

    /**
     * Get fake data of Users
     *
     * @param array $postFields
     * @return array
     */
    public function fakeUsersData($usersFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'role_id' => $fake->randomDigitNotNull,
            'tax_id' => $fake->randomDigitNotNull,
            'provider' => $fake->word,
            'status' => $fake->word,
            'is_admin' => $fake->word,
            'name' => $fake->word,
            'email' => $fake->word,
            'password' => $fake->word,
            'temp_password' => $fake->word,
            'description' => $fake->text,
            'api_token' => $fake->word,
            'zip_code' => $fake->word,
            'avatar' => $fake->text,
            'remember_token' => $fake->word,
            'phone' => $fake->word,
            'gender' => $fake->randomDigitNotNull,
            'address' => $fake->text,
            'location' => $fake->word,
            'credit_card_info' => $fake->word,
            'credit_balance' => $fake->randomDigitNotNull,
            'fb_id' => $fake->word,
            'fb_token' => $fake->text,
            'gg_id' => $fake->word,
            'gg_token' => $fake->text,
            'tw_id' => $fake->word,
            'tw_token' => $fake->text,
            'last_login' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $usersFields);
    }
}
