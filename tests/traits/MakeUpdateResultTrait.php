<?php

use Faker\Factory as Faker;
use App\Entities\UpdateResult;
use App\Repositories\UpdateResultRepository;

trait MakeUpdateResultTrait
{
    /**
     * Create fake instance of UpdateResult and save it in database
     *
     * @param array $updateResultFields
     * @return UpdateResult
     */
    public function makeUpdateResult($updateResultFields = [])
    {
        /** @var UpdateResultRepository $updateResultRepo */
        $updateResultRepo = App::make(UpdateResultRepository::class);
        $theme = $this->fakeUpdateResultData($updateResultFields);
        return $updateResultRepo->create($theme);
    }

    /**
     * Get fake instance of UpdateResult
     *
     * @param array $updateResultFields
     * @return UpdateResult
     */
    public function fakeUpdateResult($updateResultFields = [])
    {
        return new UpdateResult($this->fakeUpdateResultData($updateResultFields));
    }

    /**
     * Get fake data of UpdateResult
     *
     * @param array $postFields
     * @return array
     */
    public function fakeUpdateResultData($updateResultFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'active' => $fake->word,
            'name' => $fake->word,
            'nickname' => $fake->word,
            'photo' => $fake->text,
            'is_injured' => $fake->word,
            'is_goalkeeper' => $fake->word,
            'is_defender' => $fake->word,
            'is_midfielder' => $fake->word,
            'is_attacker' => $fake->word,
            'main_position' => $fake->word,
            'minor_position' => $fake->word,
            'transfer_value' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'real_club_id' => $fake->randomDigitNotNull,
            'reference_id' => $fake->word
        ], $updateResultFields);
    }
}
