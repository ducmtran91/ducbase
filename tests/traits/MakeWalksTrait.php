<?php

use Faker\Factory as Faker;
use App\Entities\Walks;
use App\Repositories\WalksRepository;

trait MakeWalksTrait
{
    /**
     * Create fake instance of Walks and save it in database
     *
     * @param array $walksFields
     * @return Walks
     */
    public function makeWalks($walksFields = [])
    {
        /** @var WalksRepository $walksRepo */
        $walksRepo = App::make(WalksRepository::class);
        $theme = $this->fakeWalksData($walksFields);
        return $walksRepo->create($theme);
    }

    /**
     * Get fake instance of Walks
     *
     * @param array $walksFields
     * @return Walks
     */
    public function fakeWalks($walksFields = [])
    {
        return new Walks($this->fakeWalksData($walksFields));
    }

    /**
     * Get fake data of Walks
     *
     * @param array $postFields
     * @return array
     */
    public function fakeWalksData($walksFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'owner_id' => $fake->randomDigitNotNull,
            'walker_id' => $fake->randomDigitNotNull,
            'status' => $fake->randomDigitNotNull,
            'payment_status' => $fake->word,
            'location_to' => $fake->word,
            'location_from' => $fake->word,
            'service_type_id' => $fake->randomDigitNotNull,
            'package_group_id' => $fake->randomDigitNotNull,
            'package_item_id' => $fake->randomDigitNotNull,
            'date' => $fake->word,
            'number_of_dogs' => $fake->randomDigitNotNull,
            'walk_type' => $fake->word,
            'package_pet_id' => $fake->randomDigitNotNull,
            'repeat' => $fake->word,
            'to_do_list' => $fake->text,
            'additional_note' => $fake->text,
            'home_access' => $fake->text,
            'summary' => $fake->text,
            'price' => $fake->randomDigitNotNull,
            'currency_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $walksFields);
    }
}
