<?php

use Faker\Factory as Faker;
use App\Entities\PlayerStatistic;
use App\Repositories\PlayerStatisticRepository;

trait MakePlayerStatisticTrait
{
    /**
     * Create fake instance of PlayerStatistic and save it in database
     *
     * @param array $playerStatisticFields
     * @return PlayerStatistic
     */
    public function makePlayerStatistic($playerStatisticFields = [])
    {
        /** @var PlayerStatisticRepository $playerStatisticRepo */
        $playerStatisticRepo = App::make(PlayerStatisticRepository::class);
        $theme = $this->fakePlayerStatisticData($playerStatisticFields);
        return $playerStatisticRepo->create($theme);
    }

    /**
     * Get fake instance of PlayerStatistic
     *
     * @param array $playerStatisticFields
     * @return PlayerStatistic
     */
    public function fakePlayerStatistic($playerStatisticFields = [])
    {
        return new PlayerStatistic($this->fakePlayerStatisticData($playerStatisticFields));
    }

    /**
     * Get fake data of PlayerStatistic
     *
     * @param array $postFields
     * @return array
     */
    public function fakePlayerStatisticData($playerStatisticFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'season_id' => $fake->randomDigitNotNull,
            'player_id' => $fake->randomDigitNotNull,
            'total_goals' => $fake->randomDigitNotNull,
            'total_assists' => $fake->randomDigitNotNull,
            'total_clean_sheet' => $fake->randomDigitNotNull,
            'total_duels_they_win' => $fake->randomDigitNotNull,
            'total_duels_they_win_display' => $fake->word,
            'total_passes' => $fake->randomDigitNotNull,
            'total_passes_display' => $fake->word,
            'total_shots' => $fake->randomDigitNotNull,
            'total_shots_display' => $fake->word,
            'total_saves' => $fake->randomDigitNotNull,
            'total_saves_display' => $fake->word,
            'total_yellow_cards' => $fake->randomDigitNotNull,
            'total_dribbles' => $fake->randomDigitNotNull,
            'total_turnovers' => $fake->randomDigitNotNull,
            'total_balls_recovered' => $fake->randomDigitNotNull,
            'total_fouls_committed' => $fake->randomDigitNotNull,
            'total_point' => $fake->randomDigitNotNull,
            'has_left' => $fake->word,
            'has_left_at' => $fake->date('Y-m-d H:i:s')
        ], $playerStatisticFields);
    }
}
