<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ZipCodeApiTest extends TestCase
{
    use MakeZipCodeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateZipCode()
    {
        $zipCode = $this->fakeZipCodeData();
        $this->json('POST', '/api/v1/zipCodes', $zipCode);

        $this->assertApiResponse($zipCode);
    }

    /**
     * @test
     */
    public function testReadZipCode()
    {
        $zipCode = $this->makeZipCode();
        $this->json('GET', '/api/v1/zipCodes/'.$zipCode->id);

        $this->assertApiResponse($zipCode->toArray());
    }

    /**
     * @test
     */
    public function testUpdateZipCode()
    {
        $zipCode = $this->makeZipCode();
        $editedZipCode = $this->fakeZipCodeData();

        $this->json('PUT', '/api/v1/zipCodes/'.$zipCode->id, $editedZipCode);

        $this->assertApiResponse($editedZipCode);
    }

    /**
     * @test
     */
    public function testDeleteZipCode()
    {
        $zipCode = $this->makeZipCode();
        $this->json('DELETE', '/api/v1/zipCodes/'.$zipCode->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/zipCodes/'.$zipCode->id);

        $this->assertResponseStatus(404);
    }
}
