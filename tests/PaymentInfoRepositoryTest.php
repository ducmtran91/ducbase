<?php

use App\Models\PaymentInfo;
use App\Repositories\PaymentInfoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PaymentInfoRepositoryTest extends TestCase
{
    use MakePaymentInfoTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PaymentInfoRepository
     */
    protected $paymentInfoRepo;

    public function setUp()
    {
        parent::setUp();
        $this->paymentInfoRepo = App::make(PaymentInfoRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePaymentInfo()
    {
        $paymentInfo = $this->fakePaymentInfoData();
        $createdPaymentInfo = $this->paymentInfoRepo->create($paymentInfo);
        $createdPaymentInfo = $createdPaymentInfo->toArray();
        $this->assertArrayHasKey('id', $createdPaymentInfo);
        $this->assertNotNull($createdPaymentInfo['id'], 'Created PaymentInfo must have id specified');
        $this->assertNotNull(PaymentInfo::find($createdPaymentInfo['id']), 'PaymentInfo with given id must be in DB');
        $this->assertModelData($paymentInfo, $createdPaymentInfo);
    }

    /**
     * @test read
     */
    public function testReadPaymentInfo()
    {
        $paymentInfo = $this->makePaymentInfo();
        $dbPaymentInfo = $this->paymentInfoRepo->find($paymentInfo->id);
        $dbPaymentInfo = $dbPaymentInfo->toArray();
        $this->assertModelData($paymentInfo->toArray(), $dbPaymentInfo);
    }

    /**
     * @test update
     */
    public function testUpdatePaymentInfo()
    {
        $paymentInfo = $this->makePaymentInfo();
        $fakePaymentInfo = $this->fakePaymentInfoData();
        $updatedPaymentInfo = $this->paymentInfoRepo->update($fakePaymentInfo, $paymentInfo->id);
        $this->assertModelData($fakePaymentInfo, $updatedPaymentInfo->toArray());
        $dbPaymentInfo = $this->paymentInfoRepo->find($paymentInfo->id);
        $this->assertModelData($fakePaymentInfo, $dbPaymentInfo->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePaymentInfo()
    {
        $paymentInfo = $this->makePaymentInfo();
        $resp = $this->paymentInfoRepo->delete($paymentInfo->id);
        $this->assertTrue($resp);
        $this->assertNull(PaymentInfo::find($paymentInfo->id), 'PaymentInfo should not exist in DB');
    }
}
