<?php

use App\Models\ReviewJob;
use App\Repositories\ReviewJobRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReviewJobRepositoryTest extends TestCase
{
    use MakeReviewJobTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ReviewJobRepository
     */
    protected $reviewJobRepo;

    public function setUp()
    {
        parent::setUp();
        $this->reviewJobRepo = App::make(ReviewJobRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateReviewJob()
    {
        $reviewJob = $this->fakeReviewJobData();
        $createdReviewJob = $this->reviewJobRepo->create($reviewJob);
        $createdReviewJob = $createdReviewJob->toArray();
        $this->assertArrayHasKey('id', $createdReviewJob);
        $this->assertNotNull($createdReviewJob['id'], 'Created ReviewJob must have id specified');
        $this->assertNotNull(ReviewJob::find($createdReviewJob['id']), 'ReviewJob with given id must be in DB');
        $this->assertModelData($reviewJob, $createdReviewJob);
    }

    /**
     * @test read
     */
    public function testReadReviewJob()
    {
        $reviewJob = $this->makeReviewJob();
        $dbReviewJob = $this->reviewJobRepo->find($reviewJob->id);
        $dbReviewJob = $dbReviewJob->toArray();
        $this->assertModelData($reviewJob->toArray(), $dbReviewJob);
    }

    /**
     * @test update
     */
    public function testUpdateReviewJob()
    {
        $reviewJob = $this->makeReviewJob();
        $fakeReviewJob = $this->fakeReviewJobData();
        $updatedReviewJob = $this->reviewJobRepo->update($fakeReviewJob, $reviewJob->id);
        $this->assertModelData($fakeReviewJob, $updatedReviewJob->toArray());
        $dbReviewJob = $this->reviewJobRepo->find($reviewJob->id);
        $this->assertModelData($fakeReviewJob, $dbReviewJob->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteReviewJob()
    {
        $reviewJob = $this->makeReviewJob();
        $resp = $this->reviewJobRepo->delete($reviewJob->id);
        $this->assertTrue($resp);
        $this->assertNull(ReviewJob::find($reviewJob->id), 'ReviewJob should not exist in DB');
    }
}
