<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackageGroupApiTest extends TestCase
{
    use MakePackageGroupTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePackageGroup()
    {
        $packageGroup = $this->fakePackageGroupData();
        $this->json('POST', '/api/v1/packageGroups', $packageGroup);

        $this->assertApiResponse($packageGroup);
    }

    /**
     * @test
     */
    public function testReadPackageGroup()
    {
        $packageGroup = $this->makePackageGroup();
        $this->json('GET', '/api/v1/packageGroups/'.$packageGroup->id);

        $this->assertApiResponse($packageGroup->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePackageGroup()
    {
        $packageGroup = $this->makePackageGroup();
        $editedPackageGroup = $this->fakePackageGroupData();

        $this->json('PUT', '/api/v1/packageGroups/'.$packageGroup->id, $editedPackageGroup);

        $this->assertApiResponse($editedPackageGroup);
    }

    /**
     * @test
     */
    public function testDeletePackageGroup()
    {
        $packageGroup = $this->makePackageGroup();
        $this->json('DELETE', '/api/v1/packageGroups/'.$packageGroup->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packageGroups/'.$packageGroup->id);

        $this->assertResponseStatus(404);
    }
}
