<?php

use App\Models\Waste;
use App\Repositories\WasteRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WasteRepositoryTest extends TestCase
{
    use MakeWasteTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var WasteRepository
     */
    protected $wasteRepo;

    public function setUp()
    {
        parent::setUp();
        $this->wasteRepo = App::make(WasteRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateWaste()
    {
        $waste = $this->fakeWasteData();
        $createdWaste = $this->wasteRepo->create($waste);
        $createdWaste = $createdWaste->toArray();
        $this->assertArrayHasKey('id', $createdWaste);
        $this->assertNotNull($createdWaste['id'], 'Created Waste must have id specified');
        $this->assertNotNull(Waste::find($createdWaste['id']), 'Waste with given id must be in DB');
        $this->assertModelData($waste, $createdWaste);
    }

    /**
     * @test read
     */
    public function testReadWaste()
    {
        $waste = $this->makeWaste();
        $dbWaste = $this->wasteRepo->find($waste->id);
        $dbWaste = $dbWaste->toArray();
        $this->assertModelData($waste->toArray(), $dbWaste);
    }

    /**
     * @test update
     */
    public function testUpdateWaste()
    {
        $waste = $this->makeWaste();
        $fakeWaste = $this->fakeWasteData();
        $updatedWaste = $this->wasteRepo->update($fakeWaste, $waste->id);
        $this->assertModelData($fakeWaste, $updatedWaste->toArray());
        $dbWaste = $this->wasteRepo->find($waste->id);
        $this->assertModelData($fakeWaste, $dbWaste->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteWaste()
    {
        $waste = $this->makeWaste();
        $resp = $this->wasteRepo->delete($waste->id);
        $this->assertTrue($resp);
        $this->assertNull(Waste::find($waste->id), 'Waste should not exist in DB');
    }
}
