<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReviewJobApiTest extends TestCase
{
    use MakeReviewJobTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateReviewJob()
    {
        $reviewJob = $this->fakeReviewJobData();
        $this->json('POST', '/api/v1/reviewJobs', $reviewJob);

        $this->assertApiResponse($reviewJob);
    }

    /**
     * @test
     */
    public function testReadReviewJob()
    {
        $reviewJob = $this->makeReviewJob();
        $this->json('GET', '/api/v1/reviewJobs/'.$reviewJob->id);

        $this->assertApiResponse($reviewJob->toArray());
    }

    /**
     * @test
     */
    public function testUpdateReviewJob()
    {
        $reviewJob = $this->makeReviewJob();
        $editedReviewJob = $this->fakeReviewJobData();

        $this->json('PUT', '/api/v1/reviewJobs/'.$reviewJob->id, $editedReviewJob);

        $this->assertApiResponse($editedReviewJob);
    }

    /**
     * @test
     */
    public function testDeleteReviewJob()
    {
        $reviewJob = $this->makeReviewJob();
        $this->json('DELETE', '/api/v1/reviewJobs/'.$reviewJob->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/reviewJobs/'.$reviewJob->id);

        $this->assertResponseStatus(404);
    }
}
