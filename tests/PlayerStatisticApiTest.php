<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PlayerStatisticApiTest extends TestCase
{
    use MakePlayerStatisticTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePlayerStatistic()
    {
        $playerStatistic = $this->fakePlayerStatisticData();
        $this->json('POST', '/api/v1/playerStatistics', $playerStatistic);

        $this->assertApiResponse($playerStatistic);
    }

    /**
     * @test
     */
    public function testReadPlayerStatistic()
    {
        $playerStatistic = $this->makePlayerStatistic();
        $this->json('GET', '/api/v1/playerStatistics/'.$playerStatistic->id);

        $this->assertApiResponse($playerStatistic->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePlayerStatistic()
    {
        $playerStatistic = $this->makePlayerStatistic();
        $editedPlayerStatistic = $this->fakePlayerStatisticData();

        $this->json('PUT', '/api/v1/playerStatistics/'.$playerStatistic->id, $editedPlayerStatistic);

        $this->assertApiResponse($editedPlayerStatistic);
    }

    /**
     * @test
     */
    public function testDeletePlayerStatistic()
    {
        $playerStatistic = $this->makePlayerStatistic();
        $this->json('DELETE', '/api/v1/playerStatistics/'.$playerStatistic->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/playerStatistics/'.$playerStatistic->id);

        $this->assertResponseStatus(404);
    }
}
