<?php

use App\Models\RealRound;
use App\Repositories\RealRoundRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RealRoundRepositoryTest extends TestCase
{
    use MakeRealRoundTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RealRoundRepository
     */
    protected $realRoundRepo;

    public function setUp()
    {
        parent::setUp();
        $this->realRoundRepo = App::make(RealRoundRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateRealRound()
    {
        $realRound = $this->fakeRealRoundData();
        $createdRealRound = $this->realRoundRepo->create($realRound);
        $createdRealRound = $createdRealRound->toArray();
        $this->assertArrayHasKey('id', $createdRealRound);
        $this->assertNotNull($createdRealRound['id'], 'Created RealRound must have id specified');
        $this->assertNotNull(RealRound::find($createdRealRound['id']), 'RealRound with given id must be in DB');
        $this->assertModelData($realRound, $createdRealRound);
    }

    /**
     * @test read
     */
    public function testReadRealRound()
    {
        $realRound = $this->makeRealRound();
        $dbRealRound = $this->realRoundRepo->find($realRound->id);
        $dbRealRound = $dbRealRound->toArray();
        $this->assertModelData($realRound->toArray(), $dbRealRound);
    }

    /**
     * @test update
     */
    public function testUpdateRealRound()
    {
        $realRound = $this->makeRealRound();
        $fakeRealRound = $this->fakeRealRoundData();
        $updatedRealRound = $this->realRoundRepo->update($fakeRealRound, $realRound->id);
        $this->assertModelData($fakeRealRound, $updatedRealRound->toArray());
        $dbRealRound = $this->realRoundRepo->find($realRound->id);
        $this->assertModelData($fakeRealRound, $dbRealRound->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteRealRound()
    {
        $realRound = $this->makeRealRound();
        $resp = $this->realRoundRepo->delete($realRound->id);
        $this->assertTrue($resp);
        $this->assertNull(RealRound::find($realRound->id), 'RealRound should not exist in DB');
    }
}
