<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PlayerUpdatesApiTest extends TestCase
{
    use MakePlayerUpdatesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePlayerUpdates()
    {
        $playerUpdates = $this->fakePlayerUpdatesData();
        $this->json('POST', '/api/v1/playerUpdates', $playerUpdates);

        $this->assertApiResponse($playerUpdates);
    }

    /**
     * @test
     */
    public function testReadPlayerUpdates()
    {
        $playerUpdates = $this->makePlayerUpdates();
        $this->json('GET', '/api/v1/playerUpdates/'.$playerUpdates->id);

        $this->assertApiResponse($playerUpdates->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePlayerUpdates()
    {
        $playerUpdates = $this->makePlayerUpdates();
        $editedPlayerUpdates = $this->fakePlayerUpdatesData();

        $this->json('PUT', '/api/v1/playerUpdates/'.$playerUpdates->id, $editedPlayerUpdates);

        $this->assertApiResponse($editedPlayerUpdates);
    }

    /**
     * @test
     */
    public function testDeletePlayerUpdates()
    {
        $playerUpdates = $this->makePlayerUpdates();
        $this->json('DELETE', '/api/v1/playerUpdates/'.$playerUpdates->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/playerUpdates/'.$playerUpdates->id);

        $this->assertResponseStatus(404);
    }
}
