<?php

use App\Models\PlayerUpdates;
use App\Repositories\PlayerUpdatesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PlayerUpdatesRepositoryTest extends TestCase
{
    use MakePlayerUpdatesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PlayerUpdatesRepository
     */
    protected $playerUpdatesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->playerUpdatesRepo = App::make(PlayerUpdatesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePlayerUpdates()
    {
        $playerUpdates = $this->fakePlayerUpdatesData();
        $createdPlayerUpdates = $this->playerUpdatesRepo->create($playerUpdates);
        $createdPlayerUpdates = $createdPlayerUpdates->toArray();
        $this->assertArrayHasKey('id', $createdPlayerUpdates);
        $this->assertNotNull($createdPlayerUpdates['id'], 'Created PlayerUpdates must have id specified');
        $this->assertNotNull(PlayerUpdates::find($createdPlayerUpdates['id']), 'PlayerUpdates with given id must be in DB');
        $this->assertModelData($playerUpdates, $createdPlayerUpdates);
    }

    /**
     * @test read
     */
    public function testReadPlayerUpdates()
    {
        $playerUpdates = $this->makePlayerUpdates();
        $dbPlayerUpdates = $this->playerUpdatesRepo->find($playerUpdates->id);
        $dbPlayerUpdates = $dbPlayerUpdates->toArray();
        $this->assertModelData($playerUpdates->toArray(), $dbPlayerUpdates);
    }

    /**
     * @test update
     */
    public function testUpdatePlayerUpdates()
    {
        $playerUpdates = $this->makePlayerUpdates();
        $fakePlayerUpdates = $this->fakePlayerUpdatesData();
        $updatedPlayerUpdates = $this->playerUpdatesRepo->update($fakePlayerUpdates, $playerUpdates->id);
        $this->assertModelData($fakePlayerUpdates, $updatedPlayerUpdates->toArray());
        $dbPlayerUpdates = $this->playerUpdatesRepo->find($playerUpdates->id);
        $this->assertModelData($fakePlayerUpdates, $dbPlayerUpdates->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePlayerUpdates()
    {
        $playerUpdates = $this->makePlayerUpdates();
        $resp = $this->playerUpdatesRepo->delete($playerUpdates->id);
        $this->assertTrue($resp);
        $this->assertNull(PlayerUpdates::find($playerUpdates->id), 'PlayerUpdates should not exist in DB');
    }
}
