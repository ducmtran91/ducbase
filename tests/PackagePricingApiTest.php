<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackagePricingApiTest extends TestCase
{
    use MakePackagePricingTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePackagePricing()
    {
        $packagePricing = $this->fakePackagePricingData();
        $this->json('POST', '/api/v1/packagePricings', $packagePricing);

        $this->assertApiResponse($packagePricing);
    }

    /**
     * @test
     */
    public function testReadPackagePricing()
    {
        $packagePricing = $this->makePackagePricing();
        $this->json('GET', '/api/v1/packagePricings/'.$packagePricing->id);

        $this->assertApiResponse($packagePricing->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePackagePricing()
    {
        $packagePricing = $this->makePackagePricing();
        $editedPackagePricing = $this->fakePackagePricingData();

        $this->json('PUT', '/api/v1/packagePricings/'.$packagePricing->id, $editedPackagePricing);

        $this->assertApiResponse($editedPackagePricing);
    }

    /**
     * @test
     */
    public function testDeletePackagePricing()
    {
        $packagePricing = $this->makePackagePricing();
        $this->json('DELETE', '/api/v1/packagePricings/'.$packagePricing->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packagePricings/'.$packagePricing->id);

        $this->assertResponseStatus(404);
    }
}
