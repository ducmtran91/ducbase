<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ServiceTypesApiTest extends TestCase
{
    use MakeServiceTypesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateServiceTypes()
    {
        $serviceTypes = $this->fakeServiceTypesData();
        $this->json('POST', '/api/v1/serviceTypes', $serviceTypes);

        $this->assertApiResponse($serviceTypes);
    }

    /**
     * @test
     */
    public function testReadServiceTypes()
    {
        $serviceTypes = $this->makeServiceTypes();
        $this->json('GET', '/api/v1/serviceTypes/'.$serviceTypes->id);

        $this->assertApiResponse($serviceTypes->toArray());
    }

    /**
     * @test
     */
    public function testUpdateServiceTypes()
    {
        $serviceTypes = $this->makeServiceTypes();
        $editedServiceTypes = $this->fakeServiceTypesData();

        $this->json('PUT', '/api/v1/serviceTypes/'.$serviceTypes->id, $editedServiceTypes);

        $this->assertApiResponse($editedServiceTypes);
    }

    /**
     * @test
     */
    public function testDeleteServiceTypes()
    {
        $serviceTypes = $this->makeServiceTypes();
        $this->json('DELETE', '/api/v1/serviceTypes/'.$serviceTypes->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/serviceTypes/'.$serviceTypes->id);

        $this->assertResponseStatus(404);
    }
}
