<?php

use App\Models\State;
use App\Repositories\StateRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StateRepositoryTest extends TestCase
{
    use MakeStateTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var StateRepository
     */
    protected $stateRepo;

    public function setUp()
    {
        parent::setUp();
        $this->stateRepo = App::make(StateRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateState()
    {
        $state = $this->fakeStateData();
        $createdState = $this->stateRepo->create($state);
        $createdState = $createdState->toArray();
        $this->assertArrayHasKey('id', $createdState);
        $this->assertNotNull($createdState['id'], 'Created State must have id specified');
        $this->assertNotNull(State::find($createdState['id']), 'State with given id must be in DB');
        $this->assertModelData($state, $createdState);
    }

    /**
     * @test read
     */
    public function testReadState()
    {
        $state = $this->makeState();
        $dbState = $this->stateRepo->find($state->id);
        $dbState = $dbState->toArray();
        $this->assertModelData($state->toArray(), $dbState);
    }

    /**
     * @test update
     */
    public function testUpdateState()
    {
        $state = $this->makeState();
        $fakeState = $this->fakeStateData();
        $updatedState = $this->stateRepo->update($fakeState, $state->id);
        $this->assertModelData($fakeState, $updatedState->toArray());
        $dbState = $this->stateRepo->find($state->id);
        $this->assertModelData($fakeState, $dbState->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteState()
    {
        $state = $this->makeState();
        $resp = $this->stateRepo->delete($state->id);
        $this->assertTrue($resp);
        $this->assertNull(State::find($state->id), 'State should not exist in DB');
    }
}
