<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ContactsApiTest extends TestCase
{
    use MakeContactsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateContacts()
    {
        $contacts = $this->fakeContactsData();
        $this->json('POST', '/api/v1/contacts', $contacts);

        $this->assertApiResponse($contacts);
    }

    /**
     * @test
     */
    public function testReadContacts()
    {
        $contacts = $this->makeContacts();
        $this->json('GET', '/api/v1/contacts/'.$contacts->id);

        $this->assertApiResponse($contacts->toArray());
    }

    /**
     * @test
     */
    public function testUpdateContacts()
    {
        $contacts = $this->makeContacts();
        $editedContacts = $this->fakeContactsData();

        $this->json('PUT', '/api/v1/contacts/'.$contacts->id, $editedContacts);

        $this->assertApiResponse($editedContacts);
    }

    /**
     * @test
     */
    public function testDeleteContacts()
    {
        $contacts = $this->makeContacts();
        $this->json('DELETE', '/api/v1/contacts/'.$contacts->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/contacts/'.$contacts->id);

        $this->assertResponseStatus(404);
    }
}
