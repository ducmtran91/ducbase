<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackagePetApiTest extends TestCase
{
    use MakePackagePetTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePackagePet()
    {
        $packagePet = $this->fakePackagePetData();
        $this->json('POST', '/api/v1/packagePets', $packagePet);

        $this->assertApiResponse($packagePet);
    }

    /**
     * @test
     */
    public function testReadPackagePet()
    {
        $packagePet = $this->makePackagePet();
        $this->json('GET', '/api/v1/packagePets/'.$packagePet->id);

        $this->assertApiResponse($packagePet->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePackagePet()
    {
        $packagePet = $this->makePackagePet();
        $editedPackagePet = $this->fakePackagePetData();

        $this->json('PUT', '/api/v1/packagePets/'.$packagePet->id, $editedPackagePet);

        $this->assertApiResponse($editedPackagePet);
    }

    /**
     * @test
     */
    public function testDeletePackagePet()
    {
        $packagePet = $this->makePackagePet();
        $this->json('DELETE', '/api/v1/packagePets/'.$packagePet->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packagePets/'.$packagePet->id);

        $this->assertResponseStatus(404);
    }
}
