<?php

use App\Models\PackageItem;
use App\Repositories\PackageItemRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackageItemRepositoryTest extends TestCase
{
    use MakePackageItemTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PackageItemRepository
     */
    protected $packageItemRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packageItemRepo = App::make(PackageItemRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePackageItem()
    {
        $packageItem = $this->fakePackageItemData();
        $createdPackageItem = $this->packageItemRepo->create($packageItem);
        $createdPackageItem = $createdPackageItem->toArray();
        $this->assertArrayHasKey('id', $createdPackageItem);
        $this->assertNotNull($createdPackageItem['id'], 'Created PackageItem must have id specified');
        $this->assertNotNull(PackageItem::find($createdPackageItem['id']), 'PackageItem with given id must be in DB');
        $this->assertModelData($packageItem, $createdPackageItem);
    }

    /**
     * @test read
     */
    public function testReadPackageItem()
    {
        $packageItem = $this->makePackageItem();
        $dbPackageItem = $this->packageItemRepo->find($packageItem->id);
        $dbPackageItem = $dbPackageItem->toArray();
        $this->assertModelData($packageItem->toArray(), $dbPackageItem);
    }

    /**
     * @test update
     */
    public function testUpdatePackageItem()
    {
        $packageItem = $this->makePackageItem();
        $fakePackageItem = $this->fakePackageItemData();
        $updatedPackageItem = $this->packageItemRepo->update($fakePackageItem, $packageItem->id);
        $this->assertModelData($fakePackageItem, $updatedPackageItem->toArray());
        $dbPackageItem = $this->packageItemRepo->find($packageItem->id);
        $this->assertModelData($fakePackageItem, $dbPackageItem->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePackageItem()
    {
        $packageItem = $this->makePackageItem();
        $resp = $this->packageItemRepo->delete($packageItem->id);
        $this->assertTrue($resp);
        $this->assertNull(PackageItem::find($packageItem->id), 'PackageItem should not exist in DB');
    }
}
