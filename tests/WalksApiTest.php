<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WalksApiTest extends TestCase
{
    use MakeWalksTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateWalks()
    {
        $walks = $this->fakeWalksData();
        $this->json('POST', '/api/v1/walks', $walks);

        $this->assertApiResponse($walks);
    }

    /**
     * @test
     */
    public function testReadWalks()
    {
        $walks = $this->makeWalks();
        $this->json('GET', '/api/v1/walks/'.$walks->id);

        $this->assertApiResponse($walks->toArray());
    }

    /**
     * @test
     */
    public function testUpdateWalks()
    {
        $walks = $this->makeWalks();
        $editedWalks = $this->fakeWalksData();

        $this->json('PUT', '/api/v1/walks/'.$walks->id, $editedWalks);

        $this->assertApiResponse($editedWalks);
    }

    /**
     * @test
     */
    public function testDeleteWalks()
    {
        $walks = $this->makeWalks();
        $this->json('DELETE', '/api/v1/walks/'.$walks->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/walks/'.$walks->id);

        $this->assertResponseStatus(404);
    }
}
