<?php

use App\Models\Walks;
use App\Repositories\WalksRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WalksRepositoryTest extends TestCase
{
    use MakeWalksTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var WalksRepository
     */
    protected $walksRepo;

    public function setUp()
    {
        parent::setUp();
        $this->walksRepo = App::make(WalksRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateWalks()
    {
        $walks = $this->fakeWalksData();
        $createdWalks = $this->walksRepo->create($walks);
        $createdWalks = $createdWalks->toArray();
        $this->assertArrayHasKey('id', $createdWalks);
        $this->assertNotNull($createdWalks['id'], 'Created Walks must have id specified');
        $this->assertNotNull(Walks::find($createdWalks['id']), 'Walks with given id must be in DB');
        $this->assertModelData($walks, $createdWalks);
    }

    /**
     * @test read
     */
    public function testReadWalks()
    {
        $walks = $this->makeWalks();
        $dbWalks = $this->walksRepo->find($walks->id);
        $dbWalks = $dbWalks->toArray();
        $this->assertModelData($walks->toArray(), $dbWalks);
    }

    /**
     * @test update
     */
    public function testUpdateWalks()
    {
        $walks = $this->makeWalks();
        $fakeWalks = $this->fakeWalksData();
        $updatedWalks = $this->walksRepo->update($fakeWalks, $walks->id);
        $this->assertModelData($fakeWalks, $updatedWalks->toArray());
        $dbWalks = $this->walksRepo->find($walks->id);
        $this->assertModelData($fakeWalks, $dbWalks->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteWalks()
    {
        $walks = $this->makeWalks();
        $resp = $this->walksRepo->delete($walks->id);
        $this->assertTrue($resp);
        $this->assertNull(Walks::find($walks->id), 'Walks should not exist in DB');
    }
}
