<?php

use App\Models\ReportCard;
use App\Repositories\ReportCardRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReportCardRepositoryTest extends TestCase
{
    use MakeReportCardTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ReportCardRepository
     */
    protected $reportCardRepo;

    public function setUp()
    {
        parent::setUp();
        $this->reportCardRepo = App::make(ReportCardRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateReportCard()
    {
        $reportCard = $this->fakeReportCardData();
        $createdReportCard = $this->reportCardRepo->create($reportCard);
        $createdReportCard = $createdReportCard->toArray();
        $this->assertArrayHasKey('id', $createdReportCard);
        $this->assertNotNull($createdReportCard['id'], 'Created ReportCard must have id specified');
        $this->assertNotNull(ReportCard::find($createdReportCard['id']), 'ReportCard with given id must be in DB');
        $this->assertModelData($reportCard, $createdReportCard);
    }

    /**
     * @test read
     */
    public function testReadReportCard()
    {
        $reportCard = $this->makeReportCard();
        $dbReportCard = $this->reportCardRepo->find($reportCard->id);
        $dbReportCard = $dbReportCard->toArray();
        $this->assertModelData($reportCard->toArray(), $dbReportCard);
    }

    /**
     * @test update
     */
    public function testUpdateReportCard()
    {
        $reportCard = $this->makeReportCard();
        $fakeReportCard = $this->fakeReportCardData();
        $updatedReportCard = $this->reportCardRepo->update($fakeReportCard, $reportCard->id);
        $this->assertModelData($fakeReportCard, $updatedReportCard->toArray());
        $dbReportCard = $this->reportCardRepo->find($reportCard->id);
        $this->assertModelData($fakeReportCard, $dbReportCard->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteReportCard()
    {
        $reportCard = $this->makeReportCard();
        $resp = $this->reportCardRepo->delete($reportCard->id);
        $this->assertTrue($resp);
        $this->assertNull(ReportCard::find($reportCard->id), 'ReportCard should not exist in DB');
    }
}
