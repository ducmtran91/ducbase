<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PaymentInfoApiTest extends TestCase
{
    use MakePaymentInfoTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePaymentInfo()
    {
        $paymentInfo = $this->fakePaymentInfoData();
        $this->json('POST', '/api/v1/paymentInfos', $paymentInfo);

        $this->assertApiResponse($paymentInfo);
    }

    /**
     * @test
     */
    public function testReadPaymentInfo()
    {
        $paymentInfo = $this->makePaymentInfo();
        $this->json('GET', '/api/v1/paymentInfos/'.$paymentInfo->id);

        $this->assertApiResponse($paymentInfo->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePaymentInfo()
    {
        $paymentInfo = $this->makePaymentInfo();
        $editedPaymentInfo = $this->fakePaymentInfoData();

        $this->json('PUT', '/api/v1/paymentInfos/'.$paymentInfo->id, $editedPaymentInfo);

        $this->assertApiResponse($editedPaymentInfo);
    }

    /**
     * @test
     */
    public function testDeletePaymentInfo()
    {
        $paymentInfo = $this->makePaymentInfo();
        $this->json('DELETE', '/api/v1/paymentInfos/'.$paymentInfo->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/paymentInfos/'.$paymentInfo->id);

        $this->assertResponseStatus(404);
    }
}
