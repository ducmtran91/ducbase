<?php

use App\Models\PackagePet;
use App\Repositories\PackagePetRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackagePetRepositoryTest extends TestCase
{
    use MakePackagePetTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PackagePetRepository
     */
    protected $packagePetRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packagePetRepo = App::make(PackagePetRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePackagePet()
    {
        $packagePet = $this->fakePackagePetData();
        $createdPackagePet = $this->packagePetRepo->create($packagePet);
        $createdPackagePet = $createdPackagePet->toArray();
        $this->assertArrayHasKey('id', $createdPackagePet);
        $this->assertNotNull($createdPackagePet['id'], 'Created PackagePet must have id specified');
        $this->assertNotNull(PackagePet::find($createdPackagePet['id']), 'PackagePet with given id must be in DB');
        $this->assertModelData($packagePet, $createdPackagePet);
    }

    /**
     * @test read
     */
    public function testReadPackagePet()
    {
        $packagePet = $this->makePackagePet();
        $dbPackagePet = $this->packagePetRepo->find($packagePet->id);
        $dbPackagePet = $dbPackagePet->toArray();
        $this->assertModelData($packagePet->toArray(), $dbPackagePet);
    }

    /**
     * @test update
     */
    public function testUpdatePackagePet()
    {
        $packagePet = $this->makePackagePet();
        $fakePackagePet = $this->fakePackagePetData();
        $updatedPackagePet = $this->packagePetRepo->update($fakePackagePet, $packagePet->id);
        $this->assertModelData($fakePackagePet, $updatedPackagePet->toArray());
        $dbPackagePet = $this->packagePetRepo->find($packagePet->id);
        $this->assertModelData($fakePackagePet, $dbPackagePet->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePackagePet()
    {
        $packagePet = $this->makePackagePet();
        $resp = $this->packagePetRepo->delete($packagePet->id);
        $this->assertTrue($resp);
        $this->assertNull(PackagePet::find($packagePet->id), 'PackagePet should not exist in DB');
    }
}
