<?php

use App\Models\UpdateResult;
use App\Repositories\UpdateResultRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UpdateResultRepositoryTest extends TestCase
{
    use MakeUpdateResultTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var UpdateResultRepository
     */
    protected $updateResultRepo;

    public function setUp()
    {
        parent::setUp();
        $this->updateResultRepo = App::make(UpdateResultRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateUpdateResult()
    {
        $updateResult = $this->fakeUpdateResultData();
        $createdUpdateResult = $this->updateResultRepo->create($updateResult);
        $createdUpdateResult = $createdUpdateResult->toArray();
        $this->assertArrayHasKey('id', $createdUpdateResult);
        $this->assertNotNull($createdUpdateResult['id'], 'Created UpdateResult must have id specified');
        $this->assertNotNull(UpdateResult::find($createdUpdateResult['id']), 'UpdateResult with given id must be in DB');
        $this->assertModelData($updateResult, $createdUpdateResult);
    }

    /**
     * @test read
     */
    public function testReadUpdateResult()
    {
        $updateResult = $this->makeUpdateResult();
        $dbUpdateResult = $this->updateResultRepo->find($updateResult->id);
        $dbUpdateResult = $dbUpdateResult->toArray();
        $this->assertModelData($updateResult->toArray(), $dbUpdateResult);
    }

    /**
     * @test update
     */
    public function testUpdateUpdateResult()
    {
        $updateResult = $this->makeUpdateResult();
        $fakeUpdateResult = $this->fakeUpdateResultData();
        $updatedUpdateResult = $this->updateResultRepo->update($fakeUpdateResult, $updateResult->id);
        $this->assertModelData($fakeUpdateResult, $updatedUpdateResult->toArray());
        $dbUpdateResult = $this->updateResultRepo->find($updateResult->id);
        $this->assertModelData($fakeUpdateResult, $dbUpdateResult->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteUpdateResult()
    {
        $updateResult = $this->makeUpdateResult();
        $resp = $this->updateResultRepo->delete($updateResult->id);
        $this->assertTrue($resp);
        $this->assertNull(UpdateResult::find($updateResult->id), 'UpdateResult should not exist in DB');
    }
}
