<?php

use App\Models\Contacts;
use App\Repositories\ContactsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ContactsRepositoryTest extends TestCase
{
    use MakeContactsTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ContactsRepository
     */
    protected $contactsRepo;

    public function setUp()
    {
        parent::setUp();
        $this->contactsRepo = App::make(ContactsRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateContacts()
    {
        $contacts = $this->fakeContactsData();
        $createdContacts = $this->contactsRepo->create($contacts);
        $createdContacts = $createdContacts->toArray();
        $this->assertArrayHasKey('id', $createdContacts);
        $this->assertNotNull($createdContacts['id'], 'Created Contacts must have id specified');
        $this->assertNotNull(Contacts::find($createdContacts['id']), 'Contacts with given id must be in DB');
        $this->assertModelData($contacts, $createdContacts);
    }

    /**
     * @test read
     */
    public function testReadContacts()
    {
        $contacts = $this->makeContacts();
        $dbContacts = $this->contactsRepo->find($contacts->id);
        $dbContacts = $dbContacts->toArray();
        $this->assertModelData($contacts->toArray(), $dbContacts);
    }

    /**
     * @test update
     */
    public function testUpdateContacts()
    {
        $contacts = $this->makeContacts();
        $fakeContacts = $this->fakeContactsData();
        $updatedContacts = $this->contactsRepo->update($fakeContacts, $contacts->id);
        $this->assertModelData($fakeContacts, $updatedContacts->toArray());
        $dbContacts = $this->contactsRepo->find($contacts->id);
        $this->assertModelData($fakeContacts, $dbContacts->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteContacts()
    {
        $contacts = $this->makeContacts();
        $resp = $this->contactsRepo->delete($contacts->id);
        $this->assertTrue($resp);
        $this->assertNull(Contacts::find($contacts->id), 'Contacts should not exist in DB');
    }
}
