<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RealClubApiTest extends TestCase
{
    use MakeRealClubTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateRealClub()
    {
        $realClub = $this->fakeRealClubData();
        $this->json('POST', '/api/v1/realClubs', $realClub);

        $this->assertApiResponse($realClub);
    }

    /**
     * @test
     */
    public function testReadRealClub()
    {
        $realClub = $this->makeRealClub();
        $this->json('GET', '/api/v1/realClubs/'.$realClub->id);

        $this->assertApiResponse($realClub->toArray());
    }

    /**
     * @test
     */
    public function testUpdateRealClub()
    {
        $realClub = $this->makeRealClub();
        $editedRealClub = $this->fakeRealClubData();

        $this->json('PUT', '/api/v1/realClubs/'.$realClub->id, $editedRealClub);

        $this->assertApiResponse($editedRealClub);
    }

    /**
     * @test
     */
    public function testDeleteRealClub()
    {
        $realClub = $this->makeRealClub();
        $this->json('DELETE', '/api/v1/realClubs/'.$realClub->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/realClubs/'.$realClub->id);

        $this->assertResponseStatus(404);
    }
}
