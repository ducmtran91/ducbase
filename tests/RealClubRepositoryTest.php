<?php

use App\Models\RealClub;
use App\Repositories\RealClubRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RealClubRepositoryTest extends TestCase
{
    use MakeRealClubTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RealClubRepository
     */
    protected $realClubRepo;

    public function setUp()
    {
        parent::setUp();
        $this->realClubRepo = App::make(RealClubRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateRealClub()
    {
        $realClub = $this->fakeRealClubData();
        $createdRealClub = $this->realClubRepo->create($realClub);
        $createdRealClub = $createdRealClub->toArray();
        $this->assertArrayHasKey('id', $createdRealClub);
        $this->assertNotNull($createdRealClub['id'], 'Created RealClub must have id specified');
        $this->assertNotNull(RealClub::find($createdRealClub['id']), 'RealClub with given id must be in DB');
        $this->assertModelData($realClub, $createdRealClub);
    }

    /**
     * @test read
     */
    public function testReadRealClub()
    {
        $realClub = $this->makeRealClub();
        $dbRealClub = $this->realClubRepo->find($realClub->id);
        $dbRealClub = $dbRealClub->toArray();
        $this->assertModelData($realClub->toArray(), $dbRealClub);
    }

    /**
     * @test update
     */
    public function testUpdateRealClub()
    {
        $realClub = $this->makeRealClub();
        $fakeRealClub = $this->fakeRealClubData();
        $updatedRealClub = $this->realClubRepo->update($fakeRealClub, $realClub->id);
        $this->assertModelData($fakeRealClub, $updatedRealClub->toArray());
        $dbRealClub = $this->realClubRepo->find($realClub->id);
        $this->assertModelData($fakeRealClub, $dbRealClub->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteRealClub()
    {
        $realClub = $this->makeRealClub();
        $resp = $this->realClubRepo->delete($realClub->id);
        $this->assertTrue($resp);
        $this->assertNull(RealClub::find($realClub->id), 'RealClub should not exist in DB');
    }
}
