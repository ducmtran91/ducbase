<?php

use App\Models\PackagePricing;
use App\Repositories\PackagePricingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackagePricingRepositoryTest extends TestCase
{
    use MakePackagePricingTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PackagePricingRepository
     */
    protected $packagePricingRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packagePricingRepo = App::make(PackagePricingRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePackagePricing()
    {
        $packagePricing = $this->fakePackagePricingData();
        $createdPackagePricing = $this->packagePricingRepo->create($packagePricing);
        $createdPackagePricing = $createdPackagePricing->toArray();
        $this->assertArrayHasKey('id', $createdPackagePricing);
        $this->assertNotNull($createdPackagePricing['id'], 'Created PackagePricing must have id specified');
        $this->assertNotNull(PackagePricing::find($createdPackagePricing['id']), 'PackagePricing with given id must be in DB');
        $this->assertModelData($packagePricing, $createdPackagePricing);
    }

    /**
     * @test read
     */
    public function testReadPackagePricing()
    {
        $packagePricing = $this->makePackagePricing();
        $dbPackagePricing = $this->packagePricingRepo->find($packagePricing->id);
        $dbPackagePricing = $dbPackagePricing->toArray();
        $this->assertModelData($packagePricing->toArray(), $dbPackagePricing);
    }

    /**
     * @test update
     */
    public function testUpdatePackagePricing()
    {
        $packagePricing = $this->makePackagePricing();
        $fakePackagePricing = $this->fakePackagePricingData();
        $updatedPackagePricing = $this->packagePricingRepo->update($fakePackagePricing, $packagePricing->id);
        $this->assertModelData($fakePackagePricing, $updatedPackagePricing->toArray());
        $dbPackagePricing = $this->packagePricingRepo->find($packagePricing->id);
        $this->assertModelData($fakePackagePricing, $dbPackagePricing->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePackagePricing()
    {
        $packagePricing = $this->makePackagePricing();
        $resp = $this->packagePricingRepo->delete($packagePricing->id);
        $this->assertTrue($resp);
        $this->assertNull(PackagePricing::find($packagePricing->id), 'PackagePricing should not exist in DB');
    }
}
