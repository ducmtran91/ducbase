<?php

use App\Models\PackageGroup;
use App\Repositories\PackageGroupRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackageGroupRepositoryTest extends TestCase
{
    use MakePackageGroupTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PackageGroupRepository
     */
    protected $packageGroupRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packageGroupRepo = App::make(PackageGroupRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePackageGroup()
    {
        $packageGroup = $this->fakePackageGroupData();
        $createdPackageGroup = $this->packageGroupRepo->create($packageGroup);
        $createdPackageGroup = $createdPackageGroup->toArray();
        $this->assertArrayHasKey('id', $createdPackageGroup);
        $this->assertNotNull($createdPackageGroup['id'], 'Created PackageGroup must have id specified');
        $this->assertNotNull(PackageGroup::find($createdPackageGroup['id']), 'PackageGroup with given id must be in DB');
        $this->assertModelData($packageGroup, $createdPackageGroup);
    }

    /**
     * @test read
     */
    public function testReadPackageGroup()
    {
        $packageGroup = $this->makePackageGroup();
        $dbPackageGroup = $this->packageGroupRepo->find($packageGroup->id);
        $dbPackageGroup = $dbPackageGroup->toArray();
        $this->assertModelData($packageGroup->toArray(), $dbPackageGroup);
    }

    /**
     * @test update
     */
    public function testUpdatePackageGroup()
    {
        $packageGroup = $this->makePackageGroup();
        $fakePackageGroup = $this->fakePackageGroupData();
        $updatedPackageGroup = $this->packageGroupRepo->update($fakePackageGroup, $packageGroup->id);
        $this->assertModelData($fakePackageGroup, $updatedPackageGroup->toArray());
        $dbPackageGroup = $this->packageGroupRepo->find($packageGroup->id);
        $this->assertModelData($fakePackageGroup, $dbPackageGroup->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePackageGroup()
    {
        $packageGroup = $this->makePackageGroup();
        $resp = $this->packageGroupRepo->delete($packageGroup->id);
        $this->assertTrue($resp);
        $this->assertNull(PackageGroup::find($packageGroup->id), 'PackageGroup should not exist in DB');
    }
}
