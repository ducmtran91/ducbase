<?php

use App\Models\PlayerStatistic;
use App\Repositories\PlayerStatisticRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PlayerStatisticRepositoryTest extends TestCase
{
    use MakePlayerStatisticTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PlayerStatisticRepository
     */
    protected $playerStatisticRepo;

    public function setUp()
    {
        parent::setUp();
        $this->playerStatisticRepo = App::make(PlayerStatisticRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePlayerStatistic()
    {
        $playerStatistic = $this->fakePlayerStatisticData();
        $createdPlayerStatistic = $this->playerStatisticRepo->create($playerStatistic);
        $createdPlayerStatistic = $createdPlayerStatistic->toArray();
        $this->assertArrayHasKey('id', $createdPlayerStatistic);
        $this->assertNotNull($createdPlayerStatistic['id'], 'Created PlayerStatistic must have id specified');
        $this->assertNotNull(PlayerStatistic::find($createdPlayerStatistic['id']), 'PlayerStatistic with given id must be in DB');
        $this->assertModelData($playerStatistic, $createdPlayerStatistic);
    }

    /**
     * @test read
     */
    public function testReadPlayerStatistic()
    {
        $playerStatistic = $this->makePlayerStatistic();
        $dbPlayerStatistic = $this->playerStatisticRepo->find($playerStatistic->id);
        $dbPlayerStatistic = $dbPlayerStatistic->toArray();
        $this->assertModelData($playerStatistic->toArray(), $dbPlayerStatistic);
    }

    /**
     * @test update
     */
    public function testUpdatePlayerStatistic()
    {
        $playerStatistic = $this->makePlayerStatistic();
        $fakePlayerStatistic = $this->fakePlayerStatisticData();
        $updatedPlayerStatistic = $this->playerStatisticRepo->update($fakePlayerStatistic, $playerStatistic->id);
        $this->assertModelData($fakePlayerStatistic, $updatedPlayerStatistic->toArray());
        $dbPlayerStatistic = $this->playerStatisticRepo->find($playerStatistic->id);
        $this->assertModelData($fakePlayerStatistic, $dbPlayerStatistic->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePlayerStatistic()
    {
        $playerStatistic = $this->makePlayerStatistic();
        $resp = $this->playerStatisticRepo->delete($playerStatistic->id);
        $this->assertTrue($resp);
        $this->assertNull(PlayerStatistic::find($playerStatistic->id), 'PlayerStatistic should not exist in DB');
    }
}
