<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ContactUsApiTest extends TestCase
{
    use MakeContactUsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateContactUs()
    {
        $contactUs = $this->fakeContactUsData();
        $this->json('POST', '/api/v1/contactuses', $contactUs);

        $this->assertApiResponse($contactUs);
    }

    /**
     * @test
     */
    public function testReadContactUs()
    {
        $contactUs = $this->makeContactUs();
        $this->json('GET', '/api/v1/contactuses/'.$contactUs->id);

        $this->assertApiResponse($contactUs->toArray());
    }

    /**
     * @test
     */
    public function testUpdateContactUs()
    {
        $contactUs = $this->makeContactUs();
        $editedContactUs = $this->fakeContactUsData();

        $this->json('PUT', '/api/v1/contactuses/'.$contactUs->id, $editedContactUs);

        $this->assertApiResponse($editedContactUs);
    }

    /**
     * @test
     */
    public function testDeleteContactUs()
    {
        $contactUs = $this->makeContactUs();
        $this->json('DELETE', '/api/v1/contactuses/'.$contactUs->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/contactuses/'.$contactUs->id);

        $this->assertResponseStatus(404);
    }
}
