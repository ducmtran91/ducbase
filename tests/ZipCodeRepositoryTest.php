<?php

use App\Models\ZipCode;
use App\Repositories\ZipCodeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ZipCodeRepositoryTest extends TestCase
{
    use MakeZipCodeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ZipCodeRepository
     */
    protected $zipCodeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->zipCodeRepo = App::make(ZipCodeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateZipCode()
    {
        $zipCode = $this->fakeZipCodeData();
        $createdZipCode = $this->zipCodeRepo->create($zipCode);
        $createdZipCode = $createdZipCode->toArray();
        $this->assertArrayHasKey('id', $createdZipCode);
        $this->assertNotNull($createdZipCode['id'], 'Created ZipCode must have id specified');
        $this->assertNotNull(ZipCode::find($createdZipCode['id']), 'ZipCode with given id must be in DB');
        $this->assertModelData($zipCode, $createdZipCode);
    }

    /**
     * @test read
     */
    public function testReadZipCode()
    {
        $zipCode = $this->makeZipCode();
        $dbZipCode = $this->zipCodeRepo->find($zipCode->id);
        $dbZipCode = $dbZipCode->toArray();
        $this->assertModelData($zipCode->toArray(), $dbZipCode);
    }

    /**
     * @test update
     */
    public function testUpdateZipCode()
    {
        $zipCode = $this->makeZipCode();
        $fakeZipCode = $this->fakeZipCodeData();
        $updatedZipCode = $this->zipCodeRepo->update($fakeZipCode, $zipCode->id);
        $this->assertModelData($fakeZipCode, $updatedZipCode->toArray());
        $dbZipCode = $this->zipCodeRepo->find($zipCode->id);
        $this->assertModelData($fakeZipCode, $dbZipCode->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteZipCode()
    {
        $zipCode = $this->makeZipCode();
        $resp = $this->zipCodeRepo->delete($zipCode->id);
        $this->assertTrue($resp);
        $this->assertNull(ZipCode::find($zipCode->id), 'ZipCode should not exist in DB');
    }
}
