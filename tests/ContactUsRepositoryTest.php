<?php

use App\Models\ContactUs;
use App\Repositories\ContactUsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ContactUsRepositoryTest extends TestCase
{
    use MakeContactUsTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ContactUsRepository
     */
    protected $contactUsRepo;

    public function setUp()
    {
        parent::setUp();
        $this->contactUsRepo = App::make(ContactUsRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateContactUs()
    {
        $contactUs = $this->fakeContactUsData();
        $createdContactUs = $this->contactUsRepo->create($contactUs);
        $createdContactUs = $createdContactUs->toArray();
        $this->assertArrayHasKey('id', $createdContactUs);
        $this->assertNotNull($createdContactUs['id'], 'Created ContactUs must have id specified');
        $this->assertNotNull(ContactUs::find($createdContactUs['id']), 'ContactUs with given id must be in DB');
        $this->assertModelData($contactUs, $createdContactUs);
    }

    /**
     * @test read
     */
    public function testReadContactUs()
    {
        $contactUs = $this->makeContactUs();
        $dbContactUs = $this->contactUsRepo->find($contactUs->id);
        $dbContactUs = $dbContactUs->toArray();
        $this->assertModelData($contactUs->toArray(), $dbContactUs);
    }

    /**
     * @test update
     */
    public function testUpdateContactUs()
    {
        $contactUs = $this->makeContactUs();
        $fakeContactUs = $this->fakeContactUsData();
        $updatedContactUs = $this->contactUsRepo->update($fakeContactUs, $contactUs->id);
        $this->assertModelData($fakeContactUs, $updatedContactUs->toArray());
        $dbContactUs = $this->contactUsRepo->find($contactUs->id);
        $this->assertModelData($fakeContactUs, $dbContactUs->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteContactUs()
    {
        $contactUs = $this->makeContactUs();
        $resp = $this->contactUsRepo->delete($contactUs->id);
        $this->assertTrue($resp);
        $this->assertNull(ContactUs::find($contactUs->id), 'ContactUs should not exist in DB');
    }
}
