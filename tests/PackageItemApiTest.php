<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackageItemApiTest extends TestCase
{
    use MakePackageItemTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePackageItem()
    {
        $packageItem = $this->fakePackageItemData();
        $this->json('POST', '/api/v1/packageItems', $packageItem);

        $this->assertApiResponse($packageItem);
    }

    /**
     * @test
     */
    public function testReadPackageItem()
    {
        $packageItem = $this->makePackageItem();
        $this->json('GET', '/api/v1/packageItems/'.$packageItem->id);

        $this->assertApiResponse($packageItem->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePackageItem()
    {
        $packageItem = $this->makePackageItem();
        $editedPackageItem = $this->fakePackageItemData();

        $this->json('PUT', '/api/v1/packageItems/'.$packageItem->id, $editedPackageItem);

        $this->assertApiResponse($editedPackageItem);
    }

    /**
     * @test
     */
    public function testDeletePackageItem()
    {
        $packageItem = $this->makePackageItem();
        $this->json('DELETE', '/api/v1/packageItems/'.$packageItem->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packageItems/'.$packageItem->id);

        $this->assertResponseStatus(404);
    }
}
