<?php

use App\Models\ServiceTypes;
use App\Repositories\ServiceTypesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ServiceTypesRepositoryTest extends TestCase
{
    use MakeServiceTypesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ServiceTypesRepository
     */
    protected $serviceTypesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->serviceTypesRepo = App::make(ServiceTypesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateServiceTypes()
    {
        $serviceTypes = $this->fakeServiceTypesData();
        $createdServiceTypes = $this->serviceTypesRepo->create($serviceTypes);
        $createdServiceTypes = $createdServiceTypes->toArray();
        $this->assertArrayHasKey('id', $createdServiceTypes);
        $this->assertNotNull($createdServiceTypes['id'], 'Created ServiceTypes must have id specified');
        $this->assertNotNull(ServiceTypes::find($createdServiceTypes['id']), 'ServiceTypes with given id must be in DB');
        $this->assertModelData($serviceTypes, $createdServiceTypes);
    }

    /**
     * @test read
     */
    public function testReadServiceTypes()
    {
        $serviceTypes = $this->makeServiceTypes();
        $dbServiceTypes = $this->serviceTypesRepo->find($serviceTypes->id);
        $dbServiceTypes = $dbServiceTypes->toArray();
        $this->assertModelData($serviceTypes->toArray(), $dbServiceTypes);
    }

    /**
     * @test update
     */
    public function testUpdateServiceTypes()
    {
        $serviceTypes = $this->makeServiceTypes();
        $fakeServiceTypes = $this->fakeServiceTypesData();
        $updatedServiceTypes = $this->serviceTypesRepo->update($fakeServiceTypes, $serviceTypes->id);
        $this->assertModelData($fakeServiceTypes, $updatedServiceTypes->toArray());
        $dbServiceTypes = $this->serviceTypesRepo->find($serviceTypes->id);
        $this->assertModelData($fakeServiceTypes, $dbServiceTypes->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteServiceTypes()
    {
        $serviceTypes = $this->makeServiceTypes();
        $resp = $this->serviceTypesRepo->delete($serviceTypes->id);
        $this->assertTrue($resp);
        $this->assertNull(ServiceTypes::find($serviceTypes->id), 'ServiceTypes should not exist in DB');
    }
}
