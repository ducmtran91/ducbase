<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RealRoundApiTest extends TestCase
{
    use MakeRealRoundTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateRealRound()
    {
        $realRound = $this->fakeRealRoundData();
        $this->json('POST', '/api/v1/realRounds', $realRound);

        $this->assertApiResponse($realRound);
    }

    /**
     * @test
     */
    public function testReadRealRound()
    {
        $realRound = $this->makeRealRound();
        $this->json('GET', '/api/v1/realRounds/'.$realRound->id);

        $this->assertApiResponse($realRound->toArray());
    }

    /**
     * @test
     */
    public function testUpdateRealRound()
    {
        $realRound = $this->makeRealRound();
        $editedRealRound = $this->fakeRealRoundData();

        $this->json('PUT', '/api/v1/realRounds/'.$realRound->id, $editedRealRound);

        $this->assertApiResponse($editedRealRound);
    }

    /**
     * @test
     */
    public function testDeleteRealRound()
    {
        $realRound = $this->makeRealRound();
        $this->json('DELETE', '/api/v1/realRounds/'.$realRound->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/realRounds/'.$realRound->id);

        $this->assertResponseStatus(404);
    }
}
