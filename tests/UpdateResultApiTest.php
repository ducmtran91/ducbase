<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UpdateResultApiTest extends TestCase
{
    use MakeUpdateResultTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateUpdateResult()
    {
        $updateResult = $this->fakeUpdateResultData();
        $this->json('POST', '/api/v1/updateResults', $updateResult);

        $this->assertApiResponse($updateResult);
    }

    /**
     * @test
     */
    public function testReadUpdateResult()
    {
        $updateResult = $this->makeUpdateResult();
        $this->json('GET', '/api/v1/updateResults/'.$updateResult->id);

        $this->assertApiResponse($updateResult->toArray());
    }

    /**
     * @test
     */
    public function testUpdateUpdateResult()
    {
        $updateResult = $this->makeUpdateResult();
        $editedUpdateResult = $this->fakeUpdateResultData();

        $this->json('PUT', '/api/v1/updateResults/'.$updateResult->id, $editedUpdateResult);

        $this->assertApiResponse($editedUpdateResult);
    }

    /**
     * @test
     */
    public function testDeleteUpdateResult()
    {
        $updateResult = $this->makeUpdateResult();
        $this->json('DELETE', '/api/v1/updateResults/'.$updateResult->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/updateResults/'.$updateResult->id);

        $this->assertResponseStatus(404);
    }
}
