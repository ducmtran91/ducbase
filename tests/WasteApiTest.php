<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WasteApiTest extends TestCase
{
    use MakeWasteTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateWaste()
    {
        $waste = $this->fakeWasteData();
        $this->json('POST', '/api/v1/wastes', $waste);

        $this->assertApiResponse($waste);
    }

    /**
     * @test
     */
    public function testReadWaste()
    {
        $waste = $this->makeWaste();
        $this->json('GET', '/api/v1/wastes/'.$waste->id);

        $this->assertApiResponse($waste->toArray());
    }

    /**
     * @test
     */
    public function testUpdateWaste()
    {
        $waste = $this->makeWaste();
        $editedWaste = $this->fakeWasteData();

        $this->json('PUT', '/api/v1/wastes/'.$waste->id, $editedWaste);

        $this->assertApiResponse($editedWaste);
    }

    /**
     * @test
     */
    public function testDeleteWaste()
    {
        $waste = $this->makeWaste();
        $this->json('DELETE', '/api/v1/wastes/'.$waste->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/wastes/'.$waste->id);

        $this->assertResponseStatus(404);
    }
}
