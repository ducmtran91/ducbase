<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReportCardApiTest extends TestCase
{
    use MakeReportCardTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateReportCard()
    {
        $reportCard = $this->fakeReportCardData();
        $this->json('POST', '/api/v1/reportCards', $reportCard);

        $this->assertApiResponse($reportCard);
    }

    /**
     * @test
     */
    public function testReadReportCard()
    {
        $reportCard = $this->makeReportCard();
        $this->json('GET', '/api/v1/reportCards/'.$reportCard->id);

        $this->assertApiResponse($reportCard->toArray());
    }

    /**
     * @test
     */
    public function testUpdateReportCard()
    {
        $reportCard = $this->makeReportCard();
        $editedReportCard = $this->fakeReportCardData();

        $this->json('PUT', '/api/v1/reportCards/'.$reportCard->id, $editedReportCard);

        $this->assertApiResponse($editedReportCard);
    }

    /**
     * @test
     */
    public function testDeleteReportCard()
    {
        $reportCard = $this->makeReportCard();
        $this->json('DELETE', '/api/v1/reportCards/'.$reportCard->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/reportCards/'.$reportCard->id);

        $this->assertResponseStatus(404);
    }
}
