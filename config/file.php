<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | File
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */
    'image' => array(
        'types_string' => 'jpg,jpeg,png',
        'types_array' => array('jpg', 'jpeg', 'png'),
        'size' => 10240,
        'validate' => 'mimes:jpg,jpeg,png|max:10240',
        'validate_message' => ' must be less than 10MB'
    ),
    'audio' => array(
        'types_string' => 'mp3,wma,ape,flac,aac,ac3,mmf,amr,m4a,m4r,ogg,wavpack,mp2',
        'types_array' => array('mp3','wma','ape','flac','aac','ac3','mmf','amr','m4a','m4r','ogg','wavpack','mp2'),
        'size' => 102400,
        'validate' => 'mimes:mp3,wma,ape,flac,aac,ac3,mmf,amr,m4a,m4r,ogg,wavpack,mp2|max:102400'
    ),
    'video' => array(
        'types_string' => 'mp4,mkv,avi,webm,3gb,wmv,mpg,vob,mov,flv,swf',
        'types_array' => array('mp4','mkv','avi','webm','3gb','wmv','mpg','vob','mov','flv','swf'),
        'size' => 1024000,
        'validate' => 'mimes:mp4,mkv,avi,webm,3gb,wmv,mpg,vob,mov,flv,swf|max:1024000'
    ),
    'doc' => array(
        'types_string' => 'doc,docx,pdf,ppt,pptx',
        'types_array' => array('doc', 'docx', 'pdf', 'ppt', 'pptx'),
        'size' => 10240,
        'validate' => 'mimes:doc,docx,pdf,ppt,pptx|max:10240'
    )
);
