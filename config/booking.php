<?php

return [
    'services' => [
        'dog_walking' => [
            'time' => [
                '0608am' => [
                    'start' => '06:00:00',
                    'end' => '07:59:59',
                    'label' => '6-8 AM',
                    'booked_time_type' => 0
                ],
                '0810am' => [
                    'start' => '08:00:00',
                    'end' => '09:59:59',
                    'label' => '8-10 AM',
                    'booked_time_type' => 1
                ],
                '1012am' => [
                    'start' => '10:00:00',
                    'end' => '11:59:59',
                    'label' => '10-12 AM',
                    'booked_time_type' => 2
                ],
                '1202pm' => [
                    'start' => '12:00:00',
                    'end' => '13:59:59',
                    'label' => '12-2 PM',
                    'booked_time_type' => 3
                ],
                '0204pm' => [
                    'start' => '14:00:00',
                    'end' => '15:59:59',
                    'label' => '2-4 PM',
                    'booked_time_type' => 4
                ],
                '0406pm' => [
                    'start' => '16:00:00',
                    'end' => '17:59:59',
                    'label' => '4-6 PM',
                    'booked_time_type' => 5
                ],
                '0608pm' => [
                    'start' => '18:00:00',
                    'end' => '19:59:59',
                    'label' => '6-8 PM',
                    'booked_time_type' => 6
                ],
                '0810pm' => [
                    'start' => '20:00:00',
                    'end' => '21:59:59',
                    'label' => '8-10 PM',
                    'booked_time_type' => 7
                ],
            ],
            'validation' => [
                'before_booking' => 2
            ]
        ],
        'dog_running' => [
            'time' => [
                '0608am' => [
                    'start' => '06:00:00',
                    'end' => '07:59:59',
                    'label' => '6-8 AM',
                    'booked_time_type' => 0
                ],
                '0810am' => [
                    'start' => '08:00:00',
                    'end' => '09:59:59',
                    'label' => '8-10 AM',
                    'booked_time_type' => 1
                ],
                '1012am' => [
                    'start' => '10:00:00',
                    'end' => '11:59:59',
                    'label' => '10-12 AM',
                    'booked_time_type' => 2
                ],
                '1202pm' => [
                    'start' => '12:00:00',
                    'end' => '13:59:59',
                    'label' => '12-2 PM',
                    'booked_time_type' => 3
                ],
                '0204pm' => [
                    'start' => '14:00:00',
                    'end' => '15:59:59',
                    'label' => '2-4 PM',
                    'booked_time_type' => 4
                ],
                '0406pm' => [
                    'start' => '16:00:00',
                    'end' => '17:59:59',
                    'label' => '4-6 PM',
                    'booked_time_type' => 5
                ],
                '0608pm' => [
                    'start' => '18:00:00',
                    'end' => '19:59:59',
                    'label' => '6-8 PM',
                    'booked_time_type' => 6
                ],
                '0810pm' => [
                    'start' => '20:00:00',
                    'end' => '21:59:59',
                    'label' => '8-10 PM',
                    'booked_time_type' => 7
                ],
            ],
            'validation' => [
                'before_booking' => 2
            ]
        ],
        'bathroom_break' => [
            'time' => [
                '0608am' => [
                    'start' => '06:00:00',
                    'end' => '07:59:59',
                    'label' => '6-8 AM',
                    'booked_time_type' => 0
                ],
                '0810am' => [
                    'start' => '08:00:00',
                    'end' => '09:59:59',
                    'label' => '8-10 AM',
                    'booked_time_type' => 1
                ],
                '1012am' => [
                    'start' => '10:00:00',
                    'end' => '11:59:59',
                    'label' => '10-12 AM',
                    'booked_time_type' => 2
                ],
                '1202pm' => [
                    'start' => '12:00:00',
                    'end' => '13:59:59',
                    'label' => '12-2 PM',
                    'booked_time_type' => 3
                ],
                '0204pm' => [
                    'start' => '14:00:00',
                    'end' => '15:59:59',
                    'label' => '2-4 PM',
                    'booked_time_type' => 4
                ],
                '0406pm' => [
                    'start' => '16:00:00',
                    'end' => '17:59:59',
                    'label' => '4-6 PM',
                    'booked_time_type' => 5
                ],
                '0608pm' => [
                    'start' => '18:00:00',
                    'end' => '19:59:59',
                    'label' => '6-8 PM',
                    'booked_time_type' => 6
                ],
                '0810pm' => [
                    'start' => '20:00:00',
                    'end' => '21:59:59',
                    'label' => '8-10 PM',
                    'booked_time_type' => 7
                ],
            ],
            'validation' => [
                'before_booking' => 2
            ]
        ]
    ],
    'home_access' => [
        [
            'id' => 1,
            'name' => 'Someone will be home'
        ],
        [
            'id' => 2,
            'name' => 'Hidden key (add access info below)'
        ],
        [
            'id' => 3,
            'name' => 'Key with doorman'
        ]
    ],
    'report_card' => [
        ''
    ]
];