<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Common configs
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */
    'role' => [
        0 => 'walker',
        1 => 'owner',
    ],
    'my_format' => 'm/y',
    'ym_format' => 'Y/m',
    'date_format' => 'Y/m/d',
    'date_db_format' => 'Y-m-d',
    'datetime_format' => 'Y/m/d H:i:s',
    'datetime_db_format' => 'Y-m-d H:i:s',

    'times' => array(
        array(
            'start' => '06:00:00',
            'end' => '07:59:59',
            'label' => '6-8 AM'
        ),
        array(
            'start' => '08:00:00',
            'end' => '09:59:59',
            'label' => '8-10 AM'
        ),
        array(
            'start' => '10:00:00',
            'end' => '11:59:59',
            'label' => '10-12 AM'
        ),
        array(
            'start' => '12:00:00',
            'end' => '13:59:59',
            'label' => '12-2 PM'
        ),
        array(
            'start' => '14:00:00',
            'end' => '15:59:59',
            'label' => '2-4 PM'
        ),
        array(
            'start' => '16:00:00',
            'end' => '17:59:59',
            'label' => '4-6 PM'
        ),
        array(
            'start' => '18:00:00',
            'end' => '19:59:59',
            'label' => '6-8 PM'
        ),
        array(
            'start' => '20:00:00',
            'end' => '21:59:59',
            'label' => '8-10 PM'
        )
    )
);