<?php

return [
    'dashboard' => [
        'name' => 'Dashboard Management',
        'actions' => [
            [
                'name' => 'List',
                'action' => 'dashboard@index'
            ],
            [
                'name' => 'Export',
                'action' => 'dashboard@export'
            ]
        ]
    ],
    'roles' => [
        'name' => 'Roles Management',
        'actions' => [
            [
                'name' => 'List',
                'action' => 'roles@index'
            ],
            [
                'name' => 'Add New',
                'action' => 'roles@create'
            ],
            [
                'name' => 'Edit',
                'action' => 'roles@edit'
            ],
            [
                'name' => 'Quick: delete, active, inactive',
                'action' => 'roles@itemactions'
            ],
            [
                'name' => 'Bulk action',
                'action' => 'roles@bulkactions'
            ]
        ]
    ],
    'categories' => [
        'name' => 'Categories Management',
        'actions' => [
            [
                'name' => 'List',
                'action' => 'categories@index'
            ],
            [
                'name' => 'View',
                'action' => 'categories@show'
            ],
            [
                'name' => 'Add New',
                'action' => 'categories@create'
            ],
            [
                'name' => 'Edit',
                'action' => 'categories@edit'
            ],
            [
                'name' => 'Quick: delete, active, inactive',
                'action' => 'categories@itemactions'
            ],
            [
                'name' => 'Bulk action',
                'action' => 'categories@bulkactions'
            ]
        ]
    ],
    'posts' => [
        'name' => 'Posts Management',
        'actions' => [
            [
                'name' => 'List',
                'action' => 'posts@index'
            ],
            [
                'name' => 'View',
                'action' => 'owners@show'
            ],
            [
                'name' => 'Add New',
                'action' => 'posts@create'
            ],
            [
                'name' => 'Edit',
                'action' => 'posts@edit'
            ],
            [
                'name' => 'Quick: delete, active, inactive',
                'action' => 'posts@itemactions'
            ],
            [
                'name' => 'Bulk action',
                'action' => 'posts@bulkactions'
            ]
        ]
    ],
    'detailPosts' => [
        'name' => 'Detail Posts Management',
        'actions' => [
            [
                'name' => 'List',
                'action' => 'detailPosts@index'
            ],
            [
                'name' => 'View',
                'action' => 'detailPosts@show'
            ],
            [
                'name' => 'Add New',
                'action' => 'detailPosts@create'
            ],
            [
                'name' => 'Edit',
                'action' => 'detailPosts@edit'
            ],
            [
                'name' => 'Quick: delete, active, inactive',
                'action' => 'detailPosts@itemactions'
            ],
            [
                'name' => 'Bulk action',
                'action' => 'detailPosts@bulkactions'
            ]
        ]
    ],
    'slider' => [
        'name' => 'Slider Management',
        'actions' => [
            [
                'name' => 'List',
                'action' => 'slider@index'
            ],
            [
                'name' => 'View',
                'action' => 'slider@show'
            ],
            [
                'name' => 'Add New',
                'action' => 'slider@create'
            ],
            [
                'name' => 'Edit',
                'action' => 'slider@edit'
            ],
            [
                'name' => 'Quick: delete, active, inactive',
                'action' => 'slider@itemactions'
            ],
            [
                'name' => 'Bulk action',
                'action' => 'slider@bulkactions'
            ]
        ]
    ],
    'banner' => [
        'name' => 'Banner Management',
        'actions' => [
            [
                'name' => 'List',
                'action' => 'banner@index'
            ],
            [
                'name' => 'View',
                'action' => 'banner@show'
            ],
            [
                'name' => 'Add New',
                'action' => 'banner@create'
            ],
            [
                'name' => 'Edit',
                'action' => 'banner@edit'
            ],
            [
                'name' => 'Quick: delete, active, inactive',
                'action' => 'banner@itemactions'
            ],
            [
                'name' => 'Bulk action',
                'action' => 'banner@bulkactions'
            ]
        ]
    ],
];
