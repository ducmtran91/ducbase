<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Validate messages
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'required' => 'field_is_required',
    'max_40' => 'field_max_40_character',
    'max_255' => 'field_max_255_character',
    'field_unique' => 'field_is_unique',
    'confirmed' => 'field_confirmed',
    'min_6' => 'field_min_6_character',
    'in' => "selected_is_invalid",
    'success' => "success",
    'internal_exception' => "internal_exception",
    'file' => '',
    'invalid_status' => 'invalid_status',

    'login' => [
        'failure' => 'login_failure',
        'invalid' => 'login_invalid_credentials',
        'failed_created_token' => 'jwt_failed_to_create_token'
    ],
    'register' => [
        'failure' => 'register_failure',
        'exist' => 'register_user_exist',
        'success' => 'register_success',
    ],
    'user' => [
        'register_exist' => 'user_register_exist',
        'register_false' => 'user_register_false',
    ],
    'forgot' => [
        'user_forgot_email' => 'forgot_user_forgot_email',
        'user_forgot_email_invalid' => 'forgot_user_forgot_email_invalid',
        'user_forgot_email_null' => 'forgot_user_forgot_email_null',
        'user_forgot_email_update' => 'forgot_user_forgot_email_update',
        'user_email_does_not_sent_successfully' => 'forgot_user_email_does_not_sent_successfully',
        'user_forgot_email_system' => 'forgot_user_forgot_email_system',
    ],
    'league' => [
        'add_invalid' => 'league_add_invalid_credentials',
        'update_invalid' => 'league_update_invalid_credentials',
        'invalid_season' => 'league_invalid_season',
        'invalid_budget' => 'league_invalid_budget',
        'invalid_setup_time' => 'league_invalid_setup_time',
        'invalid_start_transfer_time' => 'league_invalid_start_transfer_time',
        'invalid_start_draft_time' => 'league_invalid_start_draft_time',
        'invalid_trade_review' => 'league_invalid_trade_review',
        'invalid_gameplay_option' => 'league_invalid_gameplay_option',
        'number_user_not_edit' => 'league_number_user_not_edit',
        'invalid_number_user' => 'league_invalid_number_user',
        'invalid_scoring_system_no_edit' => 'league_invalid_scoring_system_no_edit',
        'invalid_setup_time_number_user_change' => 'league_invalid_setup_time_number_user_change',
        'not_found' => 'league_not_found',
        'please_select_other_team' => 'please_select_other_team',
        'team_exist' => 'league_team_exist'
    ],

    'filesystems' => [
        'cloud' => [
            'not_found' => 'could_not_found_cloud_storage'
        ],
        'delete' => [
            'failure' => 'delete_file_failure',
            'success' => 'delete_file_success',
        ]
    ],
    'team' => [
        'not_found' => 'team_not_found',
        'my_team_not_found' => 'my_team_not_found',
        'full_of_entrants' => 'this_league_is_already_full_of_entrants',
        'invalid_params' => 'invalid_params',
    ],
    'invitation' => [
        'invitation_already_sent' => 'invitation_already_sent',
        'invitation_not_found' => 'invitation_not_found',
    ],
    'post' => [
        'not_found' => 'post_not_found',
    ],
    'player' => [
        'not_found' => 'player_not_found',
        'invalid_position' => 'invalid_player_position',
        'already_add' => 'player_already_add',
    ],
    'league_user' => [
        'maximum_5_leagues' => 'maximum_5_leagues', /* You can only join maximum 5 leagues at a time */
    ],
);