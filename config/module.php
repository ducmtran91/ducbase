<?php
return [
    'modules' => [
        'ContentManager' => 'contentManager.index',
        'TemplateManager' => 'templateManager.index',
        'SiteManager' => 'siteManager.index',
        'LanguageManager' => 'languageManager.index',
        'UserGuide' => 'userGuide.index',
        'WhyUs' => 'why-us.index',
        'Others' => 'others.index',
        'TalkAboutUs' => 'talk-about-us.index',
        'CustomerReview' => 'customer-review.index',
    ],
    'backend' => 'admin'
];