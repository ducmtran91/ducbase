<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    'time_available' => array(
        'morning' => 'Morning',
        'evening_night' => 'Evening/Night',
        'daytime' => 'Daytime',
        'not_available_weekends' => 'I am not available on weekends'
    ),
    'plan_traveling' => array(
        'walk_public_transit' => 'Walk/Public Transit',
        'car' => 'Car',
        'bike' => 'Bike',
        'other' => 'Other'
    ),
);