<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | File path
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'no_image' => '/themes/no-image.png',

    'user_image' => 'uploads/users/images/',
    'user_image_show' => '/uploads/users/images/',
    
    'sample_image' => 'sample-image/',
    'sample_image_show' => '/sample-image/',
    
    'proposal_image' => 'uploads/default/',
    'proposal_image_show' => '/uploads/default/',
    
    'report_card_image' => 'uploads/default/',
    'report_card_image_show' => '/uploads/default/',

    'report_card_route_image' => 'uploads/default/',
    'report_card_route_image_show' => '/uploads/default/',

    'slider_image' => 'uploads/sliders/',
    'banner_image' => 'uploads/banners/',
);