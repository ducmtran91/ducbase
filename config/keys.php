<?php

return [
    'common' => [
        'status' => [
            0 => 'deactive',
            1 => 'active'
        ],
        'sIsDeleted' => [
            0 => 'deleted',
            1 => 'active'
        ]
    ],
    'order' => [
        'status' => [
            0 => 'oPending',
            1 => 'oPaid',
            2 => 'oShipping',
            3 => 'oCompleted',
            4 => 'oDelay',
            5 => 'oCancel'
        ]
    ]


];