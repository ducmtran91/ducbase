<?php

namespace App\Response;

use Illuminate\Contracts\Pagination\Paginator;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Pagination\PaginatorInterface;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;

class AbstractResponse
{
    protected $manager;

    public function __construct()
    {
        $this->manager = new Manager();
    }

    public function item($node, $transformer)
    {
        return $this->manager->createData(new Item($node, $transformer))->toArray()['data'];
    }

    public function collection($node, $transformer)
    {
        return $this->manager->createData(new Collection($node, $transformer))->toArray()['data'];
    }

    public function paginator(Paginator $paginator, $transformer)
    {
        $collection = (new Collection($paginator->getCollection()))
             ->setTransformer($transformer)
            ->setPaginator(new IlluminatePaginatorAdapter($paginator));
        $res = $this->manager->createData($collection)->toArray();
dd($res);
        return [
            'data' => $res['data'],
            'pagination' => $res['meta']['pagination']
        ];
    }
}
