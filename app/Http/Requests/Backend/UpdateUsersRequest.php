<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class UpdateUsersRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'role_id' => 'required',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,' . $this->users,
            'new_password' => 'min:6|max:255',
            'confirm_new_password' => 'min:6|max:255|same:new_password',
            'image' => config('file.image.validate')
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages() {
        return [
            'role_id.required' => 'The role field is required',
            'image.max' => 'The avatar field' . config('file.image.validate_message')
        ];
    }

}
