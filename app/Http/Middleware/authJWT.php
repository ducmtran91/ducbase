<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;

class authJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');
        $token = str_replace('Bearer ', null, $token);
        try {
            JWTAuth::toUser($token);
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json([
                    'meta' => [
                        'success' => false,
                        'code' => 401,
                        'message' => 'Token is Invalid'
                    ]

                ], 401);
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return response()->json([
                    'meta' => [
                        'success' => false,
                        'code' => 401,
                        'message' => 'Token is Expired'
                    ]

                ], 401);
            } else {
                return response()->json([
                    'meta' => [
                        'success' => false,
                        'code' => 401,
                        'message' => 'Something is wrong'
                    ]

                ], 401);
            }
        }
        return $next($request);
    }
}