<?php

namespace App\Http\Middleware;

use Closure;

class Admin {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'admin') {        
        // User is guest
        if (\Auth::guard($guard)->guest()) {
            return redirect('/');
        }
        
        /** @var \App\User $user */
        $user = \Auth::guard($guard)->user();

        // User is super admin        
        if ($user->isSuperAdmin()) {
            $this->convertRequestDataHtmlSpecialEncode($request);
            
            return $next($request);
        }

        // Get role from cache if exist
        $roleCacheName = "role_permission_{$user->role_id}.php";
        $roleCacheFile = base_path() . '/storage/app/' . $roleCacheName;

        if (empty($user->roles) && file_exists($roleCacheFile)) {
            include_once $roleCacheFile;
            // $role_permission get from include file

            if (!empty($role_permission)) {
                $user->setRelation('roles', new \App\Entities\Roles($role_permission));
            }
        }

        // Check permission by user role (permission)
        $role = $user->roles;
        
        // When is not admin user
        if (!$user->isAdmin($role)) {
            return redirect('/');
        }

        if (!empty($role)) {
            if (!$role->status) {
                return redirect('/');
            }
        }
        
        // Check user has access
        if ($this->userHasAccessTo($request)) {
            $this->convertRequestDataHtmlSpecialEncode($request);
            
            return $next($request);
        }

        // Request ajax
        if ($request->ajax()) {
            return response()->json([
                'meta' => [
                    'success' => false,
                    'message' => 'You do not have permission to access this module!',
                    'code' => 403
                ],
                'response' => null// ($response === null) ? null : $this->transforms($response)
            ]);            
        }
        
        return abort(403);
    }

    /*
      |--------------------------------------------------------------------------
      | Additional helper methods for the handle method
      |--------------------------------------------------------------------------
     */

    /**
     * Checks if user has access to this requested route
     *
     * @param  \Illuminate\Http\Request $request
     * @return Boolean true if has permission otherwise false
     */
    protected function userHasAccessTo($request, $guard = 'admin') {
        return $this->hasPermission($request, $guard);
    }

    /**
     * hasPermission Check if user has requested route permimssion
     *
     * @param  \Illuminate\Http\Request $request
     * @return Boolean true if has permission otherwise false
     */
    protected function hasPermission($request, $guard = 'admin') {
        $required = $this->requiredPermission($request, $guard);

        return !$this->forbiddenRoute($request) && \Auth::guard($guard)->user()->hasPermission($required);
    }

    /**
     * Extract required permission from requested route
     *
     * @param  \Illuminate\Http\Request $request
     * @return String permission_slug connected to the Route
     */
    protected function requiredPermission($request) {
        $action = $request->route()->getAction();
        $required = [];

        if (isset($action['controller'])) {
            $controller = isset($action['namespace']) ? explode("{$action['namespace']}\\", $action['controller']) : [];

            $required = !empty($controller) ? (array) $controller[1] : (array) $action['controller'];
        }

        return $required;
    }

    /**
     * Check if current route is hidden to current user role
     *
     * @param  \Illuminate\Http\Request $request
     * @return Boolean true/false
     */
    protected function forbiddenRoute($request, $guard = 'admin') {
        $action = $request->route()->getAction();

        if (isset($action['except'])) {
            return $action['except'] == \Auth::guard($guard)->user()->role->id;
        }

        return false;
    }

    protected function convertRequestDataHtmlSpecialEncode($request) {
        if($request->isMethod('POST') || $request->isMethod('PUT') || $request->isMethod('PATCH')) {                        
            $inputs = $request->toArray();
                    
            foreach($inputs as $key => $val) {
                if(is_string($val)) {
                    $input[$key] = htmlspecialchars(trim($val));
                }                
            }
            
            $request->merge($input);   
        }
    }
}
