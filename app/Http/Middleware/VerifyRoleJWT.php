<?php

namespace App\Http\Middleware;

use App\Entities\Users;
use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class VerifyRoleJWT
{
    public function handle($request, Closure $next, $role = '')
    {
        $token = $request->header('Authorization');
        $token = str_replace('Bearer ', null, $token);
        try {
            if (!$user = JWTAuth::toUser($token)) {
                throw new TokenInvalidException('Token Invalid.');
            }

            if (Users::whereHas('role', function ($query) use ($role) {
                    $query->where('roles.type', $role);
                })->where('id', $user->id)->count() == 0) {
                throw new Exception('This User Do Not Has Permission.', 403);
            }

            return $next($request);
        } catch (TokenInvalidException $exception) {

            return response()->json([
                "meta" => [
                    'success' => false,
                    'code' => 401,
                    'message' => 'Token is Invalid.'
                ]
            ], 401);
        } catch (TokenExpiredException $exception) {

            return response()->json([
                "meta" => [
                    'success' => false,
                    'code' => 401,
                    'message' => 'Token is Expired.'
                ]
            ], 401);
        } catch (Exception $exception) {

            return response()->json([
                "meta" => [
                    'success' => false,
                    'code' => 403,
                    'message' => 'This User Do Not Has Permission.'
                ]
            ], 403);
        }

    }
}