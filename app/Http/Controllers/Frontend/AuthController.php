<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 11/27/2018
 * Time: 5:11 PM
 */

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\Request;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

class AuthController extends Controller
{
    use DispatchesJobs, ValidatesRequests;

    private $guard;
    const BASE_URL = '/';

    public function __construct()
    {
        $this->guard = Auth::guard('frontend');
    }

    public function getLogin() {
        return view(
          'frontend.auth.login'
        );
    }
}