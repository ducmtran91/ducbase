<?php

namespace App\Http\Controllers\Frontend;

use App\Repositories\PostsRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Prettus\Repository\Criteria\RequestCriteria;

class CategoriesController extends Controller
{
    /**
     * @var PostsRepositories
     */
    protected $postsRepositories;

    /**
     * CategoriesController constructor.
     * @param PostsRepositories $postsRepositories
     */
    public function __construct(PostsRepositories $postsRepositories)
    {
        parent::__construct();
        $this->postsRepositories = $postsRepositories;
    }

    /**
     * @param $slug
     * @param Request $request
     * @return $this
     */
    public function index($slug, Request $request) {
        $posts = $this->postsRepositories->whereHas('categories', function($query) use ($slug){
            return $query->where('name', 'like', $slug);
        })->orderBy('position')
            ->paginate($request->get('limit', $this->perPage));
        return view('frontend.posts.category')->with([
            'posts' => $posts
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \Throwable
     */
    public function getAjaxPosts(Request $request) {
        $this->postsRepositories->pushCriteria(new RequestCriteria($request));
        $result = $this->postsRepositories->scopeQuery(function($query) use ($request) {
            return $query->where('category_id', $request->get('id'));
        })->orderBy('position')
            ->paginate($this->perPage);

        $html = '';
        if($request) {
            foreach ($result as $item) {
                $html .= view('frontend.posts.item_post', ['item' => $item])->render();
            }
        }
        return $this->sendResponse([
            'html' => $html,
            'page' => (int)$request->get('page') + 1
        ], true);
    }
}
