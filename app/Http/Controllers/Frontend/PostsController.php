<?php

namespace App\Http\Controllers\Frontend;

use App\Repositories\PostsRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostsController extends Controller
{
    protected $postsRepositories;
    public function __construct(PostsRepositories $postsRepositories)
    {
        parent::__construct();
        $this->postsRepositories = $postsRepositories;
    }

    public function detail($slugCategory, $slug) {
        $post = $this->postsRepositories->whereHas('categories', function($query) use($slugCategory){
            $query->where('slug', 'like', "%{$slugCategory}%");
        })->findWhere([['slug', 'like', "%{$slug}%"]])->first();
        return view('frontend.posts.post')->with(['post' => $post]);
    }
}
