<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUsersAPIRequest;
use App\Http\Requests\API\UpdateUsersAPIRequest;
use App\Entities\Users;
use App\Repositories\UsersRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class UsersController
 * @package App\Http\Controllers\API
 */

class UsersAPIController extends AppBaseController
{
    /** @var  UsersRepository */
    private $usersRepository;

    public function __construct(UsersRepository $usersRepo)
    {
        $this->usersRepository = $usersRepo;
    }

    /**
     * @api {get} /users Pagination List
     * @apiGroup Users
     * @apiName List
     * @apiDescription Display a listing of the Users.
     *
     * @apiPermission api
     * @apiPermission jwt.auth
     *
     * @apiHeader {string} Authorization Authorization Header
     *
     * @apiParam {Request} request
     *
     * @apiSuccess {int} id Id of Users
     * @apiSuccess {date} created_at Created at of Users
     * @apiSuccess {date} updated_at Updated at of Users
     *
     * @apiSuccessExample {json} Success-Response:
     * {
     *      "meta": {
     *         "success": true,
     *         "message": "success",
     *         "code": 200
     *      },
     *      "response": [
     *          "total": 33,
     *          "per_page": 5,
     *          "current_page": 1,
     *          "last_page": 7,
     *          "next_page_url": "api/v1/users?page=2",
     *          "prev_page_url": null,
     *          "from": 1,
     *          "to": 5,
     *          "data": [
     *              {
     *                  "id": 1,
     *                  "created_at": "2018-05-16 14:32:15",
     *                  "updated_at": "2018-05-16 14:32:15"
     *              },
     *              {
     *                  "id": 2,
     *                  "created_at": "2018-05-16 14:32:15",
     *                  "updated_at": "2018-05-16 14:32:15"
     *              }
     *          ]
     * }
     * @apiError Error Get list failure
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 500 GetErrorFailure
     * {
     *    "meta": {
     *        "success": false,
     *        "message": "failure",
     *        "code": 401,
     *        "errors": null
     *    },
     *        "response": null
     * }
     *
     */
    public function index(Request $request)
    {
        $this->usersRepository->pushCriteria(new RequestCriteria($request));
        $this->usersRepository->pushCriteria(new LimitOffsetCriteria($request));
        $users = $this->usersRepository->all();

        return $this->sendResponse($users->toArray(), 'Users retrieved successfully');
    }

    /**
      * @api {post} /users Store
      * @apiGroup Users
      * @apiName Store
      * @apiDescription Store a newly created Users in storage.
      *
      * @apiPermission api
      * @apiPermission jwt.auth
      *
      * @apiHeader {string} Authorization Authorization Header
      *
      * @apiParam {CreateUsersAPIRequest} request
      *
      * @apiSuccess {int} id Id of Users
      * @apiSuccess {date} created_at Created at of Users
      * @apiSuccess {date} updated_at Updated at of Users
      *
      * @apiSuccessExample {json} Success-Response:
      * {
      *      "meta": {
      *         "success": true,
      *         "message": "success",
      *         "code": 200
      *      },
      *      "response": {
      *          "id": 1,
      *          "created_at": "2018-05-16 14:32:15",
      *          "updated_at": "2018-05-16 14:32:15"
      *      }
      * }
      * @apiError Error Get list failure
      * @apiErrorExample {json} Error-Response:
      * HTTP/1.1 500 GetErrorFailure
      * {
      *    "meta": {
      *        "success": false,
      *        "message": "failure",
      *        "code": 401,
      *        "errors": null
      *    },
      *        "response": null
      * }
      *
      */
    public function store(CreateUsersAPIRequest $request)
    {
        $input = $request->all();

        $users = $this->usersRepository->create($input);

        return $this->sendResponse($users->toArray(), 'Users saved successfully');
    }

    /**
      * @api {get} /users/:id Show
      * @apiGroup Users
      * @apiName Show
      * @apiDescription Display the specified Users.
      *
      * @apiPermission api
      * @apiPermission jwt.auth
      *
      * @apiHeader {string} Authorization Authorization Header
      *
      * @apiParam {int} id
      * @apiParam {Request} request
      *
      * @apiSuccess {int} id Id of Users
      * @apiSuccess {date} created_at Created at of Users
      * @apiSuccess {date} updated_at Updated at of Users
      *
      * @apiSuccessExample {json} Success-Response:
      * {
      *      "meta": {
      *         "success": true,
      *         "message": "success",
      *         "code": 200
      *      },
      *      "response": {
      *          "id": 1,
      *          "created_at": "2018-05-16 14:32:15",
      *          "updated_at": "2018-05-16 14:32:15"
      *      }
      * }
      * @apiError Error Get list failure
      * @apiErrorExample {json} Error-Response:
      * HTTP/1.1 500 GetErrorFailure
      * {
      *    "meta": {
      *        "success": false,
      *        "message": "failure",
      *        "code": 401,
      *        "errors": null
      *    },
      *        "response": null
      * }
      *
      */
    public function show($id)
    {
        /** @var Users $users */
        $users = $this->usersRepository->findWithoutFail($id);

        if (empty($users)) {
            return $this->sendError('Users not found');
        }

        return $this->sendResponse($users->toArray(), 'Users retrieved successfully');
    }

    /**
     * @api {put} /users/:id Update
     * @apiGroup Users
     * @apiName Update
     * @apiDescription Update the specified Users in storage.
     *
     * @apiPermission api
     * @apiPermission jwt.auth
     *
     * @apiHeader {string} Authorization Authorization Header
     *
     * @apiParam {UpdateUsersAPIRequest} request
     *
     * @apiSuccess {int} id Id of Users
     * @apiSuccess {date} created_at Created at of Users
     * @apiSuccess {date} updated_at Updated at of Users
     *
     * @apiSuccessExample {json} Success-Response:
     * {
     *      "meta": {
     *         "success": true,
     *         "message": "success",
     *         "code": 200
     *      },
     *      "response": {
     *          "id": 1,
     *          "created_at": "2018-05-16 14:32:15",
     *          "updated_at": "2018-05-16 14:32:15"
     *      }
     * }
     * @apiError Error Get list failure
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 500 GetErrorFailure
     * {
     *    "meta": {
     *        "success": false,
     *        "message": "failure",
     *        "code": 401,
     *        "errors": null
     *    },
     *        "response": null
     * }
     *
     */
    public function update($id, UpdateUsersAPIRequest $request)
    {
        $input = $request->all();

        /** @var Users $users */
        $users = $this->usersRepository->findWithoutFail($id);

        if (empty($users)) {
            return $this->sendError('Users not found');
        }

        $users = $this->usersRepository->update($input, $id);

        return $this->sendResponse($users->toArray(), 'Users updated successfully');
    }

       /**
     * @api {delete} /users/:id Delete
     * @apiGroup Users
     * @apiName Delete
     * @apiDescription Remove the specified Users from storage.
     *
     * @apiPermission api
     * @apiPermission jwt.auth
     *
     * @apiHeader {string} Authorization Authorization Header
     *
     * @apiParam {int} id
     * @apiParam {UpdateUsersAPIRequest} request
     *
     * @apiSuccess {int} id Id of Users
     * @apiSuccess {date} created_at Created at of Users
     * @apiSuccess {date} updated_at Updated at of Users
     *
     * @apiSuccessExample {json} Success-Response:
     * {
     *      "meta": {
     *         "success": true,
     *         "message": "success",
     *         "code": 200
     *      },
     *      "response": {
     *          "id": 1,
     *          "created_at": "2018-05-16 14:32:15",
     *          "updated_at": "2018-05-16 14:32:15"
     *      }
     * }
     * @apiError Error Get list failure
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 500 GetErrorFailure
     * {
     *    "meta": {
     *        "success": false,
     *        "message": "failure",
     *        "code": 401,
     *        "errors": null
     *    },
     *        "response": null
     * }
     *
     */
    public function destroy($id)
    {
        /** @var Users $users */
        $users = $this->usersRepository->findWithoutFail($id);

        if (empty($users)) {
            return $this->sendError('Users not found');
        }

        $users->delete();

        return $this->sendResponse($id, 'Users deleted successfully');
    }
}
