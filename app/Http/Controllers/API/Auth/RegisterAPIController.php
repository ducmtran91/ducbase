<?php

namespace App\Http\Controllers\API\Auth;

use App\Facades\QSoftVN;
use App\Http\Controllers\AppBaseController;
use App\Repositories\RolesRepository;
use App\Repositories\TaxRepository;
use App\Repositories\ZipCodeRepository;
use Illuminate\Http\Request;
use App\Repositories\UsersRepository;
use App\Services\Common;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Validator, Exception;
use DB, Hash;

class RegisterAPIController extends AppBaseController
{
    use ResetsPasswords;

    private $userRepository;
    private $roleRepository;
    private $taxRepository;
    /**
     * The attribute tax repository.
     *
     * @var $taxRepository
     */
    private $zipCodeRepository;
    protected $commonService;
    private $photoURL;

    public function __construct(
        Common $common,
        UsersRepository $userRepository,
        RolesRepository $rolesRepository,
        TaxRepository $taxRepository,
        ZipCodeRepository $zipCodeRepository
    )
    {
        $this->commonService = $common;
        $this->userRepository = $userRepository;
        $this->roleRepository = $rolesRepository;
        $this->taxRepository = $taxRepository;
        $this->zipCodeRepository = $zipCodeRepository;
        $this->photoURL = config('app.url') . '/uploads/users/images';
    }

    /**
     * @api {post} /auth/register/owner OwnerRegister
     * @apiGroup Auth
     * @apiName OwnerRegister
     * @apiDescription Store a newly registed User in storage when register.
     *
     * @apiPermission guest
     *
     * @apiParam {Request} request
     * @apiParam (Request Body) {string} first_name First name of User
     * @apiParam (Request Body) {string} last_name Last name of User
     * @apiParam (Request Body) {string} email Email of User
     * @apiParam (Request Body) {string} password Password
     * @apiParam (Request Body) {string} password_confirmation Password confirmation
     *
     * @apiSuccess {int} id Id of user
     * @apiSuccess {int} role_id Role id of user
     * @apiSuccess {string} name Name of user
     * @apiSuccess {string} email Email of user
     * @apiSuccess {string} description Description of user
     * @apiSuccess {int} is_admin User is admin
     * @apiSuccess {string} api_token Token login of user
     * @apiSuccess {string} phone Phone of user
     * @apiSuccess {bool} active Active of user
     * @apiSuccess {string} first_name First name of user
     * @apiSuccess {string} last_name Last name of user
     * @apiSuccess {string} username Username of user
     * @apiSuccess {date} birthday Date of user
     * @apiSuccess {bool} gender Gender of user
     * @apiSuccess {string} avatar Avatar of user
     * @apiSuccess {string} address Address of user
     * @apiSuccess {string} fb_id Facebook Id social when user login
     * @apiSuccess {string} fb_token Facebook token social when user login
     * @apiSuccess {string} gg_id Google id social when user login
     * @apiSuccess {string} gg_token Google token social when user login
     * @apiSuccess {string} tw_id Twitter id social when user login
     * @apiSuccess {string} tw_token Twitter token social when user login
     * @apiSuccess {string} photo Photo of user
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "meta": {
     *              "success": true,
     *              "message": "user_register_success",
     *              "code": 200
     *          },
     *          "response": {
     *              "id": 1,
     *              "role_id": 1,
     *              "name": "Administrator",
     *              "email": "panna@fantasy-games.be",
     *              "description": "",
     *              "is_admin": 1,
     *              "api_token": "ayJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6Ly9wYW42ODkuZHYvYXBpL3YxL2xvZ2luIiwiaWF0IjoxNTI3NzQxNzU3LCJleHAiOjE1MzAzMzM3NTcsIm5iZiI6MTUyNzc0MTc1NywianRpIjoieHlwcEJIcjdyczBKRFlzNyJ9.oyEaB0mtTYeRQ1_6VVkJUTFOkD3HsV0Uf4fMwxreg8g",
     *              "phone": null,
     *              "active": 1,
     *              "first_name": "",
     *              "last_name": "",
     *              "username": "",
     *              "birthday": "0000-00-00",
     *              "gender": 0,
     *              "avatar": "http://pan689.dv/themes//images/avatar-default.png",
     *              "address": "",
     *              "fb_id": "",
     *              "fb_token": "",
     *              "gg_id": "",
     *              "gg_token": "",
     *              "tw_id": "",
     *              "tw_token": "",
     *              "photo": null,
     *              "created_at": "2018-05-11 10:38:48",
     *              "updated_at": "2018-05-11 10:38:48",
     *              "deleted_at": null,
     *          }
     *      }
     *
     * @apiError RegisterFailure Register failure
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 401 RegisterInvalid
     * {
     *    "meta": {
     *        "success": false,
     *        "message": "user_register_failure",
     *        "code": 401,
     *        "errors": null
     *    },
     *        "response": null
     * }
     */
    public function ownerRegister(Request $request)
    {
        $locale = $request->get('locale', app()->getLocale());
        $credentials = $request->only('first_name', 'last_name', 'state_id', 'city_id', 'zip_code', 'email', 'password', 'password_confirmation');
        $validator = Validator::make($credentials, [
            'first_name' => 'required|max:40',
            'last_name'  => 'required|max:40',
            'state_id'   => 'required|exists:state,id',
            'city_id'    => 'exists:city,id',
            'zip_code'   => 'required|exists:zip_code,zip_code',
            'email'      => 'required|max:255|unique:users',
            'password'   => 'required|min:6'
        ], [
            'required'   => trans('auth.register.validation.required'),
            'unique'     => trans('auth.register.validation.unique'),
            'confirmed'  => trans('auth.register.validation.confirmed'),
            'max'        => trans('auth.register.validation.max'),
            'min'        => trans('auth.register.validation.min'),
            'exists'     => trans('auth.register.validation.exists'),
        ], [
            'state_id'   => 'State',
            'city_id'    => 'City',
        ]);

        if ($validator->fails()) {
            return $this->responseAppError(trans('validation.register.failure'), 401, $validator->errors()->all());
        }

        DB::beginTransaction();
        try {
            $userModel = $this->userRepository->model();
            $inputCheck = [
                'email' => $credentials['email'],
            ];

            if ($user = $this->userRepository->firstWhere($inputCheck)) {
                return $this->responseAppError(trans('validation.register.unique'), 400, []);
            }

            //
            if (!$tax = $this->taxRepository->firstWhere(['state_id' => $credentials['state_id']])) {
                return $this->responseAppError(trans('state.show.404'), 400, []);
            }

            if (!$role = $this->roleRepository->getByType($userModel::ROLE_OWNER)->first()) {
                return $this->responseAppError(trans('role.show.404'), 400, []);
            }

            $zipCode = $this->zipCodeRepository->firstWhere(['zip_code' => $credentials['zip_code']]);
            $input['email'] = $credentials['email'];
            $input['first_name'] = $credentials['first_name'];
            $input['last_name'] = $credentials['last_name'];
            $input['password'] = Hash::make($credentials['password']);
            $input['api_token'] = Hash::make(time());
            $input['role_id'] = $role->id;
            $input['role_check'] = $userModel::IS_YES;
            $input['status'] = $userModel::IS_YES;
            $input['tax_id'] = $tax['id'];
            $input['state_id'] = $credentials['state_id'];
            $input['city_id'] = $credentials['city_id'];
            $input['zip_code'] = $zipCode['id'];
            $name = $input['name'] = "{$credentials['first_name']} {$credentials['last_name']}";

            if (!$user = $this->userRepository->create($input)) {
                throw new Exception(trans('auth.register.failure'));
            }

            // Send Email
            $link = url('/') . '/confirm?key=' . $input['api_token'] . '&locale=' . $locale;
            $subject = trans('auth.email.register.subject', ['name' => $name]);
            $content = trans('auth.email.register.content', ['name' => $name, 'link' => $link]);
            QSoftVN::sendEmail([
                'subject' => $subject,
                'email' => $credentials['email'],
                'content' => $content
            ], 'admin.email_template.auth.register.successfully');
            DB::commit();

            return $this->responseApp(true, 200, trans('auth.register.successfully'), $user);
        } catch (Exception $exception) {
            DB::rollback();

            return $this->responseAppError(trans('auth.register.failure'), 500);
        }
    }


    public function walkerRegister(Request $request)
    {
    }
}