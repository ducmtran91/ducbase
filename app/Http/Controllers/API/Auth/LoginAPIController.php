<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\AppBaseController;
use App\Repositories\UsersRepository;
use Illuminate\Http\Request;
use JWTAuth, JWTAuthException;
use Validator;
use Exception;

class LoginAPIController extends AppBaseController
{

    /**
     * The attribute user repository.
     *
     * @var $userRepository
     */
    private $userRepository;

    /**
     * The attribute user model.
     *
     * @var $userModel
     */
    private $userModel;

    /**
     * The attribute guard.
     *
     * @var $guard
     */
    protected $guard = 'api';

    /**
     * Create new instance Controller.
     *
     * @return void
     */
    public function __construct(UsersRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->userModel = $this->userRepository->model();
    }

    /**
     * @api {post} /auth/login Login via email
     * @apiGroup Auth
     * @apiName LoginEmail
     * @apiDescription Login via email & password.
     *
     * @apiPermission guest
     *
     * @apiParam {Request} request
     * @apiParam (Request Body) {string} email Email of User
     * @apiParam (Request Body) {string} password Password of User
     *
     * @apiSuccess {int} id Id of user
     * @apiSuccess {int} role_id Role id of user
     * @apiSuccess {string} name Name of user
     * @apiSuccess {string} email Email of user
     * @apiSuccess {string} description Description of user
     * @apiSuccess {int} is_admin User is admin
     * @apiSuccess {string} api_token Token login of user
     * @apiSuccess {string} phone Phone of user
     * @apiSuccess {bool} active Active of user
     * @apiSuccess {string} first_name First name of user
     * @apiSuccess {string} last_name Last name of user
     * @apiSuccess {string} username Username of user
     * @apiSuccess {date} birthday Date of user
     * @apiSuccess {bool} gender Gender of user
     * @apiSuccess {string} avatar Avatar of user
     * @apiSuccess {string} address Address of user
     * @apiSuccess {string} fb_id Facebook Id social when user login
     * @apiSuccess {string} fb_token Facebook token social when user login
     * @apiSuccess {string} gg_id Google id social when user login
     * @apiSuccess {string} gg_token Google token social when user login
     * @apiSuccess {string} tw_id Twitter id social when user login
     * @apiSuccess {string} tw_token Twitter token social when user login
     * @apiSuccess {string} photo Photo of user
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "meta": {
     *              "success": true,
     *              "message": "user_login_success",
     *              "code": 200
     *          },
     *          "response": {
     *              "id": 1,
     *              "role_id": 1,
     *              "name": "Administrator",
     *              "email": "panna@fantasy-games.be",
     *              "description": "",
     *              "is_admin": 1,
     *              "api_token": "ayJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6Ly9wYW42ODkuZHYvYXBpL3YxL2xvZ2luIiwiaWF0IjoxNTI3NzQxNzU3LCJleHAiOjE1MzAzMzM3NTcsIm5iZiI6MTUyNzc0MTc1NywianRpIjoieHlwcEJIcjdyczBKRFlzNyJ9.oyEaB0mtTYeRQ1_6VVkJUTFOkD3HsV0Uf4fMwxreg8g",
     *              "phone": null,
     *              "active": 1,
     *              "first_name": "",
     *              "last_name": "",
     *              "username": "",
     *              "birthday": "0000-00-00",
     *              "gender": 0,
     *              "avatar": "http://pes92.dev/themes//images/avatar-default.png",
     *              "address": "",
     *              "fb_id": "",
     *              "fb_token": "",
     *              "gg_id": "",
     *              "gg_token": "",
     *              "tw_id": "",
     *              "tw_token": "",
     *              "photo": null,
     *              "created_at": "2018-05-11 10:38:48",
     *              "updated_at": "2018-05-11 10:38:48",
     *              "deleted_at": null,
     *          }
     *      }
     *
     * @apiError LoginFailure Login failure
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 401 LoginInvalid
     * {
     *    "meta": {
     *        "success": false,
     *        "message": "login_invalid_credentials",
     *        "code": 401,
     *        "errors": null
     *    },
     *        "response": null
     * }
     */
    public function login(Request $request)
    {
        $params = $request->only('email', 'password', 'role');
        $credentials = $request->only('email', 'password');

        $validator = Validator::make($params, [
            'role' => 'required|in:0,1',
            'email' => 'required|email',
            'password' => 'required'
        ], [
            'required' => [
                'role' => trans('validation.required'),
                'email' => trans('auth.walker.validation.login.email.required')
                ],
        ]);

        if ($validator->fails()) {
            return $this->responseAppError(trans('auth.login.validation'), 401, $validator->errors()->all());
        }

        $credentials = array_merge($credentials, [
            'status' => ($this->userModel)::IS_YES,
        ]);

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->responseAppError(trans('auth.login.validation'), 401);
            }

            $roleCfg = config('common.role');
            $role = $roleCfg[$params['role']];
            if (!$user = $this->userRepository
                ->whereHas('role', function ($query) use ($role) {
                    $query->where('roles.type', $role);
                })->firstWhere([
                    'email' => $credentials['email'],
                    'status' => ($this->userModel)::IS_YES,
                ])) {
                return $this->responseAppError(trans('auth.login.403'), 403);
            }
            $user->api_token = $token;
            if (!$user->save()) {
                throw new Exception(trans('auth.login.failure'));
            }

            return $this->responseAppSuccess(trans('auth.login.successfully'), 200, $user);
        } catch (JWTAuthException $exception) {

            return $this->responseAppError(trans('auth.login.token.failure'), $exception->getStatusCode());
        } catch (Exception $exception) {

            return $this->responseAppError(trans('auth.login.failure'), 500);
        }
    }
}