<?php

namespace App\Http\Controllers\API\Auth;

use App\Exceptions\PermissionException;
use App\Http\Controllers\AppBaseController;
use App\Repositories\RolesRepository;
use App\Repositories\TaxRepository;
use App\Repositories\ZipCodeRepository;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Repositories\UsersRepository;
use Exception, Validator;
use Illuminate\Validation\ValidationException;
use Laravel\Socialite\Facades\Socialite;
use JWTAuth, JWTAuthException;
use Hash;

class LoginSocialAPIController extends AppBaseController
{
    use AuthenticatesUsers;
    /**
     * The attribute user repository.
     *
     * @var $userRepository
     */
    private $userRepository;

    /**
     * The attribute role repository.
     *
     * @var $roleRepository
     */
    private $roleRepository;
    /**
     * The attribute tax repository.
     *
     * @var $taxRepository
     */
    private $taxRepository;

    /**
     * The attribute tax repository.
     *
     * @var $taxRepository
     */
    private $zipCodeRepository;

    protected $guard = 'api';

    /**
     * Create new instance Controller.
     *
     * @return void
     */
    public function __construct(
        UsersRepository $userRepository,
        RolesRepository $rolesRepository,
        TaxRepository $taxRepository,
        ZipCodeRepository $zipCodeRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $rolesRepository;
        $this->taxRepository = $taxRepository;
        $this->zipCodeRepository = $zipCodeRepository;
    }

    /**
     * @api {post} /auth/social Login via social
     * @apiGroup Auth
     * @apiName LoginSocial
     * @apiDescription Login via social.
     *
     * @apiPermission guest
     *
     * @apiParam {Request} request
     * @apiParam (Request Body) {string} provider Social provider as facebook/google/twitter
     * @apiParam (Request Body) {string} access_token User social access token
     * @apiParam (Request Body) {string} secret_key User social secret key (only for twitter)
     *
     * @apiSuccess {int} id Id of user
     * @apiSuccess {int} role_id Role id of user
     * @apiSuccess {string} name Name of user
     * @apiSuccess {string} email Email of user
     * @apiSuccess {string} description Description of user
     * @apiSuccess {int} is_admin User is admin
     * @apiSuccess {string} api_token Token login of user
     * @apiSuccess {string} phone Phone of user
     * @apiSuccess {bool} active Active of user
     * @apiSuccess {string} first_name First name of user
     * @apiSuccess {string} last_name Last name of user
     * @apiSuccess {string} username Username of user
     * @apiSuccess {date} birthday Date of user
     * @apiSuccess {bool} gender Gender of user
     * @apiSuccess {string} avatar Avatar of user
     * @apiSuccess {string} address Address of user
     * @apiSuccess {string} fb_id Facebook Id social when user login
     * @apiSuccess {string} fb_token Facebook token social when user login
     * @apiSuccess {string} gg_id Google id social when user login
     * @apiSuccess {string} gg_token Google token social when user login
     * @apiSuccess {string} tw_id Twitter id social when user login
     * @apiSuccess {string} tw_token Twitter token social when user login
     * @apiSuccess {string} photo Photo of user
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "meta": {
     *              "success": true,
     *              "message": "user_login_success",
     *              "code": 200
     *          },
     *          "response": {
     *              "id": 1,
     *              "role_id": 1,
     *              "name": "Administrator",
     *              "email": "panna@fantasy-games.be",
     *              "description": "",
     *              "is_admin": 1,
     *              "api_token": "ayJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6Ly9wYW42ODkuZHYvYXBpL3YxL2xvZ2luIiwiaWF0IjoxNTI3NzQxNzU3LCJleHAiOjE1MzAzMzM3NTcsIm5iZiI6MTUyNzc0MTc1NywianRpIjoieHlwcEJIcjdyczBKRFlzNyJ9.oyEaB0mtTYeRQ1_6VVkJUTFOkD3HsV0Uf4fMwxreg8g",
     *              "phone": null,
     *              "active": 1,
     *              "first_name": "",
     *              "last_name": "",
     *              "username": "",
     *              "birthday": "0000-00-00",
     *              "gender": 0,
     *              "avatar": "http://pes92.dev/themes//images/avatar-default.png",
     *              "address": "",
     *              "fb_id": "",
     *              "fb_token": "",
     *              "gg_id": "",
     *              "gg_token": "",
     *              "tw_id": "",
     *              "tw_token": "",
     *              "photo": null,
     *              "created_at": "2018-05-11 10:38:48",
     *              "updated_at": "2018-05-11 10:38:48",
     *              "deleted_at": null,
     *          }
     *      }
     *
     * @apiError LoginFailure Login failure
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 401 LoginInvalid
     * {
     *    "meta": {
     *        "success": false,
     *        "message": "login_invalid_credentials",
     *        "code": 401,
     *        "errors": null
     *    },
     *        "response": null
     * }
     */
    public function onLogin(Request $request)
    {
        try {
            $input = $request->only('provider', 'access_token', 'secret_key', 'state_id', 'zip_code');
            $validator = Validator::make($input, [
                'provider' => 'required|in:facebook,google,twitter',
                'secret_key' => 'required_if:provider,twitter',
                'access_token' => 'required',
            ], [
                'required' => trans('auth.register.validation.required'),
                'in' => trans('auth.register.validation.in'),
                'min' => trans('auth.register.validation.min'),
                'exists' => trans('auth.register.validation.exists'),
            ]);

            if ($validator->fails()) {
                return $this->responseAppError(trans('auth.login.validation'), 401, $validator->errors()->all());
            }

            if (!$role = $this->roleRepository->getByType('owner')->first()) {
                return $this->responseAppError(trans('role.show.404'), 400);
            }

            switch ($input['provider']) {
                case 'twitter':
                case 'bitbucket':
                    $socialUser = Socialite::driver($input['provider'])
                        ->userFromTokenAndSecret($input['access_token'], $input['secret_key']);
                    break;
                default:
                    $socialUser = Socialite::driver($input['provider'])
                        ->userFromToken($input['access_token']);
                    break;
            }

            $user = $this->findOrCreateUser($socialUser, array_merge($input, ['role_id' => $role->id]));

            if (!$token = JWTAuth::fromUser($user)) {
                return $this->responseAppError(trans('auth.login.validation'), 401);
            }

            $user->api_token = $token;
            if (!$user->save()) {
                throw new Exception(trans('auth.login.failure'));
            }

            return $this->responseApp(true, 200, 'done', $user);
        } catch (JWTAuthException $exception) {

            return $this->responseAppError(trans('auth.login.token.failure'), $exception->getStatusCode());
        } catch (ValidationException $exception) {

            return $this->responseAppError(trans('auth.login.validation'), 400);
        } catch (PermissionException $exception) {

            return $this->responseAppError(trans('auth.login.403'), 403);
        } catch (Exception $exception) {

            return $this->responseAppError(trans('auth.login.failure'), 500);
        }
    }

    /**
     * If a user has registerd before using social auth, return the user
     * Else, create a new user object.
     *
     * @param Socialite $user Socialite user object
     * @param string $provider Socialite auth provider
     * @reuturn User
     */
    private function findOrCreateUser($user, $input)
    {
        try {
            $userModel = $this->userRepository->model();
            $socialName = $user->getName();
            $socialEmail = is_null($user->getEmail()) ? $user->getId() . "@{$input['provider']}.com" : $user->getEmail();
            $socialPhoto = $user->getAvatar();
            $socialGender = (isset($user->user['gender']) && $user->user['gender'] == 'male') ? $userModel::IS_YES : $userModel::IS_NO;
            $params = [
                'first_name' => $socialName,
                'last_name' => '',
                'name' => $socialName,
                'email' => $socialEmail,
                'avatar' => $socialPhoto,
                'gender' => $socialGender,
                'api_token' => Hash::make(time()),
                'role_id' => $input['role_id'] ?: null,
                'status' => $userModel::IS_YES,
            ];

            $validator = Validator::make($params, [
                'email' => 'required',
                'gender' => 'required',
                'api_token' => 'required',
                'role_id' => 'required',
                'status' => 'required',
            ]);
            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
            $defAttributes = [
                'email' => $socialEmail,
                'status' => $userModel::IS_YES,
                'provider' => $input['provider']
            ];
            switch ($input['provider']) {
                case 'facebook':
                    $attributes = array_merge($defAttributes, [
                        'fb_id' => $user->getId(),
                        'fb_token' => $user->token
                    ]);
                    break;
                case 'google':
                    $attributes = array_merge($defAttributes, [
                        'gg_id' => $user->getId(),
                        'gg_token' => $user->token
                    ]);
                    break;
                case 'twitter':
                    $attributes = array_merge($defAttributes, [
                        'tw_id' => $user->getId(),
                        'tw_token' => $user->token
                    ]);
                    break;
            }

            $model = app()->make($userModel);
            if ($instance = $model->where(['email' => $socialEmail])->first()) {
                if ($instance->where(['email' => $socialEmail])
                        ->whereHas('role', function ($query) {
                        $query->where('roles.type', '!=', 'owner');
                    })->count() > 0) {

                    throw new PermissionException(trans('auth.login.403'));
                }
                $instance->fill($attributes)->save();
            } else {
                $instance = clone $model;
                $instance->fill(array_merge($attributes, $params))->save();
            }

            return $instance;
        } catch (ValidationException $exception) {

            throw $exception;
        } catch (Exception $exception) {

            throw $exception;
        }
    }

}