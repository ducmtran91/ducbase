<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use JWTAuth, JWTAuthException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Exception;

class LogoutAPIController extends AppBaseController
{

    /**
     * @api {get} /auth/logout Logout
     * @apiGroup Auth
     * @apiName LogoutEmail
     * @apiDescription Logout.
     *
     * @apiPermission jwt.auth
     *
     * @apiParam {Request} request
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "meta": {
     *              "success": true,
     *              "message": "auth.logout.successfully",
     *              "code": 200
     *          },
     *          "response": {}
     *      }
     *
     * @apiError LoginFailure Login failure
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 500 LoginFailure
     * {
     *    "meta": {
     *        "success": false,
     *        "message": "auth.logout.failure",
     *        "code": 500,
     *        "errors": null
     *    },
     *        "response": null
     * }
     */
    public function logout(Request $request)
    {
        try {
            $token = $request->header('Authorization');
            $token = str_replace('Bearer ', null, $token);

            JWTAuth::invalidate($token);

            return $this->responseAppSuccess(trans('auth.logout.successfully'), 200, []);
        } catch (TokenInvalidException $exception) {

            return $this->responseAppError(trans('auth.logout.token_invalid'), 500);
        } catch (TokenExpiredException $exception) {

            return $this->responseAppError(trans('auth.logout.token_expired'), 500);
        } catch (JWTException  $exception) {

            return $this->responseAppError(trans('auth.logout.failure'), 500);
        } catch (Exception $exception) {

            return $this->responseAppError(trans('auth.logout.failure'), 500);
        }
    }
}