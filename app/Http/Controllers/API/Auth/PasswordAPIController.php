<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\AppBaseController;
use App\Repositories\UsersRepository;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use JWTAuth, JWTAuthException;
use Hash, Validator;

class PasswordAPIController extends AppBaseController
{

    use ResetsPasswords;

    /**
     * The attribute user repository.
     *
     * @var $userRepository
     */
    private $userRepository;

    /**
     * The attribute guard.
     *
     * @var string
     */
    protected $guard = 'api';

    /**
     * The attribute link request view for forgot password.
     *
     * @var string
     */
    protected $linkRequestView = 'auth.passwords.forgot';

    /**
     * The attribute reset view for reset password.
     *
     * @var string
     */
    protected $resetView = 'auth.passwords.reset';

    /**
     * Create new instance Controller.
     *
     * @return void
     */
    public function __construct(UsersRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @api {post} /auth/forgot Forgot Password
     * @apiGroup Auth
     * @apiName Forgot
     * @apiDescription Forgot password.
     *
     * @apiPermission guest
     *
     * @apiParam {Request} request
     * @apiParam (Request Body) {string} email Email of User
     *
     * @apiSuccess
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "meta": {
     *              "success": true,
     *              "message": "forgot_password_success",
     *              "code": 200
     *          },
     *          "response": {
     *
     *          }
     *      }
     *
     * @apiError ForgotFailure Forgot failure
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 401 LoginInvalid
     * {
     *    "meta": {
     *        "success": false,
     *        "message": "forgot_password_failure",
     *        "code": 401,
     *        "errors": null
     *    },
     *        "response": null
     * }
     */
    public function forgot(Request $request)
    {
        $credentials = $request->only('email');
        $validator = Validator::make($credentials, [
            'email' => 'required|email'
        ], [
            'required' => trans('validation.required'),
        ]);

        if ($validator->fails()) {
            return $this->responseAppError(trans('auth.forgot.validation'), 401, $validator->errors()->all());
        }

        try {
            if (!$user = $this->userRepository->firstWhere($credentials)) {
                return $this->responseAppError(trans('auth.forgot.404'), 404);
            }

            $password = $this->returnRandomString(10);
            $input['api_token'] = Hash::make($password);

            if (!$userUpdate = $this->userRepository->update($input, $user->id)) {
                return $this->responseAppError(trans('auth.forgot.failure'), 500);
            }

            $link = $this->sendResetLinkEmail($request);

            if (!$link) {
                return $this->responseAppError(trans('auth.forgot.send_email_failure'), 401);
            }

            return $this->responseApp(true, 200, 'auth.forgot.send_email_successfully');
        } catch (\Exception $e) {

            return $this->responseAppError(trans('auth.forgot.failure'), 500);
        }
    }
}