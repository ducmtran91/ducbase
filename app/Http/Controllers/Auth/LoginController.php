<?php

namespace App\Http\Controllers\Auth;

use App\Facades\Authorize;
use App\Facades\Theme;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Two\InvalidStateException;
use GuzzleHttp\Client;
use Socialite;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $redirectDeny = '/login';

    protected $redirectAfterLogout = '/';

    protected $loginView = 'auth.login';

    protected $guard = 'member';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        $themeActive = Theme::active();
        $this->loginView = $themeActive . '.' . $this->loginView;

        $this->redirectTo = route('front');
        $this->redirectDeny = route('auth.login.form');
        $this->redirectAfterLogout = route('front');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        // If count login failed > limit login failed
        // Send errors
        if (Authorize::verifyLoginFailed()) {
            $verify = Authorize::verifyReCaptcha($request->get('g-recaptcha-response'));
            if (!$verify['success']) {
                return $this->sendVerifyFailedLoginResponse($request);
            }
        }
        if ($this->attemptLogin($request)) {
            Authorize::resetLoginFailed();
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        Authorize::setLoginFailed();

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        // If count login failed > limit login failed
        // Send errors
        if (Authorize::verifyLoginFailed()) {
            $verify = Authorize::verifyReCaptcha($request->get('g-recaptcha-response'));
            if (!$verify['success']) {
                return $this->sendVerifyFailedLoginResponse($request);
            }
        }

        $validator = Validator::make($request->all(), [
            $this->username() => 'required', 'password' => 'required',
        ]);

        if ($validator->fails()) {
            Authorize::setLoginFailed();
            $messages = $validator->errors()->getMessages();

            if (($request->ajax() && !$request->pjax()) || $request->wantsJson()) {
                return new JsonResponse($messages, 422);
            }

            return redirect()->to($this->getRedirectUrl())
                ->withInput($request->input())
                ->withErrors($messages, $this->errorBag());
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    protected function verifyLoginFailed()
    {
        return Authorize::verifyLoginFailed();
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        /**
         * Set Credential Value
         *
         * provider: local <Login by user register by website>
         * status: 1 <Account is activated>
         * role_check: 1 <Account is member>
         */
        $credentials = array_merge($this->getCredentials($request), ['provider' => 'local', 'status' => 1]);
        $remembers = $request->has('remember');

        return Auth::guard($this->getGuard())->attempt(
            $credentials, $remembers
        );
    }

    /**
     * Get the failed login response instance.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    protected function sendFailedLoginResponse(Request $request)
    {

        if ($request->ajax()) {
            return response()->json([
                'success' => false,
                'message' => 'userLoginFailure',
                'errors' => [
                    $this->loginUsername() => $this->getFailedLoginMessage()
                ],
            ], 500);
        }

        return redirect()->back()
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage()
            ]);
    }

    /**
     * Get the verify failed login response instance.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    protected function sendVerifyFailedLoginResponse(Request $request)
    {
        if ($request->ajax()) {
            return response()->json([
                'success' => false,
                'message' => 'userLoginFailure',
                'useReCaptcha' => true,
                'errors' => [
                    'loginfailed' => Authorize::getCountLoginFailedMessage()
                ],
            ], 500);
        }
        return redirect()->back()
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                'loginfailed' => Authorize::getCountLoginFailedMessage()
            ]);
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'message' => 'userLoginSuccess',
                'redirect' => property_exists($this, 'redirectTo') ? $this->redirectTo : '/',
                'data' => $user
            ]);
        }
        return redirect(property_exists($this, 'redirectTo') ? $this->redirectTo : '/');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'api_token' => str_random(60),
            'password' => Hash::make($data['password']),
        ]);
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($driver)
    {
        try {
            $social = Socialite::driver($driver);
        } catch (InvalidArgumentException $e) {
            abort(400, $e->getMessage());
        }

        $social->setHttpClient(
            new Client([
                'curl' => [
                    CURLOPT_CAINFO => base_path('resources/assets/pem/cacert.pem'),
                ],
            ])
        );

        return $social->redirect();

    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback($driver)
    {
        try {
            $user = Socialite::driver($driver)->stateless(false)->user();
            $authUser = $this->findOrCreateUser($user, $driver);
            Auth::guard($this->guard)->login($authUser);

            return $this->authenticated(request(), $authUser);
        } catch (\Exception $exception) {
            return redirect(property_exists($this, 'redirectDeny') ? $this->redirectDeny : '/login');
        }
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        try {
            $authUser = User::where('provider_id', $user->id)->first();
            if ($authUser) {
                return $authUser;
            }
            return User::create([
                'name' => $user->getName(),
                'email' => $user->getEmail(),
                'photo' => $user->getAvatar(),
                'provider' => $provider,
                'provider_id' => $user->id,
                'api_token' => Hash::make(time()),
                'role_id' => config('utils.default_role'),
                'status' => true
            ]);
        } catch (\Exception $exception) {
            throw $exception;
        }

    }
}
