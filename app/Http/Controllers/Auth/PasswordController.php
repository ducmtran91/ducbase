<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Facades\Theme;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $linkRequestView = 'auth.passwords.forgot-password';

    protected $resetView = 'auth.passwords.reset-password';

    protected $redirectPath = '/';

//    protected $subject = '';

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->linkRequestView = Theme::active().'.'.$this->linkRequestView;
        $this->resetView = Theme::active().'.'.$this->resetView;

        $this->redirectPath = route('front');
    }
}
