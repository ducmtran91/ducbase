<?php

namespace App\Http\Controllers\Auth;

use App\Entities\Users;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Admin, Theme, Mail;

class RegisterController extends Controller
{
    protected $redirectTo = '/register/success';

    protected $registerView = 'auth.register';

    protected $guard = 'member';

    use RegistersUsers;

    /**
     * Create new register controller instance.
     *
     * @return avoid
     */
    public function __construct()
    {
        $themeActive = Theme::active();
        $this->registerView = $themeActive . '.' . $this->registerView;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required', 'max:255',
            'email' => 'required|email|max:255|unique:users,email,NULL,id,provider,local',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required',
            'privacyTerms' => 'required'
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user = $this->create($request->all());

        // Send mail Confirm
        $this->sendEmail([
            'subject' => trans('emails.subject_register_user'),
            'email' => $request->get('email'),
            'content' => trans('emails.content_register', [
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => $request->get('password'),
                'link' => route('auth.register.confirm') . '?' . http_build_query(['token' => $user->api_token, 'language' => $request->get('local')])
            ])
        ]);

        $this->redirectTo = route('auth.register.success') . '?' . http_build_query(['token' => $user->api_token]);
        return redirect($this->redirectTo);
    }

    /**
     * Show form register success.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function success(Request $request)
    {
        if (!Users::where('api_token', $request->get('token'))->first()) {
            return redirect()->to(route('front'));
        }
        return view(Theme::blade('auth.register.success'));
    }

    /**
     * confirm account success
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function confirm(Request $request)
    {
        if (!$user = Users::where('api_token', $request->get('token'))->first()) {
            return redirect()->to(route('front'));
        }

        $user->status = 1;
        $success = $user->save();
        app()->setLocale($request->get('language'));

        return view(Theme::blade('auth.register.activate'), compact('success'));
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return Users::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'api_token' => str_random(60),
            'password' => Hash::make($data['password']),
            'status' => 0,
            'role_check' => 1,
        ]);
    }

    protected function sendEmail($input = [])
    {
        $default = [
            'trademark' => 'Kizzu',
            'subject' => '',
            'email' => '',
            'content' => ''
        ];
        $inputEmail = array_merge($default, $input);
        Mail::send(
            'admin.email_template.content_email',
            [
                'trademark' => 'Kizzu',
                'name' => $inputEmail["subject"],
                'email' => $inputEmail['email'],
                'content' => $inputEmail['content']
            ],
            function ($message) use ($inputEmail) {
                $message->from(env('MAIL_ADDRESS_SEND'), $inputEmail['trademark']);
                $message->to($inputEmail['email'], '')->subject($inputEmail['subject']);
            });
    }
}