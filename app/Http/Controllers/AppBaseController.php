<?php

namespace App\Http\Controllers;

use App\Response\AbstractResponse;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use InfyOm\Generator\Utils\ResponseUtil;
use Response,
    Auth,
    Mail,
    DB;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Zipper;
use Exception;
use JWTAuth;
use JWTAuthException;
use App\Services\Common;

/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Laravel Generator APIs",
 *     version="1.0.0",
 *   )
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends Controller {

    protected $perPage;
    protected $currentUser;
    protected $commonService;
    public $response;

    public function __construct() {
        $this->perPage = 10;
        $this->commonService = new Common();
        $this->response = new AbstractResponse();
        $this->middleware(function ($request, $next) {
            $this->currentUser = (Auth::guard('admin')->check()) ? Auth::guard('admin')->user() : null;

            return $next($request);
        });
    }

    public function sendResponse($result, $message) {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404) {
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    public function responseApp($success, $code, $message, $response = null) {
        return Response::json([
                    'meta' => [
                        'success' => $success,
                        'message' => $message,
                        'code' => $code
                    ],
                    'response' => $response
        ]);
    }

    public function responseAppSuccess($message, $code = 200, $response = null) {
        return Response::json([
                    'meta' => [
                        'success' => true,
                        'message' => $message,
                        'code' => $code
                    ],
                    'response' => $response
        ]);
    }

    public function responseAppError($message, $code = 404, $errors = null) {
        $resMessage = is_array($errors) ? $this->recursiveError($errors, '') : $message;
        return Response::json([
                    'meta' => [
                        'success' => false,
                        'message' => $resMessage,
                        'code' => $code,
                    // 'errors' => $errors
                    ],
                        // 'response' => []
        ]);
    }

    protected function recursiveError($errors, $out) {
        if (is_array($errors)) {
            if (isset($errors[0])) {
                $out = self::recursiveError($errors[0], $out);
            } else {
                $out = $errors;
            }
        } else {
            $out = $errors;
        }

        return $out;
    }

    public function returnRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function returnLanguage($request) {
        $arrayLanguage = array(
            'vi', 'en'
        );
        $language = $request->input('language');
        if (!in_array($language, $arrayLanguage)) {
            $language = 'vi';
        }
        app()->setLocale($language);
        return $language;
    }

    public function sendEmail($email, $subject = '', $content = '') {
        // send email
        $arrayEmail['subject'] = $subject;
        $arrayEmail['email'] = $email;
        $arrayEmail['editor'] = $content;
        $arrayEmail['trademark'] = 'PES';

        Mail::send('admin.email_template.content_email', array('name' => $arrayEmail['subject'], 'email' => $arrayEmail['email'], 'content' => $arrayEmail['editor']), function ($message) use ($arrayEmail) {
            $message->from('huonglm@qsoftvietnam.com', $arrayEmail['trademark']);
            $message->to($arrayEmail['email'], '')->subject($arrayEmail['subject']);
        });
    }

    public function returnDateFormatConvert($date) {
        if ($date == '') {
            $datesRemainder = '';
        } else {
            $datesRemainder = date("Y-m-d", strtotime($date));
        }
        return $datesRemainder;
    }

    public function returnDateTimeFormatConvert($date) {
        if ($date == '') {
            $datesRemainder = '';
        } else {
            $datesRemainder = date("Y-m-d H:i:s", strtotime($date));
        }
        return $datesRemainder;
    }

    function returnCheckLibraryId($id) {
        $library = DB::table('libraries')->where('id', $id)->first();
        if (empty($library)) {
            return 0;
        }
        return $library;
    }

    public function saveNewMode($input) {
        $saveNewMode = false;

        if (!empty($input['save_new'])) {
            unset($input['save_new']);
            $saveNewMode = true;
        }

        return $saveNewMode;
    }

    public function sortOrderProcess($model, $oldRecord, $dest, $field) {
        $current = $oldRecord->order;

        $minRecord = $model->orderBy($field, 'ASC')->first();
        $maxRecord = $model->orderBy($field, 'DESC')->first();

        $min = (!empty($minRecord)) ? $minRecord->order : 1;
        $max = (!empty($maxRecord)) ? $maxRecord->order : 1;

        $dest = ($dest > $max) ? $max : $dest;
        $dest = ($dest < $min) ? $min : $dest;

        if ($current != $dest) {
            if ($current < $dest) {
                $this->sortHigherThan($model, $current, $dest, $field);
            } else {
                $this->sortLessThan($model, $current, $dest, $field);
            }

            $oldRecord->order = $dest;
            $oldRecord->save();
        }
    }

    public function sortHigherThan($model, $current, $dest, $field) {
        $items = $model->where($field, '>', $current)->where($field, '<=', $dest)->orderBy($field, 'ASC')->get();

        if (!empty($items)) {
            foreach ($items as $item) {
                $item->order = $item->order - 1;
                $item->save();
            }
        }
    }

    public function sortLessThan($model, $current, $dest, $field) {
        $items = $model->where($field, '<', $current)->where($field, '>=', $dest)->orderBy($field, 'DESC')->get();

        if (!empty($items)) {
            foreach ($items as $item) {
                $item->order = $item->order + 1;
                $item->save();
            }
        }
    }

    public function getUserCurrent($request) {
        try {
            $token = $request->header('Authorization');
            $token = str_replace('Bearer ', null, $token);
            if (!$user = JWTAuth::toUser($token)) {
                throw new TokenInvalidException('Token Invalid.');
            }

            unset($user->api_token);

            return $user;
        } catch (TokenInvalidException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * return curl.
     *
     * @param collection $node
     * @return json
     */
    function returnCURL($param = array(), $link) {
        $res = '_error_';
        if (count($param) > 0 && $link) {
            $count_param = 1;
            $string_param = '';
            foreach ($param as $key => $row) {
                if ($count_param == 1) {
                    $string_param .= $key . "=" . $row;
                } else {
                    $string_param .= "&" . $key . "=" . $row;
                }
                $count_param++;
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $link);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $string_param);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $res = curl_exec($ch);
            curl_close($ch);
        }
        return $res;
    }

}
