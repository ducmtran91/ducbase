<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\CategoriesRepositories;
use App\Repositories\PostsRepositories;
use App\Http\Controllers\AppBaseController;
use App\Repositories\TagsRepositories;
use App\Validators\PostsValidator;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Validator\Exceptions\ValidatorException;

class PostsController extends AppBaseController
{

    /** @var  $repository */
    private $repository;
    private $model;
    private $statuses;
    private $validation;
    private $categoriesRepositories;
    private $tagsRepositories;

    public function __construct(
        PostsRepositories $repository,
        CategoriesRepositories $categoriesRepositories,
        TagsRepositories $tagsRepositories,
        PostsValidator $validation
    )
    {
        parent::__construct();
        $this->repository = $repository;
        $this->categoriesRepositories = $categoriesRepositories;
        $this->tagsRepositories = $tagsRepositories;
        $this->validation = $validation;
        $getModel = $repository->model();
        $this->model = new $getModel();
        $this->statuses = $this->model->statuses();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->repository->pushCriteria(new RequestCriteria($request));
        $breadcrumb = '<li><a href="' . route('admin.posts.index') . '">Posts Management</a></li><li class="active">List</li>';
        $input = $request->all();
        $perPage = !empty($input['limit']) ? $input['limit'] : $this->perPage;
        $keyword = !empty($input['keyword']) ? trim($input['keyword']) : '';
        $filterStatus = isset($input['filter_status']) && $input['filter_status'] !== 'none' ? (int)$input['filter_status'] : 'none';
        $model = $this->model;
        if (!empty($keyword) || ($filterStatus !== 'none')) {
            if (!empty($keyword)) {
                $model = $model->where(function ($query) use ($keyword) {
                    $query->where('name', 'LIKE', '%' . $keyword . '%');
                });
            }

            if ($filterStatus !== 'none') {
                $model = $model->where('status', $filterStatus);
            }
        }

        $total = $model->count();
        $datas = $model->paginate($perPage);

        return view('admin.posts.index')
            ->with(array(
                'breadcrumb' => $breadcrumb,
                'total' => $total,
                'datas' => $datas,
                'limit' => $perPage,
                'keyword' => $keyword,
                'filterStatus' => $filterStatus,
                'statuses' => $this->statuses
            ));
    }

    /**
     * Show the form for creating a new posts.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $breadcrumb = '<li><a href="' . route('admin.posts.index') . '">Posts Management</a></li><li class="active">Add new Post</li>';
        $categories = $this->categoriesRepositories->all();

        return view('admin.posts.create', [
            'breadcrumb' => $breadcrumb,
            'categories' => $categories,
        ]);
    }

    /**
     * Store a newly created posts in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            $model = $this->model;
            $validation = $this->validation;
            $validator = $validation->with( $request->all())->passes( $validation::RULE_CREATE );
            if (!$validator) {
                return redirect(route('admin.posts.create'))
                    ->withErrors($validation->errors())
                    ->withInput($request->all());
            }
            $request->merge(['status' => $request->get('status') ? $model::STATUS_ENABLE : 0]);
            $request->merge(['slug' => str_slug($request->get('name'))]);
            $post = $this->repository->create($request->all());
            //get tags id if have push tags else insert
            $arrayTagNames = explode(',', $request->get('tags_id'));
            $tagIds = [];
            $getTags = $this->tagsRepositories->model();
            $modelTags = new $getTags();
            $tags = $this->tagsRepositories->findWhereIn('name', $arrayTagNames)->pluck('id','name')->toArray();
            foreach($arrayTagNames as $name) {
                if(array_key_exists($name, $tags)) {
                    $tag_id = $tags[$name];
                } else {
                    $tag = $this->tagsRepositories->create([
                        'name' => $name,
                        'status' => $modelTags::STATUS_ENABLE
                    ]);
                    $tag_id = $tag->id;
                }
                $tagIds = array_merge($tagIds, [$tag_id]);
            }
            $post->tags()->attach($tagIds);

            Flash::success(trans('messages.store.success'));
            return redirect(route('admin.posts.index'));

        } catch (ValidatorException $exception) {
            Flash::success($exception->getMessage());
            return redirect(route('admin.posts.create'));
        }
    }

    /**
     * Display the specified posts.
     * @param  int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $data = $this->repository->findWithoutFail($id);
        $breadcrumb = '<li><a href="' . route('admin.posts.index') . '">Posts Management</a></li><li class="active text-more-breadcrumb">Detail Post: "' . $data->name . '"</li>';

        if (empty($data)) {
            Flash::error(trans('messages.show.not_found'));
            return redirect(route('admin.posts.index'));
        }

        return view('admin.posts.show')->with(array(
            'breadcrumb' => $breadcrumb,
            'data' => $data
        ));
    }

    /**
     * Show the form for editing the specified posts.
     * @param  int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $breadcrumb = '<li><a href="' . route('admin.posts.index') . '">Posts Management</a></li><li class="active">Edit Post</li>';
        $data = $this->repository->findWithoutFail($id);
        $categories = $this->categoriesRepositories->all();

        if (empty($data)) {
            Flash::error(trans('messages.show.not_found'));

            return redirect(route('admin.posts.index'));
        }

        return view('admin.posts.edit')->with([
            'breadcrumb' => $breadcrumb,
            'categories' => $categories,
            'data' => $data,
        ]);
    }

    /**
     * Update the specified categories in storage.
     * @param  int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            $model = $this->model;
            $validation = $this->validation;
            $validation->setId($id);
            $validator = $validation->with( $request->all())->passes( $validation::RULE_UPDATE );
            if (!$validator) {
                return redirect(route('admin.posts.update', ['posts' => $id]))
                    ->withErrors($validation->errors())
                    ->withInput($request->all());
            }
            $request->merge(['status' => ($request->get('status', 0)) ? $model::STATUS_ENABLE : 0]);
            $request->merge(['slug' => str_slug($request->get('name'))]);
            $request->merge(['description' => e($request->get('description'))]);
            $post = $this->repository->update($request->all(), $id);

            //get tags id if have push tags else insert
            $arrayTagNames = explode(',', $request->get('tags_id'));
            $tagIds = [];
            $getTags = $this->tagsRepositories->model();
            $modelTags = new $getTags();
            $tags = $this->tagsRepositories->findWhereIn('name', $arrayTagNames)->pluck('id','name')->toArray();
            foreach($arrayTagNames as $name) {
                if(array_key_exists($name, $tags)) {
                    $tag_id = $tags[$name];
                } else {
                    $tag = $this->tagsRepositories->create([
                        'name' => $name,
                        'status' => $modelTags::STATUS_ENABLE
                    ]);
                    $tag_id = $tag->id;
                }
                $tagIds = array_merge($tagIds, [$tag_id]);
            }
            $post->tags()->sync($tagIds);

            Flash::success(trans('messages.edit.success'));
            return redirect(route('admin.posts.index'));

        } catch (ValidatorException $exception) {
            Flash::success($exception->getMessage());
            return redirect(route('admin.posts.update', ['posts' => $id]));
        }
    }

    /**
     * Remove the specified categories from storage.
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $category = $this->repository->findWithoutFail($id);

        if (empty($category)) {
            Flash::error(trans('messages.show.not_found'));

            return redirect(route('admin.posts.index'));
        }

        $this->repository->delete($id);

        Flash::success(trans('messages.delete.success'));

        return redirect(route('admin.posts.index'));
    }

    /**
     * Remove the specified Course from storage.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkActions(Request $request)
    {
        $id = $request->get('ids');
        $key = $request->get('key');
        $value = $request->get('value');
        if($this->checkValueAction($id, $key, $value)){
            return $this->responseApp(false, 404, trans('messages.validator.invalid'));
        }

        $ids = explode(',', $id);
        $datas = $this->model->whereIn('id', $ids)->get();

        if ($key == 'status') {
            foreach ($datas as $item) {
                $item->status = $value;
                $item->save();
            }
        } else if ($key == 'delete') {
            foreach ($datas as $item) {
                $item->delete();
            }
        }

        return $this->responseApp(true, 200, trans('messages.action.success'));
    }

    /**
     * Remove the specified ActivationCode from storage.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function itemActions(Request $request)
    {
        $id = $request->get('id');
        $key = $request->get('key');
        $value = $request->get('value');
        if($this->checkValueAction($id, $key, $value)){
            return $this->responseApp(false, 404, trans('messages.validator.invalid'));
        }
        $data = $this->model->find($id);

        if (empty($data)) {
            return $this->responseApp(false, 404, trans('messages.show.not_found'));
        }

        if ($key == 'status') {
            $data->status = $value;
            $data->save();
        } else if ($key == 'delete') {
            $data->delete();
        }

        return $this->responseApp(true, 200, trans('messages.action.success'));
    }

    /**
     * @param $id
     * @param $key
     * @param $value
     * @return bool
     */
    private function checkValueAction($id, $key, $value) {
        $error = false;
        if (!$id || !$key || !isset($value)) {
            $error = true;

        }
        return $error;
    }
}
