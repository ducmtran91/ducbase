<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\LanguagesRepositories;
use App\Http\Controllers\AppBaseController;
use App\Services\Image;
use App\Validators\LanguagesValidator;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Validator\Exceptions\ValidatorException;

class LanguagesController extends AppBaseController
{

    /** @var  $repository */
    private $repository;
    private $model;
    private $statuses;
    private $validation;

    public function __construct(
        LanguagesRepositories $repository,
        LanguagesValidator $validator
    )
    {
        parent::__construct();
        $this->repository = $repository;
        $this->validation = $validator;
        $getModel = $repository->model();
        $this->model = new $getModel();
        $this->statuses = $this->model->statuses();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->repository->pushCriteria(new RequestCriteria($request));
        $breadcrumb = '<li><a href="' . route('admin.languages.index') . '">Languages Management</a></li><li class="active">List</li>';
        $input = $request->all();
        $perPage = !empty($input['limit']) ? $input['limit'] : $this->perPage;
        $keyword = !empty($input['keyword']) ? trim($input['keyword']) : '';
        $filterStatus = isset($input['filter_status']) && $input['filter_status'] !== 'none' ? (int)$input['filter_status'] : 'none';
        $model = $this->model;
        if (!empty($keyword) || ($filterStatus !== 'none')) {
            if (!empty($keyword)) {
                $model = $model->where(function ($query) use ($keyword) {
                    $query->where('name', 'LIKE', '%' . $keyword . '%');
                });
            }

            if ($filterStatus !== 'none') {
                $model = $model->where('status', $filterStatus);
            }
        }

        $total = $model->count();
        $datas = $model->paginate($perPage);
        return view('admin.languages.index')
            ->with(array(
                'breadcrumb' => $breadcrumb,
                'total' => $total,
                'datas' => $datas,
                'limit' => $perPage,
                'keyword' => $keyword,
                'filterStatus' => $filterStatus,
                'statuses' => $this->statuses
            ));
    }

    /**
     * Show the form for creating a new languages.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $breadcrumb = '<li><a href="' . route('admin.languages.index') . '">Languages Management</a></li><li class="active">Add new Language</li>';
        if (\Session::hasOldInput('image_tmp')) {
            $oldImage = \Session::getOldInput('image_tmp');
        }

        if (\Session::hasOldInput('srcImage')) {
            $oldImage = \Session::getOldInput('srcImage');
        }
        return view('admin.languages.create', [
            'breadcrumb' => $breadcrumb,
            'oldImage' => !empty($oldImage) ? $oldImage : ''
        ]);
    }

    /**
     * Store a newly created languages in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {

            $validation = $this->validation;
            $validator = $validation->with($request->all())->passes($validation::RULE_CREATE);
            $request = $this->checkImageBeforeValidator($request);
            if (!$validator) {
                return redirect(route('admin.languages.create'))
                    ->withErrors($validation->errors())
                    ->withInput($request->input());
            }
            $model = $this->model;
            $request->merge(['status' => ($request->get('status', 0)) ? $model::STATUS_ENABLE : 0]);
            $request = $this->checkImage($request);
            $this->repository->create($request->input());
            Flash::success(trans('messages.store.success'));
            return redirect(route('admin.languages.index'));

        } catch (ValidatorException $exception) {
            Flash::success($exception->getMessage());
            return redirect(route('admin.languages.create'));
        }
    }

    private function checkImageBeforeValidator($request)
    {
        $files = $request->file();
        $toolImage = new Image();
        if (!empty($files['image'])) {
            $checkImageBase64 = $toolImage->convertImageToBase64($files['image']->getPathname());

            if ($checkImageBase64['success']) {
                $request->merge(['srcImage' => $checkImageBase64['data']]);
            } else {
                Flash::error('Convert image to base64 have failed!');
                return redirect(route('admin.languages.create'))->withInput($request->input());
            }
        }
        return $request;
    }

    private function checkImage($request)
    {
        $files = $request->file();
        $toolImage = new Image();
        if ($imageTmp = $request->get('image_tmp')) {
            $data = $toolImage->convertBase64ToImage($imageTmp, config('filepath.languages_image'));

            if ($data['success']) {
                $request->merge(['image' => $data['data']]);
            } else {
                $request->merge(['srcImage' => $imageTmp]);
                Flash::error('Convert base64 to image have failed!');
                return redirect(route('admin.languages.create'))->withInput($request->input());
            }
        } else if (!empty($files['image'])) {
            $request = $this->upload_file($files['image'], $request);
        }
        return $request;
    }

    private function upload_file($files, $request) {
        $uploadImage = $this->commonService->uploadFile($files, config('filepath.languages_image'));
        if (!empty($uploadImage)) {
            $request->merge(['image' => $uploadImage]);
        }
        return $request;
    }

    private function checkImageUpdate($request, $id)
    {
        $files = $request->file();
        $data = $this->repository->findWithoutFail($id);

        if (empty($data)) {
            Flash::error('languages not found');
            return redirect(route('admin.languages.index'));
        }
        if (!empty($files['image'])) {
            $request = $this->upload_file($files['image'], $request);
            $this->commonService->removeFile($data->image, config('filepath.languages_image'));
        } else {
            if ($imageTmp = $request->get('image_tmp')) {
                $request->merge(['image' => $imageTmp]);
            } else {
                $request->merge(['image' => null]);
                $this->commonService->removeFile($data->image, config('filepath.languages_image'));
            }
        }
        return $request;
    }
    /**
     * Display the specified languages.
     * @param  int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $data = $this->repository->findWithoutFail($id);
        $breadcrumb = '<li><a href="' . route('admin.languages.index') . '">Languages Management</a></li><li class="active text-more-breadcrumb">Detail Language: "' . $data->name . '"</li>';

        if (empty($data)) {
            Flash::error(trans('messages.show.not_found'));
            return redirect(route('admin.languages.index'));
        }

        return view('admin.languages.show')->with(array(
            'breadcrumb' => $breadcrumb,
            'data' => $data
        ));
    }

    /**
     * Show the form for editing the specified languages.
     * @param  int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $breadcrumb = '<li><a href="' . route('admin.languages.index') . '">Languages Management</a></li><li class="active">Edit Language</li>';
        $data = $this->repository->findWithoutFail($id);

        if (empty($data)) {
            Flash::error(trans('messages.show.not_found'));

            return redirect(route('admin.languages.index'));
        }
        $languages = $this->repository->findWhereNotIn('id', [$id]);

        return view('admin.languages.edit')->with([
            'breadcrumb' => $breadcrumb,
            'languages' => $languages,
            'data' => $data,
        ]);
    }

    /**
     * Update the specified languages in storage.
     * @param  int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            $model = $this->model;
            $validation = $this->validation;
            $validation->setId($id);
            $request->merge(['status' => ($request->get('status', 0)) ? $model::STATUS_ENABLE : 0]);
            $validator = $validation->with($request->all())->passes($validation::RULE_UPDATE);
            if (!$validator) {
                return redirect(route('admin.languages.edit', ['languages' => $id]))
                    ->withErrors($validation->errors())
                    ->withInput($request->all());
            }
            $request->merge(['status' => ($request->get('status', 0)) ? $model::STATUS_ENABLE : 0]);
            $request = $this->checkImageUpdate($request, $id);
            $this->repository->update($request->input(), $id);
            Flash::success(trans('messages.edit.success'));
            return redirect(route('admin.languages.index'));

        } catch (ValidatorException $exception) {
            Flash::success($exception->getMessage());
            return redirect(route('admin.languages.edit', ['languages' => $id]));
        }
    }



    /**
     * Remove the specified languages from storage.
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $category = $this->repository->findWithoutFail($id);

        if (empty($category)) {
            Flash::error(trans('messages.show.not_found'));

            return redirect(route('admin.languages.index'));
        }

        $this->repository->delete($id);

        Flash::success(trans('messages.delete.success'));

        return redirect(route('admin.languages.index'));
    }

    /**
     * Remove the specified Course from storage.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkActions(Request $request)
    {
        $id = $request->get('ids');
        $key = $request->get('key');
        $value = $request->get('value');
        if ($this->checkValueAction($id, $key, $value)) {
            return $this->responseApp(false, 404, trans('messages.validator.invalid'));
        }

        $ids = explode(',', $id);
        $datas = $this->model->whereIn('id', $ids)->get();

        if ($key == 'status') {
            foreach ($datas as $item) {
                $item->status = $value;
                $item->save();
            }
        } else if ($key == 'delete') {
            foreach ($datas as $item) {
                $item->delete();
            }
        }

        return $this->responseApp(true, 200, trans('messages.action.success'));
    }

    /**
     * Remove the specified ActivationCode from storage.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function itemActions(Request $request)
    {
        $id = $request->get('id');
        $key = $request->get('key');
        $value = $request->get('value');
        if ($this->checkValueAction($id, $key, $value)) {
            return $this->responseApp(false, 404, trans('messages.validator.invalid'));
        }
        $data = $this->model->find($id);

        if (empty($data)) {
            return $this->responseApp(false, 404, trans('messages.show.not_found'));
        }

        if ($key == 'status') {
            $data->status = $value;
            $data->save();
        } else if ($key == 'delete') {
            $data->delete();
        }

        return $this->responseApp(true, 200, trans('messages.action.success'));
    }

    /**
     * @param $id
     * @param $key
     * @param $value
     * @return bool
     */
    private function checkValueAction($id, $key, $value)
    {
        $error = false;
        if (!$id || !$key || !isset($value)) {
            $error = true;

        }
        return $error;
    }
}