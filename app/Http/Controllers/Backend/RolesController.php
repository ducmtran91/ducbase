<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Roles;
use App\Http\Requests\CreateRolesRequest;
use App\Http\Requests\UpdateRolesRequest;
use App\Repositories\RolesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RolesController extends AppBaseController {

    /** @var  RolesRepository */
    private $rolesRepository;
    public $roleTypes = array(
        'admin' => 'Administrator',
        'user' => 'User',
        'owner' => 'Owner',
        'walker' => 'Walker'
    );

    public function __construct(RolesRepository $rolesRepo) {
        parent::__construct();

        $this->rolesRepository = $rolesRepo;
    }

    /**
     * Display a listing of the Roles.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request) {        
        $this->rolesRepository->pushCriteria(new RequestCriteria($request));
        $breadcrumb = '<li><a href="' . route('admin.roles.index') . '">Roles Management</a></li><li class="active">List Roles</li>';
        $input = $request->all();
        $perPage = !empty($input['limit']) ? $input['limit'] : $this->perPage;
        $keyword = !empty($input['keyword']) ? trim($input['keyword']) : '';
        $filterStatus = isset($input['filter_status']) ? $input['filter_status'] : 'none';
        $getModel = $this->rolesRepository->model();
        $model = new $getModel();

        if (!empty($keyword) || ($filterStatus !== 'none')) {
            if (!empty($keyword)) {
                $model = $model->where('name', 'LIKE', '%' . $keyword . '%');
            }

            if ($filterStatus !== 'none') {
                $model = $model->where('status', $filterStatus);
            }
        }

        $model = $model->where('type', Roles::ROLE_USER)->where('id', '!=', $this->currentUser->role_id);
        $total = $model->count();
        $roles = $model->paginate($perPage);

        return view('admin.roles.index')->with([
                    'breadcrumb' => $breadcrumb,
                    'total' => $total,
                    'roles' => $roles,
                    'limit' => $perPage,
                    'keyword' => $keyword,
                    'filterStatus' => $filterStatus
        ]);
    }

    /**
     * Show the form for creating a new Roles.
     *
     * @return Response
     */
    public function create() {
        $breadcrumb = '<li><a href="' . route('admin.roles.index') . '">Roles Management</a></li><li class="active">Add new Role</li>';

        return view('admin.roles.create', [
            'breadcrumb' => $breadcrumb
        ]);
    }

    /**
     * Store a newly created Roles in storage.
     *
     * @param CreateRolesRequest $request
     *
     * @return Response
     */
    public function store(CreateRolesRequest $request) {
        $role = new Roles();

        $request->permission = $this->_addMorePermissionPosted($request->permission);

        $role->name = $request->name;
        $role->description = $request->description;
        $role->type = 'user';
        $role->permission = (($request->permission != null) ? json_encode($request->permission) : null);
        $role->status = $request->status;
        $role->save();

        Flash::success('Successful create new role!');

        return redirect(route('admin.roles.index'));
    }

    /**
     * Show the form for editing the specified Roles.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id) {
        $breadcrumb = '<li><a href="' . route('admin.roles.index') . '">Roles Management</a></li><li class="active">Edit Role</li>';
        $roles = $this->rolesRepository->findWithoutFail($id);

        if (empty($roles)) {
            Flash::error('Roles not found');

            return redirect(route('admin.roles.index'));
        }

        return view('admin.roles.edit')->with([
                    'breadcrumb' => $breadcrumb,
                    'roles' => $roles
        ]);
    }

    /**
     * Update the specified Roles in storage.
     *
     * @param  int              $id
     * @param UpdateRolesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRolesRequest $request) {
        $roles = $this->rolesRepository->findWithoutFail($id);
        if (empty($roles)) {
            Flash::error('Roles not found');

            return redirect(route('admin.roles.index'));
        }
        
        $request->permission = $this->_addMorePermissionPosted($request->permission);

        $roles->name = $request->name;
        $roles->description = $request->description;
        $roles->permission = (($request->permission != null) ? json_encode($request->permission) : null);
        $roles->status = $request->status;
        $roles->save();

        Flash::success('Successful edit role!');

        return redirect(route('admin.roles.index'));
    }

    /**
     * Remove the specified Roles from storage.
     *
     * @param Request $request
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request, $id) {
        $roles = $this->rolesRepository->findWithoutFail($id);

        if (empty($roles)) {
            Flash::error('Roles not found');

            return redirect(route('admin.roles.index'));
        }
        
        if($this->currentUser->role_id == $id){
            Flash::error('You do not have permission to take this action!');

            return redirect(route('admin.roles.index'));
        }

        $this->rolesRepository->delete($id);

        Flash::success('Successful delete role!');

        return redirect(route('admin.roles.index'));
    }

    /**
     * Add more permission to permission posted (Example : if permission is create, need to add store)
     * 
     * @param array $permission
     * @return array
     */
    protected function _addMorePermissionPosted($permission) {
        // Check duplicate and get unique values
        $permission = $this->_checkDuplicatePermissions($permission);

        $permission['dashboard'][] = 'default@index';
        $permission['login'][] = 'login@logout';
        $permission['auth'][] = 'auth@logout';

        if (!empty($permission)) {
            foreach ($permission as $key => $arrAction) {                
                if($key == 'owners' || $key == 'walkers') {
                    $permission['owners'][] = 'owners@locationdependence';
                }
                
                foreach ($arrAction as $action) {
                    $fieldData = explode('@', $action);
                    
                    switch ($fieldData[1]) {
                        case 'create':
                            if (!in_array($fieldData[0] . '@store', $permission[$key])) {
                                $permission[$key][] = $fieldData[0] . '@store';
                            }

                            if (!in_array($fieldData[0] . '@index', $permission[$key])) {
                                $permission[$key][] = $fieldData[0] . '@index';
                            }

                            if (!in_array($fieldData[0] . '@show', $permission[$key])) {
                                $permission[$key][] = $fieldData[0] . '@show';
                            }
                            break;
                        case 'edit':
                            if (!in_array($fieldData[0] . '@update', $permission[$key])) {
                                $permission[$key][] = $fieldData[0] . '@update';
                            }

                            if (!in_array($fieldData[0] . '@index', $permission[$key])) {
                                $permission[$key][] = $fieldData[0] . '@index';
                            }

                            if (!in_array($fieldData[0] . '@show', $permission[$key])) {
                                $permission[$key][] = $fieldData[0] . '@show';
                            }
                            break;
                        case 'itemactions':
                            if (!in_array($fieldData[0] . '@destroy', $permission[$key])) {
                                $permission[$key][] = $fieldData[0] . '@destroy';
                            }
                            break;
                        case 'bulkactions':
                            if (!in_array($fieldData[0] . '@destroy', $permission[$key])) {
                                $permission[$key][] = $fieldData[0] . '@destroy';
                            }
                            break;
                        case 'sendreport':
                            if (!in_array($fieldData[0] . '@storereport', $permission[$key])) {
                                $permission[$key][] = $fieldData[0] . '@storereport';
                            }
                            break;
                    }
                }
            }
        }

        return $permission;
    }

    /**
     * Check duplicate and get unique values
     *
     * @param array $allPermission
     * @return array
     */
    protected function _checkDuplicatePermissions($allPermission) {
        if ($allPermission && is_array($allPermission)) {
            foreach ($allPermission as $controller => $actions) {
                if (is_array($actions)) {
                    $allPermission[$controller] = array_values(array_unique($actions));
                }
            }
        }

        return $allPermission;
    }

    /**
     * Remove the specified Course from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function bulkActions(Request $request) {
        $input = $request->all();

        if (empty($input['ids']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Invalid information. Please try again.');
        }

        $value = $input['value'];
        $key = $input['key'];
        $ids = explode(',', $input['ids']);
        $getModel = $this->rolesRepository->model();
        $model = new $getModel;
        $datas = $model->whereIn('id', $ids)->get();

        if ($key == 'status') {
            foreach ($datas as $item) {
                $item->status = $value;
                $item->save();
            }
        } else if ($key == 'delete') {
            foreach ($datas as $item) {
                if ($item->id == 1) {
                    return $this->responseApp(false, 1, 'You do not have permission to take this action!');
                }

                $item->delete();
            }
        }

        return $this->responseApp(true, 200, 'Process successfully.');
    }

    /**
     * Remove the specified ActivationCode from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function itemActions(Request $request) {
        $input = $request->all();

        if (empty($input['id']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Invalid information. Please try again.');
        }

        $value = $input['value'];
        $key = $input['key'];
        $id = $input['id'];
        $getModel = $this->rolesRepository->model();
        $model = new $getModel;
        $data = $model->find($id);

        if (empty($data)) {
            return $this->responseApp(false, 404, 'Not found.');
        }

        if ($key == 'status') {
            $data->status = $value;
            $data->save();
        } else if ($key == 'delete') {
            if ($data->id == 1) {
                return $this->responseApp(false, 1, 'You do not have permission to take this action!');
            }

            $data->delete();
        }

        return $this->responseApp(true, 200, 'Process successfully.');
    }

}
