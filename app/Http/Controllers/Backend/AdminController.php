<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\PasswordResetRepository;
use App\Repositories\RolesRepository;
use App\Services\Image;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Entities\User;
use App\Services\Common;
use Carbon\Carbon;
use App\Repositories\UsersRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Response,
    URL;
use Validator;
use Flash;
use Admin;
use Illuminate\Support\Facades\Auth;

class AdminController extends AppBaseController
{

    use ResetsPasswords;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $userRepository;
    protected $commonService;
    private $rolesRepository;
    private $passwordResetRepository;


    public function __construct(
        Common $commonService,
        UsersRepository $userRepo,
        RolesRepository $rolesRepository,
        PasswordResetRepository $passwordResetRepository
    )
    {
        parent::__construct();
        $this->commonService = $commonService;
        $this->userRepository = $userRepo;
        $this->rolesRepository = $rolesRepository;
        $this->passwordResetRepository = $passwordResetRepository;
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $breadcrumb = '<li><a href="' . route('admin.admin.index') . '">Admin</a></li><li class="active">List Admin</li>';
        $input = $request->all();
        $perPage = !empty($input['limit']) ? $input['limit'] : $this->perPage;
        $typeSort = !empty($input['type_sort']) ? $input['type_sort'] : 'ASC';
        $sort = !empty($input['sort']) ? $input['sort'] : 'id';
        $totalActive = User::where('active', '=', '1')->where('id', '!=', '1')->count();
        $totalInActive = User::where('active', '=', '0')->where('id', '!=', '1')->count();
        $getModel = $this->userRepository->model();
        $userModel = new $getModel;
        $userModel = $userModel->where('id', '!=', '1')->where('is_admin', 1);
        $typeSort = (!empty($typeSort)) ? $typeSort : 'ASC';
        $sort = (!empty($sort)) ? $sort : 'id';
        $query_params = [
            'limit' => $perPage
        ];
        $userModel = $userModel->orderBy($sort, $typeSort);
        $users = $userModel->paginate($perPage);

        return view('admin.admin.index')
            ->with(array(
                'breadcrumb' => $breadcrumb,
                'sort' => $sort,
                'typeSort' => $typeSort,
                'limit' => $perPage,
                'totalActive' => $totalActive,
                'totalInActive' => $totalInActive,
                'users' => $users,
                'query_params' => $query_params,
            ));
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        if(\Session::hasOldInput('srcImage')) {
            $oldPhoto = \Session::getOldInput('srcImage');
        }

        $role_value = 1;
        $breadcrumb = '<li><a href="' . route('admin.admin.index') . '">Admin</a></li><li class="active">Create Admin</li>';
        $rolesModel = $this->rolesRepository->model();
        $roles = $rolesModel::where('active', 1)
            ->whereNotIn('id' , [1, 2] )
            ->get();
        $select_role = [];
        foreach($roles as $role){
            $select_role[$role->id] = $role->name;
        }

        $genders = User::genders();

        return view('admin.admin.create')
            ->with(array(
                'breadcrumb' => $breadcrumb,
                'select_role' => $select_role,
                'model' => '',
                'role_value' => $role_value,
                'genders' => $genders,
                'oldPhoto' => !empty($oldPhoto) ? $oldPhoto : '',

            ));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), User::$rules_cr, [
            'email.unique' => 'You are already registered with this email!'
        ]);

        $input = $request->all();

        $password = $this->returnRandomString(10);
        $adminModel = $this->userRepository->model();
        $model = new $adminModel;

        $model->role_id = $input['role_id'];
        $model->first_name = $input['first_name'];
        $model->last_name = $input['last_name'];
        $model->name = $model->first_name . '  ' . $model->last_name;
        $model->birthday = $request->birthday ? Carbon::createFromFormat(config('common.date_format'), $request->birthday) : null;
        $model->address = $input['address'];
        $model->email = $input['email'];
        $model->phone = $input['phone'];
        $model->gender = $input['gender'];
        $model->description = $input['description'];
        $model->password = bcrypt($password);
        $model->api_token = bcrypt(time());
        $model->is_admin = 1;

        $files = $request->file();
        $toolImage = new Image();

        //check isset input image
        if($files) {
            $checkImageBase64 = $toolImage->convertImageToBase64($files['image']->getPathname());

            //save session cakeching
            $request->session()->put('key'  , $checkImageBase64 );

           //get value session key
            $sessionImage = $request->session()->get('key');
            if ($checkImageBase64['success']) {
                $input['srcImage'] = $sessionImage['data'];
            }else{
                Flash::error(\Lang::get('image.'.$checkImageBase64['msg']));
                return redirect(route('admin.admin.create'));
            }

        }else{

            //get value session key
            $sessionImage = $request->session()->get('key');
            if ($sessionImage['success']) {
                $input['srcImage'] = $sessionImage['data'];
            }else{
                Flash::error(\Lang::get('image.'.$sessionImage['msg']));
                return redirect(route('admin.admin.create'));
            }
        }

        if($validator->fails()){
            return redirect('admin/admin/create')
                ->withErrors($validator)
                ->withInput($input);
        }

        if (!empty($files['image'])) {
            $uploadImage = $this->commonService->uploadFile($files['image'], config('filepath.user_image'));

            if (!empty($uploadImage)) {
                $model->photo = $uploadImage;
            }
        }

        $model->save();

        if (!$model->save()) {
            Flash::error('Create admin error!');
            return redirect(route('admin.admin.create'));
        }

        $link = $this->sendResetLinkEmail($request);

        if ($link){
            Flash::success($model->name .'" is added sucessfully. Please check the email to change your password.');
        }

        if (!empty($this->saveNewMode($request->all()))) {
            return redirect(route('admin.admin.create'));
        }

        return redirect(route('admin.admin.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(\Session::hasOldInput('srcImage')) {
            $oldPhoto = \Session::getOldInput('srcImage');
        }

        $role_value = 1;
        $model = $this->userRepository->findWithoutFail($id);
        if (empty($model)) {
            Flash::error('Admin not found');
            return redirect(route('admin.admin.index'));
        }

        $breadcrumb = '<li><a href="' . route('admin.admin.index') . '">Admin</a></li><li class="active">Edit Admin</li>';
        $rolesModel = $this->rolesRepository->model();
        $roles = $rolesModel::where('active', 1)
            ->whereNotIn('id' , [1, 2] )
            ->get();

        $select_role = [];
        foreach($roles as $role){
            $select_role[$role->id] = $role->name;
        }


        $genders = User::genders();

        return view("admin.admin.edit", [
            'breadcrumb' => $breadcrumb,
            'model' => $model,
            'select_role' => $select_role,
            'role_value' => $role_value,
            'genders' => $genders,
            'oldPhoto' => !empty($oldPhoto) ? $oldPhoto : '',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $model = $this->userRepository->findWithoutFail($id);

        if (empty($model)) {
            Flash::error('Không tìm thấy mã!');
            return redirect(route('admin.admin.index'));
        }

        //add validator
        $validator = Validator::make($request->all(), User::$rules_edit);

        $model->role_id = $input['role_id'];
        $model->first_name = $input['first_name'];
        $model->last_name = $input['last_name'];
        $model->name = $model->first_name . '  ' . $model->last_name;
        $model->birthday = $request->birthday ? Carbon::createFromFormat(config('common.date_format'), $request->birthday) : null;
        $model->address = $input['address'];
        $model->phone = $input['phone'];
        $model->gender = $input['gender'];
        $model->description = $input['description'];
        $input = $request->all();
        $image = $model->photo;
        $files = $request->file();
        $toolImage = new Image();

        //check isset input image
        if($files) {
            $checkImageBase64 = $toolImage->convertImageToBase64($files['image']->getPathname());

            //save session cakeching
            $request->session()->put('key'  , $checkImageBase64 );

            //get value session key
            $sessionImage = $request->session()->get('key');
            if ($checkImageBase64['success']) {
                $input['srcImage'] = $sessionImage['data'];
            }else{
                Flash::error(\Lang::get('image.'.$checkImageBase64['msg']));
                return redirect(route('admin.admin.create'));
            }

        }else{

            //get value session key
            $sessionImage = $request->session()->get('key');
            if ($sessionImage['success']) {
                $input['srcImage'] = $sessionImage['data'];
            }else{
                Flash::error(\Lang::get('image.'.$sessionImage['msg']));
                return redirect(route('admin.admin.create'));
            }

        }

        if($validator->fails()){
            return redirect("admin/admin/$id/edit")
                ->withErrors($validator)
                ->withInput($input);
        }

        if (!empty($files['image'])) {
            $uploadImage = $this->commonService->uploadFile($files['image'], config('filepath.user_image'));
            if (!empty($uploadImage)) {
                $image = $uploadImage;
            }
            $this->commonService->removeFile($model->photo, config('filepath.user_image'));
        } else {
            if (!empty($input['image_tmp'])) {
                $image = $input['image_tmp'];
                unset($input['image_tmp']);
            } else {
                $image = null;
                $this->commonService->removeFile($model->photo, config('filepath.user_image'));
            }
        }

        $model->photo = $image;
        $model->save();
        Flash::success('The information is updated successfully!');
        Admin::userLog(\Auth::guard('admin')->user()->id, 'Update user ' . $request->name);

        return redirect(route('admin.admin.index'));
    }

    public function show($id, Request $request)
    {
        $input = $request->all();
        $model = $this->userRepository->findWithoutFail($id);
        $breadcrumb = '<li><a href="' . route('admin.admin.index') . '">Admin</a></li><li class="active">'. $model->name  .'</li>';

        if (empty($model)) {
            Flash::error('No code exists!');
            return redirect(route('admin.admin.index'));
        }

        $inputEmail = [
            'email' => $model->email
        ];

        $passwordReset = $this->passwordResetRepository->findWhere($inputEmail)->first();

        if (!empty($passwordReset)){
            $token = $passwordReset->token;
        }  else{
            $token = '';
        }

        $rolesModel = $this->rolesRepository->model();
        $roles = $rolesModel::where('active', 1)->get();

        return view("admin.admin.show", [
            'breadcrumb' => $breadcrumb,
            'model' => $model,
            'users' => $model->userSecondaries,
            'roles' => $roles,
            'token' => $token,
            'passwordReset' => $passwordReset,

        ]);
    }

    public function sendPassword( Request $request)
    {
        $link = $this->sendResetLinkEmail($request);
        if ($link){
            Flash::success('Please check your email to create a new password.');
            return back()->withInput();
        }

    }

    public function itemActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['id']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Invalid information. Please try again.');
        }

        $value = $input['value'];
        $key = $input['key'];
        $id = $input['id'];
        $getModel = $this->userRepository->model();
        $model = new $getModel;
        $data = $model->find($id);

        if ($data->id == Auth::id()){
            return $this->responseApp(false, 404, 'you cannot ban your primary.');
        }


        if (empty($data)) {
            return $this->responseApp(false, 404, 'This admin was not found.');
        }

        if ($key == 'status') {
            $data->active = $value;
            $data->save();
        }

        return $this->responseApp(true, 200, 'successfully.');
    }

    public function profiles($id)
    {
        $model = $this->userRepository->findWithoutFail($id);
        $breadcrumb = '<li class="active">Profile</li>';

        if (empty($model)) {
            Flash::error('No code exists!');
            return redirect(route('admin.admin.index'));
        }

        return view("admin.admin.profile", [
            'breadcrumb' => $breadcrumb,
            'model' => $model,
        ]);
    }

    public function getProfiles($id)
    {
        $rolesModel = $this->rolesRepository->model();
        $roles = $rolesModel::where('active', 1)
            ->whereNotIn('id' , [1, 2] )
            ->get();

        $select_role = [];
        foreach($roles as $role){
            $select_role[$role->id] = $role->name;
        }

        $role_profile = true;

        $model = $this->userRepository->findWithoutFail($id);
        if (empty($model)) {
            Flash::error('Không tìm thấy mã');
            return redirect(route('admin.admin.index'));
        }

        $breadcrumb = '<li class="active">Edit Profile</li>';
        return view('admin.admin.edit_profile', [
            'breadcrumb' => $breadcrumb,
            'model' => $model,
            'select_role' => $select_role,
            'role_profile' => $role_profile,
        ]);
    }

    public function postProfiles(Request $request, $id)
    {

        $input = $request->all();
        $model = $this->userRepository->findWithoutFail($id);

        if (empty($model)) {
            Flash::error('Không tìm thấy mã!');
            return redirect(route('admin.admin.index'));
        }

        //add rules
        $this->validate($request, User::$rules_prifile);

        $model->first_name = $input['first_name'];
        $model->last_name = $input['last_name'];
        $model->name = $model->first_name . '  ' . $model->last_name;
        $model->birthday = $request->birthday ? Carbon::createFromFormat(config('common.date_format'), $request->birthday) : null;

        $model->address = $input['address'];
        $model->email = $input['email'];
        $model->phone = $input['phone'];
        $model->gender = $input['gender'];
        $model->description = $input['description'];
        $files = $request->file();
        $input = $request->all();
        $image = $model->photo;

        if (!empty($files['image'])) {
            $uploadImage = $this->commonService->uploadFile($files['image'], config('filepath.user_image'));
            if (!empty($uploadImage)) {
                $image = $uploadImage;
            }
            $this->commonService->removeFile($model->photo, config('filepath.user_image'));
        } else {
            if (!empty($input['image_tmp'])) {
                $image = $input['image_tmp'];
                unset($input['image_tmp']);
            } else {
                $image = null;
                $this->commonService->removeFile($model->photo, config('filepath.user_image'));
            }
        }
        $model->photo = $image;
        $model->save();
        Flash::success('Update Profile "' . $model->name . '" success');

        Admin::userLog(\Auth::guard('admin')->user()->id, 'Update user ' . $request->name);

        return redirect()->route('admin.admin.profiles',['id' => $model->id] );
    }


}
