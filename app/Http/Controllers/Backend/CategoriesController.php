<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\CategoriesRepositories;
use App\Http\Controllers\AppBaseController;
use App\Validators\CategoriesValidator;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Validator\Exceptions\ValidatorException;

class CategoriesController extends AppBaseController
{

    /** @var  $repository */
    private $repository;
    private $model;
    private $statuses;
    private $validation;

    public function __construct(
        CategoriesRepositories $repository,
        CategoriesValidator $validator
    )
    {
        parent::__construct();
        $this->repository = $repository;
        $this->validation = $validator;
        $getModel = $repository->model();
        $this->model = new $getModel();
        $this->statuses = $this->model->statuses();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->repository->pushCriteria(new RequestCriteria($request));
        $breadcrumb = '<li><a href="' . route('admin.categories.index') . '">'.trans('backend.label.management.categories.edit').'</a></li><li class="active">'.trans("backend.label.list").'</li>';
        $input = $request->all();
        $perPage = !empty($input['limit']) ? $input['limit'] : $this->perPage;
        $keyword = !empty($input['keyword']) ? trim($input['keyword']) : '';
        $filterStatus = isset($input['filter_status']) && $input['filter_status'] !== 'none' ? (int)$input['filter_status'] : 'none';
        $model = $this->model;
        if (!empty($keyword) || ($filterStatus !== 'none')) {
            if (!empty($keyword)) {
                $model = $model->whereTranslationLike('name', '%' . $keyword . '%');
            }

            if ($filterStatus !== 'none') {
                $model = $model->where('status', $filterStatus);
            }
        }

        $total = $model->count();
        $datas = $model->paginate($perPage);
        return view('admin.categories.index')
            ->with(array(
                'breadcrumb' => $breadcrumb,
                'total' => $total,
                'datas' => $datas,
                'limit' => $perPage,
                'keyword' => $keyword,
                'filterStatus' => $filterStatus,
                'statuses' => $this->statuses
            ));
    }

    /**
     * Show the form for creating a new categories.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $breadcrumb = '<li><a href="' . route('admin.categories.index') . '">'.trans('backend.label.management.categories.create').'</a></li><li class="active">'.trans('backend.label.actions.add').'</li>';
        $categories = $this->repository->all();

        return view('admin.categories.create', [
            'breadcrumb' => $breadcrumb,
            'categories' => $categories,
        ]);
    }

    /**
     * Store a newly created categories in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            $model = $this->model;
            $validation = $this->validation;
            $request->merge(['status' => ($request->get('status', 0)) ? $model::STATUS_ENABLE : 0]);
            $validator = $validation->with( $request->all())->passes( $validation::RULE_CREATE );
            if (!$validator) {
                return redirect(route('admin.categories.create'))
                    ->withErrors($validation->errors())
                    ->withInput($request->all());
            }
            $this->repository->create($request->all());
            Flash::success(trans('messages.store.success'));
            return redirect(route('admin.categories.index'));

        } catch (ValidatorException $exception) {
            Flash::success($exception->getMessage());
            return redirect(route('admin.categories.create'));
        }
    }

    /**
     * Display the specified categories.
     * @param  int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $data = $this->repository->findWithoutFail($id);
        $breadcrumb = '<li><a href="' . route('admin.categories.index') . '">'.trans('backend.label.management.categories.show').'</a></li><li class="active text-more-breadcrumb">'.trans('backend.label.info').': '.$data->name.'</li>';

        if (empty($data)) {
            Flash::error(trans('backend.errors.not_found'));
            return redirect(route('admin.categories.index'));
        }

        return view('admin.categories.show')->with(array(
            'breadcrumb' => $breadcrumb,
            'data' => $data
        ));
    }

    /**
     * Show the form for editing the specified categories.
     * @param  int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $breadcrumb = '<li><a href="' . route('admin.categories.index') . '">'.trans('backend.label.management.categories.edit').'</a></li><li class="active">'.trans('backend.label.actions.edit').'</li>';
        $data = $this->repository->findWithoutFail($id);

        if (empty($data)) {
            Flash::error(trans('backend.errors.not_found'));
            return redirect(route('admin.categories.index'));
        }
        $categories = $this->repository->findWhereNotIn('id', [$id]);

        return view('admin.categories.edit')->with([
            'breadcrumb' => $breadcrumb,
            'categories' => $categories,
            'data' => $data,
        ]);
    }

    /**
     * Update the specified categories in storage.
     * @param  int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            $model = $this->model;
            $validation = $this->validation;
            $validation->setId($id);
            $request->merge(['status' => ($request->get('status', 0)) ? $model::STATUS_ENABLE : 0]);
            $validator = $validation->with( $request->all())->passes( $validation::RULE_UPDATE );
            if (!$validator) {
                return redirect(route('admin.categories.edit', ['categories' => $id]))
                    ->withErrors($validation->errors())
                    ->withInput($request->all());
            }
            $this->repository->update($request->all(), $id);
            Flash::success(trans('messages.edit.success'));
            return redirect(route('admin.categories.index'));

        } catch (ValidatorException $exception) {
            Flash::success($exception->getMessage());
            return redirect(route('admin.categories.edit', ['categories' => $id]));
        }
    }

    /**
     * Remove the specified categories from storage.
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $category = $this->repository->findWithoutFail($id);

        if (empty($category)) {
            Flash::error(trans('backend.errors.not_found'));
            return redirect(route('admin.categories.index'));
        }

        $this->repository->delete($id);

        Flash::success(trans('messages.delete.success'));

        return redirect(route('admin.categories.index'));
    }

    /**
     * Remove the specified Course from storage.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkActions(Request $request)
    {
        $id = $request->get('ids');
        $key = $request->get('key');
        $value = $request->get('value');
        if($this->checkValueAction($id, $key, $value)){
            return $this->responseApp(false, 404, trans('messages.validator.invalid'));
        }

        $ids = explode(',', $id);
        $datas = $this->model->whereIn('id', $ids)->get();

        if ($key == 'status') {
            foreach ($datas as $item) {
                $item->status = $value;
                $item->save();
            }
        } else if ($key == 'delete') {
            foreach ($datas as $item) {
                $item->delete();
            }
        }

        return $this->responseApp(true, 200, trans('messages.action.success'));
    }

    /**
     * Remove the specified ActivationCode from storage.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function itemActions(Request $request)
    {
        $id = $request->get('id');
        $key = $request->get('key');
        $value = $request->get('value');
        if($this->checkValueAction($id, $key, $value)){
            return $this->responseApp(false, 404, trans('messages.validator.invalid'));
        }
        $data = $this->model->find($id);

        if (empty($data)) {
            return $this->responseApp(false, 404, trans('backend.errors.not_found'));
        }

        if ($key == 'status') {
            $data->status = $value;
            $data->save();
        } else if ($key == 'delete') {
            $data->delete();
        }

        return $this->responseApp(true, 200, trans('messages.action.success'));
    }

    /**
     * @param $id
     * @param $key
     * @param $value
     * @return bool
     */
    private function checkValueAction($id, $key, $value) {
        $error = false;
        if (!$id || !$key || !isset($value)) {
            $error = true;

        }
        return $error;
    }
}
