<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Roles;
use App\Http\Requests\Backend\UpdateUsersRequest;
use App\Repositories\UsersRepository;
use App\Repositories\RolesRepository;
use App\Http\Controllers\AppBaseController;
use App\Services\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Validator;

class UsersController extends AppBaseController {

    /** @var  UsersRepository */
    private $usersRepository;
    private $rolesRepository;

    public function __construct(
        UsersRepository $usersRepo,
        RolesRepository $rolesRepo
    ) {
        parent::__construct();

        $this->usersRepository = $usersRepo;
        $this->rolesRepository = $rolesRepo;
    }

    /**
     * Display a listing of the Users.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request) {
        $this->usersRepository->pushCriteria(new RequestCriteria($request));
        $breadcrumb = '<li><a href="' . route('admin.users.index') . '">Administrators Management</a></li><li class="active">List Administrators</li>';
        $input = $request->all();
        $perPage = !empty($input['limit']) ? $input['limit'] : $this->perPage;
        $keyword = !empty($input['keyword']) ? trim($input['keyword']) : '';
        $filterStatus = isset($input['filter_status']) ? $input['filter_status'] : 'none';
        $getModel = $this->usersRepository->model();
        $model = new $getModel();
        $roleIds = $this->rolesRepository->getByType(Roles::ROLE_USER)->pluck('id')->all();

        if (!empty($keyword) || ($filterStatus !== 'none')) {
            if (!empty($keyword)) {
                $model = $model->where(function ($query) use ($keyword) {
                    $query->where('first_name', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('last_name', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('email', 'LIKE', '%' . $keyword . '%');
                });
            }

            if ($filterStatus !== 'none') {
                $model = $model->where('status', $filterStatus);
            }
        }

        $model = $model->where('id', '!=', $this->currentUser->id)->whereIn('role_id', $roleIds);
        $total = $model->count();
        $users = $model->paginate($perPage);

        return view('admin.users.index')
                        ->with(array(
                            'breadcrumb' => $breadcrumb,
                            'total' => $total,
                            'users' => $users,
                            'limit' => $perPage,
                            'keyword' => $keyword,
                            'filterStatus' => $filterStatus
        ));
    }

    /**
     * Show the form for creating a new Users.
     *
     * @return Response
     */
    public function create() {
        $breadcrumb = '<li><a href="' . route('admin.users.index') . '">Administrators Management</a></li><li class="active">Add new Administrator</li>';
        $modelRole = new Roles();
        $roles = $modelRole->whereNotIn('id', $modelRole->getHiddenRoles())->where('type', Roles::ROLE_USER)->get();

        if (\Session::hasOldInput('image_tmp')) {
            $oldAvatar = \Session::getOldInput('image_tmp');
        }

        if (\Session::hasOldInput('srcImage')) {
            $oldAvatar = \Session::getOldInput('srcImage');
        }

        return view('admin.users.create', [
            'breadcrumb' => $breadcrumb,
            'roles' => $roles,
            'oldAvatar' => !empty($oldAvatar) ? $oldAvatar : '',
        ]);
    }

    /**
     * Store a newly created Users in storage.
     *
     * @param CreateUsersRequest $request
     *
     * @return Response
     */
    public function store(Request $request) {
        $files = $request->file();
        $input = $request->all();
        $rootPassword = $this->returnRandomString();
        $input['password'] = Hash::make($rootPassword);
        $toolImage = new Image();
        $validator = Validator::make($input, [
                    'role_id' => 'required',
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'image' => config('file.image.validate')
                ], [
                    'role_id.required' => 'The role field is required',
                    'image.max' => 'The avatar field' . config('file.image.validate_message')
                ]
        );

        if (!empty($files['image'])) {
            $checkImageBase64 = $toolImage->convertImageToBase64($files['image']->getPathname());

            if ($checkImageBase64['success']) {
                $input['srcImage'] = $checkImageBase64['data'];
            } else {
                Flash::error('Convert image to base64 have failed!');
                return redirect(route('admin.users.create'))->withInput($input);
            }
        }

        if ($validator->fails()) {
            return redirect(route('admin.users.create'))
                            ->withErrors($validator)
                            ->withInput($input);
        }

        if (!empty($input['image_tmp'])) {
            $data = $toolImage->convertBase64ToImage($input['image_tmp'], config('filepath.user_image'));

            if ($data['success']) {
                $input['avatar'] = $data['data'];
            } else {
                $input['srcImage'] = $input['image_tmp'];
                Flash::error('Convert base64 to image have failed!');
                return redirect(route('admin.users.create'))->withInput($input);
            }
        }

        if (!empty($files['image'])) {
            $uploadImage = $this->commonService->uploadFile($files['image'], config('filepath.user_image'));

            if (!empty($uploadImage)) {
                $input['avatar'] = $uploadImage;
            }
        }

        $input['name'] = $input['first_name'] .' '. $input['last_name'];                
        $this->usersRepository->create($input);

        // Send mail account info
        $this->sendEmail($input['email'], 'Account information', 'Password: ' . $rootPassword);

        Flash::success('Successful create new administrator!');

        return redirect(route('admin.users.index'));
    }

    /**
     * Display the specified Users.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id) {                
        $users = $this->usersRepository->findWithoutFail($id);
        $breadcrumb = '<li><a href="' . route('admin.users.index') . '">Administrators Management</a></li><li class="active text-more-breadcrumb">Detail Administrator: "'. $users->name .'"</li>';

        if (empty($users)) {
            Flash::error('Administrator not found');

            return redirect(route('admin.users.index'));
        }

        return view('admin.users.show')->with(array(
                    'breadcrumb' => $breadcrumb,
                    'users' => $users
        ));
    }

    /**
     * Show the form for editing the specified Users.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id) {
        $breadcrumb = '<li><a href="' . route('admin.users.index') . '">Administrators Management</a></li><li class="active">Edit Administrator</li>';
        $users = $this->usersRepository->findWithoutFail($id);
        
        if (empty($users)) {
            Flash::error('Administrator not found');

            return redirect(route('admin.users.index'));
        }
        
        $modelRole = new Roles();
        $roles = $modelRole->whereNotIn('id', $modelRole->getHiddenRoles())->where('type', Roles::ROLE_USER)->get();        

        return view('admin.users.edit')->with([
                    'breadcrumb' => $breadcrumb,
                    'roles' => $roles,
                    'users' => $users
        ]);
    }

    /**
     * Update the specified Users in storage.
     *
     * @param  int              $id
     * @param UpdateUsersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUsersRequest $request) {
        $files = $request->file();
        $input = $request->all();
        $users = $this->usersRepository->findWithoutFail($id);

        if (empty($users)) {
            Flash::error('Administrator not found');

            return redirect(route('admin.users.index'));
        }

        if (!empty($input['new_password'])) {
            $input['password'] = Hash::make($input['new_password']);
        }

        if (!empty($files['image'])) {
            $uploadImage = $this->commonService->uploadFile($files['image'], config('filepath.user_image'));

            if (!empty($uploadImage)) {
                $input['avatar'] = $uploadImage;
            }

            $this->commonService->removeFile($users->avatar, config('filepath.user_image'));
        } else {
            if (!empty($input['image_tmp'])) {
                $input['avatar'] = $input['image_tmp'];
                unset($input['image_tmp']);
            } else {
                $input['avatar'] = null;
                $this->commonService->removeFile($users->avatar, config('filepath.user_image'));
            }
        }

        $input['name'] = $input['first_name'] .' '. $input['last_name'];        
        $this->usersRepository->update($input, $id);

        Flash::success('Successful edit administrator!');

        return redirect(route('admin.users.index'));
    }

    /**
     * Remove the specified Users from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id) {
        $users = $this->usersRepository->findWithoutFail($id);

        if (empty($users)) {
            Flash::error('Administrator not found');

            return redirect(route('admin.users.index'));
        }

        $this->usersRepository->delete($id);

        Flash::success('Successful delete administrator!');

        return redirect(route('admin.users.index'));
    }

    /**
     * Remove the specified Course from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function bulkActions(Request $request) {
        $input = $request->all();

        if (empty($input['ids']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Invalid information. Please try again.');
        }

        $value = $input['value'];
        $key = $input['key'];
        $ids = explode(',', $input['ids']);
        $getModel = $this->usersRepository->model();
        $model = new $getModel;
        $datas = $model->whereIn('id', $ids)->get();

        if ($key == 'status') {
            foreach ($datas as $item) {
                $item->status = $value;
                $item->save();
            }
        } else if ($key == 'delete') {
            foreach ($datas as $item) {
                if ($item->role_id == 1) {
                    return $this->responseApp(false, 1, "Can't delete.");
                }

                $item->delete();
            }
        }

        return $this->responseApp(true, 200, 'Process successfully.');
    }

    /**
     * Remove the specified ActivationCode from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function itemActions(Request $request) {
        $input = $request->all();

        if (empty($input['id']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Invalid information. Please try again.');
        }

        $value = $input['value'];
        $key = $input['key'];
        $id = $input['id'];
        $getModel = $this->usersRepository->model();
        $model = new $getModel;
        $data = $model->find($id);

        if (empty($data)) {
            return $this->responseApp(false, 404, 'Not found.');
        }

        if ($key == 'status') {
            $data->status = $value;
            $data->save();
        } else if ($key == 'delete') {
            if ($data->role_id == 1) {
                return $this->responseApp(false, 1, "Can't delete.");
            }

            $data->delete();
        }

        return $this->responseApp(true, 200, 'Process successfully.');
    }

}
