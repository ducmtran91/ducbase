<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\DetailPostsRepositories;
use App\Repositories\PostsRepositories;
use App\Http\Controllers\AppBaseController;
use App\Validators\DetailPostsValidator;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Validator\Exceptions\ValidatorException;

class DetailPostsController extends AppBaseController
{

    /** @var  $repository */
    private $repository;
    private $model;
    private $statuses;
    private $validation;
    private $postsRepositories;

    public function __construct(
        DetailPostsRepositories $repository,
        PostsRepositories $postsRepositories,
        DetailPostsValidator $validation
    )
    {
        parent::__construct();
        $this->repository = $repository;
        $this->postsRepositories = $postsRepositories;
        $this->validation = $validation;
        $getModel = $repository->model();
        $this->model = new $getModel();
        $this->statuses = $this->model->statuses();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->repository->pushCriteria(new RequestCriteria($request));
        $breadcrumb = '<li><a href="' . route('admin.detailPosts.index') . '">Detail Posts Management</a></li><li class="active">List</li>';
        $input = $request->all();
        $perPage = !empty($input['limit']) ? $input['limit'] : $this->perPage;
        $keyword = !empty($input['keyword']) ? trim($input['keyword']) : '';
        $filterStatus = isset($input['filter_status']) && $input['filter_status'] !== 'none' ? (int)$input['filter_status'] : 'none';
        $model = $this->model;
        if (!empty($keyword) || ($filterStatus !== 'none')) {
            if (!empty($keyword)) {
                $model = $model->where(function ($query) use ($keyword) {
                    $query->where('name', 'LIKE', '%' . $keyword . '%');
                });
            }

            if ($filterStatus !== 'none') {
                $model = $model->where('status', $filterStatus);
            }
        }

        $total = $model->count();
        $datas = $model->paginate($perPage);

        return view('admin.detail_posts.index')
            ->with(array(
                'breadcrumb' => $breadcrumb,
                'total' => $total,
                'datas' => $datas,
                'limit' => $perPage,
                'keyword' => $keyword,
                'filterStatus' => $filterStatus,
                'statuses' => $this->statuses
            ));
    }

    /**
     * Show the form for creating a new posts.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $breadcrumb = '<li><a href="' . route('admin.detailPosts.index') . '">Detail Posts Management</a></li><li class="active">Add new Detail Post</li>';
        $posts = $this->postsRepositories->all();

        return view('admin.detail_posts.create', [
            'breadcrumb' => $breadcrumb,
            'posts' => $posts,
        ]);
    }

    /**
     * Store a newly created posts in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {

            $validation = $this->validation;
            $validator = $validation->with( $request->all())->passes( $validation::RULE_CREATE );
            $data = $request->all();
            if (!$validator) {
                return redirect(route('admin.detailPosts.create'))
                    ->withErrors($validation->errors())
                    ->withInput($data);
            }
            $this->insertDetail($request);
            Flash::success(trans('messages.store.success'));
            return redirect(route('admin.detailPosts.index'));

        } catch (ValidatorException $exception) {
            Flash::success($exception->getMessage());
            return redirect(route('admin.detailPosts.create'));
        }
    }

    /**
     * Display the specified posts.
     * @param  int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $data = $this->repository->findWithoutFail($id);
        $breadcrumb = '<li><a href="' . route('admin.detailPosts.index') . '">Detail Posts Management</a></li><li class="active text-more-breadcrumb">Detail Post: "' . $data->name . '"</li>';

        if (empty($data)) {
            Flash::error(trans('messages.show.not_found'));
            return redirect(route('admin.detailPosts.index'));
        }

        return view('admin.detail_posts.show')->with(array(
            'breadcrumb' => $breadcrumb,
            'data' => $data
        ));
    }

    /**
     * Show the form for editing the specified posts.
     * @param  int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $breadcrumb = '<li><a href="' . route('admin.detailPosts.index') . '">Detail Posts Management</a></li><li class="active">Edit Detail Post</li>';
        $data = $this->repository->findWhere(['post_id' => $id])->all();
        $posts = $this->postsRepositories->all();

        if (empty($data)) {
            Flash::error(trans('messages.show.not_found'));

            return redirect(route('admin.detailPosts.index'));
        }

        return view('admin.detail_posts.edit')->with([
            'breadcrumb' => $breadcrumb,
            'posts' => $posts,
            'data' => $data,
        ]);
    }

    /**
     * Update the specified categories in storage.
     * @param  int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            $model = $this->model;
            $validation = $this->validation;
            $validation->setId($id);
            $request->merge(['status' => ($request->get('status', 0)) ? $model::STATUS_ENABLE : 0]);
            $validator = $validation->with( $request->all())->passes( $validation::RULE_UPDATE );
            if (!$validator) {
                return redirect(route('admin.detailPosts.edit', ['detailPosts' => $id]))
                    ->withErrors($validation->errors())
                    ->withInput($request->all());
            }
            //delete
            $this->repository->deleteWhere(['post_id' => $id]);
            //insert
            $this->insertDetail($request);

            Flash::success(trans('messages.edit.success'));
            return redirect(route('admin.detailPosts.index'));

        } catch (ValidatorException $exception) {
            Flash::success($exception->getMessage());
            return redirect(route('admin.detailPosts.edit', ['detailPosts' => $id]));
        }
    }

    /**
     * @param $request
     * @throws ValidatorException
     */
    private function insertDetail($request) {
        $model = $this->model;
        $arrayNames = $request->get('name');
        $arrayDescriptions = $request->get('description');
        $arrayPositions = $request->get('position');
        $status = $request->get('status') ? $model::STATUS_ENABLE : 0;
        $postId = $request->get('post_id');
        foreach ($arrayNames as $key => $value) {
            $this->repository->create([
                'post_id' => $postId,
                'name' => $value,
                'description' => $arrayDescriptions[$key],
                'position' => $arrayPositions[$key],
                'status' => $status
            ]);
        }
    }

    /**
     * Remove the specified categories from storage.
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $category = $this->repository->findWithoutFail($id);

        if (empty($category)) {
            Flash::error(trans('messages.show.not_found'));

            return redirect(route('admin.detailPosts.index'));
        }

        $this->repository->delete($id);

        Flash::success(trans('messages.delete.success'));

        return redirect(route('admin.detailPosts.index'));
    }

    /**
     * Remove the specified Course from storage.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkActions(Request $request)
    {
        $id = $request->get('ids');
        $key = $request->get('key');
        $value = $request->get('value');
        if($this->checkValueAction($id, $key, $value)){
            return $this->responseApp(false, 404, trans('messages.validator.invalid'));
        }

        $ids = explode(',', $id);
        $datas = $this->model->whereIn('id', $ids)->get();

        if ($key == 'status') {
            foreach ($datas as $item) {
                $item->status = $value;
                $item->save();
            }
        } else if ($key == 'delete') {
            foreach ($datas as $item) {
                $item->delete();
            }
        }

        return $this->responseApp(true, 200, trans('messages.action.success'));
    }

    /**
     * Remove the specified ActivationCode from storage.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function itemActions(Request $request)
    {
        $id = $request->get('id');
        $key = $request->get('key');
        $value = $request->get('value');
        if($this->checkValueAction($id, $key, $value)){
            return $this->responseApp(false, 404, trans('messages.validator.invalid'));
        }
        $data = $this->model->find($id);

        if (empty($data)) {
            return $this->responseApp(false, 404, trans('messages.show.not_found'));
        }

        if ($key == 'status') {
            $data->status = $value;
            $data->save();
        } else if ($key == 'delete') {
            $data->delete();
        }

        return $this->responseApp(true, 200, trans('messages.action.success'));
    }

    /**
     * @param $id
     * @param $key
     * @param $value
     * @return bool
     */
    private function checkValueAction($id, $key, $value) {
        $error = false;
        if (!$id || !$key || !isset($value)) {
            $error = true;

        }
        return $error;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getTemplate() {
        return $this->responseAppSuccess(true, 200, view("admin.detail_posts.partials.tem_detail")->render());
    }
}
