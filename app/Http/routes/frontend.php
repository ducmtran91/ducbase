<?php
/*
 |-------------------------------------------------------------------
 |  Auth Route
 |-------------------------------------------------------------------
 */
/*
 | HOME route
 */
//Route::get('/', ['as' => "front", 'uses' => 'App\Http\Controllers\HomeController@index']);

/*
 |-------------------------------------------------------------------
 |  Frontend Route not Authorize
 |-------------------------------------------------------------------
 | CMS route
 | Route fixed
 */
/*Route::get('/user-guide', ['as' => "{$this->locale}.userguide.index", 'uses' => 'App\Modules\UserGuide\Controllers\Frontend\UserGuideController@index']);
Route::get('news', ['as' => "{$this->locale}.category.news", 'uses' => 'App\Modules\ContentManager\Controllers\CategoryController@news']);

// Route dynamic
Route::get('{slug?}.html', ['as' => "{$this->locale}.page.show", 'uses' => 'App\Modules\ContentManager\Controllers\PageController@show']);
Route::get('{slug?}', ['as' => "{$this->locale}.post.show", 'uses' => 'App\Modules\ContentManager\Controllers\PostController@show']);
Route::get('category/{slug?}', ['as' => "{$this->locale}.category.show", 'uses' => 'App\Modules\ContentManager\Controllers\CategoryController@show']);
Route::get('tag/{slug?}', ['as' => "{$this->locale}.tag.show", 'uses' => 'App\Modules\ContentManager\Controllers\TagController@show']);*/