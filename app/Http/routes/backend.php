<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
/*
  |--------------------------------------------------------------------------
  | Backend routes of Role
  |--------------------------------------------------------------------------
 */
Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function () {
    Route::get('roles', ['as' => 'admin.roles.index', 'uses' => 'RolesController@index']);
    Route::post('roles', ['as' => 'admin.roles.store', 'uses' => 'RolesController@store']);
    Route::get('roles/create', ['as' => 'admin.roles.create', 'uses' => 'RolesController@create']);
    Route::put('roles/{roles}', ['as' => 'admin.roles.update', 'uses' => 'RolesController@update'])->where('roles', '[0-9]+');
    Route::patch('roles/{roles}', ['as' => 'admin.roles.update', 'uses' => 'RolesController@update'])->where('roles', '[0-9]+');
    Route::delete('roles/{roles}', ['as' => 'admin.roles.destroy', 'uses' => 'RolesController@destroy'])->where('roles', '[0-9]+');
    Route::get('roles/{roles}/edit', ['as' => 'admin.roles.edit', 'uses' => 'RolesController@edit'])->where('roles', '[0-9]+');
    Route::put('roles/item-actions', ['as' => 'admin.roles.itemActions', 'uses' => 'RolesController@itemActions']);
    Route::put('roles/bulk-actions', ['as' => 'admin.roles.bulkActions', 'uses' => 'RolesController@bulkActions']);
});

/*
  |--------------------------------------------------------------------------
  | Backend routes of Admin
  |--------------------------------------------------------------------------
 */
Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function () {
    Route::get('admin/{profiles}/profiles', ['as' => 'admin.admin.profiles', 'uses' => 'AdminController@profiles']);
    Route::get('admin/{profiles}/profiles/edit', ['as' => 'admin.admin.getProfiles', 'uses' => 'AdminController@getProfiles']);
    Route::put('admin/{profiles}/profiles', ['as' => 'admin.admin.postProfiles', 'uses' => 'AdminController@postProfiles']);
});

/*
  |--------------------------------------------------------------------------
  | Backend routes of Users
  |--------------------------------------------------------------------------
 */
Route::group(['prefix' => 'admin/', 'middleware' => ['admin']], function () {
    Route::get('users', ['as' => 'admin.users.index', 'uses' => 'UsersController@index']);
    Route::post('users', ['as' => 'admin.users.store', 'uses' => 'UsersController@store']);
    Route::get('users/create', ['as' => 'admin.users.create', 'uses' => 'UsersController@create']);
    Route::put('users/{users}', ['as' => 'admin.users.update', 'uses' => 'UsersController@update'])->where('users', '[0-9]+');
    Route::patch('users/{users}', ['as' => 'admin.users.update', 'uses' => 'UsersController@update'])->where('users', '[0-9]+');
    Route::delete('users/{users}', ['as' => 'admin.users.destroy', 'uses' => 'UsersController@destroy'])->where('users', '[0-9]+');
    Route::get('users/{users}', ['as' => 'admin.users.show', 'uses' => 'UsersController@show'])->where('users', '[0-9]+');
    Route::get('users/{users}/edit', ['as' => 'admin.users.edit', 'uses' => 'UsersController@edit'])->where('users', '[0-9]+');
    Route::put('users/item-actions', ['as' => 'admin.users.itemActions', 'uses' => 'UsersController@itemActions']);
    Route::put('users/bulk-actions', ['as' => 'admin.users.bulkActions', 'uses' => 'UsersController@bulkActions']);
});


/*
|--------------------------------------------------------------------------
| Backend routes of categories
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'admin/', 'middleware' => ['admin']], function () {
    Route::get('categories', ['as' => 'admin.categories.index', 'uses' => 'CategoriesController@index']);
    Route::post('categories', ['as' => 'admin.categories.store', 'uses' => 'CategoriesController@store']);
    Route::get('categories/create', ['as' => 'admin.categories.create', 'uses' => 'CategoriesController@create']);
    Route::put('categories/{categories}', ['as' => 'admin.categories.update', 'uses' => 'CategoriesController@update'])->where('categories', '[0-9]+');
    Route::patch('categories/{categories}', ['as' => 'admin.categories.update', 'uses' => 'CategoriesController@update'])->where('categories', '[0-9]+');
    Route::delete('categories/{categories}', ['as' => 'admin.categories.destroy', 'uses' => 'CategoriesController@destroy'])->where('categories', '[0-9]+');
    Route::get('categories/{categories}', ['as' => 'admin.categories.show', 'uses' => 'CategoriesController@show'])->where('categories', '[0-9]+');
    Route::get('categories/{categories}/edit', ['as' => 'admin.categories.edit', 'uses' => 'CategoriesController@edit'])->where('categories', '[0-9]+');
    Route::put('categories/item-actions', ['as' => 'admin.categories.itemActions', 'uses' => 'CategoriesController@itemActions']);
    Route::put('categories/bulk-actions', ['as' => 'admin.categories.bulkActions', 'uses' => 'CategoriesController@bulkActions']);
});

/*
  |--------------------------------------------------------------------------
  | Backend routes of posts
  |--------------------------------------------------------------------------
 */
Route::group(['prefix' => 'admin/', 'middleware' => ['admin']], function () {
    Route::get('posts', ['as' => 'admin.posts.index', 'uses' => 'PostsController@index']);
    Route::post('posts', ['as' => 'admin.posts.store', 'uses' => 'PostsController@store']);
    Route::get('posts/create', ['as' => 'admin.posts.create', 'uses' => 'PostsController@create']);
    Route::put('posts/{posts}', ['as' => 'admin.posts.update', 'uses' => 'PostsController@update'])->where('posts', '[0-9]+');
    Route::patch('posts/{posts}', ['as' => 'admin.posts.update', 'uses' => 'PostsController@update'])->where('posts', '[0-9]+');
    Route::delete('posts/{posts}', ['as' => 'admin.posts.destroy', 'uses' => 'PostsController@destroy'])->where('posts', '[0-9]+');
    Route::get('posts/{posts}', ['as' => 'admin.posts.show', 'uses' => 'PostsController@show'])->where('posts', '[0-9]+');
    Route::get('posts/{posts}/edit', ['as' => 'admin.posts.edit', 'uses' => 'PostsController@edit'])->where('posts', '[0-9]+');
    Route::put('posts/item-actions', ['as' => 'admin.posts.itemActions', 'uses' => 'PostsController@itemActions']);
    Route::put('posts/bulk-actions', ['as' => 'admin.posts.bulkActions', 'uses' => 'PostsController@bulkActions']);
});

/*
  |--------------------------------------------------------------------------
  | Backend routes of detailPosts
  |--------------------------------------------------------------------------
 */
Route::group(['prefix' => 'admin/', 'middleware' => ['admin']], function () {
    Route::get('detailPosts', ['as' => 'admin.detailPosts.index', 'uses' => 'DetailPostsController@index']);
    Route::post('detailPosts', ['as' => 'admin.detailPosts.store', 'uses' => 'DetailPostsController@store']);
    Route::get('detailPosts/create', ['as' => 'admin.detailPosts.create', 'uses' => 'DetailPostsController@create']);
    Route::put('detailPosts/{detailPosts}', ['as' => 'admin.detailPosts.update', 'uses' => 'DetailPostsController@update'])->where('detailPosts', '[0-9]+');
    Route::patch('detailPosts/{detailPosts}', ['as' => 'admin.detailPosts.update', 'uses' => 'DetailPostsController@update'])->where('detailPosts', '[0-9]+');
    Route::delete('detailPosts/{detailPosts}', ['as' => 'admin.detailPosts.destroy', 'uses' => 'DetailPostsController@destroy'])->where('detailPosts', '[0-9]+');
    Route::get('detailPosts/{detailPosts}', ['as' => 'admin.detailPosts.show', 'uses' => 'DetailPostsController@show'])->where('detailPosts', '[0-9]+');
    Route::get('detailPosts/{detailPosts}/edit', ['as' => 'admin.detailPosts.edit', 'uses' => 'DetailPostsController@edit'])->where('detailPosts', '[0-9]+');
    Route::put('detailPosts/item-actions', ['as' => 'admin.detailPosts.itemActions', 'uses' => 'DetailPostsController@itemActions']);
    Route::put('detailPosts/bulk-actions', ['as' => 'admin.detailPosts.bulkActions', 'uses' => 'DetailPostsController@bulkActions']);
    Route::get('detailPosts/get-template', ['as' => 'admin.detailPosts.getTemplate', 'uses' => 'DetailPostsController@getTemplate']);
});

/*
|--------------------------------------------------------------------------
| Backend routes of slider
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'admin/', 'middleware' => ['admin']], function () {
    Route::get('slider', ['as' => 'admin.slider.index', 'uses' => 'SliderController@index']);
    Route::post('slider', ['as' => 'admin.slider.store', 'uses' => 'SliderController@store']);
    Route::get('slider/create', ['as' => 'admin.slider.create', 'uses' => 'SliderController@create']);
    Route::put('slider/{slider}', ['as' => 'admin.slider.update', 'uses' => 'SliderController@update'])->where('slider', '[0-9]+');
    Route::patch('slider/{slider}', ['as' => 'admin.slider.update', 'uses' => 'SliderController@update'])->where('slider', '[0-9]+');
    Route::delete('slider/{slider}', ['as' => 'admin.slider.destroy', 'uses' => 'SliderController@destroy'])->where('slider', '[0-9]+');
    Route::get('slider/{slider}', ['as' => 'admin.slider.show', 'uses' => 'SliderController@show'])->where('slider', '[0-9]+');
    Route::get('slider/{slider}/edit', ['as' => 'admin.slider.edit', 'uses' => 'SliderController@edit'])->where('slider', '[0-9]+');
    Route::put('slider/item-actions', ['as' => 'admin.slider.itemActions', 'uses' => 'SliderController@itemActions']);
    Route::put('slider/bulk-actions', ['as' => 'admin.slider.bulkActions', 'uses' => 'SliderController@bulkActions']);
});

/*
  |--------------------------------------------------------------------------
  | Backend routes of banner
  |--------------------------------------------------------------------------
 */
Route::group(['prefix' => 'admin/', 'middleware' => ['admin']], function () {
    Route::get('banner', ['as' => 'admin.banner.index', 'uses' => 'BannerController@index']);
    Route::post('banner', ['as' => 'admin.banner.store', 'uses' => 'BannerController@store']);
    Route::get('banner/create', ['as' => 'admin.banner.create', 'uses' => 'BannerController@create']);
    Route::put('banner/{banner}', ['as' => 'admin.banner.update', 'uses' => 'BannerController@update'])->where('banner', '[0-9]+');
    Route::patch('banner/{banner}', ['as' => 'admin.banner.update', 'uses' => 'BannerController@update'])->where('banner', '[0-9]+');
    Route::delete('banner/{banner}', ['as' => 'admin.banner.destroy', 'uses' => 'BannerController@destroy'])->where('banner', '[0-9]+');
    Route::get('banner/{banner}', ['as' => 'admin.banner.show', 'uses' => 'BannerController@show'])->where('banner', '[0-9]+');
    Route::get('banner/{banner}/edit', ['as' => 'admin.banner.edit', 'uses' => 'BannerController@edit'])->where('banner', '[0-9]+');
    Route::put('banner/item-actions', ['as' => 'admin.banner.itemActions', 'uses' => 'BannerController@itemActions']);
    Route::put('banner/bulk-actions', ['as' => 'admin.banner.bulkActions', 'uses' => 'BannerController@bulkActions']);
});