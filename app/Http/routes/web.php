<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::get('/', ['as' => "front", 'uses' => 'Frontend\HomeController@index']);
Route::get('/category/{category}', ['as' => 'front.category.index', 'uses' => 'Frontend\CategoriesController@index']);
Route::post('/category/postAjax', ['as' => 'front.category.postAjax', 'uses' => 'Frontend\CategoriesController@getAjaxPosts']);
Route::get('/{category}/{slug}.html', ['as' => 'front.post.detail', 'uses' => 'Frontend\PostsController@detail']);
/*
 |-------------------------------------------------------------------
 |  Auth Route
 |-------------------------------------------------------------------
 */
Route::group(['namespace' => 'Auth'], function () {
    // Middleware Guest
    Route::group(['middleware' => ['guest:member']], function () {
        // LOGIN
        Route::get('/login', ['as' => 'auth.login.form', 'uses' => 'LoginController@showLoginForm']);
        Route::post('/login', ['as' => 'auth.login.attempt', 'uses' => 'LoginController@login']);
        Route::get('/auth/{driver}', ['as' => 'auth.socialite.provider', 'uses' => 'LoginController@redirectToProvider']);
        Route::get('/auth/{driver}/callback', ['as' => 'auth.socialite.callback', 'uses' => 'LoginController@handleProviderCallback']);
        // REGISTER
        Route::get('/register', ['as' => 'auth.register.form', 'uses' => 'RegisterController@showRegistrationForm']);
        Route::post('/register', ['as' => 'auth.register.store', 'uses' => 'RegisterController@register']);
        Route::get('/register/success', ['as' => 'auth.register.success', 'uses' => 'RegisterController@success']);
        Route::get('/register/confirm', ['as' => 'auth.register.confirm', 'uses' => 'RegisterController@confirm']);
        // PASSWORD
        Route::get('/password/reset', ['as' => 'auth.password.resetForm', 'uses' => 'PasswordController@showLinkRequestForm']);
        Route::post('/password/email', ['as' => 'auth.password.email', 'uses' => 'PasswordController@sendResetLinkEmail']);
        Route::get('password/reset/{token}', ['as' => 'auth.password.token', 'uses' => 'PasswordController@showResetForm']);
        Route::post('password/reset', ['as' => 'auth.password.reset', 'uses' => 'PasswordController@reset']);
    });
    // Middleware Auth
    Route::group(['middleware' => ['web']], function () {
        Route::get('/login', ['as' => 'auth.login.form', 'uses' => 'LoginController@showLoginForm']);
        Route::get('/logout', ['as' => 'auth.logout', 'uses' => 'LoginController@logout']);
    });

});
Route::group(
    [
        'namespace'=>'Frontend',
        'middleware' => ['web']
    ], function(){

    Route::get('/', function () {
        echo "Hello Backend. <a href='" . url('/logout') . "'>Logout</a>";
    });

    Route::get('/login', ['as' => 'frontend.login', 'uses' => 'AuthController@getLogin']);

}
);
