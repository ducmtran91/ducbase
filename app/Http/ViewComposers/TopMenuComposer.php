<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 11/9/2018
 * Time: 5:48 PM
 */

namespace App\Http\ViewComposers;

use App\Repositories\CategoriesRepositories;
use Illuminate\View\View;

class TopMenuComposer
{
    /**
     * @var CategoriesRepositories
     */
    protected $categoriesRepositories;

    /**
     * TopMenuComposer constructor.
     * @param CategoriesRepositories $categoriesRepositories
     */
    public function __construct(CategoriesRepositories $categoriesRepositories)
    {
        $this->categoriesRepositories = $categoriesRepositories;
    }

    public function compose(View $view) {
        $view->with('menus', $this->categoriesRepositories->tree());
    }
}