<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 11/15/2018
 * Time: 2:42 PM
 */

namespace App\Http\ViewComposers;
use App\Repositories\LanguagesRepositories;
use Illuminate\View\View;

class LanguagesComposer
{
    /**
     * @var LanguagesRepositories
     */
    protected $languagesRepositories;

    /**
     * TopMenuComposer constructor.
     * @param LanguagesRepositories $languagesRepositories
     */
    public function __construct(LanguagesRepositories $languagesRepositories)
    {
        $this->languagesRepositories = $languagesRepositories;
    }

    public function compose(View $view) {
        $getModel = $this->languagesRepositories->model();
        $model = new $getModel();
        $view->with('languages', $this->languagesRepositories->findWhere(['status' => $model::STATUS_ENABLE]));
    }
}