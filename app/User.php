<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\UserACL;
use App\Traits\UserCustom;

/**
 * Class User
 * @package App
 *
 * @property int id
 */
class User extends Authenticatable {

    use UserACL,
        UserCustom;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'email',
        'api_token',
        'role_id',
        'avatar',
        'description',
        'phone',
        'status',
        'provider',
        'provider_id'
    ];
    
    protected $cast = [
        'is_admin' => 'boolean',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role() {
        return $this->hasOne(\App\Entities\Roles::class);
    }

    /**
     * @param \App\User|null $user
     * @return bool
     */
    public function isAdmin($role) {
        if (empty($role)) {
            return false;
        }
        
        return ($role->type == 'user');
    }

    /**
     * @param \App\User|null $user
     * @return bool
     */
    public function isSuperAdmin($user = null) {
        if (empty($user)) {
            $user = $this;
        }

        return ($user->is_admin);
    }

}
