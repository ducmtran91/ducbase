<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Schema;
// use Trans;
use App;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    protected $namespaceBackend = 'App\Http\Controllers\Backend';
    protected $namespaceFrontend = 'App\Http\Controllers\Frontend';
    protected $namespaceAPI = 'App\Http\Controllers\API';
    protected $locale = 'en';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    public function map(Router $router)
    {
        $this->mapBackendRoutes($router);
        $this->mapFrontendRoutes($router);
        $this->mapAPIRoutes($router);
        $this->mapWebRoutes($router);
    }

    /**
     * Define the "backend" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    protected function mapBackendRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespaceBackend,
            'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/routes/backend.php');
        });
    }

    /**
     * Define the "frontend" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    protected function mapFrontendRoutes(Router $router)
    {
        $this->locale = App::getLocale();
        $router->get('/', function () {
            return redirect($this->locale);
        });

        /*$locales = Schema::hasTable('countries') ? \App\Modules\LanguageManager\Models\Countries::all()->pluck('locale')->toArray() : [];
        $locale = \Illuminate\Support\Facades\Request::segment(1);

        if (in_array($locale, $locales)) {
            $this->locale = Trans::setLocale($locale)->locale();

        }*/

        $router->group([
            'prefix' => $this->locale,
            'namespace' => $this->namespaceFrontend,
            'middleware' => ['web'],
        ], function ($router) {
            require app_path('Http/routes/frontend.php');
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    protected function mapAPIRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespaceAPI
        ], function ($router) {
            require app_path('Http/routes/api.php');
        });
    }

    /**
     * Define the "webLocale" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    protected function mapWebRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace,
            'middleware' => ['web'],
        ], function ($router) {
            require app_path('Http/routes/web.php');
        });

    }

}
