<?php

namespace App\Providers;

use App\Helpers\Booking;
use App\Helpers\QSoftVN;
use Carbon\Carbon;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Blade;
use View;
use Helper;
use Validator;
use Illuminate\Support\Facades\Hash;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('pushonce', function ($expression) {
            $isDisplayed = '__pushonce_' . trim(substr($expression, 2, -2));
            return "<?php if(!isset(\$__env->{$isDisplayed})): \$__env->{$isDisplayed} = true; \$__env->startPush{$expression}; ?>";
        });

        Blade::directive('endpushonce', function ($expression) {
            return '<?php $__env->stopPush(); endif; ?>';
        });

        View::share('appTitle', "");

        // Custom validator
        Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, current($parameters));
        });

        Validator::extend('before_hours', function ($attribute, $value, $parameters, $validator) {
            if (
                !empty($parameters[0]) &&
                !empty($parameters[1]) &&
                !empty($parameters[2])
            ) {
                $tz = new \DateTimeZone($parameters[2]);
                $timeNow = Carbon::now();
                $timeStart = Carbon::createFromFormat($parameters[1], $value, $tz);

                return $timeNow->diffInHours($timeStart, false) >= $parameters[0];
            }
            return false;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->booting(function () {
            $loader = AliasLoader::getInstance();
            $loader->alias('QSoftVN', 'App\Facades\QSoftVN');
            $loader->alias('Booking', 'App\Facades\Booking');
        });

        $this->app->singleton(QSoftVN::class, function ($app) {
            return new QSoftVN();
        });

        $this->app->singleton(Booking::class, function ($app) {
            return new Booking();
        });
    }
}
