<?php

namespace App\Exceptions;

use Illuminate\Support\Collection;

class CustomValidation
{
    protected $errors;

    protected $messages;

    public function __construct($errors = [], $messages = [])
    {
        $this->errors = $errors;
        $this->messages = $messages;
    }

    public function errors()
    {
        return collect($this->errors);
    }

    public function messages()
    {
        return collect($this->messages);
    }

    public function all()
    {
        if ($this->errors instanceof Collection) {
            return $this->errors->toArray();
        }

        if (is_array($this->errors)) {
            return $this->errors;
        }

        return (array)$this->errors;
    }
}