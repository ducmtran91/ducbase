<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Authorize extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'App\Helpers\Authorize';
    }
}