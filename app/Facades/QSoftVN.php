<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class QSoftVN extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \App\Helpers\QSoftVN::class;
    }
}