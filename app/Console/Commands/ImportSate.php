<?php

namespace App\Console\Commands;

use App\Entities\Tax;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use App\Entities\City;
use App\Entities\State;
use App\Entities\ZipCode;
use Carbon\Carbon;

class ImportSate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import state, city, zip code from file Excel.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');

        if ('tax' == $type) {
            $files = glob(storage_path("import/{$type}/*.csv"));
            foreach ($files as $file) {
                switch ($type) {
                    case 'tax':
                        $this->importTax($file);
                        break;
                }

            }
        } else {
            $files = glob(storage_path('data/*.csv'));
            foreach ($files as $file) {
                switch ($type) {
                    case 'state':
                        $this->importState($file);
                        break;
                    case 'city':
                        $this->importCity($file);
                        break;
                    case 'zip_code':
                        $this->importZipCode($file);
                        break;
                }
            }
        }

    }

    /**
     * Load data from files excel (csv)
     *
     * @return mixed
     */
    protected
    function loadFile($file)
    {
        return Excel::load($file, function ($excel) {
        });
    }

    /**
     * Import data from file excel to state table in DB.
     *
     * @return mixed
     */
    protected
    function importState($file)
    {
        $dataFile = $this->loadFile($file);
        $arrData = $dataFile->select(['state', 'abbreviation'])->toArray();
        $datas = collect($arrData)
            ->unique()
            ->map(function ($item) {
                return [
                    'name' => $item['state'],
                    'code' => $item['abbreviation'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];
            })
            ->values()
            ->chunk(1000);

        foreach ($datas as $data) {
            State::insert($data->toArray());
        }
    }

    /**
     * Import data from file excel to city table in DB.
     *
     * @return mixed
     */
    protected
    function importCity($file)
    {
        $dataFile = $this->loadFile($file);
        $arrData = $dataFile->select(['state', 'city'])->toArray();
        $states = State::select('id', 'name')->get();

        $datas = collect($arrData)
            ->unique()
            ->map(function ($item) use ($states) {
                if ($state = $states->where('name', $item['state'])->first()) {
                    return [
                        'name' => $item['city'],
                        'state_id' => $state->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];
                } else {
                    return null;
                }
            })
            ->reject(function ($item) {
                return is_null($item);
            })
            ->values()
            ->chunk(1000);

        foreach ($datas as $data) {
            City::insert($data->toArray());
        }

    }

    /**
     * Import data from file excel to zip_code table in DB.
     *
     * @return mixed
     */
    protected
    function importZipCode($file)
    {
        $dataFile = $this->loadFile($file);
        $arrData = $dataFile->select(['city', 'zip_code'])->toArray();
        $cities = City::select(['id', 'name'])->get();

        $datas = collect($arrData)
            ->unique()
            ->map(function ($item) use ($cities) {
                if ($city = $cities->where('name', $item['city'])->first()) {
                    return [
                        'name' => $item['zip_code'],
                        'city_id' => $city->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];
                } else {
                    return null;
                }
            })
            ->reject(function ($item) {
                return is_null($item);
            })
            ->values()
            ->chunk(1000);

        foreach ($datas as $data) {
            ZipCode::insert($data->toArray());
        }

    }

    protected
    function importTax($file)
    {
        $dataFile = $this->loadFile($file);
        $arrData = $dataFile->select(['state', 'tax_rate'])->toArray();
        $states = State::select('id', 'name')->get();
        $datas = collect($arrData)
            ->unique()
            ->map(function ($item) use ($states) {
                if ($state = $states->where('name', $item['state'])->first()) {
                    return [
                        'state_id' => $state->id,
                        'tax' => $item['tax_rate'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];
                } else {
                    return null;
                }

            })
            ->reject(function ($item) {
                return is_null($item);
            })
            ->values()
            ->chunk(1000);

        foreach ($datas as $data) {
            Tax::insert($data->toArray());
        }
    }
}