<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 10/25/2018
 * Time: 10:30 AM
 */

namespace App\Validators;

use Prettus\Validator\LaravelValidator;

class BannerValidator extends LaravelValidator
{
    /**
     * Specify Validator Rules
     * @var array
     */
    protected $rules = [
        'slug' => 'required|unique:gallery|max:255',
        'image' => 'mimes:jpg,jpeg,png|max:10240',
    ];

    protected $messages = [
        'image.max' => 'The image field must be less than 10MB'
    ];

    public function passes($action = null)
    {
        $rules      = $this->getRules($action);
        $messages   = $this->getMessages();
        $attributes = $this->getAttributes();
        $this->data = array_merge($this->data, ['slug' => !empty($this->data['slug']) ? $this->data['slug'] : str_slug($this->data[app()->getLocale()]['name'])]);
        $validator  = $this->validator->make($this->data, $rules, $messages, $attributes);
        $validator->after(function ($validator) {
            $lang = config('translatable.locales');
            foreach ($lang as $locale) {
                if(count(array_filter(array_values($this->data[$locale]))) != 1) {
                    $validator->errors()->add('data', 'Please enter full information!');
                    break;
                }
            }
        });

        if ($validator->fails()) {
            $this->errors = $validator->messages();
            return false;
        }

        return true;
    }
}