<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 10/25/2018
 * Time: 10:30 AM
 */

namespace App\Validators;

use Prettus\Validator\LaravelValidator;

class DetailPostsValidator extends LaravelValidator
{
    /**
     * Specify Validator Rules
     * @var array
     */
    protected $rules = [
        'name' => 'required',
        'description' => 'required',
        'post_id'=> 'required',
        'position' => 'required'
    ];

    public function passes($action = null)
    {
        $rules      = $this->getRules($action);
        $messages   = $this->getMessages();
        $attributes = $this->getAttributes();
        $validator  = $this->validator->make($this->data, $rules, $messages, $attributes);
        $validator->after(function ($validator) {
            $arrayNames = $this->data['name'];
            $arrayDescriptions = $this->data['description'];
            $arrayPositions = $this->data['position'];
            if(!is_array($arrayNames) || !is_array($arrayDescriptions) || !is_array($arrayPositions)) {
                $validator->errors()->add('data', 'Data invalid!');
            } else {
                $countName = count(array_filter($arrayNames));
                $countDescription = count(array_filter($arrayDescriptions));
                $countPosition = count(array_filter($arrayPositions));
                if ($countName != $countDescription || $countDescription != $countPosition) {
                    $validator->errors()->add('data', 'Please enter full information!');
                }
            }
        });

        if ($validator->fails()) {
            $this->errors = $validator->messages();
            return false;
        }

        return true;
    }
}