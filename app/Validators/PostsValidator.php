<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 10/25/2018
 * Time: 10:30 AM
 */

namespace App\Validators;

use Prettus\Validator\LaravelValidator;

class PostsValidator extends LaravelValidator
{
    /**
     * Specify Validator Rules
     * @var array
     */
    protected $rules = [
        'name' => 'required|max:255',
        'excerpt'  => 'required|max:255',
        'description'=> 'required',
        'category_id'=> 'required|numeric',
        'position' => 'required|numeric',
        'tags_id' => 'required'
    ];
}