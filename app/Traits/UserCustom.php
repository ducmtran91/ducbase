<?php

namespace App\Traits;

use Theme;

trait UserCustom {

    /**
     * Get the user's avatar attribute.
     *
     * @return string
     */
    public function getAvatarAttribute() {        
        return empty($this->attributes['avatar']) ? config('filepath.sample_image_show') . 'default-user.png' : config('filepath.user_image_show') . "/{$this->attributes['avatar']}";
    }

}
