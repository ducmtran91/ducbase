<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 10/29/2018
 * Time: 2:13 PM
 */

namespace App\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

abstract class TransformerAbstractOverride extends TransformerAbstract
{
    /**
     * Override this method to provide transformation definition
     * @param $node
     * @return mixed
     */
    public abstract function formatData($model);

    /**
     * This method provide common data transformation such as createdAt, createdBy, etc. so you do not have to re-declare it on every transformer file
     * @param $node
     * @return array
     */
    public function transform($model)
    {
        $defaultData = [
            'created_at' => $model->created_at ? Carbon::createFromFormat('Y-m-d H:i:s',$model->created_at) : null,
            'updated_at' => $model->updated_at ? Carbon::createFromFormat('Y-m-d H:i:s', $model->updated_at) : null,
        ];

        return array_merge($this->formatData($model), $defaultData);
    }
}