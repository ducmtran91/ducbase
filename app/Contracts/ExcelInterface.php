<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 10/15/2018
 * Time: 10:22 AM
 */
namespace App\Contracts;

interface ExcelInterface{
    public function export($data);
}