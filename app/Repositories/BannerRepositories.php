<?php

namespace App\Repositories;

use App\Entities\Banner;
use InfyOm\Generator\Common\BaseRepository;

class BannerRepositories extends BaseRepository
{
    protected $fieldSearchable = [
        'name',
        'status'
    ];

    /**
     * @return mixed
     */
    public function model()
    {
        return Banner::class;
    }
}
