<?php

namespace App\Repositories;

use App\Entities\PasswordReset;
use InfyOm\Generator\Common\BaseRepository;

class PasswordResetRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'email',
        'token',
        'created_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PasswordReset::class;
    }
}
