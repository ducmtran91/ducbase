<?php

namespace App\Repositories;

use App\Entities\Languages;
use InfyOm\Generator\Common\BaseRepository;

class LanguagesRepositories extends BaseRepository
{
    protected $fieldSearchable = [
        'name',
        'status'
    ];

    /**
     * @return mixed
     */
    public function model()
    {
        return Languages::class;
    }
}
