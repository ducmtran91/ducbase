<?php

namespace App\Repositories;

use App\Entities\Tags;
use InfyOm\Generator\Common\BaseRepository;

class TagsRepositories extends BaseRepository
{
    protected $fieldSearchable = [
        'name',
        'status'
    ];

    /**
     * @return mixed
     */
    public function model()
    {
        return Tags::class;
    }
}
