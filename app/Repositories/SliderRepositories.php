<?php

namespace App\Repositories;

use App\Entities\Slider;
use InfyOm\Generator\Common\BaseRepository;

class SliderRepositories extends BaseRepository
{
    protected $fieldSearchable = [
        'name',
        'status'
    ];

    /**
     * @return mixed
     */
    public function model()
    {
        return Slider::class;
    }
}
