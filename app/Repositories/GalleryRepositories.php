<?php

namespace App\Repositories;

use App\Entities\Gallery;
use InfyOm\Generator\Common\BaseRepository;

class GalleryRepositories extends BaseRepository
{
    protected $fieldSearchable = [
        'status'
    ];

    /**
     * @return mixed
     */
    public function model()
    {
        return Gallery::class;
    }
}
