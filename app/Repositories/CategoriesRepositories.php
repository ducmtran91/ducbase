<?php

namespace App\Repositories;

use App\Entities\Categories;
use InfyOm\Generator\Common\BaseRepository;

class CategoriesRepositories extends BaseRepository
{
    protected $fieldSearchable = [
        'name',
        'status'
    ];

    /**
     * @return mixed
     */
    public function model()
    {
        return Categories::class;
    }

    public function tree() {
        return static::with(implode('.', array_fill(0,3,'child')))->orderBy('position')->findWhere(['parent_id' => 0]);
    }
}
