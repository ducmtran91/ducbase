<?php

namespace App\Repositories;

use App\Entities\Posts;
use InfyOm\Generator\Common\BaseRepository;

class PostsRepositories extends BaseRepository
{
    protected $fieldSearchable = [
        'name',
        'status',
        'category_id'
    ];

    /**
     * @return mixed
     */
    public function model()
    {
        return Posts::class;
    }
}
