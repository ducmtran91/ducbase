<?php

namespace App\Repositories;

use App\Entities\Users;
use InfyOm\Generator\Common\BaseRepository;

class UsersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'role_id',
        'tax_id',
        'provider',
        'status',
        'is_admin',
        'name',
        'first_name',
        'last_name',
        'email',
        'password',
        'temp_password',
        'description',
        'api_token',
        'state_id',
        'city_id',
        'zip_code',
        'avatar',
        'remember_token',
        'phone',
        'gender',
        'address',
        'location',
        'credit_card_info',
        'credit_balance',
        'fb_id',
        'fb_token',
        'gg_id',
        'gg_token',
        'tw_id',
        'tw_token',
        'last_login',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Users::class;
    }

    /**
     * Get first data by multiple fields.
     *
     * @param array $where
     * @param array $columns
     *
     * @return mixed
     */
    public function firstWhere(array $where, $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();

        $this->applyConditions($where);

        $model = $this->model->first($columns);
        $this->resetModel();

        return $this->parserResult($model);
    }
}
