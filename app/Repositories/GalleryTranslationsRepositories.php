<?php

namespace App\Repositories;

use App\Entities\GalleryTranslations;
use InfyOm\Generator\Common\BaseRepository;

class GalleryTranslationsRepositories extends BaseRepository
{
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * @return mixed
     */
    public function model()
    {
        return GalleryTranslations::class;
    }
}
