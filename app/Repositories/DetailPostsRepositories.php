<?php

namespace App\Repositories;

use App\Entities\DetailPosts;
use InfyOm\Generator\Common\BaseRepository;

class DetailPostsRepositories extends BaseRepository
{
    protected $fieldSearchable = [
        'post_id',
        'name',
        'description',
        'position',
        'status'
    ];

    /**
     * @return mixed
     */
    public function model()
    {
        return DetailPosts::class;
    }
}
