@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="x_panel">
            <div class="x_title">
                <h2>Manage Users</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a id="btn-sel-del" style="display:none;"
                           data-url="{{ Admin::route('contentManager.user.index') }}/" href="#"
                           class="btn-toolbox danger"><i class="fa fa-trash"></i> Delete Selected Users</a></li>
                    <li><a href="{{ Admin::route('contentManager.user.create') }}" class="btn-toolbox success"><i
                                    class="fa fa-plus"></i> Create User</a></li>
                </ul>
                <div class="clearfix"></div>
            </div>

            {{--filter--}}
            <form action="" method="get" id="form_custom_list">
                <div class="div_option" style="margin-bottom: 10px">
                    <div class="col-xs-12 col-md-5 col-sm-5  pd-left-0">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="name">Name</label>
                            <div class="col-sm-10 input-group">
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                <input id="name" class="form-control search_name enter-submit" name="name"
                                       placeholder="Name..." value="{{$name or ''}}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Email</label>
                            <div class="col-sm-10 input-group">
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                <input id="email" class="form-control search_name enter-submit" name="email"
                                       placeholder="Email..." value="{{$email or ''}}"/>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-5 col-sm-5  pd-left-0">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="status">Status</label>
                            <div class="col-sm-10 input-group">
                                <select name="filter_status" class="form-control filter_header" id="status">
                                    <option value="none">Gender</option>
                                    <option value="0">
                                        Inactive
                                    </option>
                                    <option value="1">
                                        Active
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="status">Gender</label>
                            <div class="col-sm-10 input-group">
                                <select name="filter_gender" class="form-control" id="gender">
                                    <option value="none">Gender</option>
                                    <option value="1">
                                        Male
                                    </option>
                                    <option value="0">
                                        Female
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-2 col-sm-2  pd-left-0">
                        <button class="btn btn-success" type="submit">Search</button>
                        <div class="input-group">

                            <a class="btn btn-danger" href="{{ url('admin/contentManager/user') }}">Clear</a>
                        </div>
                    </div>
                </div>
            </form>

            <div class="x_content">
                @include('ContentManager::user.partials.table')
            </div>
        </div>
    </div>
@endsection

@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="x_panel">
            <div class="x_title col-xs-12">
                <div class="col-xs-12 col-sm-6 col-md-8 pd-left-0">
                    <h2>
                        Danh sách người dùng
                    </h2>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="col-xs-12 col-sm-6 col-md-6 user-total-left">
                        <p> Người dùng hoạt động</p>
                        <p class="green"><b>{{$totalActive}}</b></p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 user-total-right">
                        <p>Người dùng không hoạt động</p>
                        <p><b class="red">{{$totalInActive}}</b></p>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="message">
                    @include('flash::message')
                </div>
            </div>

            <input id="data-token" value="{{ csrf_token()}}" hidden><!-- viewing -->
            <input id="data-link-delete" value="{{ Url('api/v1/user') }}" hidden>
            <input id="data-link-status" value="{{ Url('api/v1/user/status') }}" hidden>
            <input id="name_status_un" value="Vô hiệu" hidden>
            <input id="name_status" value="Kích hoạt" hidden>
            {{--<input id="data-link-bulk-actions" value="{{ URL::action('UserController@bulkActions') }}" hidden>--}}
            {{--<input id="data-link-item-actions" value="{{ URL::action('UserController@itemActions') }}" hidden>--}}

            <form action="" method="get" id="form_custom_list">
                <div class="div_option" style="margin-bottom: 10px">
                    <div class="col-xs-12 col-md-2 col-sm-2  pd-left-0">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            <input class="form-control search_name enter-submit" name="name" placeholder="Tìm kiếm..."
                                   value="{{$name or ''}}"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4 col-sm-4 pd-left-0">
                        <select name="sort" class="type-sort" id="sort_field">
                            <option value="id" {!! ($sort == 'id') ? 'selected="selected"' : '' !!}>-- Sắp xếp theo --
                            </option>
                            <option value="name" {!! ($sort == 'name') ? 'selected="selected"' : '' !!}>Họ tên</option>
                            <option value="email" {!! ($sort == 'email') ? 'selected="selected"' : '' !!}>Email</option>
                            <option value="number_course" {!! ($sort == 'number_course') ? 'selected="selected"' : '' !!}>
                                Số khóa học đã mua
                            </option>
                            <option value="number_money" {!! ($sort == 'number_money') ? 'selected="selected"' : '' !!}>
                                Tổng tiền đã mua
                            </option>
                            <option value="created_at" {!! ($sort == 'created_at') ? 'selected="selected"' : '' !!}>Ngày
                                tạo
                            </option>
                            <option value="status" {!! ($sort == 'status') ? 'selected="selected"' : '' !!}>Trạng thái
                            </option>
                        </select>
                        <div class="sort-order">
                        <span class="order-asc {{strtolower($typeSort) == 'asc' ? 'active' : ''}}" data-order="asc">
                            <i class="fa fa-caret-up" aria-hidden="true"></i>
                        </span>
                            <span class="order-desc {{strtolower($typeSort) == 'desc' ? 'active' : ''}}"
                                  data-order="desc">
                            <i class="fa fa-caret-down" aria-hidden="true"></i>
                        </span>
                        </div>
                        <input type="hidden" class="type_sort" name="type_sort" value="{!! strtolower($typeSort) !!}">
                    </div>
                    <div class="col-xs-12 col-md-6 col-sm-6">
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a href="{!! URL::action('UserController@create') !!}" class="btn-toolbox success"><i class="fa fa-plus"></i> Thêm thành viên</a></li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                    Bulk action <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li role="presentation">
                                        <a href="javascript:void(0);" value="1" class="in_active_all"
                                           data-key="status" data-title="khoá học"
                                           data-text="kích hoạt">Kích hoạt người dùng</a>
                                    </li>
                                    <li role="presentation">
                                        <a href="javascript:void(0);" value="0" class="in_active_all"
                                           data-key="status" data-title="khoá học"
                                           data-text="vô hiệu">Vô hiệu người dùng</a>
                                    </li>
                                    <li role="presentation">
                                        <a href="javascript:void(0);" value="1" class="bulk_action_change"
                                           data-link="data-link-bulk-actions" data-key="featured" data-title="khoá học"
                                           data-text="kích hoạt nổi bật">Kích hoạt nổi bật</a>
                                    </li>
                                    <li role="presentation"><a href="javascript:void(0);" id="delete_all">Delete</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="div_option wrap-filters ">
                    <div class="col-xs-12 col-md-2 col-sm-2 pd-left-0">
                        <fieldset>
                            <div class="control-group">
                                <div class="controls">
                                    <div class="input-prepend input-group">
                                <span class="add-on input-group-addon"><i
                                            class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                        <input type="text" name="filter_date" id="reservation"
                                               class="form-control filter_header" value="{{$filterDate}}"/>
                                        <input hidden value="{{$startDate}}" id="startDate">
                                        <input hidden value="{{$endDate}}" id="endDate">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-xs-12 col-md-2 col-sm-2 pd-left-0">
                        <select name="filter_status" class="form-control filter_header">
                            <option value="none">Khóa học tham gia</option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-md-2 col-sm-2 pd-left-0">
                        <select name="filter_money" class="form-control filter_header">
                            <option value="none" {!! ($filterMoney == 'none') ? 'selected="selected"' : '' !!}>Số tiền
                                đã mua
                            </option>
                            <option value="100000-1000000" {!! ($filterMoney == '100000-1000000') ? 'selected="selected"' : '' !!}>
                                100.000 vnđ - 1.000.000 vnđ
                            </option>
                            <option value="1000000-3000000" {!! ($filterMoney == '1000000-3000000') ? 'selected="selected"' : '' !!}>
                                1.000.000 vnđ - 3.000.000 vnđ
                            </option>
                            <option value="3000000-5000000" {!! ($filterMoney == '3000000-5000000') ? 'selected="selected"' : '' !!}>
                                3.000.000 vnđ - 5.000.000 vnđ
                            </option>
                            <option value="5000000-7000000" {!! ($filterMoney == '5000000-7000000') ? 'selected="selected"' : '' !!}>
                                5.000.000 vnđ - 7.000.000 vnđ
                            </option>
                            <option value="7000000-10000000" {!! ($filterMoney == '7000000-10000000') ? 'selected="selected"' : '' !!}>
                                7.000.000 vnđ - 10.000.000 vnđ
                            </option>
                            <option value="10000000-15000000" {!! ($filterMoney == '10000000-15000000') ? 'selected="selected"' : '' !!}>
                                10.000.000 vnđ - 15.000.000 vnđ
                            </option>
                            <option value="15000000-20000000" {!! ($filterMoney == '15000000-20000000') ? 'selected="selected"' : '' !!}>
                                15.000.000 vnđ - 20.000.000 vnđ
                            </option>
                            <option value="20000000" {!! ($filterMoney == '20000000') ? 'selected="selected"' : '' !!}>
                                Trên 20.000.000 vnđ
                            </option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-md-2 col-sm-2 pd-left-0">
                        <select name="filter_status" class="form-control filter_header">
                            <option value="none" {!! ($filterStatus == 'none') ? 'selected="selected"' : '' !!}>Trạng
                                thái
                            </option>
                            <option value="1" {!! ($filterStatus == '1') ? 'selected="selected"' : '' !!}>Hoạt động
                            </option>
                            <option value="0" {!! ($filterStatus == '0') ? 'selected="selected"' : '' !!}>Không hoạt
                                động
                            </option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-md-2 col-sm-2 pd-left-0">
                        <select name="filter_role_check" class="form-control filter_header">
                            <option value="none" {!! ($filterRoleCheck == 'none') ? 'selected="selected"' : '' !!}>Loại user
                            </option>
                            <option value="0" {!! ($filterRoleCheck == '0') ? 'selected="selected"' : '' !!}>Admin
                            </option>
                            <option value="1" {!! ($filterRoleCheck == '1') ? 'selected="selected"' : '' !!}>User
                            </option>
                        </select>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 pd-left-0">
                        <a class="btn btn-danger" href="{{ URL::action('UserController@index') }}">Xóa</a>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="div_option">
                    <div class="col-xs-4 col-md-2 col-sm-2 count_record">
                        {{--<span class="viewing">{{$total}} khoá học</span>--}}
                    </div>
                    <div class="col-xs-8 col-md-10 col-sm-10">
                        <ul class="nav navbar-right panel_toolbox">
                            <li class="viewing">Chế độ xem</li>
                            <li class="dropdown">
                                <select class="form-control" name="limit" id="limit_pagination">
                                    <option @if ($limit == 10)selected="selected" @endif value="10">10</option>
                                    <option @if ($limit == 25)selected="selected" @endif value="25">25</option>
                                    <option @if ($limit == 50)selected="selected" @endif value="50">50</option>
                                    <option @if ($limit == 75)selected="selected" @endif value="75">75</option>
                                    <option @if ($limit == 100)selected="selected" @endif value="100">100</option>
                                </select>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
            <div class="x_content">
                @include('ContentManager::user.partials.table')
            </div>
        </div>
    </div>
@endsection