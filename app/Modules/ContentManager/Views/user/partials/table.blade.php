<table class="table table-striped jambo_table bulk_action">
    <thead>
    <tr>
        <th><input id="checkAll" type="checkbox" class="flat"></th>
        <th>
            No.
        </th>
        <th>Name</th>
        <th>Gender</th>
        <th>Email</th>
        <th>Role</th>
        <th>Created at</th>
        <th>Status</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php  $i = 0; ?>
    @foreach ($model as $data)
        <?php $i++;?>
        <tr id="tr-{{ $data->id }}">
            <td>
                <input type="checkbox" class="flat" name="checkbox" data-role="checkbox" value="{{$data->id}}"/>
                <input type="hidden" id="idPost" value="{{ $data->id }}">
            </td>
            <td>
                <span>{{ (($model->currentPage() - 1) * ($model->perPage())) + $i }}</span>
            </td>
            <td>
                {{$data->name}}
            </td>
            <td>
                @if($data->gender == 1)
                    Male
                @elseif($data->gender == 0)
                    Female
                @endif
            </td>
            <td>
                {{$data->email}}
            </td>
            <td>
                @if($data->roles)
                    {{ $data->roles->name }}
                @endif
            </td>
            <td>
                {{$data->created_at}}
            </td>
            <td>
                <span id="status">
                    {{$data->status == 1 ? 'Active' : 'Inactive'}}
                </span>
            </td>
            <td>
                <div class="btn-edit-delete">
                    <a href="{{ Admin::route('contentManager.user.edit',['user'=>$data->id]) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>
                    @if($data->id != $userLogin->id)
                        |
                    <a href="#" data-role="delete-user" data-idpost="{{ $data->id }}"
                       data-url="{{ Admin::route('contentManager.user.destroy',['tag'=>'']) }}/"> <i class="fa fa-trash-o" aria-hidden="true"></i> </a>
                        |
                        <a href="#" data-role="ban-user" data-idpost="{{ $data->id }}" data-url=" {{Admin::route('contentManager.user.ban',['tag'=>'']) }} " data-val={{ $data->status }}>
                            @if($data->status)
                                <i class='fa fa-play-circle'  aria-hidden='true'></i>
                            @else
                                <i class='fa fa-ban' aria-hidden='true'></i>
                            @endif
                        </a>
                    @endif


                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $model->links() }}

@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $("a[data-role='delete-user']").on("click", function () {
            var idpost = $(this).data('idpost');
            var urlDelete = $(this).data('url');
            swal({
                title: "Are you sure?",
                text: "Delete this user.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                confirmButtonText: "Yes",
                confirmButtonClass: "btn-danger",
                cancelButtonText: "No"
            }, function () {
                $.ajax({
                    type: 'DELETE',
                    url: urlDelete + idpost,
                    data: {"_token": "{{ csrf_token() }}"}
                })
                    .done(function () {
                        location.reload();
                    });
            });
        });

    });
</script>
@endpush