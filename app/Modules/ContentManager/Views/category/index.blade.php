@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('ContentManager::partials.alert')
            @include('ContentManager::category.partials.tablemanage')
        </div>
    </div>
@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        $("a[data-role='delete-post']").on("click", function () {
            var idpost = $(this).data('idpost');
            swal({
                title: "Bạn có muốn xóa danh mục?",
                text: "",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                confirmButtonText: "Yes",
                confirmButtonClass: "btn-danger",
                cancelButtonText: "No"
            }, function () {
                $.ajax({
                    type: 'DELETE',
                    url: "{{ Admin::route('contentManager.category.index') }}/" + idpost,
                    data: {"_token": "{{ csrf_token() }}"}
                })
                        .done(function () {
                            location.reload();
                        });
            });
            return false;
        });

        $("#checkAll").on('change', function () {

            $("input:checkbox[name=checkbox]").prop('checked', $(this).prop("checked"));
            if ($("#btn-sel-del").css('display') == 'none') {
                $("#btn-sel-del").css("display", "inline-block");
            } else {
                $("#btn-sel-del").css("display", "none");
            }
        });

        $("input[type=checkbox]").on("change", function () {
            var n = $("input[name=checkbox]:checked").length;
            if (n == 0) {
                $("#btn-sel-del").css("display", "none");
            } else {
                $("#btn-sel-del").css("display", "inline-block");
            }
            if(n == $("input[name=checkbox]").length) {
                $("#checkAll").prop('checked', true);
            }
        });

        $("#btn-sel-del").on("click", function () {
            var array = new Array();
            $("input[name=checkbox]:checked").each(function () {
                array.push($(this).val());
            });
            var id = array.join();
            swal({
                title: "Bạn có muốn xóa các danh mục đã chọn?",
                text: "",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                confirmButtonText: "Có",
                confirmButtonClass: "btn-danger",
                cancelButtonText: "Không"
            }, function () {
                $.ajax({
                    type: 'DELETE',
                    url: "{{ Admin::route('contentManager.category.index') }}/" + id,
                    data: {"_token": "{{ csrf_token() }}"}
                })
                        .done(function () {
//                            swal("Deleted!", "Delete Success", "success");
                            location.reload();
                        });
            });
            return false;
        });
    });
</script>
@endpush