<?php
$limit = request('limit', 10);
?>
@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="x_panel">
            <div class="x_title">
                <h2>Danh sách video</h2><!-- /.title -->
                <div class="clearfix"></div>
                @include('flash::message')
            </div><!-- /.x-title -->

            <input id="data-token" value="{{ csrf_token()}}" hidden>
            <input id="data-link-bulk-actions" value="{{ route('admin.contentManager.video.bulkActions') }}" hidden>
            <input id="data-link-item-actions" value="{{ route('admin.contentManager.video.itemActions') }}" hidden>

            <div class="x_content">
                <ul class="nav nav-tabs" role="tablist">
                    @foreach(Trans::langs() as $key => $lang)
                        <li role="presentation" class="{{ ($locale == $lang['locale']) ? 'active': '' }}">
                            <a href="#{{ "video_language_{$lang['locale']}" }}" role="tab"
                               aria-controls="{{ "video_language_{$lang['locale']}" }}"
                               data-toggle="tab" class="{{ "video_language_{$lang['locale']}" }}">{{ $lang['name'] }}</a>
                        </li>
                    @endforeach
                </ul><!-- /.Nav tabs -->

                <div class="tab-content mg-top-15">
                    @foreach(Trans::langs() as $key => $lang)
                        <div role="tabpanel" class="tab-pane {{ ($locale == $lang['locale']) ? 'active': '' }}"
                             id="{{ "video_language_{$lang['locale']}" }}">
                            <div class="col-xs-12 mg-bottom-20" style="text-align: center !important;">
                                <ul class="nav navbar-right panel_toolbox">
                                    <li>
                                        <a href="{{ Admin::route('contentManager.video.create') }}" class="btn-toolbox success">
                                            <i class="fa fa-plus"></i> Thêm video </a>
                                    </li>
                                    <li class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"
                                           aria-expanded="false">
                                            Hành động <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li role="presentation">
                                                <a href="javascript:void(0);" value="Draft" class="bulk_action_change"
                                                   data-link="data-link-bulk-actions" data-key="post_status"
                                                   data-title="video"
                                                   data-text="Video Bí mật">Video Bí mật </a>
                                            </li>
                                            <li role="presentation">
                                                <a href="javascript:void(0);" value="publish" class="bulk_action_change"
                                                   data-link="data-link-bulk-actions" data-key="post_status"
                                                   data-title="video"
                                                   data-text="Video Công khai">Video Công khai </a>
                                            </li>
                                            <li role="presentation">
                                                <a href="javascript:void(0);" value="none" class="bulk_action_change"
                                                   data-link="data-link-bulk-actions" data-key="delete"
                                                   data-title="video"
                                                   data-text="xoá">Xoá</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul><!-- /.action -->

                                <form action="" method="get" id="form_custom_list">
                                    <div class="row div_option mg-bottom-10">
                                        <div class="col-xs-12 col-md-2 col-sm-2 pd-left-0">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                <input class="form-control search_name enter-submit" name="post_name"
                                                       placeholder="Tìm kiếm theo tên video "
                                                       value="{{ $input['post_name'] or '' }}"/>
                                            </div>
                                        </div><!-- /.search -->

                                        <div class="col-xs-12 col-md-2 col-sm-2">
                                            <select name="filter_status" class="form-control filter_header_video">
                                                <option value="none" {!! (isset($filterStatus) && $filterStatus == 'none') ? 'selected="selected"' : '' !!}>
                                                    Trạng thái
                                                </option>
                                                <option value="publish" {{ (isset($filterStatus) && $filterStatus == 'publish') ? 'selected="selected"'  : '' }}>
                                                    Công khai
                                                </option>
                                                <option value="Draft" {{ (isset($filterStatus) && $filterStatus == 'Draft') ? 'selected="selected"'  : '' }}>
                                                    Bí mật
                                                </option>
                                            </select>
                                        </div><!-- /.status -->

                                        <button class="btn btn-success" type="submit">Tìm kiếm</button>
                                        <a class="btn btn-danger mg-left-25"
                                           href="{{ Admin::route('contentManager.video.index') . '?' . http_build_query(['limit' => request('limit', 10), 'locale' => $lang['locale']]) }}">Xóa lọc</a>
                                    </div><!-- /.row -->

                                    <div class="row div_options mg-bottom-10">



                                    </div><!-- /.row -->

                                    <div class="row div_option">
                                        <div class="col-xs-4 col-md-2 col-sm-2 count_record">
                                        </div>
                                        <div class="col-xs-8 col-md-10 col-sm-10">
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li class="viewing">Chế độ xem</li>
                                                <li class="dropdown">
                                                    <select class="form-control" name="limit" id="limit_pagination">
                                                        <option {{ ($limit == 10) ? 'selected' : '' }} value="10">
                                                            10
                                                        </option>
                                                        <option {{ ($limit == 25) ? 'selected' : '' }} value="25">
                                                            25
                                                        </option>
                                                        <option {{ ($limit == 50) ? 'selected' : '' }} value="50">
                                                            50
                                                        </option>
                                                        <option {{ ($limit == 75) ? 'selected' : '' }} value="75">
                                                            75
                                                        </option>
                                                        <option {{ ($limit == 100) ? 'selected' : '' }} value="100">
                                                            100
                                                        </option>
                                                    </select>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <input type="hidden" name="locale" value="{{ $lang['locale'] }}">
                                </form>
                            </div><!-- /.filter -->
                            @include('ContentManager::video.partials.table')
                        </div>
                    @endforeach
                </div><!-- /.Tab panes -->
            </div><!-- /.x-content -->
        </div>

        <!-- Button trigger modal -
      <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"></h4>
                    </div>
                    <div class="modal-body">
                        <iframe id="abc_frame" width="560" height="315" src="" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" id="id_input_show" value="0" name="id_input_show">
                        <input type="hidden" id="link_video" value="">
                        <button type="button" class="btn btn-default close_show" data-dismiss="modal">Thoát</button>
                    </div>
                </div>
            </div>
        </div>


@endsection
