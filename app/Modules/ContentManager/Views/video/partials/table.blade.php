
<table class="table table-responsive" id="vouchers-table">
    <thead>
    <th>STT</th>
    <th>Tên video </th>
    <th>Ngày tạo </th>
    <th>Người tạo </th>
    <th>Nổi bật </th>
    <th>Hành động </th>
    <th class="text_right">
        <div class="checkbox">
            <label>
                <input type="checkbox" value="" id="select_all" name="select_all" class="select_all">
                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            </label>
        </div>
    </th>
    </thead>
    <tbody>
    <?php
    $stt = ($limit * (request('page', 1) - 1)) + 1;
    ?>
    @foreach($model as $data)
        <tr>
            <td>{!! $stt !!}</td>
            <td>{!! (is_null($data->getTranslation($lang['locale'])) ? 0 : $data->getTranslation($lang['locale'])->post_title)  !!}</td>
            <td>{!! date( 'd/m/Y', strtotime( $data->created_at ) ) !!}</td>
            <td>{!! $data->user->name !!}</td>
            <td>
                @if($data->post_status == 'Draft')
                    <button type="button"
                            class="btn_status btn_status_false item_actions"
                            data-link="data-link-item-actions"
                            data-key="post_status"
                            data-title="Video"
                            data-text="Công khai"
                            data-val="publish"
                            data-id="{!! $data->id !!}">Bí mật
                    </button>
                @else
                    <button type="button"
                            class="btn_status btn_status_success item_actions"
                            data-link="data-link-item-actions"
                            data-key="post_status"
                            data-title="Video"
                            data-text="Bí mật"
                            data-val="Draft"
                            data-id="{!! $data->id !!}">Công khai
                    </button>
                @endif
            </td>
            <td>
                {!! Form::open(['route' => ['admin.vouchers.destroy', $data->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="#" class='btn btn-default btn-xs show_video' data-toggle="modal" data-target="#myModal" data-id="{!! $data->id !!}" data-link="{!! $data->getMetaValue('link') !!}">
                        <i class="glyphicon glyphicon-eye-open"></i>
                    </a>
                    <a href="{{ Admin::route('contentManager.video.edit',['id'=>$data->id]) }}" class='btn btn-default btn-xs'>
                        <i class="glyphicon glyphicon-edit"></i>
                    </a>
                    <a href="javascript:void(0)"
                       class="btn btn-danger btn-xs btn-edit item_actions"
                       data-link="data-link-item-actions"
                       data-key="delete" data-title="chương trình khuyến mãi"
                       data-text="xoá"
                       data-val="none" data-id="{!! $data->id !!}">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                </div>
                {!! Form::close() !!}
            </td>

            <td colspan="3" class="text_right">
                <div class="checkbox">
                    <label>
                        <input class="checkbox_item" type="checkbox" name="checkbox" value="{{$data->id}}">
                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                    </label>
                </div>
            </td>
        </tr>
        <?php $stt++ ?>
    @endforeach
    </tbody>
</table>
