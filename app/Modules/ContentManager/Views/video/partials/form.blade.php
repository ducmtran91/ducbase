<form method="POST"
      action="{{ (!empty($model)) ? Admin::route('contentManager.video.update',['post'=>$model->id]) : Admin::route('contentManager.video.store') }}"
      style="margin-bottom: 70px;">

    <div class="row">
        <div class="col-sm-12">
            {{ csrf_field() }}
            @if(!empty($model))
                <input name="_method" type="hidden" value="PUT">
        @endif
        <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                @foreach(Trans::languages() as $key => $language)
                    <li role="presentation"
                        class="{{ (Trans::locale() == $language->country->locale) ? 'active': '' }}">
                        <a href="#{{ "language_{$language->country->locale}" }}" role="tab"
                           aria-controls="{{ "language_{$language->country->locale}" }}"
                           data-toggle="tab">{{ $language->name }}</a>
                    </li>
                @endforeach
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                @foreach(Trans::languages() as $key => $language)
                    <div role="tabpanel"
                         class="tab-pane {{ (Trans::locale() == $language->country->locale) ? 'active': '' }}"
                         id="{{ "language_{$language->country->locale}" }}">


                        <div class="mg-top-20 form-group col-sm-12 col-xs-12 col-md-12" data-id="counter-root">
                            <div class="control-label col-md-2 col-sm-2 col-xs-12">
                                <label for="title-post">Tên video <span class="text-danger">*</span></label>
                            </div>
                            <div class="form-group col-sm-8 col-xs-10">
                                <input type="text" class="form-control"
                                       name="trans[{{$language->country->locale}}][post_title]"
                                       value="{{ (!empty($model)) ?
                                   $model->getTranslation($language->country->locale)->post_title :
                                   old("trans.{$language->country->locale}.post_title") }}"
                                       id="title-post"
                                       placeholder="Name">
                            </div>
                            <div class="form-group col-sm-2 col-xs-2">
                                <span data-id="counter-to">0</span> / 255
                            </div>
                        </div>


                        <div class="mg-top-20 form-group col-sm-12 col-xs-12 col-md-12" data-id="counter-root">
                            <div class="control-label col-md-2 col-sm-2 col-xs-12">
                                <label for="content-post">Mô tả <span class="text-danger">*</span></label>
                            </div>
                            <div class="form-group col-sm-8 col-xs-10">
                                <textarea name="trans[{{$language->country->locale}}][post_content]"
                                          init="ckeditor"
                                          data-locale="{{$language->country->locale}}"
                                          data-modal="{{ "summer_{$language->country->locale}_image" }}"
                                          id="{{ "summer_{$language->country->locale}_image" }}"
                                          class="content-post-{{$language->country->locale}} {{--content-post--}} form-control"
                                          rows="18">{{ (!empty($model)) ?
                                      Helper::bbcode($model->getTranslation($language->country->locale)->post_content) :
                                      old("trans.{$language->country->locale}.post_content") }}</textarea>
                                <!-- /.Insert image -->
                                @include('ContentManager::partials.summernote.img_upload', [
                                    'data' => [
                                        'name' => "summer_{$language->country->locale}_image"
                                    ]
                                ])
                            </div>
                        </div>

                    </div>
                @endforeach

                <div class="mg-top-20 form-group col-sm-12 col-xs-12 col-md-12" data-id="counter-root">
                    <div class="control-label col-md-2 col-sm-2 col-xs-12">
                        <label for="title-post">Link video <span class="text-danger">*</span></label>
                    </div>
                    <div class="form-group col-sm-8 col-xs-10">
                        <input type="text" class="form-control" id="video-link"
                               name="meta[link]" placeholder="http://"
                               value="{{ (!empty($model)) ? $model->getMetaValue('link') : old('meta.link') }}">
                    </div>
                </div>


                <div class="mg-top-20 form-group col-sm-12 col-xs-12 col-md-12" data-id="counter-root">
                    <div class="control-label col-md-2 col-sm-2 col-xs-12">
                        <label for="title-post">Trạng thái </label>
                    </div>
                    <div class="form-group col-sm-8 col-xs-10">
                        @if(!empty($model))
                            <select name="status" id="program_learning_select_sound"
                                    class="form-control js-select-multiple-dependence">
                                <option {{ ($model->post_status == "publish") ? "selected" : "" }} value="publish">
                                    Publish
                                </option>
                                <option {{ ($model->post_status == "Draft") ? "selected" : "" }} value="Draft">
                                    Draft
                                </option>
                            </select>
                        @else
                            <select name="status" id="program_learning_select_sound"
                                    class="form-control js-select-multiple-dependence">
                                <option value="publish">Publish</option>
                                <option value="Draft">Draft</option>
                            </select>
                        @endif
                    </div>
                </div>


                <div class="mg-top-20 form-group col-sm-12 col-xs-12 col-md-12" data-id="counter-root">
                    <div class="control-label col-md-2 col-sm-2 col-xs-12">
                        <div class="panel-heading">Bố Cục</div>
                    </div>
                    <div class="form-group col-sm-8 col-xs-10">
                        <select name="meta[layout]" id="program_learning_select_sound"
                                class="form-control js-select-multiple-dependence">

                            <option value="">Use default layout for post</option>
                            @foreach($layouts as $key => $value)
                                @if (empty($model))
                                    <option value="{{ $key }}">
                                        {{ ucwords(str_replace("-", " ", $value)) }}
                                    </option>
                                @else
                                    <option value="{{ $key }}"
                                            {{ ($key == $model->getMetaValue('layout')) ? 'selected':'' }}>
                                        {{ ucwords(str_replace("-", " ", $value)) }}
                                    </option>
                                @endif

                            @endforeach
                        </select>

                    </div>
                </div>



                <div class="mg-top-20 form-group col-sm-12 col-xs-12 col-md-12" data-id="counter-root">
                    <div class="control-label col-md-2 col-sm-2 col-xs-12">
                        <div class="panel-heading">Upload ảnh</div>
                    </div>
                    <div class="form-group col-sm-8 col-xs-10">
                        @include('ContentManager::partials.x_form.btn_upload', [
                                    'idModal'=>'modal_featuredImage',
                                    'model' => (!empty($model)? $model->getMetaValue('featured_img') : ''),
                                    'label' => '',
                                    'input'=>'meta[featured_img]'
                            ])
                    </div>
                </div>

            </div>
        </div><!-- /.main-col -->
        <!-- Submit Field -->
        <div class="form-group row col-sm-12 text-center">
            <a href="{{ Admin::route('contentManager.video.index') }}" class="btn btn-default">Huỷ</a>
            <input type="hidden" name="save_new" id="save_new" class="save_new" value="off"/>
            {!! Form::button('Lưu & tạo video mới', ['class' => 'btn btn-success btn-save-and-new-video']) !!}
            {!! Form::submit('Lưu video', ['class' => 'btn btn-success']) !!}
        </div>
    </div>


</form>




