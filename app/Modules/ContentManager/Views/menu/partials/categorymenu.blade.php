<li id="term-{{ $node->term_id }}">
    <div class="checkbox">
        <label style="padding-left: 0;">
            <?php
            $value = [];
            foreach ($languages as $language) {
                $translation = $node->getTranslation($language->country->locale);
                $value[$language->country->locale] = $translation ? $translation->name : $language->country->locale;
            }
            ?>
            <input class="catmenu flat" data-role="checkbox"
                   data-url="{{ $node->slug }}" value="{{ json_encode($value) }}"
                   data-type="category" type="checkbox"> {{ $node->name }}
        </label>
    </div>
</li>
@if(count($datas->get()) > 0 )
    @foreach($datas->get() as $node)
        <li id="child-{{ $node->term_id }}" class="category-child-list">
            <ul id="parent-{{ $node->parent  }}" class="list-unstyled">
                @include('ContentManager::menu.partials.categorymenu', ['datas' => $node->children(),'post'=>false])
            </ul>
        </li>
    @endforeach
@endif

