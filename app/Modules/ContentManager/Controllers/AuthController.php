<?php

namespace App\Modules\ContentManager\Controllers;

use App\Entities\Roles;
use App\Entities\Users;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RegistersUsers;
use Validator;
use App\Facades\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

    use AuthenticatesUsers, RegistersUsers {
        AuthenticatesUsers::redirectPath insteadof RegistersUsers;
        AuthenticatesUsers::guard insteadof RegistersUsers;
    }

    /**
     * Where to redirect users after login / registration.
     *L
     * @var string
     */
    protected $redirectTo = 'administrator/contentManager/index';
    protected $loginView = 'auth.login';
    protected $guard = 'admin';
    protected $redirectAfterLogout = 'admin';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        $this->loginView = 'ContentManager::' . $this->loginView;
        $this->redirectTo = Admin::StrUrl("contentManager/index");
    }

    public function showLoginForm()
    {
        return view($this->loginView);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        // Cache role
        /** @var Roles $role */
        $role = Roles::where('status', 1)
            ->where('id', $user->role_id)
            ->first();

        $role->cachePermission(false);

        // Default
        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'message' => 'userLoginSuccess',
                'redirect' => property_exists($this, 'redirectTo') ? $this->redirectTo : '/',
                'data' => $user
            ]);
        }

        return redirect(property_exists($this, 'redirectTo') ? $this->redirectTo : '/');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        $roleAllows = Roles::where('status', 1)->whereIn('type', [Roles::ROLE_ADMIN, Roles::ROLE_USER])->pluck('id');
        $credentials = array_merge($this->getCredentials($request), ['provider' => 'local', 'status' => 1]);
        $checkAccount = Users::where('email', $credentials['email'])->first();

        if ($checkAccount) {
            if (in_array($checkAccount->role_id, $roleAllows->toArray())) {
                if (Auth::guard($this->getGuard())->attempt($credentials, $request->has('remember'))) {
                    return $this->handleUserWasAuthenticated($request);
                }
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Get the guest middleware for the application.
     */
    public function guestMiddleware()
    {
        $guard = $this->getGuard();

        return $guard ? 'guest:'.$guard : 'guest';
    }
    protected function guard()
    {
        return Auth::guard($this->guard);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return string|null
     */
    protected function getGuard()
    {
        return property_exists($this, 'guard') ? $this->guard : null;
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  bool  $throttles
     * @return \Illuminate\Http\Response
     */
    protected function handleUserWasAuthenticated(Request $request)
    {

        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::guard($this->getGuard())->user());
        }

        return redirect()->intended($this->redirectPath());
    }
}
