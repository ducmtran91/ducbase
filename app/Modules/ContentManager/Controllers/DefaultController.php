<?php

namespace App\Modules\ContentManager\Controllers;

use App\Entities\League;
use App\Entities\PlayerUpdates;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Users;
use App\Entities\Team;
use Carbon\Carbon;
use Maatwebsite\Excel\Excel;

class DefaultController extends Controller
{
    public function index(){
        return view("ContentManager::index", array());
    }

    public function exportExcel() {
        $toolExcel = new Excel(new Export);
        $toolExcel->create('Statistic', function($excel){
            // Set the title
            $excel->setTitle('Our new awesome title');

            // Chain the setters
            $excel->setCreator('Maatwebsite')
                ->setCompany('Maatwebsite');

            // Call them separately
            $excel->setDescription('A demonstration to change the file properties');
        })->download('xls');
    }
}
