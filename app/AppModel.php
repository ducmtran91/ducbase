<?php

namespace App;

use App\Traits\Filterable;
use DB;
use Illuminate\Database\Eloquent\Model;
use Lang;

/**
 * Class AppModel
 * @package App
 *
 * @property int id
 * @property string created_at
 * @property string updated_at
 * @property int status
 */
class AppModel extends Model
{
    use Filterable;

//    public function __construct(array $attributes)
//    {
//        parent::__construct($attributes);
//
////        $logs = DB::getQueryLog();
//    }

    /**
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->table;
    }

    /**
     * static enums
     * @access static
     *
     * @param mixed $value
     * @param array $options
     * @param string $default
     * @return string|array
     */
    public static function enum($value, $options, $default = '')
    {
        if ($value !== null) {
            if (array_key_exists($value, $options)) {
                return $options[$value];
            }
            return $default;
        }
        return $options;
    }

    /**
     * const
     */
    const STATUS_DELETE = -1;
    /**
     * const
     */
    const STATUS_ENABLE = 1;
    /**
     * const
     */
    const STATUS_DISABLE = 0;

    /**
     * static enum: Model::function()
     *
     * @access static
     * @param integer|null $value
     * @return string
     */
    public function statuses($value = null)
    {
        $options = array(
            self::STATUS_ENABLE => 'Enable',
            self::STATUS_DISABLE => 'Disable'
        );
        return self::enum($value, $options);
    }

    const IS_NO = 0;
    const IS_YES = 1;

    const ORDER_ASC = 'asc';
    const ORDER_DESC = 'desc';

    /**
     * static enum: Model::function()
     *
     * @access static
     * @param integer|null $value
     * @return array|string|null
     */
    public static function by_orders($value = null)
    {
        $options = array(
            self::ORDER_ASC => Lang::get('Ascending'),
            self::ORDER_DESC => Lang::get('Descending'),
        );
        return self::enum($value, $options);
    }

    public $summary_fields = [];

    /**
     * @return array
     */
    public function getSummaryInfo() {
        if (empty($this)) {
            return null;
        }

        $arrInfo = $this->toArray();

        if (empty($this->summary_fields)) {
            return $arrInfo;
        }

        $data = [];

        foreach ($arrInfo as $field => $value) {
            if (in_array($field, $this->summary_fields)) {
                $data[$field] = $value;
            }
        }

        return $data;
    }

    /**
     * Fire events when create, update roles
     * The "booting" method of the model.
     * @link https://stackoverflow.com/a/38685534
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        // When saving
        /*self::saving(function ($model) {
            // do something...
        });*/

        // When saved
        /*self::saved(function ($model) {
            // do something...
        });*/

        /*self::updating(function ($model) {
            // do something...
        });

        self::updated(function ($model) {
            // do something...
        });*/

        /*self::creating(function ($model) {
            // do something...
        });

        self::created(function ($model) {
            // do something...
        });*/

        /*self::deleting(function ($model) {
            // do something...
        });*/

        /*self::deleted(function ($model) {
            // do something...
        });*/

    }

}
