<?php

namespace App\Helpers;

use Log, Mail;
use App\Entities\PaymentMethod;

class QSoftVN
{
    /**
     * Send Email.
     *
     * @param array $input
     * @param string $template
     * @return void
     */
    public function sendEmail($input, $template = 'admin.email_template.content_email')
    {
        try {
            if (!view()->exists($template)) {
                throw new \Exception('Could not found the sendEmail template.');
            }

            if (!isset($input['email']) || !filter_var($input['email'], FILTER_VALIDATE_EMAIL)) {
                throw new \Exception('The Email not exist or not validate.');
            }

            $default = [
                'trademark' => 'PES692',
                'subject' => '',
                'email' => '',
                'content' => ''
            ];
            $inputEmail = array_merge($default, $input);
            Mail::send(
                $template, $inputEmail,
                function ($message) use ($inputEmail) {
                    $message->from(env('MAIL_ADDRESS_SEND'), $inputEmail['trademark']);
                    $message->to($inputEmail['email'], '')->subject($inputEmail['subject']);
                });

            // Write log to storage/logs
            Log::info("Send Email Successfully: to => {$inputEmail['email']}, subject => {$inputEmail['subject']}");
        } catch (\Exception $exception) {
            // Write log to storage/logs
            Log::error("Send Email Failure: {$exception->getMessage()}");
        }
    }

    /**
     * Get Payment Method by key
     *
     * @param string $key
     * @return key
     */
    public function paymentMethod($key = '')
    {
        if ($paymentMethod = PaymentMethod::where('machine_name', strtoupper($key))->first()) {
            return $paymentMethod->name;
        }

        return '';
    }
}