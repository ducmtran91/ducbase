<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 10/15/2018
 * Time: 10:47 AM
 */

namespace App\Helpers;
use App\Contracts\ExcelInterface;
use Maatwebsite\Excel\Facades\Excel;

class ExcelHelper implements ExcelInterface
{
    public function export($data) {
        $fileName = $data['file_name'];
        $dataObject = $data['data'];
        $ext = $data['ext'];
        $sheetName = $data['sheet_name'];

        Excel::create($fileName, function($excel) use ($dataObject, $sheetName) {

            $excel->sheet($sheetName, function($sheet) use ($dataObject) {
                $sheet->fromArray($dataObject, null, 'A1', false, false);
                $this->setStyleExcel($sheet);
            });

        })->export($ext);
    }

    private function setStyleExcel($sheet) {
        $contentStyle = array(
            'font' => array(
                'name'      =>  'Calibri',
                'size'      =>  11,
                'bold'      =>  false
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );
        $sheet->setStyle($contentStyle);
        $sheet->setPageMargin(array(
            0.25, 0.30, 0.25, 0.30
        ));
        $sheet->row(1, function($row){
            $row->setFont(array(
                'family'     => 'Calibri',
                'size'       => '11',
                'bold'       =>  true
            ))->setAlignment('center');
        });
    }
}