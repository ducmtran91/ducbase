<?php

namespace App\Helpers;


use App\Entities\PackageItem;
use App\Entities\PackagePricing;
use App\Entities\Promotion;
use App\Entities\Tax;
use App\Exceptions\CustomValidation;
use App\Exceptions\CustomValidationException;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Validator;
use Illuminate\Validation\Validator as MakeValidator;
use Illuminate\Validation\ValidationException;

class Booking
{
    const NOTI_ACCEPTED = 'accepted';
    const NOTI_STARTED = 'started';
    const NOTI_FINISHED = 'finished';

    /**
     * The attribute Price
     *
     * @var double $price
     */
    protected $price = 0;

    /**
     * The attribute Discount
     *
     * @var double $discount
     */
    protected $discount = 0;

    /**
     * The attribute Tax
     *
     * @var double $tax
     */
    protected $tax = 0;

    /**
     * Get Dog Walking's Price.
     *
     * @return mixed
     */
    public function getPriceWalking($params)
    {
        try {
            $validate = Validator::make($params, [
                'service_type' => 'required|exists:service_types,type',
                'pet_number' => 'required',
                'date' => 'required|date_format:' . config('common.date_format'),
                'time' => 'required_with:date',
                'duration' => 'required_if:service_type,dog_walking|exists:package_groups,id',
            ], [
                'required' => trans('booking.owner.validation.required'),
                'required_with' => trans('booking.owner.validation.required_with'),
                'required_if' => trans('booking.owner.validation.required_if'),
                'exists' => trans('booking.owner.validation.exists'),
                'date_format' => trans('booking.owner.validation.date_format'),
            ]);
            if ($validate->fails()) {
                throw new ValidationException($validate);
            }

            $time = $this->getTimeSchedule($params['time'], config('booking.services.dog_walking.time'));
            $dateBooking = Carbon::createFromFormat(config('common.datetime_format'), "{$params['date']} {$time}");
            $dateNow = Carbon::now();

            if ($timeSchedule = $dateNow->diffInHours($dateBooking, false) < config('booking.services.dog_walking.validation.before_booking')) {
                throw new CustomValidationException(new CustomValidation(array(trans('booking.owner.validation.before_booking'))));
            }
            $timeSchedule = $dateNow->diffInHours($dateBooking, false) < 24 ? PackageItem::LESS_THAN_24 : PackageItem::SCHEDULE_ONE_TIME;

            return PackagePricing::whereHas('packagePet', function ($query) use ($params) {
                $query->where('package_pets.number_of_dogs', $params['pet_number']);
            })
                ->whereHas('packageItem', function ($query) use ($timeSchedule) {
                    $query->where('package_items.type', $timeSchedule);
                })
                ->whereHas('packageItem.packageGroup', function ($query) use ($params) {
                    $query->where('package_groups.id', $params['duration']);
                })
                ->first();

        } catch (ValidationException $exception) {

            throw $exception;
        } catch (CustomValidationException $exception) {

            throw $exception;
        } catch (\Exception $exception) {

            throw $exception;
        }
    }

    /**
     * Get Bathroom Break's Price.
     *
     * @return mixed
     */
    public function getPriceBathroom($params)
    {
        $validator = Validator::make($params, [
            'service_type' => 'required|exists:service_types,type',
            'pet_number' => 'required',
            'date' => 'required|date_format:' . config('common.date_format'),
            'time' => 'required_with:date',
            'duration' => 'required_if:service_type,bathroom_break|exists:package_groups,id',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $time = $this->getTimeSchedule($params['time'], config('booking.services.bathroom_break.time'));
        $dateBooking = Carbon::createFromFormat(config('common.datetime_format'), "{$params['date']} {$time}");
        $timeSchedule = $dateBooking->diffInHours(Carbon::now()) < 24 ? PackageItem::LESS_THAN_24 : PackageItem::SCHEDULE_ONE_TIME;

        return PackagePricing::whereHas('packagePet', function ($query) use ($params) {
            $query->where('package_pets.number_of_dogs', $params['pet_number']);
        })
            ->whereHas('packageItem', function ($query) use ($timeSchedule) {
                $query->where('package_items.type', $timeSchedule);
            })
            ->whereHas('packageItem.packageGroup', function ($query) use ($params) {
                $query->where('package_groups.id', $params['duration']);
                // ->where('package_groups.type', 'bathroom_break')
            })
            ->first();
    }

    /**
     * Get Dog Running's Price.
     *
     * @return mixed
     */
    public function getPriceRunning($params)
    {
        $validator = Validator::make($params, [
            'service_type' => 'required|exists:service_types,type',
            'pet_number' => 'required',
            'date' => 'required|date_format:' . config('common.date_format'),
            'time' => 'required_with:date',
            'distance' => 'required_if:service_type,dog_running|exists:package_groups,id',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
        $time = $this->getTimeSchedule($params['time'], config('booking.services.dog_running.time'));
        $dateBooking = Carbon::createFromFormat(config('common.datetime_format'), "{$params['date']} {$time}");
        $timeSchedule = /*$dateBooking->diffInHours(Carbon::now()) < 24 ? PackageItem::LESS_THAN_24 :*/
            PackageItem::SCHEDULE_ONE_TIME;

        return PackagePricing::whereHas('packagePet', function ($query) use ($params) {
            $query->where('package_pets.number_of_dogs', $params['pet_number']);
        })
            ->whereHas('packageItem', function ($query) use ($timeSchedule) {
                $query->where('package_items.type', $timeSchedule);
            })
            ->whereHas('packageItem.packageGroup', function ($query) use ($params) {
                $query->where('package_groups.id', $params['distance']);
                // ->where('package_groups.type', 'dog_running')
            })
            ->first();
    }

    /**
     * Get Dog Waste Removal's Price.
     *
     * @return mixed
     */
    public function getPriceWasteRemoval($params)
    {
        $validator = Validator::make($params, [
            'service_type' => 'required|exists:service_types,type',
            'schedule' => 'required|exists:package_items,id'
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return PackagePricing::whereHas('packagePet', function ($query) use ($params) {
            $query->where('package_pets.number_of_dogs', $params['pet_number']);
        })
            ->whereHas('packageItem', function ($query) use ($params) {
                $query->where('package_items.id', $params['schedule']);
            })
            ->whereHas('packageItem.packageGroup', function ($query) {
                $query->where('package_groups.type', 'dog_waste_removal');
            })
            ->first();
    }

    /**
     * Get Time Schedule.
     * eg: 0608am => 06:00:00
     *
     * @param $time
     * @param array $timeSchedule
     * @param string $period
     * @return mixed
     */
    public function getTimeSchedule($time, $timeSchedule, $period = 'start')
    {
        return isset($timeSchedule[$time]) ? (isset($timeSchedule[$time][$period]) ? $timeSchedule[$time][$period] : null) : null;
    }

    /**
     * Make Price.
     *
     * @param double $price
     * @return $this
     */
    public function makePrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get Price
     *
     * @return double
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Calculator Discount with promotion code.
     *
     * @param string $promotionCode
     * @return double
     */
    public function makeDiscount($serviceType = null, $promotionCode = null)
    {
        $percentage = 0;
        if ($promotion = $this->getPromotion($serviceType, $promotionCode)) {
            $percentage = $promotion->percentage;
        }
        $this->discount = round((($this->price * $percentage) / 100), 2);

        return $this;
    }

    /**
     * Get Discount
     *
     * @return double
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Calculator Tax
     *
     * @param double $price
     * @param array $option
     * @return double
     */
    public function makeTax($state)
    {
        $taxPercent = 0;
        if ($tax = Tax::where('state_id', $state)->first()) {
            $taxPercent = $tax->tax;
        }
        $this->tax = round((($this->price * $taxPercent) / 100), 2);

        return $this;
    }

    /**
     * Get Tax.
     *
     * @return double
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Calculator has Tax.
     *
     * @param double $price
     * @param  array $options
     * @return double
     */
    public function calPriceHasTax($price, $options = [])
    {
        $proCode = isset($options['code']) ? $options['code'] : null;
        $serviceType = isset($options['service_type']) ? $options['service_type'] : null;
        $stateId = isset($options['state_id']) ? $options['state_id'] : null;
        $instance = $this->makePrice($price)->makeDiscount($serviceType, $proCode);
        $price = $instance->getPrice();
        $discount = $instance->getDiscount();
        $priceHasDiscount = $price - $discount;
        $newInstance = $instance->makePrice($priceHasDiscount)->makeTax($stateId);
        $tax = $newInstance->getTax($stateId);

        return collect([
            'price' => $price,
            'discount' => $discount,
            'tax' => $tax,
            'total' => round(($priceHasDiscount + $tax), 2)
        ]);
    }

    /**
     * Calculator Payout.
     *
     * @param double $price
     * @returun double
     */
    public function calPayout()
    {
        $price = $this->price;
        return round(($price * 80) / 100);

    }

    /**
     * Get promotion from database.
     *
     * @param string $code
     * @return mixed
     */
    public function getPromotion($serviceType, $code = null)
    {
        if ($serviceType && $code) {
            $dateNow = Carbon::today(0);
            $dateNowF = $dateNow->format(config('common.date_db_format'));

            if ($promotion = Promotion::where('code', $code)
                ->where('service_type_id', $serviceType)
                ->where('status', Promotion::STATUS_ENABLE)
                ->whereDate('from_date', '<=', $dateNowF)
                ->whereNull('deleted_at')
                ->first()) {

                if ($promotion->to_date) {
                    if ($dateNow->diffInDays($promotion->to_date, false) < 0) {
                        return null;
                    }
                }

                if (
                    !empty($promotion) &&
                    ($promotion->total_promotion > $promotion->total_promotion_in_used)
                ) {

                    return $promotion;
                }
            }
        }

        return null;
    }

    /**
     * Get Timezone by location
     *
     * @param array
     * @return mixed
     */
    public function getTimezoneByLocation($location = [])
    {
        try {
            $strLocation = [
                (isset($location['lat']) ? $location['lat'] : 0),
                (isset($location['long']) ? $location['long'] : 0)
            ];
            $dateNow = Carbon::now();
            $endpoint = 'https://maps.googleapis.com/maps/api/timezone/json';
            $http_build_query = http_build_query([
                'location' => implode(',', $strLocation),
                // 'timestamp' => $dateNow->getTimestamp(),
                'key' => getenv('GOOGLE_API_KEY')
            ]);

            $client = new Client();
            $response = $client->request('GET', $endpoint . '?' . $http_build_query);
            if (200 != $response->getStatusCode()) {
                throw new \Exception('Get Timezone failure.', $response->getStatusCode());
            }

            $body = $response->getBody();
            $strContents = $body->getContents();
            $contents = \GuzzleHttp\json_decode($strContents, true);

            if (
                empty($contents['timeZoneId']) ||
                !isset($contents['status']) ||
                'OK' != strtoupper($contents['status'])
            ) {
                throw new \Exception(isset($contents['errorMessage']) ? $contents['errorMessage'] : 'Get Google Time Zone API failure.');
            }

            return $contents['timeZoneId'];
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}