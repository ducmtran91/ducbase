<?php

namespace App\Helpers;

use Blade;
use Carbon\Carbon;
use App\Modules\ContentManager\Models\Options;
use URL;

class Helper
{

    private $options;
    private $permissionAsigned;

    public function __construct()
    {
        $this->options = Options::all()->toArray();
    }

    public function menu($group = "main-menu")
    {
        $menu = new Menu($group);
        return $menu->generateMenu();
    }

    public function compress($soure, $destination)
    {
        $com = new Compress($soure, $destination);
        return $com->run();
    }

    public function extract($soure, $destination)
    {
        $com = new Compress($soure, $destination);
        return $com->extract();
    }

    public function widget($class, $option = [])
    {
        $class = "App\\Widgets\\" . str_replace(".", "\\", $class);
        $widget = new $class;
        return $widget->test();
    }

    public function taxonomyLink($taxonomy, $link = true)
    {
        $res = [];
        if ($link) {
            foreach ($taxonomy as $value) {
                $res[] = '<a href="' . url("/category/" . $value->slug) . '">' . $value->name . '</a>';
            }
        } else {
            foreach ($taxonomy as $value) {
                $res[] = $value->name;
            }
        }
        return implode(",", $res);
    }

    public function bbcode($content)
    {
        $bbcode = new BBCode();
        return $bbcode->toHTML($content);
    }

    public function option($keySearch)
    {
        $result = null;
        foreach ($this->options as $value) {
            if ($value['name'] == $keySearch) {
                $result = $value['value'];
            }
        }
        return $result;
    }

    public function appTitle($title)
    {
        return ($title == "") ? $this->option("site_title") : $title . " - " . $this->option("site_title");
    }

    public function menuList()
    {
        return true;
    }

    public function permissionList($role = 'administrator', $permissionAsigned)
    {
        $this->permissionAsigned = ($permissionAsigned != null) ? \GuzzleHttp\json_decode($permissionAsigned) : [];
        $permissionsList = config("permission");
        $permissionUI = "<ul class='list-group'><li class='list-group-item'><label><input type='checkbox' id='" . $role . "-role' name='fullaccess' value='" . $role . "'> Select all</label></li>";
        $permissionUI .= $this->passingToList($permissionsList, $role);
        $permissionUI .= '</ul>';

        return $permissionUI;
    }

    public function passingToList($array, $role)
    {
        $permissionAsigned = [];

        if ($this->permissionAsigned != null) {
            $permissionAsigned = get_object_vars($this->permissionAsigned);
        }

        $out = "<ul class='list-group'>";

        foreach ($array as $key => $permission) {
            $out .= "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#{$key}'> " . $permission['name'] . "  <span class='glyphicon glyphicon-plus'></span></button><div id='{$key}' class='collapse'>";

            foreach ($permission['actions'] as $elem) {
                $assigned = '';
                $fieldData = explode('@', $elem['action']);
                $parentKey = $this->recursive_array_search($elem['action'], $permissionAsigned);

                if ($parentKey !== false) {
                    $assigned = (array_key_exists($parentKey, $permissionAsigned) ? 'checked' : '');
                }

                //dd($elem['action'], $permissionAsigned, $parentKey, $assigned);

                $out .= "<li class='list-group-item'><input id='checkBoxChild-{$role}' name='permission[{$fieldData[0]}][]' type='checkbox' value='{$elem['action']}'  {$assigned}>" . (ucfirst($elem['name'])) . "</li>";
            }

            $out .= "</div>";
        }

        $out .= "</ul>";

        return $out;
    }

    public function recursive_array_search($needle, $haystack)
    {
        foreach ($haystack as $key => $value) {
            $current_key = $key;
            if ($needle === $value OR (is_array($value) && $this->recursive_array_search($needle, $value) !== false)) {
                return $current_key;
            }
        }

        return false;
    }

    /**
     * Recursive meta data
     *
     * @param array $meta
     * @param string $metaKey
     * @param array $request
     * @param  array $out
     * @return array $out
     */
    public function recursiveMeta($meta = [], $request = [], $out = [])
    {
        foreach ($meta as $key => $item) {
            if (
                isset($item['value']) &&
                isset($request[$item['name']]['value'])
            ) {
                $item['value'] = $request[$item['name']]['value'];
            }

            if (
                isset($item['xvalue']) &&
                isset($request[$item['name']]['xvalue'])
            ) {
                $item['xvalue'] = $request[$item['name']]['xvalue'];
            }

            if (!empty($item['items'])) {
                $item['items'] = $this->recursiveMeta($item['items'], $request[$item['name']]);
            }

            $out[] = $item;
        }

        return $out;
    }

    public function convertArray($array = [])
    {
        $result = null;

        if (!empty($array)) {
            $result = implode('", "', $array);
            $result = "\"{$result}\"";
        }

        return $result;
    }

    public function limit_content($string, $limit)
    {
        if (strlen($string) > $limit) {
            $string = substr($string, 0, $limit);
            return substr($string, 0, strrpos($string, ' ')) . '...';
        }

        return $string;
    }

    public function getItemsRemovedPrefixed($array, $prefix)
    {
        $keys = array_keys($array);
        $result = array();

        foreach ($keys as $key) {
            if (strpos($key, $prefix) === 0) {
                $result[str_replace($prefix, '', $key)] = $array[$key];
            }
        }

        return $result;
    }

    /**
     * Check if user has Route or not
     * @param String $routeName
     * @return boolean
     */
    public static function checkUserPermission($routeName)
    {
        /**
         * @var \App\User $user
         */
        $user = \Auth::guard('admin')->user();
        // If is super admin
        if ($user->isSuperAdmin()) {
            return true;
        }

        // Get role from cache if exist
        $roleCacheName = "role_permission_{$user->role_id}.php";
        $roleCacheFile = base_path() . '/storage/app/' . $roleCacheName;

        if (empty($user->roles) && file_exists($roleCacheFile)) {
            include_once $roleCacheFile;
            // $role_permission get from include file
            if (!empty($role_permission)) {
                $user->setRelation('roles', new \App\Entities\Roles($role_permission));
            }
        }

        // Check permission by user role (permission)
        $role = $user->roles;

        if (empty($role)) {
            return false;
        }

        if (empty($role->status)) {
            return false;
        }

        if ($user->hasRoute($routeName)) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public static function isAdmin()
    {
        /** @var \App\User $user */
        $user = \Auth::guard('admin')->user();

        return (!empty($user) && $user->isAdmin());
    }

    /**
     * @return bool
     */
    public static function isSuperAdmin()
    {
        /** @var \App\User $user */
        $user = \Auth::guard('admin')->user();

        return (!empty($user) && $user->isSuperAdmin());
    }

    /**
     * getLinkFromDataSource function
     *
     * @param string $link
     * @param string $default
     * @param string $path
     * @return string
     */
    public static function getLinkFromDataSource($link, $default = null, $path = null)
    {
        $baseUrl = URL::to('/') . '/';

        if ($link == null) {
            // Get default avatar if null
            $link = ($default == null) ? $baseUrl . trim(config('filepath.no_image'), '/') : $baseUrl . $default;
            return $link;
        }

        $regex = "/^(http|https):\/\//";
        $match = preg_match($regex, $link);

        if (!$match) {
            $link = ($path) ? (trim($path, '/') . '/' . $link) : $link;
            $link = $baseUrl . $link;
        }

        return $link;
    }

    /**
     * formatDate
     * @param $date
     */
    public static function formatDate($date, $format = "d/m/Y")
    {
        return date($format, strtotime($date));
    }

}
