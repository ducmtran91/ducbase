<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;

class Authorize
{
    protected $loginFailedAllow = 0;

    public function verifyLoginFailed($limit = null)
    {
        $this->loginFailedAllow = $limit ?: config('auth.loginFailedAllow', 0);

        return ($this->getLoginFailed() > $this->loginFailedAllow);
    }

    public function resetLoginFailed()
    {
        Session::put('countLoginFailed', 0);

        return $this;
    }

    public function getLoginFailed()
    {
        return Session::get('countLoginFailed');
    }

    public function setLoginFailed()
    {
        $countLoginFailed = $this->getLoginFailed();
        Session::put('countLoginFailed', $countLoginFailed + 1);

        return $this;
    }

    /**
     * Verify reCaptcha
     *
     * @param string $response
     * @return response
     */
    public function verifyReCaptcha($response)
    {
        try {
            $parameters = http_build_query([
                'secret' => env('CAPTCHA_SECRET'),
                'remoteip' => app('request')->getClientIp(),
                'response' => $response,
            ]);
            $url = 'https://www.google.com/recaptcha/api/siteverify';// . $parameters;
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_TIMEOUT, app('config')->get('recaptcha.options.curl_timeout', 30));

            $out = curl_exec($curl);
            $info = curl_getinfo($curl);
            $http_result = $info ['http_code'];
            curl_close($curl);
            if (200 != $http_result) {
                throw new \Exception('verifyReCaptchaError');
            }

            return ['success' => true];
            // return \GuzzleHttp\json_decode($out, true);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    public function getCountLoginFailedMessage()
    {
        return Lang::has('auth.countFailed')
            ? Lang::get('auth.countFailed')
            : 'You have logged in more than 3 times. Please enter the Captcha code below.';
    }
}
