<?php

namespace App\Helpers;

use App\Entities\WizepawzNotification as NotificationEntity;
use OneSignal;

class WizepawzNoti
{

    /**
     * Push notification use Onesignal.
     *
     * @param {string} $message
     * @param {array} $segment
     * @param {array} $options
     * @return void
     */
    public function pushUsingTags($message, $tags, $options = [])
    {
        try {
            $url = isset($options['url']) ? $options['url'] : null;
            $data = isset($options['data']) ? $options['data'] : null;
            $params = isset($options['params']) ? $options['params'] : [];
            $buttons = isset($options['buttons']) ? $options['buttons'] : null;
            $schedule = isset($options['schedule']) ? $options['schedule'] : null;

            OneSignal::addParams($params)->sendNotificationUsingTags($message, $tags, $url, $data, $buttons, $schedule);
        } catch (\Exception $exception) {

            \Log::error('Notification: ' . $exception->getMessage());
        }
    }

    /**
     * Push notification and store to DB use Onesignal.
     *
     * @param {string} $message
     * @param {array} $segment
     * @param {array} $options
     * @return mixed
     */
    public function pushUsingTagsAndStore($message, $tags, $options = [])
    {
        try {
            $url = isset($options['url']) ? $options['url'] : null;
            $data = isset($options['data']) ? $options['data'] : null;
            $params = isset($options['params']) ? $options['params'] : [];
            $buttons = isset($options['buttons']) ? $options['buttons'] : null;
            $schedule = isset($options['schedule']) ? $options['schedule'] : null;


            OneSignal::addParams($params)->sendNotificationUsingTags($message, $tags, $url, $data, $buttons, $schedule);

            return NotificationEntity::create($options);
        } catch (\Exception $exception) {
            \Log::error('Notification: ' . $exception->getMessage());

            return;
        }
    }

    /**
     * Push notification use Onesignal.
     *
     * @param {string} $message
     * @param {array} $segment
     * @param {array} $options
     * @return void
     */
    public function pushToSegment($message, $segment, $options = [])
    {
        try {
            $url = isset($options['url']) ? $options['url'] : null;
            $data = isset($options['data']) ? $options['data'] : null;
            $params = isset($options['params']) ? $options['params'] : [];
            $buttons = isset($options['buttons']) ? $options['buttons'] : null;
            $schedule = isset($options['schedule']) ? $options['schedule'] : null;

            OneSignal::addParams($params)->sendNotificationToSegment($message, $segment, $url, $data, $buttons, $schedule);
        } catch (\Exception $exception) {

        }
    }

    /**
     * Push notification and store to DB use Onesignal.
     *
     * @param {string} $message
     * @param {array} $segment
     * @param {array} $options
     * @return mixed
     */
    public function pushToSegmentAndStore($message, $segment, $options = [])
    {
        try {
            $url = isset($options['url']) ? $options['url'] : null;
            $data = isset($options['data']) ? $options['data'] : null;
            $params = isset($options['params']) ? $options['params'] : [];
            $buttons = isset($options['buttons']) ? $options['buttons'] : null;
            $schedule = isset($options['schedule']) ? $options['schedule'] : null;

            OneSignal::addParams($params)->sendNotificationToSegment($message, $segment, $url, $data, $buttons, $schedule);

            return NotificationEntity::create($options);
        } catch (\Exception $exception) {
            return;
        }

    }
}