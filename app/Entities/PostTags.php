<?php

namespace App\Entities;

use App\AppModel;

class PostTags extends AppModel
{
    public $timestamps = false;
    protected $fillable = [
        'id',
        'post_id',
        'tag_id'
    ];

    protected $casts = [
        'id' => 'integer',
        'post_id' => 'integer',
        'tag_id' => 'integer'
    ];

}
