<?php

namespace App\Entities;

use App\AppModel;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * Class Users
 * @package App\Entities
 * @version August 2, 2018, 9:07 am UTC
 */
class UsersFrontend extends AppModel implements AuthenticatableContract,CanResetPasswordContract{

    use Authenticatable, CanResetPassword;
    public $table = 'users_frontend';
    public $timestamps = false;
    public $fillable = [
        'full_name',
        'email',
        'mobile',
        'password',
        'active',
        'is_admin',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'full_name' => 'string',
        'email' => 'string',
        'mobile' => 'string',
        'password' => 'string',
        'active' => 'boolean',
        'remember_token' => 'string'
    ];

    protected $hidden = [
        'password',
        'remember_token'
    ];
}
