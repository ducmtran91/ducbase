<?php

namespace App\Entities;

use Eloquent as Model;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

/**
 * Class Roles
 * @package App\Entities
 * @version November 3, 2016, 2:34 am UTC
 */
class Roles extends Model {
    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';
    const ROLE_OWNER = 'owner';
    const ROLE_WALKER = 'walker';

    public $table = 'roles';
    public $fillable = [
        'id',
        'name',
        'description',
        'type',
        'permission',
        'status',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'type' => 'string',
        'permission' => 'string',
        'status' => 'integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * */
    public function users() {
        return $this->hasMany(\App\Entities\Users::class);
    }

    /**
     * @param bool $overwrite
     * @return bool
     */
    public function cachePermission($overwrite = true) {
        $fileName = "role_permission_{$this->id}.php";

        // Ignore if not allow overwrite with exist file
        if (Storage::disk('local')->exists($fileName) && !$overwrite) {
            return true;
        }

        $varContent = var_export($this->toArray(), true);
        $fileContent = '<?php $role_permission = ' . $varContent . ';';

        return Storage::disk('local')->put($fileName, $fileContent);
    }

    /**
     * @return bool
     */
    public function cleanCachePermission() {
        $fileName = "role_permission_{$this->id}.php";

        if (!Storage::disk('local')->exists($fileName)) {
            return true;
        }

        return Storage::disk('local')->delete($fileName);
    }

    /**
     * Fire events when create, update roles
     * The "booting" method of the model.
     * @link https://stackoverflow.com/a/38685534
     *
     * @return void
     */
    protected static function boot() {
        parent::boot();

        // When saved
        self::saved(function ($model) {
            $model->cachePermission();
        });

        self::created(function ($model) {
            $model->cachePermission();
        });

        self::updating(function ($model) {
            $model->updated_at = Carbon::now()->format(config('common.datetime_db_format'));
        });
        
        self::updated(function ($model) {
            $model->cachePermission();
        });

        self::deleted(function ($model) {
            $model->cleanCachePermission();
        });
    }
    
    /**
     * @return array
     */
    public static function getHiddenRoles() {
        return [1];
    }

}
