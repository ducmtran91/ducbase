<?php

namespace App\Entities;

use App\AppModel;

class SliderTranslations extends AppModel
{
    public $timestamps = false;
    protected $table = 'gallery_translations';
    protected $primaryKey = 'gallery_translations_id';

    protected $fillable = [
        'name',
        'data'
    ];

    protected $casts = [
        'name' => 'string',
        'data' => 'string',
    ];
}
