<?php

namespace App\Entities;

use App\AppModel;

class Gallery extends AppModel
{
    public $timestamps = false;
    protected $table = 'gallery';
    protected $fillable = [
        'id',
        'image',
        'url',
        'type',
        'position',
        'status'
    ];

    protected $casts = [
        'id' => 'integer',
        'image' => 'string',
        'url' => 'string',
        'type' => 'string',
        'position' => 'integer',
        'status' => 'boolean',
    ];
}
