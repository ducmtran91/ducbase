<?php

namespace App\Entities;

use App\AppModel;

class Languages extends AppModel
{
    public $timestamps = false;
    protected $table = 'languages';
    protected $fillable = [
        'id',
        'name',
        'image',
        'locale',
        'status'
    ];

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'image' => 'string',
        'locale' => 'string',
        'status' => 'boolean',
    ];
}
