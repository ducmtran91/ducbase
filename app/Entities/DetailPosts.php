<?php

namespace App\Entities;

use App\AppModel;

class DetailPosts extends AppModel
{
    public $timestamps = false;
    protected $fillable = [
        'id',
        'post_id',
        'name',
        'description',
        'position',
        'status',
    ];

    protected $casts = [
        'id' => 'integer',
        'post_id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'position' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function posts() {
        return $this->belongsTo(\App\Entities\Posts::class, 'post_id','id');
    }

}
