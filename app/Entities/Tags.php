<?php

namespace App\Entities;

use App\AppModel;

class Tags extends AppModel
{
    public $timestamps = false;

    protected $fillable = [
        'id',
        'name',
        'status'
    ];

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'status' => 'boolean'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts() {
        return $this->belongsToMany(\App\Entities\Posts::class, 'post_tags','tag_id','post_id');
    }
}
