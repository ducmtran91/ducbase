<?php

namespace App\Entities;

use App\AppModel;

class BannerTranslations extends AppModel
{
    public $timestamps = false;
    protected $table = 'gallery_translations';
    protected $primaryKey = 'gallery_translations_id';

    protected $fillable = [
        'name'
    ];

    protected $casts = [
        'name' => 'string'
    ];
}
