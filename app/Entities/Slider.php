<?php

namespace App\Entities;

use App\AppModel;

class Slider extends AppModel
{
    use \Dimsav\Translatable\Translatable;
    public $timestamps = false;
    protected $table = 'gallery';
    public $translatedAttributes  = ['name','data'];
    public $translationModel = 'App\Entities\SliderTranslations';
    public $translationForeignKey = 'gallery_id';
    protected $fillable = [
        'id',
        'image',
        'slug',
        'type',
        'position',
        'status'
    ];

    protected $casts = [
        'id' => 'integer',
        'image' => 'string',
        'slug' => 'string',
        'type' => 'string',
        'position' => 'integer',
        'status' => 'boolean',
    ];
}
