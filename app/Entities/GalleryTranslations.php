<?php

namespace App\Entities;

use App\AppModel;

class GalleryTranslations extends AppModel
{
    public $timestamps = false;
    protected $primaryKey = 'gallery_translations_id';
    protected $table = 'gallery_translations';
    protected $fillable = [
        'name',
        'data',
        'gallery_id',
        'languages_id'
    ];

    protected $casts = [
        'name' => 'string',
        'data' => 'string',
        'gallery_id' => 'integer',
        'languages_id' => 'integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gallery() {
        return $this->belongsTo(\App\Entities\Gallery::class, 'gallery_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function languages() {
        return $this->belongsTo(\App\Entities\Languages::class,'languages_id','id');
    }
}
