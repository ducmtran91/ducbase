<?php

namespace App\Entities;

use App\AppModel;
use App\Helpers\Compress;
use Config;
use File;

/**
 * Class Language
 * @package App\Entities
 * @version November 7, 2018, 3:44 am UTC
 *
 * @property int $id
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 * @property bool active Active status.<br>
 * Allow values: 0, 1. Notes: 0: false, 1: true. Default = 1.
 * @property int country_id Foreign key with countries table.
 * @property \App\Entities\Country country
 * @property string locale ISO 3166-1 alpha-2 code.<br>
 * <a href="https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2">ISO 3166-1 alpha-2</a>
 * @property string name
 */
class Language extends AppModel
{

    public $table = 'languages';

    /**
     * @var string $translatableCacheFile
     */
    public $translatableCacheFile = 'translatable.php';

    public $fillable = [
        'created_at',
        'updated_at',
        'active',
        'country_id',
        'locale',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'active' => 'boolean',
        'country_id' => 'integer',
        'locale' => 'string',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        /*'country_id' => 'integer',*/
        'locale' => 'max:2',
        'name' => 'max:255',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function country()
    {
        return $this->belongsTo(\App\Entities\Country::class);
    }

    /**
     * Cache translatable to file
     *
     * @param bool $force
     * @return bool
     */
    public function cacheLanguages($force = false)
    {
        $cacheFile = $this->table . '.php';
        $exist = \Storage::disk('local')->exists($cacheFile);

        if (!$exist || $force) {
            // Get support languages from db
            $languages = $this->active()
                ->orderBy('name', static::ORDER_ASC)
                ->pluck('name', 'locale')
                ->toArray();
            // Default locale from config
            $default = Config::get('app.locale');

            // If default locale (language) isn't define on db, we need to include it to cache
            if (!array_key_exists($default, $languages)) {
                // Check if exist on config first
                // If not in config, we will append a default locale, with "Default" name
                $configLanguages = Config::get('languages');
                $tmpDefault = [$default => 'Default'];

                if ($configLanguages[$default]) {
                    $tmpDefault = [$default => $configLanguages[$default]];
                }

                $languages = $tmpDefault + $languages;
            }

            // Flat file content to php file
            $content = '<?php return ' . var_export($languages, true) . ';';

            \Storage::disk('local')->put($cacheFile, $content);
        }

        return true;
    }

    /**
     * Remove translatable to file
     *
     * @return bool
     */
    public function clearCacheLanguages()
    {
        $cacheFile = $this->table . '.php';
        $exist = \Storage::disk('local')->exists($cacheFile);

        if ($exist) {
            \Storage::disk('local')->delete($cacheFile);
        }

        return true;
    }

    /**
     * Remove translatable to file
     * @return array|null
     */
    public function readCacheLanguages()
    {
        $cacheFile = storage_path('app') . DIRECTORY_SEPARATOR . $this->table . '.php';

        if (\File::exists($cacheFile)) {
            return include($cacheFile);
        }

        return null;
    }

    /**
     *  Clone new language localization
     *
     * @link https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
     * @param string $target To new lang. ISO 3166-1 alpha-2 code.
     * @param string $source From old lang. ISO 3166-1 alpha-2 code.
     * @return bool
     */
    public function cloneLanguageFiles($target, $source = 'en')
    {
        // Path to the project's resources folder
        $targetPath = resource_path() . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . $target;
        $sourcePath = resource_path() . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . $source;

        if (!File::exists($targetPath)) {
            // path does not exist
            return File::copyDirectory($sourcePath, $targetPath);
        }

        return false;
    }

    /**
     * Delete language localization directory
     *
     * @param $target
     * @return bool
     */
    public function clearLanguageFiles($target)
    {
        // Path to the project's resources folder
        $sourcePath = resource_path() . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . $target . DIRECTORY_SEPARATOR;
        $destPath = resource_path() . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . $target . '-' . date('Ymd_His') . '.zip';

        if (File::exists($sourcePath)) {
            // Backup localization dir before delete
            $compress = new Compress($sourcePath, $destPath);
            $compress->run();

            // path does not exist
            return File::deleteDirectory($sourcePath);
        }

        return false;
    }

    /**
     * Cache translatable to file
     *
     * @param bool $force
     * @return bool
     */
    public function cacheTranslatable($force = false)
    {
        $cacheFile = $this->translatableCacheFile;
        $exist = \Storage::disk('local')->exists($cacheFile);

        if (!$exist || $force) {
            // Get support languages from db
            $languages = $this->active()
                ->pluck('locale')
                ->toArray();
            $configTranslatable = Config::get('translatable');
            /** @link https://stackoverflow.com/a/18373723/10174865 */
            $configTranslatable['locales'] = array_unique(array_merge($configTranslatable['locales'], $languages), SORT_REGULAR);
            /** @link https://stackoverflow.com/a/5980079/10174865 */
            $varContent = preg_replace('(\d+\s=>)', '', var_export($configTranslatable, true));
            $fileContent = '<?php return ' . $varContent . ';';

            \Storage::disk('local')->put($cacheFile, $fileContent);
        }

        return true;
    }

    /**
     * Remove translatable to file
     *
     * @return bool
     */
    public function clearCacheTranslatable()
    {
        $cacheFile = $this->translatableCacheFile;
        $exist = \Storage::disk('local')->exists($cacheFile);

        if ($exist) {
            \Storage::disk('local')->delete($cacheFile);
        }

        return true;
    }

    /**
     * Remove translatable to file
     * @return array|null
     */
    public function readCacheTranslatable()
    {
        $cacheFile = storage_path('app') . DIRECTORY_SEPARATOR . $this->translatableCacheFile;

        if (\File::exists($cacheFile)) {
            return include($cacheFile);
        }

        return null;
    }

    /**
     * Fire events when create, update roles
     * The "booting" method of the model.
     * @link https://stackoverflow.com/a/38685534
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        // When saved
        self::saved(function ($model) {
            $model->cacheLanguages(true);

            // If save new record
            /*if ($model->wasRecentlyCreated) {*/
                // Clone lang for localization
                $model->cloneLanguageFiles($model->locale);
            /*}*/

            // Cache translatable if not exist
            $model->cacheTranslatable(true);
        });

        /*self::created(function ($model) {
        });*/

        /*self::updated(function ($model) {
        });*/

        self::deleted(function ($model) {
            //$model->clearCacheLanguages();
            // Update config file
            $model->cacheLanguages(true);
            // Remove localization folder
            $model->clearLanguageFiles($model->locale);
            // Cache translatable if not exist
            $model->cacheTranslatable(true);
        });

    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $locale Language locale. By ISO 3166-1 alpha-2.
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByLocale($query, $locale)
    {
        return $query->where('locale', $locale);
    }

    /**
     * Get Language record by locale
     *
     * @param string $locale
     * @return \App\Entities\Language
     */
    public static function getByLocale($locale)
    {
        $record = static::active()
            ->byLocale($locale)
            ->first();

        return $record;
    }

}
