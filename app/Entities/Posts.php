<?php

namespace App\Entities;

use App\AppModel;

class Posts extends AppModel
{
    protected $fillable = [
        'id',
        'name',
        'slug',
        'category_id',
        'excerpt',
        'description',
        'position',
        'status',
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'category_id' => 'integer',
        'excerpt' => 'string',
        'description' => 'string',
        'position' => 'integer',
        'status' => 'boolean',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categories() {
        return $this->belongsTo(\App\Entities\Categories::class, 'category_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags() {
        return $this->belongsToMany(\App\Entities\Tags::class, 'post_tags','post_id','tag_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function detailPosts() {
        return $this->hasMany(\App\Entities\DetailPosts::class, 'post_id','id');
    }
}
