<?php

namespace App\Entities;

use App\AppModel;

class Categories extends AppModel
{
    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = ['name'];
    public $translationModel = 'App\Entities\CategoryTranslations';
    public $translationForeignKey = 'category_id';
    protected $fillable = [
        'id',
        'slug',
        'parent_id',
        'position',
        'status',
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'id' => 'integer',
        'slug' => 'string',
        'parent_id' => 'integer',
        'position' => 'integer',
        'status' => 'boolean',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function child() {
        return $this->hasMany('\App\Entities\Categories', 'parent_id','id')->orderBy('position');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parent() {
        return $this->hasOne('\App\Entities\Categories', 'id','parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function posts() {
        return $this->hasOne('\App\Entities\Posts', 'id','category_id');
    }
}
