<?php

namespace App\Entities;

use App\AppModel;
use App\Services\Common;
use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;

/**
 * Class Users
 * @package App\Entities
 * @version August 2, 2018, 9:07 am UTC
 */
class Users extends AppModel {

    const SUPER_ADMIN_ID = 1; // User ID
    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';
    const ROLE_OWNER = 'owner';
    const ROLE_WALKER = 'walker';
    const IS_YES = 1;
    const IS_NO = 0;

    public $table = 'users';
    public $timestamps = false;
    public $fillable = [
        'id',
        'role_id',
        'tax_id',
        'provider',
        'status',
        'is_admin',
        'first_name',
        'last_name',
        'name',
        'email',
        'password',
        'temp_password',
        'description',
        'api_token',
        'state_id',
        'city_id',
        'zip_code',
        'avatar',
        'remember_token',
        'phone',
        'gender',
        'address',
        'location',
        'credit_card_info',
        'credit_balance',
        'car_number',
        'identification',
        'fb_id',
        'fb_token',
        'gg_id',
        'gg_token',
        'tw_id',
        'tw_token',
        'last_login',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'role_id' => 'integer',
        'tax_id' => 'integer',
        'provider' => 'string',
        'status' => 'boolean',
        'is_admin' => 'boolean',
        'first_name' => 'string',
        'last_name' => 'string',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'temp_password' => 'string',
        'description' => 'string',
        'api_token' => 'string',
        'state_id' => 'integer',
        'city_id' => 'integer',
        'zip_code' => 'integer',
        'avatar' => 'string',
        'remember_token' => 'string',
        'phone' => 'string',
        'gender' => 'integer',
        'address' => 'string',
        'location' => 'string',
        'credit_card_info' => 'string',
        'credit_balance' => 'float',
        'car_number' => 'string',
        'identification' => 'string',
        'fb_id' => 'string',
        'fb_token' => 'string',
        'gg_id' => 'string',
        'gg_token' => 'string',
        'tw_id' => 'string',
        'tw_token' => 'string'
    ];
    protected $appends = array('fullname');

    public function getFullnameAttribute() {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];

    protected function getLocationAttribute($value) {
        try {
            if (empty($value)) {
                return null;
            }

            return \GuzzleHttp\json_decode($value, true);
        } catch (\Exception $exception) {
            return null;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * */
    public function role() {
        return $this->belongsTo(\App\Entities\Roles::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * */
    public function tax() {
        return $this->belongsTo(\App\Entities\Tax::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * */
    public function pets() {
        return $this->hasMany(\App\Entities\Pet::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * */
    public function paymentInfos() {
        return $this->hasMany(\App\Entities\PaymentInfo::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * */
    public function state() {
        return $this->belongsTo(\App\Entities\State::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * */
    public function city() {
        return $this->belongsTo(\App\Entities\City::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * */
    public function zipcode() {
        return $this->belongsTo(\App\Entities\ZipCode::class, 'zip_code');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * */
    public function application() {
        return $this->hasOne(\App\Entities\Application::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * */
    public function services() {
        return $this->hasMany(\App\Entities\ObjectServices::class, 'object_id')->where('type', 'user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * */
    public function ownerJobs() {
        return $this->hasMany(\App\Entities\Walks::class, 'owner_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * */
    public function walkerJobs() {
        return $this->hasMany(\App\Entities\Walks::class, 'walker_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * */
    public function walkerBookings() {
        return $this->hasMany(\App\Entities\Booking::class, 'walker_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * */
    public function walkerJobsStatus($status, $bookedDate = array()) {
        $query = $this->hasMany(\App\Entities\Walks::class, 'walker_id')->where('status', $status);

        if (!empty($bookedDate)) {
            $query = $query->whereIn('booked_date', $bookedDate);
        }

        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * */
    public function walkerAttachedFiles() {
        return $this->hasMany(\App\Entities\ObjectFiles::class, 'object_id')->where('type', 'user');
    }

    /**
     * @return boolean
     * */
    public function removeImage() {
        if (!empty($this->avatar)) {
            $commonService = new Common();

            return $commonService->removeFile($this->avatar, config('filepath.user_image'));
        }
        
        if($this->role->type == 'walker' && !empty($this->walkerAttachedFiles)) {
            foreach ($this->walkerAttachedFiles as $file) {                
                $commonService = new Common();
                $commonService->removeFile($file->file, config('filepath.proposal_image'));
                
                $file->delete();
            }
        }

        return true;
    }

    /**
     * Fire events when create, update roles
     * The "booting" method of the model.
     * @link https://stackoverflow.com/a/38685534
     *
     * @return void
     */
    protected static function boot() {
        parent::boot();

        self::deleting(function ($model) {
            if (!empty($model->services)) {
                foreach ($model->services as $service) {
                    $service->delete();
                }
            }

            if (!empty($model->application->services)) {
                foreach ($model->application->services as $service) {
                    $service->delete();
                }
            }
        });

        self::deleted(function ($model) {
            $model->removeImage();
        });

        self::updating(function ($model) {
            $model->updated_at = Carbon::now()->format(config('common.datetime_db_format'));
            $model->name = "{$model->first_name} {$model->last_name}";
        });
    }

}
