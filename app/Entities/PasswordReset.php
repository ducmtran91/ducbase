<?php

namespace App\Entities;

use Eloquent as Model;

/**
 * Class PasswordReset
 * @package App\Entities
 * @version May 21, 2018, 4:43 am UTC
 */
class PasswordReset extends Model {

    public $table = 'password_resets';
    public $timestamps = false;
    protected $primaryKey = 'email';
    public $fillable = [
        'email',
        'token',
        'created_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'email' => 'string',
        'token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];

    public function users() {
        return $this->hasOne(\App\Entities\Users::class);
    }
}
