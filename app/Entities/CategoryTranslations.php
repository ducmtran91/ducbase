<?php

namespace App\Entities;

use App\AppModel;

class CategoryTranslations extends AppModel
{
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    protected $casts = [
        'name' => 'string'
    ];
}
