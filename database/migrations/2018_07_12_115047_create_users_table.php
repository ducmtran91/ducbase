<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->enum('provider', ['local', 'facebook', 'google', 'twitter']);
            $table->boolean('status')->default(false);
            $table->boolean('is_admin')->default(false);
            $table->string('name', 255);
            $table->string('first_name', 255)->nullable();
            $table->string('last_name', 255)->nullable();
            $table->string('email', 255)->unique();
            $table->string('password', 255)->nullable();
            $table->string('temp_password', 255)->nullable();
            $table->longText('description')->nullable();
            $table->string('api_token', 60)->unique()->nullable();
            $table->longText('avatar')->nullable();
            $table->rememberToken();
            $table->string('phone', 11)->nullable();
            $table->integer('gender')->nullable();
            $table->longText('address')->nullable();
            $table->string('location', 255)->nullable();
            $table->string('credit_card_info', 255)->nullable();
            $table->float('credit_balance')->nullable();
            $table->string('fb_id', 255)->nullable();
            $table->longText('fb_token')->nullable();
            $table->string('gg_id', 255)->nullable();
            $table->longText('gg_token')->nullable();
            $table->string('tw_id', 255)->nullable();
            $table->longText('tw_token')->nullable();
            $table->timestamp('last_login')->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->nullable();

            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('users');
    }

}
