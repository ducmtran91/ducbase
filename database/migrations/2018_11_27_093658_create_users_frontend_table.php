<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersFrontendTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_frontend', function(Blueprint $table) {
           $table->increments('id');
           $table->string('full_name');
           $table->string('email')->unique();
           $table->string('password');
           $table->string('mobile',20);
           $table->boolean('active')->default(true);
           $table->string('remember_token',100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_frontend');
    }
}
