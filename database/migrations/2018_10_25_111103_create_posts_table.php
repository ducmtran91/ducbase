<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
           $table->increments('id');
           $table->unsignedInteger('category_id');
           $table->string('name', 255);
           $table->string('excerpt', 255);
           $table->text('description');
           $table->string('slug', 255)->unique();
           $table->tinyInteger('position')->default(0);
           $table->boolean('status')->default(false);
           $table->timestamps();
           $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
