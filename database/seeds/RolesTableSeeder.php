<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('roles')->insert([
            [
                'id' => 1,
                'name' => 'Supper Admin',
                'description' => 'This is supper admin',
                'type' => 'admin',
                'status' => 1
            ],
            [
                'id' => 2,
                'name' => 'Administrator',
                'description' => 'This is administrator',
                'type' => 'user',
                'status' => 1
            ],
            [
                'id' => 3,
                'name' => 'Dog Owner',
                'description' => 'This is dog owner',
                'type' => 'owner',
                'status' => 1
            ],
            [
                'id' => 4,
                'name' => 'Dog Walker',
                'description' => 'This is dog walker',
                'type' => 'walker',
                'status' => 1
            ]
        ]);
    }

}
