<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = \Carbon\Carbon::now()->toDateTimeString();
        DB::table('languages')->insert([
            [
                'name' => 'Vietnamese',
                'locale' => 'vn',
                'updated_at' => $date,
                'created_at' => $date
            ],
            [
                'name' => 'English',
                'locale' => 'en',
                'updated_at' => $date,
                'created_at' => $date
            ]
        ]);
    }
}
