<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	[
                'role_id' => 1,
                'provider' => 'local',
                'status' => true,
                'is_admin' => true,                
	            'name' => 'Administrator',
	            'email' => 'henry.tran@qsoft.com.vn',
                'password' => Hash::make('Admin@123456!'),                
                'api_token' => str_random(60),
                'address' => 'Hà Nội',
                'location' => '21.031312,105.852433',
                'avatar' => "default-user.png"                
            ]
        ]);
    }
}
