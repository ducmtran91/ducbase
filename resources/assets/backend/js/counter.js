function Counter() {}

Counter.fn = {
    init: function() {        
        Counter.fn.initCounter.call(this);     
        Counter.fn.counterHandle.call(this);                  
    }, 
    
    initCounter: function() {
        $('[data-id="counter-from"]').map(function(){
            Counter.fn.counterCheck(this);     
        });
    },

    counterHandle: function() {
        $(document).on('keypress, keydown, keyup', '[data-id="counter-from"]', function(){
            Counter.fn.counterCheck(this);    
        });
    }, 
    
    counterCheck: function(_this) {
        var root = $(_this).closest('[data-id="counter-root"]');

        root.find('[data-id="counter-to"]').empty().text($(_this).val().length);        
    },

    rule: function() {
        $(document).ready(function(){ 
            Counter.fn.init.call(this);                        
        });
    }
};

Counter.fn.rule();
