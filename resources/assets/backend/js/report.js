/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function Report() {}

Report.fn = {
    init: function() {        
        Report.fn.view.call(this);       
        Report.fn.add.call(this);       
        Report.fn.remove.call(this);       
    }, 
    
    view: function() {
        $(document).on('click', '.btn-view-report', function() {
            var _this = this;
            var link = $(_this).attr('data-link');            
            
            $('body').loading('toggle');

            $.ajax({
                type: 'GET',
                url: link,
                success: function (response) {
                    $('body').loading('toggle');
                }
            }).success(function (response) {
                if (response.meta.code === 200) {
                    if($("#view-report-modal").length) {
                        $(".modal-backdrop").remove();
                        $("#view-report-modal").remove(); 
                    }

                    $('body').append(response.response.data);
                    $("#view-report-modal").modal('show');     
                } else {
                    swal("Error!", response.meta.message, "error");
                }
            });
        });
    },
    
    add: function() {
        $(document).on('click', '.report-btn-add-image', function(){
           var html =   '<div class="row col-sm-12">'+
                            '<div class="fileinput fileinput-new rp-image-item" data-provides="fileinput">'+
                                '<div class="fileinput-preview fileinput-exists thumbnail img-thumb"></div>'+
                                '<div class="rp-preview f-left">'+
                                    '<span class="btn btn-default btn-file btn-rp-left">'+
                                        '<span class="fileinput-new">Select image</span>'+
                                        '<span class="fileinput-exists">Change</span>'+
                                        '<input type="file" name="image[]" accept="image/*">'+
                                    '</span>'+
                                    '<a href="#" class="btn btn-default btn-rp-right fileinput-exists" data-dismiss="fileinput">Remove</a>'+
                                '</div>'+
                                '<button type="button" class="btn btn-danger f-left report-btn-remove-image">'+
                                    '<i class="fa fa-remove" aria-hidden="true"></i>'+                                
                                '</button>'+
                            '</div>'+                            
                        '</div>';
                
            $(this).before(html);
            
            if($('.report-images .rp-image-item').length == 5) {
                $('.report-btn-add-image').hide();
            }
        });
    },
    
    remove: function() {
        $(document).on('click', '.report-btn-remove-image', function() {
            $(this).closest('.row').remove();
            
            if($('.report-images .rp-image-item').length < 5) {
                $('.report-btn-add-image').show();
            }
        });
    },
    
    rule: function() {
        $(document).ready(function(){ 
            Report.fn.init.call(this);                        
        });
    }
};

Report.fn.rule();