function AssignJobs() {
}

AssignJobs.fn = {
    init: function () {
        //CASE ASSIGN TO DOG WALKER IN WALK LIST
        if($('.have-walk-assign').length) {
            AssignJobs.fn.openModalWalk.call(this);
            AssignJobs.fn.submitModalWalk.call(this);
            AssignJobs.fn.clearSearchWalk.call(this);
        }
        //CASE ASSIGN TO DOG WALKER IN WASTE REMOVAL LIST
        if($('.have-waste-assign').length) {
            AssignJobs.fn.openModalWaste.call(this);
            AssignJobs.fn.navActionsWaste.call(this);
            AssignJobs.fn.submitModalWaste.call(this);
            AssignJobs.fn.clearSearchWaste.call(this);
        }
    },
    //BEGIN: WALK
    openModalWalk: function() {
        $(document).on('click', '.btn-walk-assign', function() {
            AssignJobs.fn.openModalProcessWalk.call(this);
        });  
        
        var timeout = null;
        
        $(document).on('keyup', '.assign-input-search', function() {
            var _this = this;
            
            clearTimeout(timeout);
            timeout = setTimeout(function() {
                if($(_this).val().length > 0) {
                    AssignJobs.fn.openModalProcessWalk.call(_this);                    
                } else {
                    $('#assign-clear').trigger('click');
                }                 
            }, 1000);          
        });
    },
    
    submitModalWalk: function() {
        $(document).on('click', '#assign-modal .btn-assign', function() {
            var _this = this;
            var walkerId = $('[name="assign_walkers"]:checked').val();
            var token = $('meta[name="csrf-token"]').attr('content');
            var link = $(_this).attr('data-link'); 
            var key = $(_this).attr('data-key');
            var ids = $('#job-root-ids').val();
            var title = $('#job-title').val();
            
            if(!walkerId) {
                swal({
                    type: "error",
                    title: "You need to choose a Dog Walker",
                    confirmButtonClass: "btn-danger",
                    html: true
                });
                
                return false;
            }
            
            $('body').loading('toggle');

            $.ajax({
                type: 'POST',
                url: link,
                data: {
                    "_token": token,
                    "key": key,
                    "ids": ids,
                    "walkerId": walkerId,
                    "title": title
                },
                success: function (response) {
                    $('body').loading('toggle');
                }
            }).success(function (response) {
                if (response.meta.code === 200) {
                    swal("Success!", response.meta.message, "success");
                                                        
                    setTimeout(function(){ 
                        location.reload();
                    }, 2000);                      
                } else {
                    swal("Error!", response.meta.message, "error");
                }
            });
        });
    },
    
    clearSearchWalk: function() {
        $(document).on('click', '#assign-clear', function() {
            var jobIds = $('#job-root-ids').val();
            var jobTitle = $('#job-root-title').val();
            var jobKey = $('#job-root-key').val();
            var link = $(this).attr('data-link');
            var triggerData = {link: link, key: jobKey, title: jobTitle, id: jobIds};
            
            AssignJobs.fn.openModalProcessWalk.call(this, triggerData);
        }); 
    },
    
    openModalProcessWalk: function(triggerData){
        var token = $('meta[name="csrf-token"]').attr('content');
        
        if(!triggerData) {
            var _this = this;        
            var link = $(_this).attr('data-link');      
            var key = $(_this).attr('data-key');
            var title = $(_this).attr('data-title');
            var data = {"_token": token, "key": key, "title": title};
            
            if(key === 'open-modal-item') {
                var array = [$(_this).attr('data-id')];
                data.ids = array.join(); 
            } else if(key === 'open-modal-bulk') {
                var array = new Array();                  

                $("input.checkbox_item:checkbox[name=checkbox]:checked").each(function () {
                    array.push($(this).val());
                });  
                
                data.ids = array.join(); 
            } else {                
                data.ids = $('#job-root-ids').val();
                data.keyword = $(_this).val();
            }                         
        } else {
            var link = triggerData.link;      
            var data = {"_token": token, "key": triggerData.key, "title": triggerData.title, "ids": triggerData.id}; 
        }                

        if(!data.ids) {
            swal({
                type: "error",
                title: "Choose at least one " + title,
                confirmButtonClass: "btn-danger",
                html: true
            });

            return false;           
        }

        $('body').loading('toggle');

        $.ajax({
            type: 'POST',
            url: link,
            data: data,
            success: function (response) {
                $('body').loading('toggle');
            }
        }).success(function (response) {
            if (response.meta.code === 200) {
                if(key === 'search') {
                    $("#assign-modal").find('.modal-body').remove();
                    $("#assign-modal").find('.modal-header').after(response.response.data);
                } else {
                    if($("#assign-modal").length) {
                        $(".modal-backdrop").remove();
                        $("#assign-modal").remove(); 
                    }

                    $('body').append(response.response.data);
                    $("#assign-modal").modal('show');                        
                }         
            } else {
                swal("Error!", response.meta.message, "error");
            }
        });
    },
    //END: WALK
    
    //BEGIN: WASTE REMOVAL
    openModalWaste: function() {
        $(document).on('click', '.btn-walk-assign', function() {
            AssignJobs.fn.openModalProcessWaste.call(this);
        });  
        
        var timeout = null;
        
        $(document).on('keyup', '.assign-input-search', function() {
            var _this = this;
            
            clearTimeout(timeout);
            timeout = setTimeout(function() {
                if($(_this).val().length > 0) {
                    AssignJobs.fn.openModalProcessWaste.call(_this);                    
                } else {
                    $('#assign-clear').trigger('click');
                }                 
            }, 1000);          
        });
    },
    
    navActionsWaste: function() {
        $(document).on('click', '.btn-assign-back', function() {
            $('[aria-controls="assign-waste-step-1"]').trigger('click');
        });
        
        $(document).on('click', '.btn-assign-continue', function() {
            $('[aria-controls="assign-waste-step-2"]').trigger('click');
        });
    },
    
    submitModalWaste: function() {
        $(document).on('click', '#assign-modal .btn-assign', function() {
            var _this = this;
            var walkerId = $('[name="assign_walkers"]:checked').val();
            var bookedTimeType = $('[name="booked_time_type"]:checked').val();
            var bookedDate = $('#assign-date').val();
            var token = $('meta[name="csrf-token"]').attr('content');
            var link = $(_this).attr('data-link'); 
            var key = $(_this).attr('data-key');
            var ids = $('#job-root-ids').val();
            var title = $('#job-title').val();
            var error = "";
            
            if(!bookedTimeType) { error = "You need to choose a time to take job"; }
            if(!bookedDate) { error = "You need to choose a date to take job"; }
            if(!walkerId) { error = "You need to choose a Dog Walker"; }
            
            if(error.length > 0) {
                swal({
                    type: "error",
                    title: error,
                    confirmButtonClass: "btn-danger",
                    html: true
                });
                
                return false;
            }
            
            $('body').loading('toggle');

            $.ajax({
                type: 'POST',
                url: link,
                data: {
                    "_token": token,
                    "key": key,
                    "ids": ids,
                    "walkerId": walkerId,
                    "bookedTimeType": bookedTimeType,
                    "bookedDate": bookedDate,
                    "title": title
                },
                success: function (response) {
                    $('body').loading('toggle');
                }
            }).success(function (response) {
                if (response.meta.code === 200) {
                    swal("Success!", response.meta.message, "success");
                                                        
                    setTimeout(function(){ 
                        location.reload();
                    }, 2000);                      
                } else {
                    swal("Error!", response.meta.message, "error");
                }
            });
        });
    },
    
    clearSearchWaste: function() {
        $(document).on('click', '#assign-clear', function() {
            var jobIds = $('#job-root-ids').val();
            var jobTitle = $('#job-root-title').val();
            var jobKey = $('#job-root-key').val();
            var link = $(this).attr('data-link');
            var triggerData = {link: link, key: jobKey, title: jobTitle, id: jobIds};
            
            AssignJobs.fn.openModalProcessWaste.call(this, triggerData);
        }); 
    },
    
    openModalProcessWaste: function(triggerData){                
        var token = $('meta[name="csrf-token"]').attr('content');        
        
        if(!triggerData) {
            var _this = this;        
            var link = $(_this).attr('data-link');      
            var key = $(_this).attr('data-key');
            var title = $(_this).attr('data-title');
            var data = {"_token": token, "key": key, "title": title};
            
            if(key === 'open-modal-item') {
                var array = [$(_this).attr('data-id')];
                data.ids = array.join(); 
            } else if(key === 'open-modal-bulk') {
                var array = new Array();                  

                $("input.checkbox_item:checkbox[name=checkbox]:checked").each(function () {
                    array.push($(this).val());
                });  
                
                data.ids = array.join(); 
            } else {                
                data.ids = $('#job-root-ids').val();
                data.keyword = $(_this).val();
            }                         
        } else {
            var link = triggerData.link;      
            var data = {"_token": token, "key": triggerData.key, "title": triggerData.title, "ids": triggerData.id}; 
        }                

        if(!data.ids) {
            swal({
                type: "error",
                title: "Choose at least one " + title,
                confirmButtonClass: "btn-danger",
                html: true
            });

            return false;           
        }

        $('body').loading('toggle');

        $.ajax({
            type: 'POST',
            url: link,
            data: data,
            success: function (response) {
                $('body').loading('toggle');
            }
        }).success(function (response) {
            if (response.meta.code === 200) {
                if(key === 'search') {
                    $("#popup-assign-job-table").find('tbody').empty();
                    $("#popup-assign-job-table").find('tbody').append(response.response.data);
                } else {
                    if($("#assign-modal").length) {
                        $(".modal-backdrop").remove();
                        $("#assign-modal").remove(); 
                    }

                    $('body').append(response.response.data);
                    $("#assign-modal").modal('show');                        
                }
                
                $('.only-date-with-range').datepicker({startDate: 'now'});
            } else {
                swal("Error!", response.meta.message, "error");
            }
        });
    },
    //END: WASTE REMOVAL
    rule: function () {
        $(document).ready(function () {
            AssignJobs.fn.init.call(this);
        });
    }
};

AssignJobs.fn.rule();