function LocationDependence() {}

LocationDependence.fn = {
    init: function() {        
        LocationDependence.fn.initSelect.call(this);              
        LocationDependence.fn.initLocationDependence.call(this);              
    }, 
    
    initSelect: function() {
        $(".location-dependence").select2();

        if ($('.location-dependence-edit').length) {
            $('.location-dependence-edit').map(function () {
                var dataCat = $(this).val();
                var arrCat = dataCat.split(',');
                var jsSelectMultiple = $(this).closest('div').find('.location-dependence');
                
                jsSelectMultiple.val(arrCat);
                jsSelectMultiple.trigger('change');          
            });
        }
    },
    
    initLocationDependence: function() {
        $(document).on('change', '[data-type="location-dependence"][data-action="change"]', function () {
            var _this = $(this);
            var token = $('input[name="_token"]').val();
            var id = _this.val();
            var type = _this.attr('data-role');            
            var link = _this.attr('data-url');
            var data = {_token: token, type: type, id: id};
            
            $('body').loading('toggle');
            
            $.ajax({
                type: 'POST',
                url: link,
                data: data,
                success: function () {
                    $('body').loading('toggle');
                }
            }).success(function (response) {
                if (response.meta.code === 200) {
                    if(response.response.change) {
                        if($('[data-type="location-dependence"][data-role="'+ response.response.change +'"]').length) {
                            var elementChange = $('[data-type="location-dependence"][data-role="'+ response.response.change +'"]');                                                                                    
                                elementChange.empty();
                            
                            $.each(response.response.data, function (index, item) {  
                                elementChange.append('<option value="'+ item.id +'">'+ item.name +'</option>');                                
                            });
                                                        
                            elementChange.prepend('<option value="">--- Choose ---</option>');     
                            elementChange.select2();
                            elementChange.val('');
                            elementChange.trigger('change');
                        }
                    }
                    
                    if(response.response.reset) {
                        var elementReset = $('[data-type="location-dependence"][data-role="'+ response.response.reset +'"]');                                                        

                        elementReset.empty();
                        elementReset.append('<option value="">--- Choose ---</option>');     
                        elementReset.select2();
                        elementReset.val('');
                        elementReset.trigger('change');
                    }
                } else {
                    swal("Error!", response.meta.message, "error");
                }
            });
        });       
    },

    rule: function() {
        $(document).ready(function(){ 
            LocationDependence.fn.init.call(this);                        
        });
    }
};

LocationDependence.fn.rule();
