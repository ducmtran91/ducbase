function ContactReply() {
}

ContactReply.fn = {
    init: function () {
        ContactReply.fn.openModal.call(this);
        ContactReply.fn.submitModal.call(this);
    },
    openModal: function() {
        $(document).on('click', '.btn-reply-feedback', function() {
            ContactReply.fn.openModalProcess.call(this);
        });  
    },
    
    submitModal: function() {
        $(document).on('click', '#feedback-modal .btn-send-reply', function() {
            var _this = this;            
            var token = $('meta[name="csrf-token"]').attr('content');            
            var subject = $('#reply-subject').val();
            var content = $('#reply-body').val();            
            var link = $(_this).attr('data-link');             
            var key = $(_this).attr('data-key');
            var ids = $('#contact-ids').val();
            var error = '';
                        
            if((content.trim()).length < 1) {
                error = 'Body is required';                
            }
            
            if((subject.trim()).length < 1) {
                error = 'Subject is required';                
            }            
            
            if(error.length > 0) {
                swal({
                    type: "error",
                    title: error,
                    confirmButtonClass: "btn-danger",
                    html: true
                });
                
                return false;
            }                        
            
            $('body').loading('toggle');

            $.ajax({
                type: 'POST',
                url: link,
                data: {
                    "_token": token,
                    "key": key,
                    "ids": ids,
                    "subject": subject,
                    "content": content
                },
                success: function (response) {
                    $('body').loading('toggle');
                }
            }).success(function (response) {
                if (response.meta.code === 200) {
                    swal("Success!", response.meta.message, "success");
                                                        
                    setTimeout(function(){ 
                        location.reload();
                    }, 2000);                      
                } else {
                    swal("Error!", response.meta.message, "error");
                }
            });
        });
    },   
    
    openModalProcess: function(){
        var token = $('meta[name="csrf-token"]').attr('content');        
        var _this = this;        
        var link = $(_this).attr('data-link');      
        var key = $(_this).attr('data-key');
        var title = $(_this).attr('data-title');
        var data = {"_token": token, "key": "reply", "title": title};

        if(key === 'reply') {
            var array = [$(_this).attr('data-id')];
            data.ids = array.join(); 
        } else if(key === 'reply-multiple') {
            var array = new Array();                  

            $("input.checkbox_item:checkbox[name=checkbox]:checked").each(function () {
                array.push($(this).val());
            });  

            data.ids = array.join(); 
        }               

        if(!data.ids) {
            swal({
                type: "error",
                title: "Choose at least one " + title,
                confirmButtonClass: "btn-danger",
                html: true
            });

            return false;           
        }

        $('body').loading('toggle');

        $.ajax({
            type: 'POST',
            url: link,
            data: data,
            success: function (response) {
                $('body').loading('toggle');
            }
        }).success(function (response) {
            if (response.meta.code === 200) {
                if(key === 'search') {
                    $("#feedback-modal").find('.modal-body').remove();
                    $("#feedback-modal").find('.modal-header').after(response.response.data);
                } else {
                    if($("#feedback-modal").length) {
                        $(".modal-backdrop").remove();
                        $("#feedback-modal").remove(); 
                    }

                    $('body').append(response.response.data);
                    $("#feedback-modal").modal('show');                        
                }         
            } else {
                swal("Error!", response.meta.message, "error");
            }
        });
    },
    
    rule: function () {
        $(document).ready(function () {
            ContactReply.fn.init.call(this);
        });
    }
};

ContactReply.fn.rule();