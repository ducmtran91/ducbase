function Main() {
}

Main.fn = {
    init: function () {
        Main.fn.initSelect.call(this);
        Main.fn.initTag.call(this);
        Main.fn.initCheckbox.call(this);
        Main.fn.initBulkActionChange.call(this);
        Main.fn.initItemActions.call(this);
        Main.fn.initSort.call(this);
        Main.fn.initPerPage.call(this);
        Main.fn.initFilter.call(this);
        Main.fn.initEditor.call(this);
        Main.fn.initReadmore.call(this);
        Main.fn.initBtnSaveAndNew.call(this);
        Main.fn.initInputEnterSubmit.call(this);
        Main.fn.initPopupUpload.call(this);
        Main.fn.initPopupSearch.call(this);
        Main.fn.datetimepicker.call(this);
        Main.fn.initDateTimePicker.call(this);
        Main.fn.previewLocalImage.call(this);
        Main.fn.datetimeSelect.call(this);
        Main.fn.searchInList.call(this);
    },

    initPopupUpload: function () {
        var rootTrigger = false;

        $(document).on('click', '.popup-upload-trigger', function () {
            var _this = this;
            var root = $(_this);
            var token = $('meta[name="csrf-token"]').attr('content');
            var maxFilesize = root.attr('data-max-file-size');
            var acceptedFiles = root.attr('data-accept-type');
            var url = root.attr('data-url');
            var dest = root.attr('data-dest');
            var destShow = root.attr('data-dest-show');
            var inputName = root.attr('data-name');
            var textIntro = 'Kéo thả tệp âm thanh hoặc chọn để tải lên từ máy tính. Định dạng wav.';
            var textBtn = 'âm thanh';
            var libaryUrl = root.attr('data-library-url');
            var libraryType = root.attr('data-library-type');
            var libraryIds = root.attr('data-library-ids'),
                media_id = root.data( 'media-id' );
            var accessFileCustom='audio/wav';
            if (acceptedFiles == 'image/*') {
                accessFileCustom=acceptedFiles;
                textIntro = 'Kéo thả hình ảnh hoặc chọn để tải lên từ máy tính. Định dạng jpg hoặc png.';
                textBtn = 'hình ảnh';
            } else if (acceptedFiles == 'video/*') {
                accessFileCustom='video/mp4';
                textIntro = 'Kéo thả video hoặc chọn để tải lên từ máy tính. Định dạng mp4.';
                textBtn = 'video';
            }

            var data_media_id = media_id && typeof media_id !== 'undefined' ? 'data-source-media="' + media_id + '"' : '';            
            var uploadArea = '<div id="upload-file-dropzone-popup" class="form-group col-sm-12"' + ' ' + data_media_id + ' data-url="' + url + '" data-max-file-size="' + maxFilesize + '" data-dest="' + dest + '" data-dest-show="' + destShow + '" data-name="' + inputName + '" data-accept-type="' + acceptedFiles + '">' +
                '<div id="file-upload" class="dropzone">' +
                '<div class="dz-default dz-message">' +
                '<div>' +
                '<i class="fa fa-cloud-upload fa-5x"></i>' +
                '</div>' +
                '<span>' + textIntro + '</span>' +
                '</div>' +
                '</div>' +
                '</div>';

            rootTrigger = root;

            $('body').loading('toggle');

            $.ajax({
                type: 'POST',
                url: libaryUrl,
                data: {
                    "_token": token,
                    'library_ids': libraryIds,
                    "type": libraryType
                },
                success: function (response) {
                    var libraryArea = 'Không có sẵn tệp. Vui lòng tải lên từ máy tính.'

                    if (response.response.length > 0) {
                        var icon = '<i class="fa fa-play-circle fa-2x dz-preview-audio"></i>';

                        if (libraryType == 'image') {
                            icon = '<i class="fa fa-picture-o fa-2x dz-preview-image"></i>';
                        } else if (libraryType == 'video') {
                            icon = '<i class="fa fa-video-camera fa-2x dz-preview-video"></i>';
                        }

                        libraryArea = '';

                        $.each(response.response, function (index, item) {
                            var libAttr = 'data-lib-name="' + item.file_name + '" data-lib-path="' + item.file_path + '" data-lib-type="' + item.file_type + '" data-lib-size="' + item.file_size + '" data-lib-dest="' + item.file_dest + '" data-lib-dest-show="' + item.file_dest_show + '"';

                            libraryArea += '<div class="row col-sm-12 popup-library-row mg-bottom-5" ' + libAttr + '>' + icon + ' <span class="popup-library-file-name">' + item.file_name + '</span></div>';
                        });
                    }

                    var content = '<div class="input-group">' +
                        '<span class="input-group-addon"><i class="fa fa-search"></i></span>' +
                        '<input type="text" class="form-control popup-upload-search" name="name" placeholder="Tìm kiếm" />' +
                        '</div>' +
                        '<button type="button" class="btn btn-info popup-btn-tab-1">Chọn ' + textBtn + ' từ thư viện</button>' +
                        '<button type="button" class="btn btn-warning popup-btn-tab-2">Upload ' + textBtn + ' từ máy tính</button>' +
                        '<div id="popup-tab-1" class="popup-tab-area">' + libraryArea + '</div>' +
                        '<div id="popup-tab-2" class="popup-tab-area" style="display: none;">' + uploadArea + '</div>';

                    $('body').loading('toggle');

                    vex.open({
                        unsafeContent: content,
                        showCloseButton: false,
                        contentClassName: 'popup-upload-area'
                    });
                }
            });
        });

        // active tab 1
        $(document).on('click', '.popup-btn-tab-1', function () {
            var rootTab1 = $(this).closest('.popup-upload-area');

            rootTab1.find('#popup-tab-1').show();
            rootTab1.find('#popup-tab-2').hide();
        });

        // active tab 2
        $(document).on('click', '.popup-btn-tab-2', function () {
            var rootTab2 = $(this).closest('.popup-upload-area');

            rootTab2.find('#popup-tab-2').show();
            rootTab2.find('#popup-tab-1').hide();

            if (!$('#popup-tab-2').find('#upload-file-dropzone-popup').hasClass('dz-clickable')) {
                Main.fn.initDropzonePopup.call(this, rootTrigger);
            }
        });

        // choose file from library after load ajax
        $(document).on('click', '.popup-library-file-name', function () {
            var row = $(this).closest('.popup-library-row');
            var fileName = row.attr('data-lib-name');
            var filePath = row.attr('data-lib-path');
            var fileType = row.attr('data-lib-type');
            var fileSize = row.attr('data-lib-size');
            var fileDest = row.attr('data-lib-dest');
            var fileDestShow = row.attr('data-lib-dest-show');
            var inputName = rootTrigger.attr('data-name');
            var libraryType = rootTrigger.attr('data-library-type');
            var icon = '<i class="fa fa-play-circle fa-2x dz-preview-audio"></i>',
                is_lesson_content_page = $( '#lesson-content-page' ).val(),
                media_id = rootTrigger.data( 'media-id' );

            if (libraryType == 'image') {
                icon = '<i class="fa fa-picture-o fa-2x dz-preview-image"></i>';
            } else if (libraryType == 'video') {
                icon = '<i class="fa fa-video-camera fa-2x dz-preview-video"></i>';
            }

            if ( is_lesson_content_page === 'true' ) {
                var current_item = rootTrigger.closest('.row');
                current_item.find( '.dz-show-name' ).text( fileName );
                current_item.find( 'input.dz-name-edit-input' ).attr( 'value', fileName );
                current_item.find( 'input.hdn-name' ).attr( 'value', filePath );
                current_item.find( 'input.hdn-dest' ).attr( 'value', fileDest );
                current_item.find( 'input.hdn-show' ).attr( 'value', fileDestShow + filePath );
                current_item.find( 'input.hdn-size' ).attr( 'value', fileSize );
                current_item.find( 'input.hdn-change-media' ).attr( 'value', media_id );
                current_item.find( 'i.dz-preview-game-audio' ).attr( 'data-show', fileDestShow + filePath );
                current_item.find( '.dz-preview-game-audio' ).css( 'display', 'inline-block' );
                return;
            }

            var html = '<div class="row dz-preview-include">' +
                '<div class="dz-preview dz-file-preview dz-processing dz-success row mg-top-10 mg-bottom-5 dz-complete">' +
                '<div class="dz-filename col-sm-4">' +
                '<span data-dz-name="" class="dz-show-name">' + fileName + '</span>' +
                '<input class="dz-name-edit-input" name="edit_' + inputName + '" type="text" value="' + fileName + '">' +
                '<i class="dz-edit-name fa fa-pencil mg-left-5 font-size-16"></i>' +
                '</div>' +
                '<div class="dz-size col-sm-3" data-dz-size="">' + fileSize + '</div>' +
                '<div class="dz-progress col-sm-3 mg-top-8 pd-0">' +
                '<span class="dz-upload" data-dz-uploadprogress="" style="width: 100%;"></span>' +
                '</div>' +
                '<div class="dz-remove col-sm-2 text-center">' +
                icon +
                '<i class="fa fa-trash fa-2x mg-left-5 color-red data-type-inclue" data-dz-remove="" data-dest="' + fileDest + '"></i>' +
                '</div>' +
                '<input type="hidden" data-key="ip-hidden-name" name="' + inputName + '" value="' + filePath + '">' +
                '<input type="hidden" data-key="ip-hidden-file-dest" name="dest_' + inputName + '" value="' + fileDest + '" />' +
                '<input type="hidden" data-key="ip-hidden-file-show" class="dz-file-show" value="' + fileDestShow + filePath + '">' +
                '<input type="hidden" data-key="ip-hidden-file-size" name="size_' + inputName + '" value="' + fileSize + '">' +
                '</div>'
            '</div>';
            rootTrigger.closest('.row').after(html);
        });
    },

    initDropzonePopup: function (rootElement) {
        Dropzone.autoDiscover = false;
        vex.defaultOptions.className = 'vex-theme-os';

        var token = $('meta[name="csrf-token"]').attr('content');
        var root = $('#upload-file-dropzone-popup');
        var maxFilesize = root.attr('data-max-file-size');
        var acceptedFiles = root.attr('data-accept-type');
        var url = root.attr('data-url');
        var dest = root.attr('data-dest');
        var destShow = root.attr('data-dest-show');
        var inputName = root.attr('data-name');
        var previewBtn = '<i class="fa fa-play-circle fa-2x dz-preview-audio"></i>',
            is_lesson_content_page = $( '#lesson-content-page' ).val(),
            media_id = root.data( 'source-media' );
        var accessFileCustom='audio/wav';
        if (acceptedFiles == 'image/*') {
            accessFileCustom=acceptedFiles;
            previewBtn = '<i class="fa fa-picture-o fa-2x dz-preview-image"></i>';
        } else if (acceptedFiles == 'video/*') {
            accessFileCustom='video/mp4';
            previewBtn = '<i class="fa fa-video-camera fa-2x dz-preview-video"></i>';
        }

        var previewTemplate = '<div class="dz-preview dz-file-preview dz-processing dz-success row mg-top-10 mg-bottom-5">' +
            '<div class="dz-filename col-sm-4"><span data-dz-name class="dz-show-name"></span><i class="dz-edit-name fa fa-pencil mg-left-5 font-size-16"></i></div>' +
            '<div class="dz-size col-sm-3" data-dz-size></div>' +
            '<div class="dz-progress col-sm-3 mg-top-8 pd-0"><span class="dz-upload" data-dz-uploadprogress></span></div>' +
            '<div class="dz-remove col-sm-2 text-center">' +
            previewBtn +
            '<i class="fa fa-trash fa-2x mg-left-5 color-red" data-dz-remove=""></i>' +
            '</div>' +
            '</div>';
        var myDropzone = new Dropzone('#upload-file-dropzone-popup', {
            url: url, //API
            maxFilesize: maxFilesize, // MB
            acceptedFiles: accessFileCustom, // Image...
            previewTemplate: previewTemplate,
            sending: function (file, xhr, formData) {
                formData.append("_token", token);
                formData.append("dest", dest);
            },
            success: function (file, response) {
                var element = $(file.previewElement);
                var size = element.find('.dz-size').text();

                if (response.meta.success === true) {
                    if ( is_lesson_content_page === 'true' ) {
                        var current_item = rootElement.closest('.row');
                        current_item.find( '.dz-show-name' ).text( file.name );
                        current_item.find( 'input.dz-name-edit-input' ).attr( 'value', file.name );
                        current_item.find( 'input.hdn-name' ).attr( 'value', response.response.name );
                        current_item.find( 'input.hdn-dest' ).attr( 'value', dest );
                        current_item.find( 'input.hdn-show' ).attr( 'value', destShow + response.response.name );
                        current_item.find( 'input.hdn-size' ).attr( 'value', size );
                        current_item.find( 'input.hdn-change-media' ).attr( 'value', media_id );
                        current_item.find( 'i.dz-preview-game-audio' ).attr( 'data-show', destShow + response.response.name );
                        current_item.find( '.dz-preview-game-audio' ).css( 'display', 'inline-block' );
                    } else {
                        element.append('<input type="hidden" data-key="ip-hidden-name" name="' + inputName + '" value="' + response.response.name + '" />');
                        element.append('<input type="hidden" data-key="ip-hidden-file-dest" name="dest_' + inputName + '" value="' + dest + '" />');
                        element.append('<input type="hidden" data-key="ip-hidden-file-show" class="dz-file-show" value="' + destShow + response.response.name + '" />');
                        element.append('<input type="hidden" data-key="ip-hidden-file-size" name="size_' + inputName + '" value="' + size + '" />');
                        element.find('.dz-show-name').after('<input class="dz-name-edit-input" name="edit_' + inputName + '" type="text" value="' + file.name + '"/>');
                    }
                } else {
                    element.find('.dz-progress').attr('style', 'background: #ff0000;');
                    element.find('.dz-upload').remove();
                }

                var row = $('<div class="row"></div>');
                row.append(element);

                if ( is_lesson_content_page !== 'true' || typeof is_lesson_content_page === 'undefined' )
                    rootElement.closest('.row').after(row);

            },
            removedfile: function (file) {
                var element = $(file.previewElement);

                Main.fn.initDropzoneRemove(element, '#upload-file-dropzone-popup', token);
            }
        });
    },

    initPopupSearch: function () {
        $(document).on('keyup', '.popup-upload-search', function () {
            var keyword = $(this).val();
            var root = $(this).closest('.popup-upload-area');
            var tab1 = root.find('#popup-tab-1');

            tab1.find('.popup-library-file-name').map(function () {
                var row = $(this).closest('.popup-library-row');

                if (($(this).text().toLowerCase()).indexOf(keyword.toLowerCase()) != -1) {
                    row.show();
                } else {
                    row.hide();
                }
            });
        });

        Main.fn.initDateTimePicker.call(this);
    },

    initDropzone: function () {
        Dropzone.autoDiscover = false;
        vex.defaultOptions.className = 'vex-theme-os';

        var token = $('meta[name="csrf-token"]').attr('content');

        $('.upload-file-dropzone').map(function () {
            if (!$(this).hasClass('dz-clickable')) {
                var maxFilesize = $(this).attr('data-max-file-size');
                var acceptedFiles = $(this).attr('data-accept-type');
                var url = $(this).attr('data-url');
                var dest = $(this).attr('data-dest');
                var destShow = $(this).attr('data-dest-show');
                var inputName = $(this).attr('data-name');
                var baseUrl = window.location.origin;
                var accessFileCustom='audio/wav';
                var previewTemplate = ' <div class="dz-preview dz-file-preview dz-processing dz-success row mg-top-10 mg-bottom-5 remove_all_file">\n' +
                    '            <div class="dz-filename col-sm-4"><span data-dz-name class="dz-show-name"></span><i\n' +
                    '                        class="dz-edit-name fa fa-pencil mg-left-5 font-size-16"></i></div>\n' +
                    '            <div class="dz-size col-sm-3" data-dz-size></div>\n' +
                    '            <div class="dz-progress col-sm-3 mg-top-8 pd-0"><span class="dz-upload" data-dz-uploadprogress></span></div>\n' +
                    '            <div class="dz-remove col-sm-2 text-center">\n' +
                    '                <i class="fa fa-play-circle font-size-24 dz-preview-audio"></i>\n' +
                    '                <i class="fa fa-trash font-size-24 mg-left-5 color-red" data-dz-remove=""></i>\n' +
                    '            </div>\n' +
                    '        </div>';                

                if (acceptedFiles == 'image/*') {
                    accessFileCustom=acceptedFiles;
                    previewTemplate = '<div class="dz-preview dz-file-preview dz-processing dz-success row mg-top-10 mg-bottom-5 remove_all_file">' +
                        '<div class="dz-img col-sm-1"><img class="dz-img-thumb img-file-upload dz-preview-image" src="' + baseUrl + '/assets/images/loading.gif"/></div>' +
                        '<div class="dz-filename col-sm-4">' +
                        '<span data-dz-name class="dz-show-name"></span>' +
                        '<i class="dz-edit-name fa fa-pencil mg-left-5 font-size-16"></i></div>' +
                        '<div class="dz-size col-sm-3" data-dz-size></div>' +
                        '<div class="dz-progress col-sm-3 mg-top-8 pd-0"><span class="dz-upload" data-dz-uploadprogress></span></div>' +
                        '<div class="dz-remove col-sm-1 text-center">' +
                        '<i class="fa fa-trash fa-2x mg-left-5 color-red" data-dz-remove=""></i>' +
                        '</div>' +
                        '</div>';
                } else if (acceptedFiles == 'video/*') {
                    accessFileCustom='video/mp4';
                    previewTemplate = '<div class="dz-preview dz-file-preview dz-processing dz-success row mg-top-10 mg-bottom-5 remove_all_file">' +
                        '<div class="dz-filename col-sm-4"><span data-dz-name class="dz-show-name"></span><i class="dz-edit-name fa fa-pencil mg-left-5 font-size-16"></i></div>' +
                        '<div class="dz-size col-sm-3" data-dz-size></div>' +
                        '<div class="dz-progress col-sm-3 mg-top-8 pd-0"><span class="dz-upload" data-dz-uploadprogress></span></div>' +
                        '<div class="dz-remove col-sm-2 text-center">' +
                        '<i class="fa fa-video-camera fa-2x dz-preview-video"></i>' +
                        '<i class="fa fa-trash fa-2x mg-left-5 color-red" data-dz-remove=""></i>' +
                        '</div>' +
                        '</div>';
                }

                // image/*
                // <img class="dz-img-thumb" data-dz-thumbnail />

                // audio/*
                // <div class="dz-preview-audio"></div>

                var myDropzone = new Dropzone(this, {
                    url: url, //API
                    maxFilesize: maxFilesize, // MB
                    acceptedFiles: accessFileCustom, // Image...
                    previewTemplate: previewTemplate,
                    sending: function (file, xhr, formData) {
                        formData.append("_token", token);
                        formData.append("dest", dest);
                    },
                    success: function (file, response) {
                        var element = $(file.previewElement);
                        var size = element.find('.dz-size').text();

                        if (response.meta.success === true) {
                            element.find('.img-file-upload').attr('src', baseUrl + destShow + response.response.name);
                            element.append('<input type="hidden" data-key="ip-hidden-name" name="' + inputName + '" value="' + response.response.name + '" />');
                            element.append('<input type="hidden" data-key="ip-hidden-file-show" class="dz-file-show" value="' + destShow + response.response.name + '" />');
                            element.append('<input type="hidden" data-key="ip-hidden-file-size" name="size_' + inputName + '" value="' + size + '" />');
                            element.find('.dz-show-name').after('<input class="dz-name-edit-input" name="edit_' + inputName + '" type="text" value="' + file.name + '"/>');
                        } else {
                            element.find('.dz-progress').attr('style', 'background: #ff0000;');
                            element.find('.dz-upload').remove();
                        }
                    },
                    removedfile: function (file) {
                        var element = $(file.previewElement);
                        var root = element.closest('.upload-file-dropzone');
                        var dest = root.attr('data-dest');
                        var name = element.find('input[data-key="ip-hidden-name"]').val();
                        var url = $('#data-url-remove-file').val();

                        element.remove();

                        $('body').loading('toggle');                        

                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: {
                                "_token": token,
                                "name": name,
                                "dest": dest
                            },
                            success: function (response) {
                                $('body').loading('toggle');
                            }
                        });
                    }
                });
            }
        });

        // remove file case normal
        $(document).on('click', '.upload-file-dropzone .dz-remove .fa-trash', function () {
            var element = $(this).closest('.dz-preview');
            var edit = '';
            if ($(this).attr('data-edit') == 'edit') {
                edit = $(this).attr('data-edit');
            }

            Main.fn.initDropzoneRemove(element, '.upload-file-dropzone', token,edit);
        });

        // remove file case include file from library
        $(document).on('click', '.dz-preview-include .dz-remove .fa-trash', function () {
            var token = $('meta[name="csrf-token"]').attr('content');
            var element = $(this).closest('.dz-preview');

            Main.fn.initDropzoneRemove(element, '.dz-preview-include', token);
        });

        $(document).on('click', '.dz-preview-audio', function () {
            var filePath = $(this).closest('.dz-preview').find('.dz-file-show').val();
            var html = '<audio controls autoplay>' +
                '<source src="' + filePath + '" type="audio/mpeg">' +
                '<source src="' + filePath + '" type="audio/wav">' +
                'Trình duyệt của bạn không hỗ trợ âm thanh định dạng này' +
                '</audio>';

            vex.open({
                unsafeContent: html,
                contentClassName: 'popup-preview-audio'
            });
        });

        $(document).on('click', '.dz-preview-video', function () {
            var filePath = $(this).closest('.dz-preview').find('.dz-file-show').val();
            var html = '<video style="width: 100%" class="dz-video-thumb" controls autoplay>' +
                '<source src="' + filePath + '" type="video/mp4">' +
                '<source src="' + filePath + '" type="video/avi">' +
                'Trình duyệt của bạn không hỗ trợ video định dạng này.' +
                '</video>';

            vex.open({
                unsafeContent: html,
                contentClassName: 'popup-preview-video'
            });
        });

        $(document).on('click', '.dz-preview-image', function () {     
            var filePath = $(this).closest('.dz-preview').find('.dz-file-show').val();
            var html = '<img style="width: 100%" class="dz-img-thumb" src="' + filePath + '"/>';

            vex.open({
                unsafeContent: html,
                contentClassName: 'popup-preview-image'
            });
        });

        $(document).on('click', '.dz-preview-galleries', function () {

        });

        $(document).on('click', '.dz-edit-name', function () {
            var root = $(this).closest('.dz-preview');
            var showName = root.find('.dz-show-name');
            var inputEdit = root.find('.dz-name-edit-input');

            $(this).hide();

            showName.hide();
            inputEdit.show().select();
        });

        $(document).on('keypress', '.dz-name-edit-input', function (event) {
            if (event.which == 13 || event.keyCode == 13) {
                event.preventDefault();

                var val = $(this).val();
                var root = $(this).closest('.dz-preview');
                var showName = root.find('.dz-show-name');
                var editName = root.find('.dz-edit-name');
                
                if (val.length) {
                    showName.empty().text(val);
                }
                
                $(this).hide();

                editName.show();
                showName.show();
            }
        });

        $(document).on('blur', '.dz-name-edit-input', function () {
            var value = $(this).val();
            var root = $(this).closest('.dz-preview');
            var showName = root.find('.dz-show-name');
            var inputEdit = root.find('.dz-name-edit-input');
            var editName = root.find('.dz-edit-name');
            
            $(this).hide();

            showName.show().text(value);
            editName.show();
            inputEdit.hide();
        });
    },

    initDropzoneRemove: function (element, rootElement, token,edit) {
        var root = element.closest(rootElement);
        var dest = root.attr('data-dest');
        var name = element.find('input[data-key="ip-hidden-name"]').val();
        var data = {"_token": token, "name": name, "dest": dest};
                
        element.remove();

    },    

    initSelect: function () {
        $(".js-select-multiple").select2();

        if ($('.js-select-multiple-edit').length) {
            $('.js-select-multiple-edit').map(function () {
                var dataCat = $(this).val();
                var arrCat = dataCat.split(',');
                var jsSelectMultiple = $(this).closest('div').find('.js-select-multiple');
                
                jsSelectMultiple.val(arrCat);
                jsSelectMultiple.trigger('change');
            });
        }
    },

    initTag: function () {
        if ($('#tags').length) {
            if ($('#tag-edit').length) {
                var data = $('#tag-edit').val();
                var arr = data.split(',');

                new Taggle('tags', {
                    tags: arr,
                    duplicateTagClass: 'bounce',
                    placeholder: 'Nhập các từ khoá ...'
                });
            } else {
                new Taggle('tags', {
                    placeholder: 'Nhập các từ khoá ...'
                });
            }
        }
    },

    initCheckbox: function () {
        $('#select_all').change(function () {
            var checkboxes = $(this).closest('table').find(':checkbox');
            
            if ($(this).is(':checked')) {
                checkboxes.prop('checked', true);
                $('.table tbody tr').addClass('selected_item');
            } else {
                checkboxes.prop('checked', false);
                $('.table tbody tr').removeClass('selected_item');
            }
        });

        $('.checkbox_item').change(function () {
            var parent = $(this).parent().parent().parent().parent();

            if ($(this).is(':checked')){
                parent.addClass('selected_item');
            }else{
                parent.removeClass('selected_item');
                $('#select_all').prop('checked', false);

            }

            var length_checkbox_item =  $('.checkbox_item').length;
            var length_selected_item =  $('.selected_item').length;

            if (length_checkbox_item == length_selected_item){
                $('#select_all').prop('checked', true);
            }
        });
    },

    initBulkActionChange: function () {
        // update status list checkbox
        $(document).on('click', '.bulk_action_change', function () {

            var array = new Array();

            $("input.checkbox_item:checkbox[name=checkbox]:checked").each(function () {
                array.push($(this).val());
            });

            var token = $('#data-token').val();
            var id = array.join();
            var value = $(this).attr('value');
            var dataLink = $(this).attr('data-link');
            var link = $('#' + dataLink).val();
            var key = $(this).attr('data-key');
            var text = $(this).attr('data-text');
            var title = $(this).attr('data-title');

            if (id && link && key) {
                swal({
                    title: "Bạn có chắc chắn " + text + " " + title + " này?",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    confirmButtonText: "Yes",
                    confirmButtonClass: "btn-danger",
                    cancelButtonText: "No"
                }, function () {
                    $('body').loading('toggle');

                    $.ajax({
                        type: 'PUT',
                        url: link,
                        data: {
                            "_token": token,
                            "key": key,
                            "value": value,
                            "ids": id
                        },
                        success: function (response) {
                            $('body').loading('toggle');
                        }
                    }).success(function (response) {
                        if (response.meta.code === 200) {
                            swal("Success!", response.meta.message, "success");
                            
                            setTimeout(function(){ 
                                location.reload();
                            }, 2000);                            
                        } else {
                            swal("Error!", response.meta.message, "error");
                        }
                    });
                });

                return false;
            } else {
                swal({
                    type: "error",
                    title: "Choose at least one " + title,
                    confirmButtonClass: "btn-danger",
                    html: true
                });
            }
        });
    },

    initItemActions: function () {
        // update status list checkbox
        $(document).on('click', '.item_actions', function () {
            var token = $('#data-token').val();
            var value = $(this).attr('data-val');
            var dataLink = $(this).attr('data-link');
            var link = $('#' + dataLink).val();
            var key = $(this).attr('data-key');
            var text = $(this).attr('data-text');
            var title = $(this).attr('data-title');
            var id = $(this).attr('data-id');

            if (id && link && key) {
                swal({
                    title: "Bạn có chắc chắn " + text + " " + title + " này?",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    confirmButtonText: "Yes",
                    confirmButtonClass: "btn-danger",
                    cancelButtonText: "No"
                }, function () {
                    $('body').loading('toggle');

                    $.ajax({
                        type: 'PUT',
                        url: link,
                        data: {
                            "_token": token,
                            "key": key,
                            "value": value,
                            "id": id
                        },
                        success: function (response) {
                            $('body').loading('toggle');
                        }
                    }).success(function (response) {
                        if (response.meta.code === 200) {
                            swal("Success!", response.meta.message, "success");
                                                        
                            setTimeout(function(){ 
                                location.reload();
                            }, 2000);
                        } else if(response.meta.code === 1){
                            swal("Error!", response.meta.message);
                        } else {
                            swal("Error!", response.meta.message, "error");
                        }
                    });
                });
                return false;
            }
        });
    },

    initSort: function () {
        $(document).on('change', '#sort_field', function () {
            $('#form_custom_list').submit();
        });

        $(document).on('click', '.order-asc', function () {
            $(this).parent().parent().find('.type_sort').val('asc');
            $('#form_custom_list').submit();
        });

        $(document).on('click', '.order-desc', function () {
            $(this).parent().parent().find('.type_sort').val('desc');
            $('#form_custom_list').submit();
        });
    },

    initPerPage: function () {
        // limit and order
        $(document).on('change', '#limit_pagination', function () {
            $('#form_custom_list').submit();
        });
    },

    initFilter: function () {
        $(document).on('change', '.filter_header', function () {
            $('#form_custom_list').submit();
        });
    },

    initEditor: function () {
        if ($('[init="ckeditor"]').length) {
            $('[init="ckeditor"]').each(function () {
                var self = this;
                if (typeof $(self).attr('name') !== 'undefined') {
                    CKEDITOR.plugins.addExternal('youtube', '/../../assets/ckeditor-youtube-plugin/youtube/');
                    CKEDITOR.replace($(self).attr('name'), {
                        language: 'en',
                        extraPlugins:'youtube',
                        filebrowserBrowseUrl: '/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
                        filebrowserImageBrowseUrl: '/filemanager/dialog.php?type=1&editor=ckeditor&fldr='
                    });
                } else {
                    var editor = $(self).ckeditor();
                }
            });
        }
    },

    initReadmore: function () {
        $('.readmore').map(function () {
            var openText = $(this).attr('data-text-open');
            var closeText = $(this).attr('data-text-close'),
                defaultHeight = $( this ).data( 'height' ) || 200;

            $(this).readmore({
                speed: 250,
                moreLink: '<a href="#" class="btn-more">' + openText + ' <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>',
                lessLink: '<a href="#" class="btn-more">' + closeText + ' <i class="fa fa-angle-double-up" aria-hidden="true"></i></a>',
                collapsedHeight: defaultHeight
            });
        });
    },

    initBtnSaveAndNew: function () {
        $(document).on('click', '.btn-save-and-new', function () {
            $(this).after('<input type="hidden" name="save_new" value="yes"/>');
            $(this).closest('form').submit();
        });
    },

    initInputEnterSubmit: function () {
        $(document).on('keypress', '.enter-submit', function (event) {
            if (event.which == 13 || event.keyCode == 13) {
                $(this).closest('form').submit();
            }
        });
    },
   
    capitalizeFirstLetter: function (string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    },

    initSelect2: function () {
        var selected_items = $( '.selected-items' );
        if ( selected_items.length > 0 )
            selected_items.each( function() {
                selected_items_str = $( this ).val() || '';

                if ( selected_items_str !== '' )
                    selected_items = selected_items_str.indexOf( ',' ) === 1 ? selected_items_str.split( ',' ) : [selected_items_str];

                $( '#' + $( this ).data( 'select-id' ) ).select2().val( selected_items ).trigger( 'change' );
            } );
        else
            $( '.select2-multiple' ).select2();
    },

    datetimepicker: function () {
        $('.birthday').datetimepicker({format: 'YYYY/MM/DD'});
        $('.init-datepicker').datepicker({startDate: 'now'});
    },    

    initDateTimePicker: function() {
        $(document).ready(function () {
            $('.datetimepicker').datetimepicker({format:'YYYY/MM/DD'});
        });
    },

    renderLocalImage: function( file ) {
        var reader = new FileReader();

        reader.onload = function(event) {
            var the_url = event.target.result;
            $('#image-preview').html("<img src='" + the_url + "' />");
        };

        reader.readAsDataURL(file);
    },

    previewLocalImage: function() {
        $(".upload-image").change(function() {
            Main.fn.renderLocalImage(this.files[0]);
        });
    },

    datetimeSelect: function() {
        $(document).ready(function () {
            $('.datetimepicker').datetimepicker({format:'DD/MM/YYYY'});
            
            var el = document.getElementById("phone");

            //---------------------------------Chỉ cho nhập số---------------------------------
            if (el) {
                el.addEventListener("keypress", function (evt) {
                    if (
                        (evt.which != 8 && evt.which != 0 && evt.which < 48) ||
                        evt.which > 57
                    ) {
                        evt.preventDefault();
                    }
                });
            }
        });
    },
    
    searchInList: function() {
        if($('.search-action').length) {
            $(document).on('click', '.search-action', function(){
                $(this).closest('form#form_custom_list').submit();
            }); 
        }
    },
    
    rule: function () {
        $(document).ready(function () {
            Main.fn.init.call(this);
        });
    }
};

Main.fn.rule();