let htmlLoading = "<span class=\"back\">\n" +
    "    <span>L</span>\n" +
    "    <span>o</span>\n" +
    "    <span>a</span>\n" +
    "    <span>d</span>\n" +
    "    <span>i</span>\n" +
    "    <span>n</span>\n" +
    "    <span>g</span>\n" +
    "</span>";

function LoadMore() {
    this.link = arguments[0];
    this.id = arguments[1];
    this.append = arguments[2];
    this.page = arguments[3];
    this.loading = function() {
        if(typeof $('.back') != "undefined") {
            $('.back').remove();
        } else {
            $('#'+this.append).append(htmlLoading);
        }
    }

}

LoadMore.prototype.get = function(object) {
    if(this.page) {
        this.loading();
        let that = this;
        let button = object;
        $.ajax({
            headers: {
                'X-CSRF-Token':$('meta[name="_token"]').attr('content')
            },
            type: 'POST',
            url: this.link,
            data: {
                id: this.id,
                page: this.page
            },
            success: function(response) {
                that.loading();
            }
        }).done(function(response){
            if(response.data.html) {
                $('.'+that.append).append(response.data.html);
                $(button).data('page', response.data.page);
            } else {
                $(button).remove();
            }
        });
    }
};

$(document).ready(function(e){
    $('.load-more-ajax').on('click', function() {
        let id = $(this).data('id');
        let link = $(this).data('link');
        let append = $(this).data('append');
        let page = $(this).data('page');
        let loadMore = new LoadMore(link, id, append, page);
        loadMore.get(this);
    });
});
