<?php

return [
    'validation' => [
        'required'    => 'Please enter a valid :attribute',
        'date_format' => 'The :attribute does not match the format :format.',
        'min'         => 'The :attribute must be at least :min characters.'
    ],
    'get' => [
        '404' => 'Payment Information could not found.',
        'successfully' => 'Payment Information retrieved successfully.',
        'failure' => 'Payment Information retrieved failure.'
    ],
    'show' => [
        '404' => 'Payment Information could not found.',
        'failure' => 'Payment Information retrieved failure.',
        'successfully' => 'Payment Information retrieved successfully.'
    ],
    'store' => [
        'validation' => 'Payment information invalid.',
        'successfully' => 'Create new Payment information successfully.',
        'failure' => 'Create new Pet failure.'
    ],
    'update' => [
        'validation' => 'Payment information invalid.',
        'successfully' => 'Update this Payment information successfully.',
        'failure' => 'Update this Payment information failure.'
    ],
    'delete' => [
        'successfully' => 'Delete this Payment Information successfully.',
        'failure' => 'Delete this Payment Information failure.'
    ]
];