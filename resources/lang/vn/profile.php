<?php

return [
    'owner' => [
        'validation' => [
            'required' => 'Please enter a valid :attribute',
            'json'     => 'The :attribute does not match the format',
            'max'      => 'The :attribute may not be greater than :max characters.'
        ],
        'get' => [
            '404' => 'Could not found Dog Owners.',
            'successfully' => 'Dog Owners retrieved successfully.',
            'failure' => 'Dog Owners retrieved failure.',
        ],
        'show' => [
            '404' => 'Could not found Dog Owner.',
            'successfully' => 'Dog Owner retrieved successfully.',
            'failure' => 'Dog Owner retrieved failure.',
            'permission' => [
                'owner' => [
                    'no' => 'This user is not correct Dog Owner.'
                ],
                'walker' => [
                    'no' => 'This user is not correct Dog Walker.'
                ]
            ]
        ],
        'store' => [
            'validation' => 'Dog Owner invalid.',
            'successfully' => 'Dog Owner saved successfully.',
            'failure' => 'Dog Owner saved failure.',
        ],
        'update' => [
            'validation' => 'Dog Owner invalid.',
            'successfully' => 'Dog Owner updated successfully.',
            'failure' => 'Dog Walker updated failure.',
        ],
        'delete' => [
            'successfully' => 'Dog Owner deleted successfully.',
            'failure' => 'Dog Owner deleted failure.',
        ],
    ],

    'walker' => [
        'validation' => [
            'required' => 'Please enter a valid :attribute',
            'json'     => 'Please enter a format :attribute',
            'max'      => 'The :attribute may not be greater than :max characters.'
        ],
        'get' => [
            '404' => 'Could not found Dog Walkers.',
            'successfully' => 'Dog Walkers retrieved successfully.',
            'failure' => 'Dog Walkers retrieved failure.',
        ],
        'show' => [
            '404' => 'Could not found Dog Walker.',
            'successfully' => 'Dog Walker retrieved successfully.',
            'failure' => 'Dog Walker retrieved failure.',
        ],
        'store' => [
            'validation' => 'Dog Walker invalid.',
            'successfully' => 'Create new Dog Walker successfully.',
            'failure' => 'Dog Walker saved failure.',
        ],
        'update' => [
            'validation' => 'Dog Walker invalid.',
            'successfully' => 'Dog Walker updated successfully.',
            'failure' => 'Dog Walker updated failure.',
        ],
        'delete' => [
            'successfully' => 'Dog Walker deleted successfully.',
            'failure' => 'Dog Walker deleted failure.',
        ],
    ],

    'get' => [
        '404' => 'Could not found Users.',
        'successfully' => 'Users retrieved successfully.',
        'failure' => 'Users retrieved failure.',
    ],
    'show' => [
        '404' => 'Could not found the User.',
        'successfully' => 'User retrieved successfully.',
        'failure' => 'User retrieved failure.',
    ],
    'store' => [
        'successfully' => 'Create new User successfully.',
        'failure' => 'Create new User failure.'
    ],
    'update' => [
        'successfully' => 'Update this User successfully.',
        'failure' => 'Update this User failure.'
    ],
    'delete' => [
        'successfully' => 'Delete this User successfully.',
        'failure' => 'Delete this User failure.'
    ]
];