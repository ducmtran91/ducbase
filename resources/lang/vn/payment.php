<?php

return [
    'walker' => [
        'get' => [
            '404' => 'Earnings could not found.',
            'failure' => 'Earnings retrieved failure.',
            'successfully' => 'Earnings retrieved successfully.'
        ],
        'show' => [
            '404' => 'Earning could not found.',
            'failure' => 'Earning retrieved failure.',
            'successfully' => 'Earning retrieved successfully.'
        ]
    ]
];