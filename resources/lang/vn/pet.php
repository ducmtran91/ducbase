<?php

return [
    'validation' => [
        'required' => 'Please enter a valid :attribute',
        'date_format' => 'The :attribute does not match the format :format.',
    ],
    'get' => [
        '404' => 'Pets could not found.',
        'failure' => 'Pets retrieved failure.',
        'successfully' => 'Pets retrieved successfully.'
    ],
    'show' => [
        '404' => 'Pet could not found.',
        'failure' => 'Pet retrieved failure.',
        'successfully' => 'Pet retrieved successfully.'
    ],
    'store' => [
        'validation' => 'Pet information invalid.',
        'successfully' => 'Create new Pet successfully.',
        'failure' => 'Create new Pet failure.'
    ],
    'update' => [
        'validation' => 'Pet information invalid.',
        'successfully' => 'Your pet info was saved.',
        'failure' => 'Update this Pet failure.'
    ],
    'delete' => [
        'successfully' => 'Delete this Pet successfully.',
        'failure' => 'Delete this Pet failure.',
        'is_booking' => 'Your Pet is being booked for Wizepawz\'s Service. If you want to delete this Pet, please kindly call our office to cancel the bookings for this Pet'
    ]
];