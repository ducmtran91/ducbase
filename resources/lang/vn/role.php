<?php

return [
    'get' => [
        '404' => 'Roles could not found.'
    ],
    'show' => [
        '404' => 'Role could not found.'
    ]
];