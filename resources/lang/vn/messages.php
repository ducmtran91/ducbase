<?php
return [
    'edit' => [
        'success' => 'Sửa thành công',//'Successful edit '
    ],
    'store' => [
        'success' => 'Thêm mới thành công',//'Successful create new'
    ],
    'delete' => [
        'success' => 'Xóa thành công',//'Successful delete',
        'not_allow_delete' => 'Không cho phép xóa',//'Can\'t delete'
    ],
    'validation' => [
        'invalid' => 'Thông tin không chính xác. Vui lòng thử lại',//'Invalid information. Please try again'
    ],
    'action' => [
        'success' => 'Xử lý thành công',//'Process successfully'
    ],
];