<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'subject_forgot' => 'Forgot password',
    'content_forgot' => '<p style="margin-bottom: 5px;margin-top: 0px;">Hi <b>{{NAME}}</b>,</p>
                            <p style="margin-bottom: 5px;margin-top: 0px;">Seem like you forgot your password for [product name]. If it is true, click below to reset your password:</p>
                            <p style="margin-bottom: 5px;margin-top: 0px; text-align: center">
                            <a href="{{LINK}}" style="display: inline-block;
                            font-size: 14px;
                            padding: 7px 24px;
                            border-radius: 4px;
                            color: #fff;
                            border: 1px solid #10a107;
                            background: #10a107;
                            font-weight: 400;">Change my password</a></p>
                        
                        <p style="margin-bottom: 5px;margin-top: 0px;">If you did not forget your password, you can safety ignore this email. </p>
                        <p style="margin-bottom: 5px;margin-top: 0px;">Thanks and Best Regards, </p>',
// forgot password
    'user_forgot_user_null' => 'Opps, user does not exist!',
    'password' => 'Password',
    'password_confirm' => 'Confirm password',
    'password_title_update' => 'Update password',
    'password_btn_change' => 'Update',
    'error_password' => 'Please type your password!',
    'error_password_confirm' => 'Please confirm your password!',
    'error_password_compare' => 'Your password does not match!',
    'error_password_and_password_confirm' => 'Please type your password then confirm it!',
    'user_forgot_email_update_false' => 'Update password unsuccessfully!',
    'user_forgot_email_update_success' => 'Update password successfully!',

    // login
    'user_login_success' => 'Login successfully!',
    'user_login_false' => 'Login unsuccessully!',

    // register
    'subject_register' => 'Hi {{NAME}}! Your Account Has Been Created',
    'content_register' => '<p style="margin-bottom: 5px;margin-top: 0px;">Welcome to PANNA, <b>{{NAME}}</b>,</p>
                            <p style="margin-bottom: 5px;margin-top: 0px;">We\'re happy you\'re here. Click the button below to activate your account.</p>
                            <p style="margin-bottom: 5px;margin-top: 0px; text-align: center">
                            <a href="{{LINK}}" style="display: inline-block;
                            font-size: 14px;
                            padding: 7px 24px;
                            border-radius: 4px;
                            color: #fff;
                            border: 1px solid #10a107;
                            background: #10a107;
                            font-weight: 400;"> Activate my account!</a></p>
                        
                        <p style="margin-bottom: 5px;margin-top: 0px;">Wish you have greate time with PANNA,</p>
                       
                        <p style="margin-bottom: 5px;margin-top: 0px;">PANNA Team</p>',
    'user_confirm_user_null' => 'Opps, user does not exist',
    'user_confirm_email_update_false' => 'Confirm password unsuccessfully, please reload your browser and try again later!',
    'user_confirm_email_update_success' => 'Account Successfully Activated!',
    'content_confirm_email' => '<p style="margin-bottom: 5px;margin-top: 0px;">Congratulation!</p>
                            <p style="margin-bottom: 5px;margin-top: 0px;">You have successfully verified your email address.  </p>
                        <p style="margin-bottom: 5px;margin-top: 0px;">Wish you have greate time with PANNA,</p>
                       
                        <p style="margin-bottom: 5px;margin-top: 0px;">PANNA Team</p>',

    'subject_register_user' => 'Welcome to PANNA!',
    'content_register_user' => '<p style="margin-bottom: 5px;margin-top: 0px;">Welcome to PANNA, <b>{{NAME}}</b>,</p>
                            <p style="margin-bottom: 5px;margin-top: 0px;">We\'re happy you\'re here. Please view your account information below.</p>
                            <p style="margin-bottom: 5px;margin-top: 0px;">Email: {{EMAIL}}</p>
                            <p style="margin-bottom: 5px;margin-top: 0px;">Link Resend Password: <a href="{{LINK}}" style="display: inline-block;
                            font-size: 14px;
                            padding: 7px 24px;
                            border-radius: 4px;
                            color: #fff;
                            border: 1px solid #10a107;
                            background: #10a107;
                            font-weight: 400;">Change my password</a></p>
                           </p>
                        <p style="margin-bottom: 5px;margin-top: 0px;">You will need this everytime you want to log on to the website and application, so keep it safe and don\'t share it with anyone. </p>
                        <p style="margin-top: 0px;margin-bottom: 20px;">Thanks and Best Regards, </p>',

    'subject_register_admin' => 'Hi {{NAME}}! You Has Been Assigned As Adminstrator',
    'content_register_admin' => '<p style="margin-bottom: 5px;margin-top: 0px;">Welcome to PANNA, <b>{{NAME}}</b>!</p>
                            <p style="margin-bottom: 5px;margin-top: 0px;">We\'re glad to tell you that you has been assigned as Administrator. Now you canlogin to administration system with following credential:</p>
                            <p style="margin-bottom: 5px;margin-top: 0px;">Email: {{EMAIL}}</p>
                            <p style="margin-bottom: 5px;margin-top: 0px;">Link Resend Password: <a href="{{LINK}}" style="display: inline-block;
                            font-size: 14px;
                            padding: 7px 24px;
                            border-radius: 4px;
                            color: #fff;
                            border: 1px solid #10a107;
                            background: #10a107;
                            font-weight: 400;">Change my password</a></p>
                           </p>
                        <p style="margin-bottom: 5px;margin-top: 0px;">You will need this everytime you want to log on to the administration system, so keep it safe and don\'t share it with anyone. </p>
                        <p style="margin-bottom: 5px;margin-top: 0px;">Thanks and Best Regards, </p>
                        <p style="margin-top: 0px;margin-bottom: 20px;">PANNA Team</p>',
];
