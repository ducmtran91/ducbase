<?php

return [
    'validation' => [
        'default' => [
            'required' => 'Please choose file to upload.',
            'file'     => 'Please choose file is valid.',
            'mimes'    => 'The file upload must be a file of type: :values.',
            'max'      => 'The :attribute may not be greater than :max kilobytes.',
        ],
        'image' => [
            'required' => 'Please choose file to upload.',
            'file'     => 'Please choose file is valid.',
            'mimes'    => 'The file upload must be a file of type: :values.',
            'max'      => 'The :attribute may not be greater than :max kilobytes.',
        ],
        'video' => [
            'required' => 'Please choose file to upload.',
            'file'     => 'Please choose file is valid.',
            'mimes'    => 'The file upload must be a file of type: :values.',
            'max'      => 'The :attribute may not be greater than :max kilobytes.',
        ],
        'audio' => [
            'required' => 'Please choose file to upload.',
            'file'     => 'Please choose file is valid.',
            'mimes'    => 'The file upload must be a file of type: :values.',
            'max'      => 'The :attribute may not be greater than :max kilobytes.',
        ],
    ],
    'cloud' => [
        '404' => 'The cloud storage could not found.'
    ],
    'upload' => [
        'successfully' => 'File uploaded successfully.',
        'failure'      => 'File uploaded failure.'
    ],
    'deleted' => [
        'successfully' => 'File deleted successfully.',
        'failure'      => 'File deleted failure.'
    ]
];