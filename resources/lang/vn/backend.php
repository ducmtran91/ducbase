<?php

return [
    "label" => [
        "id" => "ID",
        "image" => "Ảnh",
        "title" => "Tiêu đề",
        "name" => "Tên",
        "link" => "Link",
        "position" => "Vị trí",
        "description" => "Mô tả",
        "status" => "Trạng thái",
        "action" => "Hành động",
        "category_parent" => "Danh mục cha",
        "filter" => [
            "paging" => "Lựa chọn",
        ],
        "actions" => [
            "bulk" => "Đa hành động",
            "add" => "Thêm mới",
            "edit" => "Sửa",
            "delete" => "Xóa",
            "cancel" => "Bỏ qua",
            "save" => "Lưu",
        ],
        "text" => [
            "button" => "Tên button",
        ],
        "list" => "Danh sách",
        "info" => "Thông tin",
        "management" => [
            "slider" => [
                "index" => "Quản lý slider",
                "edit" => "Sửa slider",
                "create" => "Thêm mới slider",
                "show" => "Thông tin slider",
            ],
            "categories" => [
                "index" => "Quản lý danh mục",
                "edit" => "Sửa danh mục",
                "create" => "Thêm mới danh mục",
                "show" => "Thông tin danh mục",
            ],
            "banner" => [
                "index" => "Quản lý banner",
                "edit" => "Sửa banner",
                "create" => "Thêm mới banner",
                "show" => "Thông tin banner",
            ]
        ],
        "search" => "Tìm kiếm theo",
        "file" => [
            'choose' => 'Chọn file',
        ],
        'choose' => 'Lựu chọn'
    ],
    "menu" => [
        "role" => "roles",
        "admin" => "cccount",
        "menu" => "phân loại",
        "slider" => "slider",
        "categories" => "danh mục",
        "banner" => "banner",
    ],
    "route" => [
        "slider" => "slider",
        "banner" => "banner",
        "categories" => "categories",
    ],
    'errors' => [
        'image' => [
            'convert' => 'Chuyển đổi image sang base64 không thành công',
        ],
        'not_found' => 'Dữ liệu không tìm thấy',
    ]
];