<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'email' => [
        'register' => [
            'subject' => ':name register successfully',
            'content' => 'You register successfully, :link'
        ]
    ],
    'login' => [
      'validation' => 'Email or Password is incorrect.',
        '403' => 'This User do not has Permission.',
        'failure' => 'User logged in failure.',
        'successfully' => 'User logged in successfully.',
        'token' => [
            'failure' => 'User retrieved token failure.'
        ]
    ],
    'register' => [
        'validation' => [
            'required'  => 'Please enter a valid :attribute.',
            'exists'    => 'The selected :attribute is invalid.',
            'unique'    => 'That :attribute address is already registered.',
            'min'       => 'The :attribute must be at least :min characters.',
            'max'       => 'The :attribute may not be greater than :max characters.',
            'confirmed' => 'The :attribute confirmation does not match.',
            'in'        => 'The selected :attribute is invalid.',
        ],
        'not_found' => 'Could not found the User.',
        'store' => [
            'successfully' => 'Sign up successfully.',
            'failure' => 'Sign up failure.'
        ],
        'update' => [
            'successfully' => 'Update this User successfully.',
            'failure' => 'Update this User failure.'
        ],
        'delete' => [
            'successfully' => 'Delete this User successfully.',
            'failure' => 'Delete this User failure.'
        ],
    ],
    'password' => [
        'validation' => [
            'required'     => 'Please enter a valid :attribute.',
            'required_with'     => 'Please enter a valid :attribute.',
            'old_password' => 'Your :attribute is incorrect.',
            'confirmed'    => 'Confirm password doesn\'t match. Please try again.',

        ]
    ],
    'forgot' => [
        'validation' => 'Email is required.',
        '404' => 'The User do not exists in system.',
        'failure' => 'Forgot password failure.',
        'send_email_failure' => 'Send email reset password to User failure.',
        'send_email_successfully' => 'Send email reset password to User successfully.'
    ],
    'logout' => [
        'successfully' => 'You have successfully logged out.',
        'failure' => 'You have failure logged out.',
        'token_invalid' => 'Your token invalid.',
        'token_expired' => 'Your token expired.'
    ],
    'owner' => [],
    'walker' => [
        'validation' => [
            'login' => [
                'email' => [
                    'required' => 'Invalid email address'
                ]
            ]
        ]
    ]
];
