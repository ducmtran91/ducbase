<?php
return [
    'owner' => [
        'validation' => [
            'required'       => 'Please enter a valid :attribute',
            'required_with'  => 'Please enter a valid :attribute',
            'required_if'    => 'Please enter a valid :attribute',
            'exists'         => 'The selected :attribute is invalid.',
            'json'           => 'The :attribute does not match the format.',
            'date_format'           => 'The :attribute does not match the format :format.',
            'before_2hours' => 'Bookings can be made at least two hours before starting time. Please call our office - (916) 542-0657 for a last minute booking.'
        ],
        'multiple_date' => [
            'add' => [
                'successfully' => 'Add service successfully.',
                'failure' => 'Add service failure.',
            ]
        ],
        'get' => [
            '404' => 'Bookings could not found.',
            'failure' => 'Bookings retrieved failure.',
            'successfully' => 'Bookings retrieved successfully.'
        ],
        'show' => [
            '404' => 'Booking could not found.',
            'failure' => 'Booking retrieved failure.',
            'successfully' => 'Booking retrieved successfully.'
        ],
        'store' => [
            'failure' => 'Booking saved failure.',
            'successfully' => 'Booking saved successfully.'
        ],
        'update' => [
            'failure' => 'Booking updated failure.',
            'successfully' => 'Booking updated successfully.'
        ],
        'delete' => [
            'failure' => 'Booking deleted failure.',
            'successfully' => 'Booking deleted successfully.'
        ],
        'completed' => [
            'no' => 'The booking not completed.'
        ],
        'report_card' => [
            'get' => [
                '404' => 'Report Cards could not found.',
                'failure' => 'Report Cards retrieved failure.',
                'successfully' => 'Report Cards retrieved successfully.'
            ],
            'show' => [
                '404' => 'Report Card could not found.',
                'failure' => 'Report Card retrieved failure.',
                'successfully' => 'Report Card retrieved successfully.'
            ],
            'store' => [
                'failure' => 'Report Card saved failure.',
                'successfully' => 'Report Card saved successfully.'
            ],
            'update' => [
                'failure' => 'Report Card updated failure.',
                'successfully' => 'Report Card updated successfully.'
            ],
            'delete' => [
                'failure' => 'Report Card deleted failure.',
                'successfully' => 'Report Card deleted successfully.'
            ]
        ],
        'review' => [
            'show' => [
                '404' => 'Review of the Job could not found.',
                'exists' => 'Review of the Job has exists.'
            ],
            'store' => [
                'failure' => 'Review of the Job saved failure.',
                'successfully' => 'Review of the Job saved successfully.'
            ]
        ],
        'next_step' => [
            'validation' => 'Please enter data is valid.',
            'successfully' => 'Booking next step successfully.',
            'failure' => 'Booking next step failure.',
        ]
    ],

    'walker' => [
        'validation' => [
            'required'       => 'Please enter a valid :attribute',
            'required_with'  => 'Please enter a valid :attribute',
            'required_if'    => 'Please enter a valid :attribute',
            'exists'         => 'The selected :attribute is invalid.',
            'json'           => 'The :attribute does not match the format.',
            'date_format'           => 'The :attribute does not match the format :format.',
            'before_2hours' => 'Bookings can be made at least two hours before starting time. Please call our office - (916) 542-0657 for a last minute booking.'
        ],
        'get' => [
            '404' => 'Bookings could not found.',
            'failure' => 'Bookings retrieved failure.',
            'successfully' => 'Bookings retrieved successfully.'
        ],
        'show' => [
            '404' => 'Booking could not found.',
            'failure' => 'Booking retrieved failure.',
            'successfully' => 'Booking retrieved successfully.'
        ],
        'store' => [
            'failure' => 'Booking saved failure.',
            'successfully' => 'Booking saved successfully.'
        ],
        'update' => [
            'failure' => 'Booking updated failure.',
            'successfully' => 'Booking updated successfully.'
        ],
        'delete' => [
            'failure' => 'Booking deleted failure.',
            'successfully' => 'Booking deleted successfully.'
        ],
        'waiting' => [
            'yes' => 'Booking do not have assign.',
            'no' => ''
        ],
        'confirmed' => [
            'yes'=>'Booking has confirmed by the Other Walker.',
            'no' => 'Booking don\'t has confirmed by the Other Walker.'
        ],
        'progress' => [
            'yes'=>'Booking has started by the Other Walker.',
            'no' => 'Booking do not have start by the Other Walker.',
            'exists' => 'The other Job has started, please complete before you can start this Job.',
        ],
        'completed' => [
            'yes'=>'Booking has completed by the Other Walker.',
            'no' => ''
        ],
        'report_card' => [
            'get' => [
                '404' => 'Report Cards could not found.',
                'failure' => 'Report Cards retrieved failure.',
                'successfully' => 'Report Cards retrieved successfully.'
            ],
            'show' => [
                '404' => 'Report Card could not found.',
                'failure' => 'Report Card retrieved failure.',
                'successfully' => 'Report Card retrieved successfully.'
            ],
            'store' => [
                'failure' => 'Report Card saved failure.',
                'successfully' => 'Report Card saved successfully.'
            ],
            'update' => [
                'failure' => 'Report Card updated failure.',
                'successfully' => 'Report Card updated successfully.'
            ],
            'delete' => [
                'failure' => 'Report Card deleted failure.',
                'successfully' => 'Report Card deleted successfully.'
            ]
        ]
    ],

    'get' => [
        '404' => 'Bookings could not found.',
        'failure' => 'Bookings retrieved failure.',
        'successfully' => 'Bookings retrieved successfully.'
    ],
    'show' => [
        '404' => 'Booking could not found.',
        'failure' => 'Booking retrieved failure.',
        'successfully' => 'Booking retrieved successfully.'
    ],
];