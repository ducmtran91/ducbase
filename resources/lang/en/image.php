<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'image_base64_required'             => "Image doesn't format exactly!",
    'image_destination_directory_not_null'           => "Directory image doesn't null.",
    'image_file_not_found'                => "Image file doesn't found",
];
