<?php

return [
    'booking' => [
        'services' => [
            'dog_walking'   => [
                'owner'  => [
                    'accepted'    => [
                        'title'   => 'Booking confirmation',
                        'content' => 'Your :serviceType booking on :time has been confirmed by :walker :timeAgo ago',
                        'action'  => 'accepted'
                    ],
                    'started'      => [
                        'title'   => 'Start!',
                        'content' => ':petName has started her :serviceType with :walker :timeAgo ago',
                        'action'  => 'started'
                    ],
                    'finished'     => [
                        'title'   => 'Finish!',
                        'content' => ':petName has finished her :serviceType with :walker :timeAgo ago',
                        'action'  => 'finished'
                    ],
                    'no_available' => [
                        'title'   => 'No available walker!',
                        'content' => 'Sorry, we cannot find any walkers to service your :serviceType booking on :time :timeAgo ago',
                        'action'  => 'no_available'
                    ],
                    'cancelled'    => [
                        'confirmed' => [
                            'title'   => 'We have cancelled your job',
                            'content' => 'Your Walking Service - :bookingId has been cancelled, please call our office if you have any questions.',
                            'action'  => 'cancelled_confirmed'
                        ],
                        'no_confirmed' => [
                            'title'   => 'You have cancelled job successfully',
                            'content' => 'Your Walking Service - :bookingId has been cancelled, please call our office if you have any questions.',
                            'action'  => 'cancelled_no_confirmed'
                        ]
                    ],
                ],

                'walker' => [
                    'new_job'   => [
                        'title'   => 'New job',
                        'content' => 'There is a new booking available in your area. Please go to your account to accept the walk.',
                        'action'  => 'new_job'
                    ],
                    'waste_accepted' => [
                        'title'   => 'You are assigned',
                        'content' => 'You have been assigned a Walking job on :timeStartJob at :locationJob :timeAgo ago.',
                        'action'  => 'waste_accepted'
                    ],
                    'cancelled' => [
                        'confirmed' => [
                            'title'   => 'We have cancelled your job',
                            'content' => 'Your Walking Service - :bookingId has been cancelled, please call our office if you have any questions.',
                            'action'  => 'cancelled_confirmed'
                        ]
                    ]
                ],

                'admin' => [
                    'pending' => [
                        'title'   => 'You have new pending jobs',
                        'content' => 'Show notification icon behind module name: Pending Jobs',
                        'action'  => 'admin_pending'
                    ],
                    'edit' => [
                        'title'   => 'We have edited your job',
                        'content' => 'Your :serviceType - :bookingId has been edited, please check and call our office if you have any questions.',
                        'action'  => 'admin_edit'
                    ],
                    'accepted' => [
                        'title'   => 'Job accepted',
                        'content' => ' :walker has accepted :serviceType - ID on :timeAgo ',
                        'action'  => 'admin_accepted'
                    ],
                    'no_available' => [
                        'title' => 'No available walker!',
                        'content' => 'There is no Walker accepting :serviceName booking - :bookingId',
                        'action' => 'admin_no_available'
                    ]
                ]

            ],

            'dog_waste_removal' => [
                'owner'  => [
                    'accepted' => [
                        'title'   => 'Booking confirmation',
                        'content' => 'Your :serviceType booking on :time has been confirmed by :walker :timeAgo ago',
                        'action'  => 'accepted'
                    ],
                    'started'  => [
                        'title'   => 'Start!',
                        'content' => 'Your Waste Removal job has been started by :walker :timeAgo ago',
                        'action'  => 'started'
                    ],
                    'finished' => [
                        'title'   => 'Finish!',
                        'content' => 'Your Waste Removal job has been completed by :walker :timeAgo ago',
                        'action'  => 'finished'
                    ],
                    'cancelled_confirmed' => [
                        'title'   => 'We have cancelled your job',
                        'content' => 'We\'re so sorry that your Waste Removal - :bookingId booking is cancelled. We were not able to confirm a scooper in your area, and we have refunded your account. Please call if you have any questions.',
                        'action'  => 'cancelled_confirmed'
                    ],
                    'cancelled_no_confirmed' => [
                        'title'   => 'You have cancelled job successfully',
                        'content' => 'Your Waste Removal Service - :bookingId has been cancelled, please call our office if you have any questions.',
                        'action'  => 'cancelled_no_confirmed'
                    ],
                    'recurred' => [
                        'title'   => 'Your booking is recurred',
                        'content' => 'Your Waste Removal Service is recurred.',
                        'action'  => 'recurred'
                    ]
                ],

                'walker' => [
                    'new_job'   => [
                        'title'   => 'New job',
                        'content' => 'There is a new booking available in your area. Please go to your account to accept the walk.',
                        'action'  => 'new_job'
                    ],
                    'walker_accepted' => [
                        'title'   => 'You are assigned',
                        'content' => 'You have been assigned a Pet Waste Removal job on :timeStartJob at :locationJob :timeAgo ago',
                        'action'  => 'walker_accepted'
                    ],
                    'cancelled_confirmed' => [
                        'title' => 'We have cancelled your job',
                        'content' => 'Your Waste Removal Service - :bookingId has been cancelled, please call our office if you have any questions.',
                        'action' => 'cancelled_confirmed'
                    ],
                ],

                'admin' => [
                    'pending' => [
                        'title'   => 'You have new pending jobs',
                        'content' => 'Show notification icon behind module name: Pending Jobs',
                        'action'  => 'admin_pending'
                    ],
                    'edit' => [
                        'title'   => 'We have edited your job',
                        'content' => 'Your :serviceType - :bookingId has been edited, please check and call our office if you have any questions.',
                        'action'  => 'admin_edit'
                    ],
                    'accepted' => [
                        'title'   => 'Job accepted',
                        'content' => ' :walker has accpeted :serviceType - ID on :timeAgo ',
                        'action'  => 'admin_accepted'
                    ],
                    'no_available' => [

                    ]
                ]
            ]
        ],
    ],

    'payment' => [
        'owner' => [
            'refund' => [
                'title'   => 'Congratulation, you have been refunded',
                'content' => 'Your booking is cancelled. You have just received a refund of the booking.',
                'action'  => 'refund'
            ]
        ],
        'walker' => [
            'paid'   => [
                'title'   => 'Payment successfully',
                'content' => 'You have got payment for :serviceType job on :timeStartJob :timeAgo ago',
                'action'  => 'walker_paid'
            ]
        ]
    ],

    'proposal' => [
        'admin' => [
            'new' => [
                'title'   => 'You have new application',
                'content' => 'Show notification icon behind module name: Application Management',
                'action'  => 'admin_new'
            ]
        ]

    ],

    'contact' => [
        'admin' => [
            'new' => [
                'title'   => 'You have new request for contact',
                'content' => 'Show notification icon behind module name: Contact Management',
                'action'  => 'admin_new'
            ],
        ],
        'reply' => [
            'title'   => 'You have a new message',
            'content' => 'We have sent email replied to your contact request. Please check your email for more information!',
            'action'  => 'reply'
        ]
    ],

    'cron_job' => [
        'booking' => [
            'owner' => [
                'during_the_day' => [
                    'title'   => 'Good morning, your jobs will be performed today!',
                    'content' => 'Today :walker will be at your home at :time for :serviceType job',
                    'action'  => 'during_the_day'
                ],
            ],
            'walker' => [
                'during_the_day' => [
                    'title'   => 'Good morning, you have jobs today',
                    'content' => 'Hi :walker, today you have :numberJob jobs, :services',
                    'action'  => 'walker_during_the_day'
                ],
            ],

            'before_2hours' => [
                'admin' => [
                    'no_available' => [
                        'title'   => 'No available walker!',
                        'content' => 'There is no Walker accepting :serviceType booking - :bookingId',
                        'action'  => 'admin_before_2hours_no_available'
                    ],
                    'no_available_pick' => [
                        'title'   => 'No available walker!',
                        'content' => ':owner has made a :bookingId at :time and :serviceType have not picked up the job.',
                        'action'  => 'admin_before_2hours_no_available_pick'
                    ]
                ]
            ]

        ]
    ],

    'get'    => [
        '404'          => 'Notifications could not found.',
        'successfully' => 'Notifications retrieved successfully.',
        'failure'      => 'Notifications retrieved failure.',
    ],
    'show'   => [
        '404' => 'Notification could not found.',
    ],
    'store'  => [
        'validation'   => 'Notification invalid.',
        'successfully' => 'Notification saved successfully.',
        'failure'      => 'Notification saved failure.',
    ],
    'delete' => [
        'successfully' => 'Notification deleted successfully.',
        'failure'      => 'Notification deleted failure.',
    ]
];