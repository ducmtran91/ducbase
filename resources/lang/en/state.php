<?php

return [
    'get' => [
        '404' => 'States could not found.',
        'successfully' => 'States retrieved successfully.',
        'failure' => 'States retrieved failure.',
    ],
    'show' => [
        '404' => 'State could not found.',
        'successfully' => 'State retrieved successfully.',
        'failure' => 'State retrieved failure.',
    ],
];