<?php

return [
    'get' => [
        'successfully' => 'List of Contact Us retrieved successfully.',
        'failure' => 'List of Contact Us retrieved failure.',
    ],
    'show' => [
        'successfully' => 'Contact Us retrieved successfully.',
        'failure' => 'Contact Us retrieved failure.',
        '404' => 'Contact Us could not found.'
    ],
    'update' => [
        'successfully' => 'Contact Us updated successfully.',
        'failure' => 'Contact Us updated failure.',
    ],
    'delete' => [
        'successfully' => 'Contact Us deleted successfully.',
        'failure' => 'Contact Us deleted failure.',
    ]
];