<?php

return [
    "label" => [
        "id" => "ID",
        "image" => "Image",
        "title" => "Title",
        "name" => "Tên",//Name
        "link" => "Link",//Link
        "position" => "Vị trí",//Position
        "description" => "Mô tả",//description
        "status" => "Trạng thái",//Status
        "action" => "Hành động",//Action
        "category_parent" => "Danh mục cha",//Category Parent
        "filter" => [
            "paging" => "Lựa chọn",//Viewing options
        ],
        "actions" => [
            "bulk" => "Đa hành động",//Bulk actions
            "add" => "Thêm mới",//Add new
            "edit" => "Sửa",//Edit
            "delete" => "Xóa",//Delete
            "cancel" => "Bỏ qua",//Delete
            "save" => "Lưu",//Delete
        ],
        "text" => [
            "button" => "Tên button",//button text
        ],
        "list" => "Danh sách",//List
        "info" => "Thông tin",//Information
        "management" => [
            "slider" => [
                "index" => "Quản lý slider",   //Slider Management
                "edit" => "Sửa slider",   //slider Edit
                "create" => "Thêm mới slider",   //slider Edit
                "show" => "Thông tin slider",   //Slider Information
            ],
            "categories" => [
                "index" => "Quản lý danh mục",   //Categories Management
                "edit" => "Sửa danh mục",   //Categories Edit
                "create" => "Thêm mới danh mục",   //Categories Edit
                "show" => "Thông tin danh mục",   //Categories Information
            ]
        ],
        "search" => "Tìm kiếm theo",//Seach by
        "file" => [
            'choose' => 'Chọn file',//Choose file
        ],
        'choose' => 'Lựu chọn'
    ],
    "menu" => [
        "role" => "roles",
        "admin" => "cccount",
        "menu" => "phân loại",
        "slider" => "slider",
        "categories" => "danh mục",
    ],
    "route" => [
        "slider" => "slider",
        "categories" => "categories",
    ],
    'errors' => [
        'image' => [
            'convert' => 'Chuyển đổi image sang base64 không thành công',//'Convert image to base64 have failed!',
        ],
        'not_found' => 'Dữ liệu không tìm thấy',//not found,
    ]
];