/**
      * @api {post} /$MODEL_NAME_PLURAL_CAMEL$ Store
      * @apiGroup $MODEL_NAME$
      * @apiName Store
      * @apiDescription Store a newly created $MODEL_NAME$ in storage.
      *
      * @apiPermission api
      * @apiPermission jwt.auth
      *
      * @apiHeader {string} Authorization Authorization Header
      *
      * @apiParam {Create$MODEL_NAME$APIRequest} request
      *
      * @apiSuccess {int} id Id of $MODEL_NAME$
      * @apiSuccess {date} created_at Created at of $MODEL_NAME$
      * @apiSuccess {date} updated_at Updated at of $MODEL_NAME$
      *
      * @apiSuccessExample {json} Success-Response:
      * {
      *      "meta": {
      *         "success": true,
      *         "message": "success",
      *         "code": 200
      *      },
      *      "response": {
      *          "id": 1,
      *          "created_at": "2018-05-16 14:32:15",
      *          "updated_at": "2018-05-16 14:32:15"
      *      }
      * }
      * @apiError Error Get list failure
      * @apiErrorExample {json} Error-Response:
      * HTTP/1.1 500 GetErrorFailure
      * {
      *    "meta": {
      *        "success": false,
      *        "message": "failure",
      *        "code": 401,
      *        "errors": null
      *    },
      *        "response": null
      * }
      *
      */