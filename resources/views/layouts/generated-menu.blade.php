<h3>For Admin</h3>
<!-- --------------------------------------------------------------------------------------------------------------------------------------------------- -->



@if(Helper::checkUserPermission('admin.roles.index'))
    <li class="{{ Request::is('roles*') ? 'active' : '' }}">
        <a href="{!! route('admin.roles.index') !!}"><i class="fa fa-user"></i><span>Roles Management</span></a>
    </li>
@endif

@if(Helper::checkUserPermission('admin.users.index'))
    <li class="{{ Request::is('users*') ? 'active' : '' }}">
        <a href="{!! route('admin.users.index') !!}"><i
                    class="fa fa-group"></i><span>Administrators Management</span></a>
    </li>
@endif

@if(Helper::checkUserPermission('admin.categories.index'))
    <li class="{{ Request::is('categories*') ? 'active' : '' }}">
        <a href="{!! route('admin.categories.index') !!}"><i class="fa fa-list-alt"></i><span>Categories</span></a>
    </li>
@endif

@if(Helper::checkUserPermission('admin.posts.index') || Helper::checkUserPermission('admin.detailPosts.index'))
    <li class="{{ Request::is('posts*') || Request::is('detailPosts*') ? 'active' : '' }}">
        <a>
            <i class="fa fa-gear"></i>
            <span>Articles Management</span>
            <span class="fa fa-chevron-down"></span>
        </a>

        <ul class="nav child_menu">
            @if(Helper::checkUserPermission('admin.posts.index'))
                <li class="{{ Request::is('posts*') ? 'active' : '' }}">
                    <a href="{!! route('admin.posts.index') !!}"><i class="fa fa-thumb-tack"></i><span>Posts</span></a>
                </li>
            @endif

            @if(Helper::checkUserPermission('admin.detailPosts.index'))
                <li class="{{ Request::is('detailPosts*') ? 'active' : '' }}">
                    <a href="{!! route('admin.detailPosts.index') !!}"><i class="fa fa-info"></i><span>Detail Posts</span></a>
                </li>
            @endif
        </ul>
    </li>
@endif

@if(Helper::checkUserPermission('admin.slider.index'))
    <li class="{{ Request::is('slider*') ? 'active' : '' }}">
        <a href="{!! route('admin.slider.index') !!}"><i class="fa fa-list-alt"></i><span>Slider</span></a>
    </li>
@endif

@if(Helper::checkUserPermission('admin.banner.index'))
    <li class="{{ Request::is('banner*') ? 'active' : '' }}">
        <a href="{!! route('admin.banner.index') !!}"><i class="fa fa-list-alt"></i><span>Banner</span></a>
    </li>
@endif