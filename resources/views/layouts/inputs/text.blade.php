<div class="form-group {{ $colLabel }}">
    {!! Form::label($id, $label) !!}
    <span class="text-danger">*</span>
</div>
<div class="form-group {{ $colInput }}">
    {!! Form::text($id, $value, ['class' => 'form-control', 'maxlength' => 255, 'placeholder' => $label]) !!}
</div>
