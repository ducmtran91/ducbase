<div class="form-group {{ $colLabel }}">
    {!! Form::label($id, $label) !!}
    <span class="text-danger">*</span>
</div>
<div class="form-group {{ $colInput }}">
    {!! Form::textarea($id, $value, array_merge(['class' => 'form-control', 'placeholder' => $label], $validation)) !!}
</div>
