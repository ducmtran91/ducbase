<!DOCTYPE html>
<html lang="en"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Administrator | PES</title>
        <!-- Bootstrap -->
        <link href="{{ URL::to('/builds/css/vendor.admin.css') }}" rel="stylesheet">          
        <link href="{{ URL::to('/assets/iCheck/skins/flat/green.css') }}" rel="stylesheet">  
        <!-- Custom Theme Style -->
        @stack('style-top')
        @stack('style')
        <link href="{{ URL::to('/builds/css/main.admin.css') }}" rel="stylesheet">
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head><body class="nav-md footer_fixed">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="{{ Admin::route('contentManager.index') }}" class="site_title">
                                <i class="fa fa-paw"></i>
                                <span>Administrator</span>
                            </a>
                        </div>

                        <div class="clearfix"></div>

                        <!-- menu profile quick info -->
                        <div class="profile">
                            <div class="profile_pic">
                                <img src="{{ Auth::guard('admin')->user()->avatar }}" alt="..."
                                     class="img-circle profile_img">
                            </div>
                            <div class="profile_info">
                                <span>Welcome,</span>
                                <h2>{{ Auth::guard('admin')->user()->name }}</h2>
                            </div>
                        </div>
                        <!-- /menu profile quick info -->

                        <br/>

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <!-- <h3>General</h3> -->
                                <ul class="nav side-menu">                                                       
                                    @include('layouts.generated-menu')                            
                                </ul>
                            </div>
                        </div>
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" title="Settings">
                                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Lock">
                                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Logout">
                                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>
                            <ul class="nav navbar-nav navbar-left">
                                <li>
                                    <a href="{{ url('/') }}" target="_blank">
                                        <strong>View Website <i class="fa fa-arrow-right"></i></strong>
                                    </a>
                                </li>
                            </ul>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="{{ Auth::guard('admin')->user()->avatar }}" alt="">{{ Auth::guard('admin')->user()->name }}
                                        <span class=" fa fa-angle-down"></span>
                                    </a>

                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <!--<li>
                                            <a href="{{ route('admin.admin.profiles',['id'=>Auth::guard('admin')->user()->id]) }}">--}}
                                                Profile
                                            </a>
                                        </li>-->
                                        <li>
                                            <a href="{{ Admin::route('logout') }}">
                                                <i class="fa fa-sign-out pull-right"></i>
                                                Log Out
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="col-xs-12 row">
                        <ol class="breadcrumb">
                            <li><a href="{{ URL::to('/admin/') }}"><i class="fa fa-home"></i> Dashboard</a></li>
                            <?php if (isset($breadcrumb)) echo $breadcrumb ?>
                        </ol>
                    </div>
                    @yield('content')
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                        Thanks for using QSOFT-CMS 1.0
                    </div>
                    <div class="clearfix"></div>
                </footer>          
                <!-- /footer content -->
            </div>
        </div>

        <!-- Custom Theme Scripts -->
        <script src="{{ URL::to('/builds/js/vendor.admin.js') }}"></script>
        <script src="http://cdn.ckeditor.com/4.9.2/full/ckeditor.js"></script>
        {{--<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>--}}
        <script src="{{ URL::to('/builds/js/main.admin.js') }}"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
        <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
        {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.2/jspdf.plugin.autotable.js"></script>--}}

        <script>
$(document).ready(function() {
    $('select.select2').select2();
});
        </script>

        @if(!empty(Config::get('app.debug')))
            <!-- <script type="text/javascript">
                document.write('<script src="//localhost:35729/livereload.js?snipver=1" type="text/javascript"><\/script>');
            </script> -->
        @endif

        @stack('scripts')
    </body></html>