<div class="wrap-load-more-ajax">
    <a href="#" class="load-more-ajax" data-id="{{ $id }}" data-append="{{ $append }}" data-link="{{ $url }}" data-page="{{ $page }}">
        Xem thêm<i class="fa fa-angle-down"></i>
    </a>
</div>