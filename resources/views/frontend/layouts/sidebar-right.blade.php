<div class="right-sidebar" id="right-sidebar">
    <ul class="section-nav" id="navbar-example3">
        <li class="toc-entry toc-h2"><a class="active" href="#how-it-works">How it works</a></li>
        <li class="toc-entry toc-h2"><a href="#example-in-navbar">Example in navbar</a>
            <ul>
                <li class="toc-entry toc-h4"><a href="#fat">@fat</a></li>
                <li class="toc-entry toc-h4"><a href="#mdo">@mdo</a></li>
                <li class="toc-entry toc-h4"><a href="#one">one</a></li>
                <li class="toc-entry toc-h4"><a href="#two">two</a></li>
                <li class="toc-entry toc-h4"><a href="#three">three</a></li>
            </ul>
        </li>
        <li class="toc-entry toc-h2"><a href="#example-with-nested-nav">Example with nested nav</a>
            <ul>
                <li class="toc-entry toc-h4"><a href="#item-1">Item 1</a></li>
                <li class="toc-entry toc-h4"><a href="#item-2">Item 2</a></li>
                <li class="toc-entry toc-h4"><a href="#item-3">Item 3</a></li>
            </ul>
        </li>
        <li class="toc-entry toc-h2"><a href="#example-with-list-group">Example with list-group</a>
            <ul>
                <li class="toc-entry toc-h4"><a href="#item-1-1">Item 1</a></li>
                <li class="toc-entry toc-h4"><a href="#item-2-1">Item 2</a></li>
                <li class="toc-entry toc-h4"><a href="#item-3-1">Item 3</a></li>
                <li class="toc-entry toc-h4"><a href="#item-4">Item 4</a></li>
            </ul>
        </li>
        <li class="toc-entry toc-h2"><a href="#usage">Usage</a>
            <ul>
                <li class="toc-entry toc-h3"><a href="#via-data-attributes">Via data attributes</a></li>
                <li class="toc-entry toc-h3"><a href="#via-javascript">Via JavaScript</a>
                    <ul>
                        <li class="toc-entry toc-h4"><a href="#resolvable-id-targets-required">Resolvable ID targets required</a></li>
                        <li class="toc-entry toc-h4"><a href="#non-visible-target-elements-ignored">Non-:visible target elements ignored</a></li>
                    </ul>
                </li>
                <li class="toc-entry toc-h3"><a href="#methods">Methods</a>
                    <ul>
                        <li class="toc-entry toc-h4"><a href="#scrollspyrefresh">.scrollspy('refresh')</a></li>
                        <li class="toc-entry toc-h4"><a href="#scrollspydispose">.scrollspy('dispose')</a></li>
                    </ul>
                </li>
                <li class="toc-entry toc-h3"><a href="#options">Options</a></li>
                <li class="toc-entry toc-h3"><a href="#events">Events</a></li>
            </ul>
        </li>
    </ul>

</div>