<div class="nav-column">
    <h5>
        <a href="#">
            {{ $item->name }}
        </a>
    </h5>
    @if(count($item->child) >0)
        <ul>
            @foreach($item->child as $item)
                @include('frontend.layouts.partials.sub_menu', $item)
            @endforeach
        </ul>
    @endif
</div>