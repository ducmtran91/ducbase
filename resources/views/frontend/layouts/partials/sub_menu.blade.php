@if(count($item->child) > 0 AND $item->parent_id > 0)
    <li><a  class="nav-link" href="#">{{ $item->name }} <i class="fa fa-chevron-right"></i></a>
@else
    <li><a  class="nav-link" href="{{ url("/category/".str_slug($item->name)) }}">{{ $item->name }}</a>
@endif
    @if(count($item->child) >0)
        <ul class="sub-menu">
            @foreach($item->child as $item)
                @include('frontend.layouts.partials.sub_menu', $item)
            @endforeach
        </ul>
    @endif
</li>