<html>
<head>
    <title><?php echo $error?></title>
</head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<body>
<div class="container" style="padding-top: 10px">
    <div class="col-md-3 col-sm-3 hidden-xs">

    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="col-xs-12" style="text-align: center">
            <h3> <?php echo $error?></h3>
        </div>
    </div>
    <div class="col-md-3 col-sm-3 hidden-xs">

    </div>

</div>
</body>
<script>
    jQuery(function ($) {
        $('body').on('click', '#submitChangePassword', function () {
            $('#errorPassword').hide();
            $('#errorPasswordConfirm').hide();
            $('#errorPasswordCompare').hide();
           var password=$('#password').val();
           var passwordConfirm=$('#passwordConfirm').val();
           if(password!='' && passwordConfirm!=''){
               if(password!=passwordConfirm){
                   $('#errorPasswordCompare').show();
               }else{
                   $('#submit_form').submit();
               }
           }else{
               if(password==''){
                   $('#errorPassword').show();
               }
               if(passwordConfirm==''){
                   $('#errorPasswordConfirm').show();
               }
           }
        });
    });
</script>
</html>