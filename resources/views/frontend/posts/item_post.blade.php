<div class="item">
    <h3>
        <a href="{{ url(str_slug($item->categories->name).'/'.str_slug($item->name)) }}.html">{{ $item->name }}</a>
    </h3>
    <div class="excerpt">
        {!! $item->excerpt !!}
    </div>
</div>