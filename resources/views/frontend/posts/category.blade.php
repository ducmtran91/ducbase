@extends('layouts.app')

@section('content')
    <div id="content">
        <div class="grid">
            <h1 class="headline">
                {!! !empty($posts[0]->categories) ? $posts[0]->categories->name : '' !!}
            </h1>
        </div>
        <div id="list-post" class="b-top">
            @if(!$posts->isEmpty())
                <div class="wrap-list-post">
                    @each('frontend.posts.item_post', $posts, 'item')
                </div>

                @include('frontend.generate.load_more', [
                    'id' => $posts[0]->category_id,
                    'append' => 'wrap-list-post',
                    'url' => route('front.category.postAjax'),
                    'page' => 2
                ])
            @endif
        </div>
    </div>

    @include('frontend.layouts.sidebar-right')
@endsection