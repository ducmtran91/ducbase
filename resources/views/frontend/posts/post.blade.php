@extends('layouts.app')

@section('content')
    <div id="content">
        <div id="the-post">
            <div class="post-content" data-spy="scroll" data-target="#detail-post" data-offset="45">
                @if($post->detailPosts)
                    <article >
                        <h1 class="post-title" id="detail-post-0">{{ $post->name }}</h1>
                        <div class="post-meta">
                            {!! html_entity_decode($post->description) !!}
                        </div>
                        @each('frontend.posts.partials.item_detail_post', $post->detailPosts, 'item')
                    </article>
                @endif
            </div>
        </div>
    </div>
    <div class="right-sidebar" id="right-sidebar">
        <aside class="widget sidebar-widget " style="position: relative;min-height: 100%;">
            @if($post->detailPosts)
                <ul class="scroll-fixed" id="detail-post">
                    <h3 class="title-widget">
                        <span>Overview</span>
                    </h3>
                    @include('frontend.posts.partials.sidebar-right', ['item' => (object)[
                        'id' => 0,
                        'name' => $post->name,
                    ]])

                    @each('frontend.posts.partials.sidebar-right', $post->detailPosts, 'item')
                </ul>
            @endif
        </aside>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('body').scrollspy({
                target: '#detail-post'
            });
            var $sticky = $('.scroll-fixed');
            var $footer = $('#footer');
            var $header = $('.top-bar');
            var $widthSidebar = $('#right-sidebar');
            if (!!$sticky.offset()) { // make sure ".sticky" element exists
                var generalSidebarHeight = $sticky.innerHeight();
                var stickyTop = $sticky.offset().top;
                var stickOffset = $header.height();
                var footerPosition = $footer.offset().top;
                var stopPoint = footerPosition - generalSidebarHeight - stickOffset;
                var diff = stopPoint + stickOffset;
                $(window).scroll(function(){ // scroll event
                    var windowTop = $(window).scrollTop(); // returns number
                    if (stopPoint < windowTop) {
                        $sticky.css({ position: 'absolute', top: diff });
                    } else if (stickyTop < windowTop + stickOffset) {
                        $sticky.css({ position: 'fixed', top: stickOffset, width:$widthSidebar.width() });
                    } else {
                        $sticky.css({position: 'absolute', top: 'initial'});
                    }
                });
            }

            $('#right-sidebar a[href^="#"]').on('click', function(e){
                e.preventDefault();
                $('html, body').animate({
                    scrollTop: $($.attr(this, 'href')).offset().top - $header.height()
                }, 500);
            })
        });

    </script>
@endpush
