<table class="table table-responsive" id="courses-table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Email</th>
            <th>Role</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr id="tr-{!! $user->id !!}">
            <td>{!! $user->id !!}</td>
            <td>{!! $user->name !!}</td>
            <?php
            if ($user->gender == 0) {
                $gender = 'Female';
            } elseif ($user->gender == 1) {
                $gender = 'Male';
            } elseif ($user->gender == 2) {
                $gender = 'Other';
            } else {
                $gender = '';
            }
            ?>
            <td>{!! $gender !!}</td>
            <td>{!!  !empty($user->email) ? $user->email : '' !!}</td>
            <td>{!!  !empty($user->role->name) ? $user->role->name : '' !!}</td>
            <td>
                @if ($user->active  == 1)
                    <button type="button" idpost="{!! $user->id !!}" data-name="{!! $user->name !!}" value="1"
                            id="status_{!! $user->id !!}" class="btn_status btn_status_success ">Active
                    </button>
                @else
                    <button type="button" idpost="{!! $user->id !!}" data-name="{!! $user->name !!}" value="0"
                            id="status_{!! $user->id !!}" class="btn_status btn_status_false ">Inactive
                    </button>
                @endif
            </td>
            <td>
                <div class='btn-1'>
                    @if(Helper::checkUserPermission('admin.admin.show'))
                        <a href="{!! route('admin.admin.show', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    @endif
                    @if(Helper::checkUserPermission('admin.admin.edit'))
                        <a href="{!! route('admin.admin.edit', [$user->id]) !!}" class='btn btn-default btn-xs '><i class="glyphicon glyphicon-edit"></i></a>
                    @endif
                    @if(Helper::checkUserPermission('admin.admin.itemActions'))
                        @if($user->active == 1)
                            <button type="button"
                                    class="btn btn-default btn-xs item_actions"
                                    data-link="data-link-item-actions"
                                    data-key="status"
                                    data-title="{!! $user->name !!}"
                                    data-text="Ban"
                                    data-val="0"
                                    data-id="{!! $user->id !!}"><i class="fa fa-ban"></i>
                            </button>
                        @else
                            <button type="button"
                                    class="btn btn-default btn-xs  item_actions"
                                    data-link="data-link-item-actions"
                                    data-key="status"
                                    data-title="{!! $user->name !!}"
                                    data-text="UnBan"
                                    data-val="1"
                                    data-id="{!! $user->id !!}"><i class="fa fa-play-circle"></i>
                            </button>
                        @endif
                    @endif
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="pagination-area">
    {!! $users->appends( $query_params )->links() !!}
</div>
