<div class="form-group col-sm-12 col-xs-12 col-md-12">
    <div class="control-label col-md-2 col-sm-2 col-xs-12">
        {!! Form::label('image', 'Image') !!}
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
        @php
            $classFileInput = 'fileinput-new';
            if(isset($model->photo) || !empty($oldPhoto)){
                $classFileInput = 'fileinput-exists';
            }
        @endphp

        <div class="fileinput {!! $classFileInput !!}"
             data-provides="fileinput">
            <div class="fileinput-preview fileinput-exists thumbnail img-thumb">
                @if(isset($model->photo) || !empty($oldPhoto))
                    @php
                        if(isset($model->photo)){
                            $src = url(config('filepath.user_image_show') . $model->photo);
                        }else{
                            $src = $oldPhoto;
                        }
                    @endphp
                    <img src="{{ $src }}">
                    <input type="hidden" name="image_tmp" value="{{ $src }}"/>
                @endif
            </div>

            <div>
                <span class="btn btn-default btn-file">
                    <span class="fileinput-new">Choose File</span>
                    <span class="fileinput-exists">Edit</span>
                    <input type="file" name="image" accept="image/*">
                </span>
                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Delete</a>
            </div>
        </div>
    </div>

    @if(empty($role_profile))
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="row">

                <div class="control-label col-md-2 col-sm-2 col-xs-12">
                    {!! Form::label('Role', 'Role') !!}
                    <span class="text-danger">*</span>
                </div>

                <div class="col-md-10 col-sm-10 col-xs-12">
                    {!!  Form::select('role_id', ['' => '-------- Select Role --------'] +  $select_role , (!empty($model)) ? $model->role_id : old('role_id') , ['class' => 'form-control']) !!}
                </div>

            </div>
        </div>
    @endif


</div>

{{ csrf_field() }}
@if(!empty($model))
    <input name="_method" type="hidden" value="PUT">
@endif

<!-- First Name Field -->
<div class="form-group col-sm-12 col-xs-12">
    <div class="control-label col-md-2 col-sm-2 col-xs-12">
        {!! Form::label('name', 'Fist Name', array()) !!}
        <span class="text-danger">*</span>
    </div>
    <div class="col-md-8 col-sm-8 col-xs-12">
        {!! Form::text('first_name', (!empty($model)) ? $model->first_name : old('first_name'), ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Last Name Field -->
<div class="form-group col-sm-12 col-xs-12">
    <div class="control-label col-md-2 col-sm-2 col-xs-12">
        {!! Form::label('name', 'Last Name', array()) !!}
        <span class="text-danger">*</span>
    </div>
    <div class="col-md-8 col-sm-8 col-xs-12">
        {!! Form::text('last_name', (!empty($model)) ? $model->last_name : old('last_name'), ['class' => 'form-control']) !!}
    </div>
</div>


{{--Date of birthday--}}
<div class="form-group col-sm-12 col-xs-12">
    <div class="control-label col-md-2 col-sm-2 col-xs-12">
        {!! Form::label('birthday', 'Date of birthday', array()) !!}
    </div>
    <div class="col-md-8 col-sm-8 col-xs-12">
        <div class="form-group">
            <div class='input-group date' id='datetimepicker'>
                <input name="birthday" type='text' class="form-control"
                       value="{!! (!empty($model) && $model->birthday) ? Helper::formatDate($model->birthday) : old('birthday') !!}"/>
                <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
            </div>
        </div>

    </div>
</div>


<!-- Gender Field -->
<div class="form-group col-sm-12 col-xs-12">
    <div class="control-label col-md-2 col-sm-2 col-xs-12">
        {!! Form::label('gender', 'Gender:') !!}
    </div>
    <div class="col-md-8 col-sm-8 col-xs-12">
        {!!  Form::select('gender', ['' => '-------- Select Gender --------'] + \App\Entities\User::genders(), (!empty($model)) ? $model->gender : old('gender') , ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Address Field -->
<div class="form-group col-sm-12 col-xs-12">
    <div class="control-label col-md-2 col-sm-2 col-xs-12">
        {!! Form::label('Address', 'Address:') !!}
    </div>
    <div class="col-md-8 col-sm-8 col-xs-12">
        {!! Form::textarea('address', (!empty($model)) ? $model->address : old('address'), ['class' => 'form-control','rows' => '4','style' => 'resize:none']) !!}
    </div>
</div>

<!-- Phone Field -->
<div class="form-group col-sm-12 col-xs-12">
    <div class="control-label col-md-2 col-sm-2 col-xs-12">
        {!! Form::label('phone', 'Phone:') !!}
    </div>
    <div class="col-md-8 col-sm-8 col-xs-12">
        {!! Form::number('phone', (!empty($model)) ? $model->phone : old('phone'), ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Email Field -->
<div class="form-group col-sm-12 col-xs-12">
    <div class="control-label col-md-2 col-sm-2 col-xs-12">
        {!! Form::label('email', 'Email', array()) !!}
        <span class="text-danger">*</span>
    </div>
    <div class="col-md-8 col-sm-8 col-xs-12">
        {!! Form::text('email', (!empty($model)) ? $model->email : old('email'), ['class' => 'form-control','id' => 'email-user', !empty($model->email)? 'readonly' : '' ]) !!}

    </div>
</div>


<!-- Description Field -->
<div class="form-group col-sm-12 col-xs-12">
    <div class="control-label col-md-2 col-sm-2 col-xs-12">
        {!! Form::label('Introduction', 'Introduction:') !!}
    </div>
    <div class="col-md-8 col-sm-8 col-xs-12">
        {!! Form::textarea('description', (!empty($model)) ? $model->description : old('description'), ['class' => 'form-control','rows' => '4','style' => 'resize:none']) !!}
    </div>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 col-xs-12 row text-center">

    @if(!empty($role_profile))
        <a href="{!! route('admin.admin.profiles', ['id' => Auth::id()]) !!}" class="btn btn-default">Cancel</a>
    @else
        <a href="{!! route('admin.admin.index') !!}" class="btn btn-default">Cancel</a>
    @endif

    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
</div>

<div class="clearfix"></div>

