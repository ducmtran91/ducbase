@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="x_panel">
    <div class="x_title">
        <h2>Edit Profile : "{{$model->name}}"</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
    	<div class="row">
		    <div class="col-md-12">
		        @include('adminlte-templates::common.errors')
		    </div>
		    {!! Form::open(['route' =>  ['admin.admin.postProfiles', $model->id], 'class' => 'form jsvalidation', 'novalidate' => 'novalidate', 'files' => true]) !!}
                @include('admin.admin.fields')
            {!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
