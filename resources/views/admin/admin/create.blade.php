@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="x_panel">
    <div class="x_title">
      <h2>Create Admin</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
    	<div class="row">
		    <div class="col-md-12">
		        @include('adminlte-templates::common.errors')
		    </div>
		    {!! Form::open(['route' => 'admin.admin.store', 'class' => 'form jsvalidation', 'novalidate' => 'novalidate', 'files' => true]) !!}
                @include('admin.admin.fields')
            {!! Form::close() !!}

		</div>
	</div>
</div>
@endsection
