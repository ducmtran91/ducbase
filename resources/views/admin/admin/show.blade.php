@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="x_panel">
            <section class="content-header">
                @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif

                <div class="message">
                    @include('flash::message')
                </div>

                <div class="wrap-detail-dealer">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="wrap-info-header">
                                <div class="wrap-image-dealers">
                                    <div id="crop-avatar" class="text-center"
                                         style="background: url('{!! (!empty($model->photo)) ? config('filepath.user_image_show') . $model->photo : config('filepath.no_image') !!}') no-repeat center center; background-size: cover;"></div>
                                </div>
                                <div class="wrap-content-dealers">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="item">
                                                <label>Full name</label>
                                                <p> {!! !empty($model->name) ? $model->name : '' !!}</p>
                                            </div>

                                            <div class="item">
                                                <label>
                                                    Date of birthday
                                                </label>
                                                <p>@if(!empty($model) && $model->birthday){!! Helper::formatDate($model->birthday) !!}@endif</p>
                                            </div>


                                            <div class="item">
                                                <label>
                                                    Gender
                                                </label>
                                                <p>
                                                    @if($model->gender == 1)
                                                        Male
                                                    @elseif($model->gender == 0)
                                                        Female
                                                    @else
                                                        Other
                                                    @endif
                                                </p>
                                            </div>

                                            <div class="item">
                                                <label>Address</label>
                                                <p>{!! !empty($model->address) ? $model->address : '' !!}</p>
                                            </div>

                                            <div class="item">
                                                <label>
                                                    Phone
                                                </label>
                                                <p>{!! !empty($model->phone) ? $model->phone : '' !!}</p>
                                            </div>

                                            <div class="item">
                                                <label>
                                                    Email
                                                </label>
                                                <p>{!! !empty($model->email) ? $model->email : '' !!}</p>
                                            </div>

                                            <div class="item">
                                                <label>
                                                    Introduction
                                                </label>
                                                <p>{!! !empty($model->description) ? $model->description : '' !!}</p>
                                            </div>

                                        </div>

                                        <div class="col-sm-3 text-right">
                                            <input id="data-token" value="{{ csrf_token()}}" hidden>
                                            @if($model->active)
                                                <button type="button" class="btn_status btn_status_success"
                                                        data-link="data-link-item-actions" data-key="status"
                                                        data-title="user" data-text="Inactive" data-val="0"
                                                        data-id="{!! $model->id !!}" title="Trạng thái">Active
                                                </button>
                                            @else
                                                <button type="button" class="btn_status btn_status_false"
                                                        data-link="data-link-item-actions" data-key="status"
                                                        data-title="user" data-text="Active" data-val="1"
                                                        data-id="{!! $model->id !!}" title="Trạng thái">Inactive
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="fixed-bottom">
                                        <form class="form-horizontal" role="form" method="POST"
                                              action="{!! url('sendpass/' ) !!}" style="float: right;">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-default pull-right btn-success"></i>
                                                Resend Password
                                            </button>
                                            <input type="text" value="{!! $model->email !!}" name="email" hidden>
                                            <input type="text" name="language" value="en" hidden>
                                            <input type="text"
                                                   value="{!! !empty($passwordReset) ? $passwordReset->token : '' !!}"
                                                   name="token" hidden>

                                        </form>

                                        @if(Helper::checkUserPermission('admin.admin.itemActions'))
                                            @if($model->id != Auth::id())
                                                @if ($model->active == 1)
                                                    <button type="button"
                                                            class='btn btn-default pull-right btn-danger item_actions'
                                                            data-link="data-link-item-actions" data-val="0"
                                                            data-key="status" data-text="ban"
                                                            data-id="{!! $model->id !!}"
                                                            data-title="{!! $model->name !!}">
                                                        Ban
                                                    </button>

                                                @else
                                                    <button type="button"
                                                            class='btn btn-default pull-right btn-success item_actions'
                                                            data-link="data-link-item-actions" data-val="1"
                                                            data-key="status" data-text="unban"
                                                            data-id="{!! $model->id !!}"
                                                            data-title="{!! $model->name !!}">
                                                        Unban
                                                    </button>
                                                @endif

                                            @endif
                                        @endif
                                        @if(Helper::checkUserPermission('admin.admin.edit'))
                                            <a href="{!! route('admin.admin.edit', [$model->id]) !!}">
                                                <button type="button" class="btn btn-default pull-right"><i
                                                            class="glyphicon glyphicon-edit"></i>&nbsp;Edit info
                                                </button>
                                            </a>
                                        @endif


                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <input id="data-link-item-actions"
                                       value="{{ URL::action('AdminController@itemActions') }}" hidden>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div>
@endsection
