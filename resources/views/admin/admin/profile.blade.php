@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="x_panel">
            <section class="content-header">
                @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif

                <div class="message">
                    @include('flash::message')
                </div>

                <div class="wrap-detail-dealer">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="wrap-info-header">
                                <div class="wrap-image-dealers">
                                    <div id="crop-avatar" class="text-center"
                                         style="background: url('{!! (!empty($model->photo)) ? config('filepath.user_image_show') . $model->photo : config('filepath.no_image') !!}') no-repeat center center; background-size: cover;"></div>
                                </div>
                                <div class="wrap-content-dealers">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="item">
                                                <label>Full name</label>
                                                <p> {!! !empty($model->name) ? $model->name : '' !!}</p>
                                            </div>

                                            <div class="item">
                                                <label>
                                                    Date of birthday
                                                </label>
                                                <p>@if(!empty($model) && $model->birthday){!! Helper::formatDate($model->birthday) !!}@endif</p>
                                            </div>


                                        @if(!empty($model) && $model->gender)
                                                <div class="item">
                                                    <label>
                                                        Gender
                                                    </label>
                                                    <p>
                                                        @if($model->gender == 1)
                                                            Male
                                                        @elseif($model->gender==0)
                                                            Female
                                                        @else
                                                            Other
                                                        @endif
                                                    </p>
                                                </div>
                                            @endif

                                            <div class="item">
                                                <label>Address</label>
                                                <p>{!! !empty($model->address) ? $model->address : '' !!}</p>
                                            </div>

                                            <div class="item">
                                                <label>
                                                    Phone
                                                </label>
                                                <p>{!! !empty($model->phone) ? $model->phone : '' !!}</p>
                                            </div>

                                            <div class="item">
                                                <label>
                                                    Email
                                                </label>
                                                <p>{!! !empty($model->email) ? $model->email : '' !!}</p>
                                            </div>

                                            <div class="item">
                                                <label>
                                                    Introduction
                                                </label>
                                                <p>{!! !empty($model->description) ? $model->description : '' !!}</p>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="fixed-bottom">
                                            <a href="{!! route('admin.admin.getProfiles', [$model->id]) !!}">
                                                <button type="button" class="btn btn-success pull-right"><i
                                                            class="glyphicon glyphicon-edit"></i>&nbsp;Edit Profile
                                                </button>
                                            </a>

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <input id="data-link-item-actions"
                                       value="{{ URL::action('\App\Http\Controllers\Backend\AdminController@itemActions') }}" hidden>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div>
@endsection
