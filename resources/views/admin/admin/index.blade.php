@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="x_panel">
            <div class="x_title col-xs-12">
                <div class="col-xs-12 col-sm-6 col-md-8 pd-left-0">
                    <h2>
                         Admin List
                    </h2>
                </div>
                <div class="clearfix"></div>
                <div class="message">
                    @include('flash::message')
                </div>
            </div>

            <input id="data-token" value="{{ csrf_token()}}" hidden><!-- viewing -->
            <input id="data-link-item-actions" value="{{ route('admin.admin.itemActions') }}" hidden>

            <form action="" method="get" id="form_custom_list">
                <div class="div_option">
                    <div class="col-xs-2 col-md-2 col-sm-2">
                        <ul class="nav navbar-left panel_toolbox">
                            <li class="viewing">Show</li>
                            <li class="dropdown">
                                <select class="form-control" name="limit" id="limit_pagination">
                                    <option @if ($limit == 10)selected="selected" @endif value="10">10</option>
                                    <option @if ($limit == 25)selected="selected" @endif value="25">25</option>
                                    <option @if ($limit == 50)selected="selected" @endif value="50">50</option>
                                    <option @if ($limit == 75)selected="selected" @endif value="75">75</option>
                                    <option @if ($limit == 100)selected="selected" @endif value="100">100</option>
                                </select>
                            </li>
                        </ul>
                    </div>

                    <div class="col-xs-10 col-md-10 col-sm-10 count_record">
                        <ul class="nav navbar-right panel_toolbox">
                            @if(Helper::checkUserPermission('admin.admin.create'))
                                <li><a href="{!! route('admin.admin.create') !!}"
                                       class="btn-toolbox success"><i class="fa fa-plus"></i> Create Admin</a>
                                </li>
                            @endif
                        </ul>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </form>
            <div class="x_content">
                @include('admin.admin.table')
            </div>
        </div>
    </div>
@endsection