@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h2>Edit Administrator</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-md-12">
                    @include('adminlte-templates::common.errors')
                </div>
                
                {!! Form::model($users, ['route' => ['admin.users.update', $users->id], 'method' => 'patch', 'class' => 'form jsvalidation', 'novalidate' => 'novalidate', 'files' => true]) !!}		    		    
                    @include('admin.users.fields')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection