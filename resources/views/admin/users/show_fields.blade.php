@php
    $addPermission = Helper::checkUserPermission('admin.users.create');
    $editPermission = Helper::checkUserPermission('admin.users.edit');    
@endphp

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Account information</h2>
                <ul class="nav navbar-right panel_toolbox">                    
                    @if($editPermission)
                        <li>
                            <a href="{!! URL::action('Backend\UsersController@edit', ['users' => $users->id]) !!}" class="btn btn-default">
                                <i class="fa fa-edit"></i>
                                Edit account
                            </a>
                        </li>
                    @endif
                    
                    @if($addPermission)
                        <li>
                            <a href="{!! URL::action('Backend\UsersController@create') !!}" class="btn btn-default">
                                <i class="fa fa-plus"></i>
                                Create new admin account
                            </a>
                        </li>
                    @endif                    
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="profile_img text-center">
                            <div id="crop-avatar">
                                <!-- Current avatar -->
                                <img class="img-responsive avatar-view" src="{!! (!empty($users->avatar) ? (config('filepath.user_image_show') . $users->avatar) : (config('filepath.sample_image_show') . 'default-user.png')) !!}" alt="Avatar" title="Change the avatar">
                            </div>
                        </div>                   
                    </div>    
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <h3>{!! $users->name or '' !!}</h3>

                        <p>Email: {!! $users->email or '' !!}</p>
                        <label class="checkbox-inline checkbox-status">
                            Status: <input readonly="true" type="checkbox" class="js-switch" {!! !empty($users->status) ? 'checked' : '' !!}/>                       
                        </label>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>