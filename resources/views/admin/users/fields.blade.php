<!-- Image Field -->
<div class="form-group row col-sm-12">
    <div class="control-label col-md-2 col-sm-2 col-xs-12">
        {!! Form::label('image', 'Avatar') !!}
    </div>
    <div class="col-sm-8 col-xs-10">
        @php
            $classFileInput = 'fileinput-new';
            
            if(isset($users->avatar) || !empty($oldAvatar)){
                $classFileInput = 'fileinput-exists';
            }
        @endphp
        
        <div class="fileinput {!! $classFileInput !!}" data-provides="fileinput">
            <div class="fileinput-preview fileinput-exists thumbnail img-thumb">                
                @if(isset($users->avatar) || !empty($oldAvatar))
                    @if(!empty($oldAvatar))
                        @php
                            $src = $oldAvatar;
                            $showSrc = $src;
                        @endphp      
                    @else 
                        @php
                            $src = $users->avatar;
                            $showSrc = config('filepath.user_image_show') . $src;
                        @endphp      
                    @endif
                    
                    <img src="{{ $showSrc }}">
                    <input type="hidden" name="image_tmp" value="{{ $src }}"/>
                @endif
            </div>
            <div>
                <span class="btn btn-default btn-file">
                    <span class="fileinput-new">Select image</span>
                    <span class="fileinput-exists">Change</span>
                    <input type="file" name="image" accept="image/*">
                </span>
                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
            </div>
        </div>
    </div>
</div>

<!-- Role Field -->
<div class="form-group row col-sm-12">
    <div class="control-label col-md-2 col-sm-2 col-xs-12">
        {!! Form::label('role_id', 'Role') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8 col-xs-10">
        @if(old('role_id', null) !== null)
            <input type="hidden" class="js-select-multiple-edit" value="{!! old('role_id') !!}"/>
        @elseif(!empty($users->role_id))
            <input type="hidden" class="js-select-multiple-edit" value="{!! $users->role_id !!}"/>
        @endif

        <select name="role_id" class="form-control js-select-multiple">
            <option value="">--- Choose ---</option>
            
            @if(!empty($roles))
                @foreach($roles as $role)
                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>

<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('first_name', 'First Name') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8">
        {!! Form::text('first_name', null, ['class' => 'form-control', 'maxlength' => 255, 'placeholder' => 'First Name']) !!}
    </div>
</div>

<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('last_name', 'Last Name') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8">
        {!! Form::text('last_name', null, ['class' => 'form-control', 'maxlength' => 255, 'placeholder' => 'Last Name']) !!}
    </div>
</div>

@if(isset($users->password))
    <div class="form-group row col-sm-12">
        <div class="form-group col-sm-2">
            {!! Form::label('name', 'Reset password') !!}
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="form-group">                
                <a href="#" class="btn btn-default btn-change-password mg-top-0 mg-bottom-0">Reset password</a>

                <div class="wrap-change-password {{ old('status_change_password') == 'change' ? 'active' : '' }}">
                    <input type="hidden"
                           value="{{ old('status_change_password') == 'nochange' || old('status_change_password') == '' ? 'nochange' : 'change' }}"
                           name="status_change_password" 
                           id="type-change-password">
                    <div class="row mg-top-20">
                        <div class="form-group">
                            <div class="control-label col-md-3 col-sm-3 col-xs-12">
                                {!! Form::label('name', 'New password') !!}
                                <span class="text-danger">*</span>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <div class="input-group">
                                    <input class="form-control" name="new_password" type="password" id="new_password" maxlength="255" placeholder="New password">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-show-password">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-label col-md-3 col-sm-3 col-xs-12">
                                {!! Form::label('name', 'Confirm password') !!}
                                <span class="text-danger">*</span>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <div class="input-group">
                                    <input class="form-control" name="confirm_new_password" type="password" id="confirm_new_password" maxlength="255" placeholder="Confirm password">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-show-password">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('name', 'Email') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8">
        {!! Form::text('email', null, ['class' => 'form-control', 'maxlength' => 255, 'placeholder' => 'Email']) !!}
    </div>
</div>

<!-- Status Field -->
<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('status', 'Status') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8 col-xs-10">
        <label class="checkbox-inline checkbox-status">
            {!! Form::hidden('status', false) !!}
            {!! Form::checkbox('status', 1, null, ['class' => 'js-switch']) !!}
        </label>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group row col-sm-12 text-center">
    <a href="{!! route('admin.users.index') !!}" class="btn btn-default">Cancel</a>
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
</div>
