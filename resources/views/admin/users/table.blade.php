@php
    $showPermission = Helper::checkUserPermission('admin.users.show');
    $editPermission = Helper::checkUserPermission('admin.users.edit');    
    $itemActionsPermission = Helper::checkUserPermission('admin.users.itemActions');
@endphp

<table class="table table-responsive" id="users-table">
    <thead>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>    
        <th>Status</th>
        <th>Action</th>
        <th class="text_right">
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="" id="select_all">
                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                </label>
            </div>
        </th>
    </thead>
    <tbody>
        @foreach($users as $key => $user)
        <tr>
            <td>
                @if($showPermission)
                    <a href="{!! URL::action('Backend\UsersController@show', ['users' => $user->id]) !!}" class="link">
                        <div class="text-more">
                            {!! $user->first_name or ''  !!}
                        </div>                    
                    </a>
                @else                    
                    <div class="text-more">
                        {!! $user->first_name or ''  !!}
                    </div>                                        
                @endif                     
            </td>
            <td>
                @if($showPermission)
                    <a href="{!! URL::action('Backend\UsersController@show', ['users' => $user->id]) !!}" class="link">
                        <div class="text-more">
                            {!! $user->last_name or ''  !!}
                        </div>                   
                    </a>
                @else                    
                    <div class="text-more">
                        {!! $user->last_name or ''  !!}
                    </div>                                         
                @endif    
            </td>
            <td>
                <div class="text-more">
                    {!! $user->email or ''  !!}
                </div>
            </td>           
            <td>
                @if($user->status)
                    <button {!! !$itemActionsPermission ? 'disabled="disabled"' : '' !!} type="button" class="btn_status btn_status_success item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="user" data-text="inactive" data-val="0"
                            data-id="{!! $user->id !!}">Active
                    </button>
                @else
                    <button {!! !$itemActionsPermission ? 'disabled="disabled"' : '' !!} type="button" class="btn_status btn_status_false item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="user" data-text="active" data-val="1"
                            data-id="{!! $user->id !!}">Inactive
                    </button>
                @endif
            </td>                     
            <td>
                <div class='btn-group'>
                    @if($showPermission)
                        <a href="{!! URL::action('Backend\UsersController@show', ['users' => $user->id]) !!}"
                            class='btn btn-default btn-xs'>
                            <i class="glyphicon glyphicon-eye-open"></i>
                        </a>
                    @endif                        
                    
                    @if($editPermission)
                        <a href="{!! URL::action('Backend\UsersController@edit', ['users' => $user->id]) !!}"
                            class='btn btn-default btn-xs btn-edit'>
                             <i class="glyphicon glyphicon-edit"></i>
                        </a> 
                    @endif                                                               
                       
                    @if($itemActionsPermission)
                        <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-edit item_actions"
                            data-link="data-link-item-actions"
                            data-key="delete" data-title="user" data-text="delete" data-val="none"
                            data-id="{!! $user->id !!}">
                             <i class="glyphicon glyphicon-trash"></i>
                         </a>
                    @endif                        
                </div>
            </td>
            <td colspan="3" class="text_right">
                <div class="checkbox">
                    <label>
                        <input class="checkbox_item" type="checkbox" name="checkbox" value="{{$user->id}}">
                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                    </label>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<div class="pagination-area">
    {!! $users->appends(app('request')->all())->links() !!}
</div>
