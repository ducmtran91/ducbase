@extends('layouts.admin')

@section('content')
<div class="content">
    <div class="box-primary">
        <div class="box-body">
            <div class="row col-sm-12">
                @include('admin.users.show_fields')               
            </div>
        </div>
    </div>
</div>
@endsection