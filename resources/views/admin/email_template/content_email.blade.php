<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      style="font-family: 'Roboto Mono', monospace !important; box-sizing: border-box; font-size: 14px; margin: 0;">
<head>
    <meta name="viewport" content="width=device-width"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>{!! $name !!}</title>
    <style type="text/css">
        img {
            max-width: 100%;
        }

        body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100% !important;
            height: 100%;
            line-height: 1.6;
        }
        @font-face {
            /*font-family: 'Roboto Mono', monospace;*/
            /*font-style: normal;*/
            /*font-weight: 400;*/
            /*src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');*/
        }

        @media only screen and (max-width: 640px) {
            h1 {
                font-weight: 600 !important;
                margin: 20px 0 5px !important;
            }

            h2 {
                font-weight: 600 !important;
                margin: 20px 0 5px !important;
            }

            h3 {
                font-weight: 600 !important;
                margin: 20px 0 5px !important;
            }

            h4 {
                font-weight: 600 !important;
                margin: 20px 0 5px !important;
            }

            h1 {
                font-size: 22px !important;
            }

            h2 {
                font-size: 18px !important;
            }

            h3 {
                font-size: 16px !important;
            }

            .container {
                width: 100% !important;
            }

            .content {
                padding: 10px !important;
            }

            .content-wrap {
                padding: 10px !important;
            }

            .invoice {
                width: 100% !important;
            }
        }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Libre+Barcode+39+Text|Roboto+Mono" rel="stylesheet">
</head>

<body itemscope itemtype="http://schema.org/EmailMessage"
      style="font-family: 'Roboto Mono', monospace !important;box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6; background: #f6f6f6; margin: 0;">

<table class="body-wrap"
       style=" box-sizing: border-box; font-size: 14px; width: 100%; background: #f6f6f6; margin: 0;">
    <tr style=" box-sizing: border-box; font-size: 14px; margin: 0;">
        <td style="box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;"
            valign="top"></td>
        <td class="container"
            style=" box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important;  clear: both !important; margin: 0 auto;"
            valign="top">
            <div class="content"
                 style="font-family: 'Roboto Mono', monospace !important; box-sizing: border-box; font-size: 14px; max-width: 705px; display: block; margin: 0 auto; padding: 20px;">
                <table class="main" width="100%" cellpadding="0" cellspacing="0"
                       style="font-family: 'Roboto Mono', monospace !important; box-sizing: border-box; font-size: 14px; border-radius: 3px; background: #fff; margin: 0; border: 1px solid #e9e9e9;">
                    <tr style="font-family: 'Roboto Mono', monospace !important; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="alert alert-warning"
                            style="font-family: 'Roboto Mono', monospace !important; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0;     background-image: linear-gradient(-42deg, #003db1 0%, #34b7fc 100%); margin: 0; padding: 15px;"
                            align="center" valign="top">
                            <div style="float: left;width: 25%">
                            </div>
                            <div style="font-family: 'Roboto Mono', monospace !important;float: left;width: 75%;padding-top: 15px;    text-transform: uppercase;">
                                {{ $name }}
                            </div>

                        </td>
                    </tr>
                    <tr style="font-family: 'Roboto Mono', monospace !important; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-wrap"
                            style="font-family: 'Roboto Mono', monospace !important; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;"
                            valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0"
                                   style="font-family: 'Roboto Mono', monospace !important; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <tr style="font-family: 'Roboto Mono', monospace !important; box-sizing: border-box; font-size: 14px; margin: 0;">
                                    <td class="content-block"
                                        style="font-family: 'Roboto Mono', monospace !important; box-sizing: border-box; font-size: 13px; vertical-align: top; margin: 0; padding: 0 0 13px;"
                                        valign="top">
                                        {{ $content }}
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                </table>
            </div>
        </td>
        <td style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;"
            valign="top"></td>
    </tr>
</table>

</body>
</html>
