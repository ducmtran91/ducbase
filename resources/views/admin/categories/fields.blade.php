@php
    $langs = config('translatable.locales');
@endphp
<div class="form-group row col-sm-12">
    <div class="" role="tabpanel" data-example-id="togglable-tabs">
        @include('admin.generates.tab-languages', ['languages' => $langs])
        <div id="myTabContent" class="tab-content">
            @foreach($langs as $key => $locale)
                <div role="tabpanel" class="tab-pane fade @if($key == 0) active in @endif" id="lang-{{ $key }}" aria-labelledby="lang-tab-{{ $key }}">
                    <div class="full-width left">
                        @include('layouts.inputs.text', [
                            'colLabel' => 'col-sm-2',
                            'colInput' => 'col-sm-8',
                            'label' => trans('backend.label.name'),
                            'id' => $locale.'[name]',
                            'value' => !empty($data) ? $data->translate($locale)->name : null
                        ]);
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

<div class="form-group row col-sm-12">
    @include('layouts.inputs.text', [
        'colLabel' => 'col-sm-2',
        'colInput' => 'col-sm-8',
        'label' => trans('backend.label.link'),
        'id' => 'slug',
        'value' => !empty($data) ? $data->slug : null
    ]);
</div>
<div class="form-group row col-sm-12">
    <div class="control-label col-md-2 col-sm-2 col-xs-12">
        {!! Form::label('parent_id', trans('backend.label.category_parent')) !!}
    </div>
    <div class="form-group col-sm-8 col-xs-10">
        @if(old('parent_id', null) !== null)
            <input type="hidden" class="js-select-multiple-edit" value="{!! old('parent_id') !!}"/>
        @elseif(!empty($data->parent_id))
            <input type="hidden" class="js-select-multiple-edit" value="{!! $data->parent_id !!}"/>
        @endif

        <select name="parent_id" class="form-control js-select-multiple">
            <option value="">--- {{ trans('backend.label.choose') }} ---</option>

            @if(!empty($categories))
                @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>

<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('position', trans('backend.label.position')) !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8">
        {!! Form::number('position', null, ['class' => 'form-control', 'maxlength' => 255, 'placeholder' => trans('backend.label.position')]) !!}
    </div>
</div>

<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('status', trans('backend.label.status')) !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8 col-xs-10">
        <label class="checkbox-inline checkbox-status">
            {!! Form::hidden('status', false) !!}
            {!! Form::checkbox('status', 1, null, ['class' => 'js-switch']) !!}
        </label>
    </div>
</div>

<div class="form-group row col-sm-12 text-center">
    <a href="{!! route('admin.categories.index') !!}" class="btn btn-default">{{ trans('backend.label.actions.cancel') }}</a>
    {!! Form::submit(trans('backend.label.actions.save'), ['class' => 'btn btn-success']) !!}
</div>
