@extends('layouts.admin')

@section('content')
    @php
        $controller = "Backend\CategoriesController@";
        $menu = trans("backend.menu.categories");
        $route = trans("backend.route.categories");
        $status = trans("backend.label.status");
        $delete = trans("backend.label.actions.delete");
        $params = [
            "menu" => $menu,
            "route" => $route,
            "status" => $status,
            "delete" => $delete,
            "controller" => $controller
        ];
    @endphp
    <div class="row">
        <div class="x_panel">
            <div class="x_title col-xs-12">
                <div class="col-xs-4 pd-left-0">
                    <h2>{{ trans('backend.label.management.'.$route.'.index') }}</h2>
                </div>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"></div>
                <div class="clearfix"></div>
                <div class="message">
                    @include('flash::message')
                </div>
            </div>

            <input id="data-token" value="{{ csrf_token()}}" hidden>
            <input id="data-link-bulk-actions" value="{{ URL::action($controller.'bulkActions') }}" hidden>
            <input id="data-link-item-actions" value="{{ URL::action($controller.'itemActions') }}" hidden>
            
            <div class="x_content">              
                <div class="tab-content mg-top-15">
                    @include('admin.'.$route.'.partials.filter', $params)
                    @include('admin.'.$route.'.table', $params)
                </div><!-- /.Tab panes -->
            </div>
        </div>
    </div>
@endsection