@extends('layouts.admin')

@section('content')
<div class="content">
    @php
        $controller = "Backend\CategoriesController@";
        $route = trans("backend.route.categories");
        $params = [
            "route" => $route,
            "controller" => $controller
        ];
    @endphp
    <div class="box-primary">
        <div class="box-body">
            <div class="row col-sm-12">
                @include('admin.'.$route.'.show_fields')
            </div>
        </div>
    </div>
</div>
@endsection