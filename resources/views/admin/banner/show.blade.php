@extends('layouts.admin')

@section('content')
@php
    $controller = "Backend\BannerController@";
    $route = trans("backend.route.banner");
    $params = [
        "route" => $route,
        "controller" => $controller
    ];
@endphp
<div class="content">
    <div class="box-primary">
        <div class="box-body">
            <div class="row col-sm-12">
                @include('admin.'.$route.'.show_fields', $params)
            </div>
        </div>
    </div>
</div>
@endsection