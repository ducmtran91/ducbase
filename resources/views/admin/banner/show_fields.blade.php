@php
    $addPermission = Helper::checkUserPermission('admin.'.$route.'.create');
    $editPermission = Helper::checkUserPermission('admin.'.$route.'.edit');
@endphp

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>{{ trans('backend.label.management.'.$route.'.show') }}</h2>
                <ul class="nav navbar-right panel_toolbox">                    
                    @if($editPermission)
                        <li>
                            <a href="{!! URL::action($controller.'edit', [$route => $data->id]) !!}" class="btn btn-default">
                                <i class="fa fa-edit"></i>
                                {{ trans('backend.label.management.'.$route.'.edit') }}
                            </a>
                        </li>
                    @endif
                    
                    @if($addPermission)
                        <li>
                            <a href="{!! URL::action($controller.'create') !!}" class="btn btn-default">
                                <i class="fa fa-plus"></i>
                                {{ trans('backend.label.management.'.$route.'.create') }}
                            </a>
                        </li>
                    @endif                    
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-10 col-sm-10 col-xs-12">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="row mg-top-20 mg-bottom-20">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label>{{ trans('backend.label.image') }}</label>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <img class="img-file-upload" src="{!! $data->image ? url(config('filepath.slider_image') . $data->image) : url(config('filepath.no_image')) !!}"/>
                                </div>
                            </div>

                            <div class="row mg-top-20 mg-bottom-20">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label>{{ trans('backend.label.title') }}</label>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    {!! $data->name or '' !!}
                                </div>
                            </div>

                            <div class="row mg-top-20 mg-bottom-20">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label>{{ trans('backend.label.link') }}</label>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    {!! $data->slug or '' !!}
                                </div>
                            </div>

                            <div class="row mg-top-20 mg-bottom-20">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label>{{ trans('backend.label.position') }}</label>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    {{$data->position}}
                                </div>
                            </div>

                            <div class="row mg-top-20 mg-bottom-20">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label>{{ trans('backend.label.status') }}</label>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <label class="checkbox-inline checkbox-status">
                                        <input readonly="true" type="checkbox" class="js-switch" {!! !empty($data->status) ? 'checked' : '' !!}/>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>