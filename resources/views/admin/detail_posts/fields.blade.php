<div class="form-group row col-sm-12">
    <div class="control-label col-md-2 col-sm-2 col-xs-12">
        {!! Form::label('post_id', 'Posts') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8 col-xs-10">
        @if(old('post_id', null) !== null)
            <input type="hidden" class="js-select-multiple-edit" value="{!! old('post_id') !!}"/>
        @elseif(!empty($data[0]->post_id))
            <input type="hidden" class="js-select-multiple-edit" value="{!! $data[0]->post_id !!}"/>
        @endif

        <select name="post_id" class="form-control js-select-multiple">
            <option value="">--- Choose ---</option>
            @if(!empty($posts))
                @foreach($posts as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="x_panel">
    <div class="x_title">
        <h4>Detail Posts</h4>
        <a href="javascript:addDetail()" class="btn btn-default">Add Detail Posts</a>
    </div>
    <div class="x_content" id="template_detail">
        @if(!empty($data))
            @each('admin.detail_posts.partials.tem_detail', $data, 'item')
        @else
            @include('admin.detail_posts.partials.tem_detail')
        @endif
    </div>
</div>
<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('status', 'Status') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8 col-xs-10">
        <label class="checkbox-inline checkbox-status">
            {!! Form::hidden('status', false) !!}
            {!! Form::checkbox('status', 1, !empty($data[0]->status) ? true : false, ['class' => 'js-switch']) !!}
        </label>
    </div>
</div>

<div class="form-group row col-sm-12 text-center">
    <a href="{!! route('admin.detailPosts.index') !!}" class="btn btn-default">Cancel</a>
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        function TemplatePost(link) {
            this.link = link;
            this.loading = function() {
                $('body').loading('toggle');
            };
            this.loadEditor = function() {
                let number = this.temp.find('[init="ckeditor"]').length;
                let self = $('[init="ckeditor"]').last().attr('name','description['+(number - 1)+']');
                if (typeof $(self).attr('name') !== 'undefined') {
                    CKEDITOR.plugins.addExternal('youtube', '/../../assets/ckeditor-youtube-plugin/youtube/');
                    CKEDITOR.replace($(self).attr('name'), {
                        language: 'en',
                        extraPlugins:'youtube',
                        filebrowserBrowseUrl: '/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
                        filebrowserImageBrowseUrl: '/filemanager/dialog.php?type=1&editor=ckeditor&fldr='
                    });
                } else {
                    $(self).ckeditor();
                }
            };
            this.temp = $('#template_detail');
        }

        TemplatePost.prototype.get = function() {
            let idTem = this.temp;
            this.loading();
            let that = this;
            $.ajax({
                method: "GET",
                url: this.link,
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                success: function(){
                    that.loading();
                }
            }).success(function(data) {
                idTem.append(data.response);
                that.loadEditor();
            });
        };

        var templatePost = new TemplatePost("{!! route('admin.detailPosts.getTemplate') !!}");

        function addDetail(){
            templatePost.get();
        }
    </script>
@endpush