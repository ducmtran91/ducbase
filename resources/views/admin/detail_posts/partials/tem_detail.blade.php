<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('name', 'Name') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-10">
        {!! Form::text('name[]', !empty($item) ? $item->name : null, ['class' => 'form-control', 'maxlength' => 255, 'placeholder' => 'Name']) !!}
    </div>
</div>

<div class="form-group row col-sm-12 col-xs-12">
    <div class="form-group col-sm-2">
        {!! Form::label('description', 'Description') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-10">
        @php
            $k = 0;
            if(!empty($key)) {
                $k = $key;
            }
        @endphp
        {!! Form::textarea("description[{$k}]", !empty($item) ? $item->description : null, ['class' => 'form-control', 'init' => 'ckeditor']) !!}
    </div>
</div>

<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('position', 'Position') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-10">
        {!! Form::number('position[]', !empty($item) ? $item->position : null, ['class' => 'form-control', 'maxlength' => 255, 'placeholder' => 'Position']) !!}
    </div>
</div>