@php
    $showPermission = Helper::checkUserPermission('admin.detailPosts.show');
    $editPermission = Helper::checkUserPermission('admin.detailPosts.edit');
    $itemActionsPermission = Helper::checkUserPermission('admin.detailPosts.itemActions');
@endphp

<table class="table table-responsive" id="posts-table">
    <thead>
    <th>ID</th>
    <th>Name</th>
    <th>Parent Posts</th>
    <th>Position</th>
    <th>Status</th>
    <th>Action</th>
    <th class="text_right">
        <div class="checkbox">
            <label>
                <input type="checkbox" value="" id="select_all">
                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            </label>
        </div>
    </th>
    </thead>
    <tbody>
    @foreach($datas as $key => $item)
        <tr>
            <td>
                <div class="text-more">
                    {!! $item->id or ''  !!}
                </div>
            </td>
            <td>
                @if($showPermission)
                    <a href="{!! URL::action('Backend\DetailPostsController@show', ['posts' => $item->id]) !!}"
                       class="link">
                        <div class="text-more">
                            {!! $item->name or ''  !!}
                        </div>
                    </a>
                @else
                    <div class="text-more">
                        {!! $item->name or ''  !!}
                    </div>
                @endif
            </td>
            <td>
                @if(Helper::checkUserPermission('admin.posts.show'))
                    <a href="{!! URL::action('Backend\PostsController@show', ['posts' => $item->post_id]) !!}"
                       class="link">
                        <div class="text-more">
                            {!! !empty($item->posts) ? $item->posts->name : '' !!}
                        </div>
                    </a>
                @else
                    <div class="text-more">
                        {!! !empty($item->posts) ? $item->posts->name : '' !!}
                    </div>
                @endif
            </td>
            <td>
                <div class="text-more">
                    {!! $item->position or ''  !!}
                </div>
            </td>
            <td>
                @if($item->status)
                    <button {!! !$itemActionsPermission ? 'disabled="disabled"' : '' !!} type="button"
                            class="btn_status btn_status_success item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="category" data-text="inactive" data-val="0"
                            data-id="{!! $item->id !!}">Active
                    </button>
                @else
                    <button {!! !$itemActionsPermission ? 'disabled="disabled"' : '' !!} type="button"
                            class="btn_status btn_status_false item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="category" data-text="active" data-val="1"
                            data-id="{!! $item->id !!}">Inactive
                    </button>
                @endif
            </td>
            <td>
                <div class='btn-group'>
                    @if($showPermission)
                        <a href="{!! URL::action('Backend\DetailPostsController@show', ['detailPosts' => $item->post_id]) !!}"
                           class='btn btn-default btn-xs'>
                            <i class="glyphicon glyphicon-eye-open"></i>
                        </a>
                    @endif

                    @if($editPermission)
                        <a href="{!! URL::action('Backend\DetailPostsController@edit', ['detailPosts' => $item->post_id]) !!}"
                           class='btn btn-default btn-xs btn-edit'>
                            <i class="glyphicon glyphicon-edit"></i>
                        </a>
                    @endif

                    @if($itemActionsPermission)
                        <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-edit item_actions"
                           data-link="data-link-item-actions"
                           data-key="delete" data-title="detail post" data-text="delete" data-val="none"
                           data-id="{!! $item->id !!}">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    @endif
                </div>
            </td>
            <td colspan="3" class="text_right">
                <div class="checkbox">
                    <label>
                        <input class="checkbox_item" type="checkbox" name="checkbox" value="{{$item->id}}">
                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                    </label>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div class="pagination-area">
    {!! $datas->appends(app('request')->all())->links() !!}
</div>
