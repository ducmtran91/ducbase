@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="x_panel">
            <div class="x_title col-xs-12">
                <div class="col-xs-4 pd-left-0">
                    <h2>Posts Management</h2>
                </div>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"></div>
                <div class="clearfix"></div>
                <div class="message">
                    @include('flash::message')
                </div>
            </div>

            <input id="data-token" value="{{ csrf_token()}}" hidden>
            <input id="data-link-bulk-actions" value="{{ URL::action('Backend\PostsController@bulkActions') }}" hidden>
            <input id="data-link-item-actions" value="{{ URL::action('Backend\PostsController@itemActions') }}" hidden>
            
            <div class="x_content">              
                <div class="tab-content mg-top-15">
                    @include('admin.posts.partials.filter')
                    @include('admin.posts.table')
                </div><!-- /.Tab panes -->
            </div>
        </div>
    </div>
@endsection