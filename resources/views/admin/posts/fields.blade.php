<div class="form-group row col-sm-12">
    <div class="control-label col-md-2 col-sm-2 col-xs-12">
        {!! Form::label('parent_id', 'Category') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8 col-xs-10">
        @if(old('category_id', null) !== null)
            <input type="hidden" class="js-select-multiple-edit" value="{!! old('category_id') !!}"/>
        @elseif(!empty($data->category_id))
            <input type="hidden" class="js-select-multiple-edit" value="{!! $data->category_id !!}"/>
        @endif

        <select name="category_id" class="form-control js-select-multiple">
            <option value="">--- Choose ---</option>
            @if(!empty($categories))
                @foreach($categories as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>

<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('name', 'Name') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control', 'maxlength' => 255, 'placeholder' => 'Name']) !!}
    </div>
</div>


<div class="form-group row col-sm-12 col-xs-12">
    <div class="form-group col-sm-2">
        {!! Form::label('excerpt', 'Excerpt') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8">
        {!! Form::textarea('excerpt', null, ['class' => 'form-control','rows' => '4','style' => 'resize:none']) !!}
    </div>
</div>

<div class="form-group row col-sm-12 col-xs-12">
    <div class="form-group col-sm-2">
        {!! Form::label('description', 'Description') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8">
        {!! Form::textarea('description', null, ['id' => 'description', 'class' => 'form-control', 'init' => 'ckeditor']) !!}
    </div>
</div>

<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('position', 'Position') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8">
        {!! Form::number('position', null, ['class' => 'form-control', 'maxlength' => 255, 'placeholder' => 'Position']) !!}
    </div>
</div>
@php
    $tagData = '';
    if(!empty($data->tags)) {
        foreach($data->tags as $tag) {
            $tagData.= $tag->name.',';
        }
    }
@endphp
<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('tags_id', 'Tags') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8 col-xs-10">
        {!! Form::text('tags_id', $tagData ? trim($tagData) : null, ['class' => 'form-control', 'placeholder' => 'Tags']) !!}
        <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
    </div>
</div>

<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('status', 'Status') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8 col-xs-10">
        <label class="checkbox-inline checkbox-status">
            {!! Form::hidden('status', false) !!}
            {!! Form::checkbox('status', 1, null, ['class' => 'js-switch']) !!}
        </label>
    </div>
</div>

<div class="form-group row col-sm-12 text-center">
    <a href="{!! route('admin.posts.index') !!}" class="btn btn-default">Cancel</a>
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
</div>

@push('scripts')
    <script src="{!! URL::to("assets//jquery.tagsinput/src/jquery.tagsinput.js") !!}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            init_TagsInput();
        });
        function onAddTag(tag) {
            alert("Added a tag: " + tag);
        }

        function onRemoveTag(tag) {
            alert("Removed a tag: " + tag);
        }

        function onChangeTag(input, tag) {
            alert("Changed a tag: " + tag);
        }

        function init_TagsInput() {

            if(typeof $.fn.tagsInput !== 'undefined'){

                $('#tags_id').tagsInput({
                    width: 'auto'
                });

            }

        }
    </script>
@endpush