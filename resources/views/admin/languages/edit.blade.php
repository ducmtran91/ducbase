@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h2>Edit Languages</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-md-12">
                    @include('adminlte-templates::common.errors')
                </div>
                
                {!! Form::model($data, ['route' => ['admin.languages.update', $data->id], 'method' => 'patch', 'class' => 'form jsvalidation', 'novalidate' => 'novalidate', 'files' => true]) !!}
                    @include('admin.languages.fields')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection