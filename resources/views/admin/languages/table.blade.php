@php
    $showPermission = Helper::checkUserPermission('admin.languages.show');
    $editPermission = Helper::checkUserPermission('admin.languages.edit');    
    $itemActionsPermission = Helper::checkUserPermission('admin.languages.itemActions');
@endphp

<table class="table table-responsive" id="languages-table">
    <thead>
    <th>ID</th>
    <th>Image</th>
    <th>Name</th>
    <th>Locale</th>
    <th>Status</th>
    <th>Action</th>
    <th class="text_right">
        <div class="checkbox">
            <label>
                <input type="checkbox" value="" id="select_all">
                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            </label>
        </div>
    </th>
    </thead>
    <tbody>
    @foreach($datas as $key => $item)
        <tr>
            <td>
                <div class="text-more">
                    {!! $item->id or ''  !!}
                </div>
            </td>
            <td>
                <div class="text-more">
                    <img class="img-responsive w_image_50"
                         src="{!! (!empty($item->image) ? ('/'.config('filepath.languages_image') . $item->image) : (config('filepath.no_image'))) !!}" alt="{{ $item->name }}" title="{{ $item->name }}">
                </div>
            </td>
            <td>
                @if($showPermission)
                    <a href="{!! URL::action('Backend\LanguagesController@show', ['languages' => $item->id]) !!}"
                       class="link">
                        <div class="text-more">
                            {!! $item->name or ''  !!}
                        </div>
                    </a>
                @else
                    <div class="text-more">
                        {!! $item->name or ''  !!}
                    </div>
                @endif
            </td>
            <td>
                <div class="text-more">
                    {!! $item->locale or ''  !!}
                </div>
            </td>
            <td>
                @if($item->status)
                    <button {!! !$itemActionsPermission ? 'disabled="disabled"' : '' !!} type="button"
                            class="btn_status btn_status_success item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="language" data-text="inactive" data-val="0"
                            data-id="{!! $item->id !!}">Active
                    </button>
                @else
                    <button {!! !$itemActionsPermission ? 'disabled="disabled"' : '' !!} type="button"
                            class="btn_status btn_status_false item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="language" data-text="active" data-val="1"
                            data-id="{!! $item->id !!}">Inactive
                    </button>
                @endif
            </td>
            <td>
                <div class='btn-group'>
                    @if($showPermission)
                        <a href="{!! URL::action('Backend\LanguagesController@show', ['languages' => $item->id]) !!}"
                           class='btn btn-default btn-xs'>
                            <i class="glyphicon glyphicon-eye-open"></i>
                        </a>
                    @endif

                    @if($editPermission)
                        <a href="{!! URL::action('Backend\LanguagesController@edit', ['languages' => $item->id]) !!}"
                           class='btn btn-default btn-xs btn-edit'>
                            <i class="glyphicon glyphicon-edit"></i>
                        </a>
                    @endif

                    @if($itemActionsPermission)
                        <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-edit item_actions"
                           data-link="data-link-item-actions"
                           data-key="delete" data-title="language" data-text="delete" data-val="none"
                           data-id="{!! $item->id !!}">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    @endif
                </div>
            </td>
            <td colspan="3" class="text_right">
                <div class="checkbox">
                    <label>
                        <input class="checkbox_item" type="checkbox" name="checkbox" value="{{$item->id}}">
                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                    </label>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div class="pagination-area">
    {!! $datas->appends(app('request')->all())->links() !!}
</div>
