<div class="form-group row col-sm-12">
    <div class="control-label col-md-2 col-sm-2 col-xs-12">
        {!! Form::label('image', 'Image') !!}
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
        @php
            $classFileInput = 'fileinput-new';
            if(isset($data->image) || !empty($oldImage)){
                $classFileInput = 'fileinput-exists';
            }
        @endphp

        <div class="fileinput {!! $classFileInput !!}"
             data-provides="fileinput">
            <div class="fileinput-preview fileinput-exists thumbnail img-thumb">
                @if(isset($data->image) || !empty($oldImage))
                    @php
                        if(isset($data->image)){
                            $src = url(config('filepath.languages_image') . $data->image);
                        }else{
                            $src = $oldImage;
                        }
                    @endphp
                    <img src="{{ $src }}">
                    <input type="hidden" name="image_tmp" value="{{ $data->image }}"/>
                @endif
            </div>

            <div>
                <span class="btn btn-default btn-file">
                    <span class="fileinput-new">Choose File</span>
                    <span class="fileinput-exists">Edit</span>
                    <input type="file" name="image" accept="image/*">
                </span>
                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Delete</a>
            </div>
            <div class="text-center">
                <span>50x100</span>
            </div>
        </div>
    </div>
</div>
<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('name', 'Name') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control', 'maxlength' => 255, 'placeholder' => 'Name']) !!}
    </div>
</div>

<div class="form-group row col-sm-12">
    <div class="control-label col-md-2 col-sm-2 col-xs-12">
        {!! Form::label('locale', 'Locale') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8 col-xs-10">
        {!! Form::text('locale', null, ['class' => 'form-control', 'maxlength' => 255, 'placeholder' => 'Locale']) !!}
    </div>
</div>

<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('status', 'Status') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8 col-xs-10">
        <label class="checkbox-inline checkbox-status">
            {!! Form::hidden('status', false) !!}
            {!! Form::checkbox('status', 1, null, ['class' => 'js-switch']) !!}
        </label>
    </div>
</div>

<div class="form-group row col-sm-12 text-center">
    <a href="{!! route('admin.languages.index') !!}" class="btn btn-default">Cancel</a>
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
</div>
