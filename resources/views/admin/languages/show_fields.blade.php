@php
    $addPermission = Helper::checkUserPermission('admin.categories.create');
    $editPermission = Helper::checkUserPermission('admin.categories.edit');    
@endphp

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Category information</h2>
                <ul class="nav navbar-right panel_toolbox">                    
                    @if($editPermission)
                        <li>
                            <a href="{!! URL::action('Backend\CategoriesController@edit', ['categories' => $data->id]) !!}" class="btn btn-default">
                                <i class="fa fa-edit"></i>
                                Edit Category
                            </a>
                        </li>
                    @endif
                    
                    @if($addPermission)
                        <li>
                            <a href="{!! URL::action('Backend\CategoriesController@create') !!}" class="btn btn-default">
                                <i class="fa fa-plus"></i>
                                Create new Category
                            </a>
                        </li>
                    @endif                    
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-10 col-sm-10 col-xs-12">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="row mg-top-20 mg-bottom-20">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label>Name</label>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    {!! $data->name or '' !!}
                                </div>
                            </div>

                            <div class="row mg-top-20 mg-bottom-20">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label>Parent Category</label>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    {!! !empty($data->parent) ? $data->parent->name : '' !!}
                                </div>
                            </div>

                            <div class="row mg-top-20 mg-bottom-20">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label>Position</label>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    {{$data->position}}
                                </div>
                            </div>

                            <div class="row mg-top-20 mg-bottom-20">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <label>Status</label>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <label class="checkbox-inline checkbox-status">
                                        <input readonly="true" type="checkbox" class="js-switch" {!! !empty($data->status) ? 'checked' : '' !!}/>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>