<div class="form-group row col-sm-12">
    <div class="control-label col-md-2 col-sm-2 col-xs-12">
        {!! Form::label('image', trans('backend.label.image')) !!}
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
        @php
            $classFileInput = 'fileinput-new';
            if(isset($data->image) || !empty($oldImage)){
                $classFileInput = 'fileinput-exists';
            }
        @endphp

        <div class="fileinput {!! $classFileInput !!}"
             data-provides="fileinput">
            <div class="fileinput-preview fileinput-exists thumbnail img-thumb">
                @if(isset($data->image) || !empty($oldImage))
                    @php
                        if(isset($data->image)){
                            $src = url(config('filepath.'.$route.'_image') . $data->image);
                        }else{
                            $src = $oldImage;
                        }
                    @endphp
                    <img src="{{ $src }}">
                    <input type="hidden" name="image_tmp" value="{{ $src }}"/>
                @endif
            </div>

            <div>

                <span class="btn btn-default btn-file">
                    <span class="fileinput-new">{{ trans('backend.label.file.choose') }}</span>
                    <span class="fileinput-exists">{{ trans('backend.label.actions.edit') }}</span>
                    <input type="file" name="image" accept="image/*">
                </span>
                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">{{ trans('backend.label.actions.delete') }}</a>
            </div>
            <div class="text-center">
                <span>1920x710</span>
            </div>
        </div>
    </div>
</div>
@php
    $langs = config('translatable.locales');
@endphp
<div class="form-group row col-sm-12">
    <div class="" role="tabpanel" data-example-id="togglable-tabs">
        @include('admin.generates.tab-languages', ['languages' => $langs])
        <div id="myTabContent" class="tab-content">
            @foreach($langs as $key => $locale)
                <div role="tabpanel" class="tab-pane fade @if($key == 0) active in @endif" id="lang-{{ $key }}" aria-labelledby="lang-tab-{{ $key }}">
                    <div class="full-width left">
                        @include('layouts.inputs.text', [
                            'colLabel' => 'col-sm-2',
                            'colInput' => 'col-sm-8',
                            'label' => trans('backend.label.title'),
                            'id' => $locale.'[name]',
                            'value' => !empty($data) ? $data->translate($locale)->name : null
                        ]);
                    </div>
                        @php
                        if(!empty($data)) {
                            $json = json_decode($data->translate($locale)->data);
                        }
                    @endphp
                    <div class="full-width left">
                        @include('layouts.inputs.text', [
                            'colLabel' => 'col-sm-2',
                            'colInput' => 'col-sm-8',
                            'label' => trans('backend.label.text.button'),
                            'id' => $locale.'[btn]',
                            'value' => isset($json) ? $json->btn : null
                        ]);
                    </div>

                    <div class="full-width left">
                        @include('layouts.inputs.textarea', [
                            'colLabel' => 'col-sm-2',
                            'colInput' => 'col-sm-8',
                            'label' => trans('backend.label.description'),
                            'id' => $locale.'[description]',
                            'value' => isset($json) ? $json->description : null,
                            'validation' => ['maxlength' => 255]
                        ]);
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
<div class="form-group row col-sm-12">
    <div class="control-label col-md-2 col-sm-2 col-xs-12">
        {!! Form::label('slug', trans('backend.label.link')) !!}
    </div>
    <div class="form-group col-sm-8 col-xs-10">
        {!! Form::text('slug', null, ['class' => 'form-control', 'maxlength' => 255, 'placeholder' => trans('backend.label.link')]) !!}
    </div>
</div>


<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('position', trans('backend.label.position')) !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8">
        {!! Form::number('position', null, ['class' => 'form-control', 'maxlength' => 255, 'placeholder' => trans('backend.label.position')]) !!}
    </div>
</div>

<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('status', trans('backend.label.status')) !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8 col-xs-10">
        <label class="checkbox-inline checkbox-status">
            {!! Form::hidden('status', false) !!}
            {!! Form::checkbox('status', 1, null, ['class' => 'js-switch']) !!}
        </label>
    </div>
</div>

<div class="form-group row col-sm-12 text-center">
    <a href="{!! route('admin.'.$route.'.index') !!}" class="btn btn-default">{{ trans('backend.label.actions.cancel') }}</a>
    {!! Form::submit(trans('backend.label.actions.save'), ['class' => 'btn btn-success']) !!}
</div>
