@php
    $showPermission = Helper::checkUserPermission('admin.'.$menu.'.show');
    $editPermission = Helper::checkUserPermission('admin.'.$menu.'.edit');
    $itemActionsPermission = Helper::checkUserPermission('admin.'.$menu.'.itemActions');
@endphp

<table class="table table-responsive" id="{{ $menu }}-table">
    <thead>
    <th>{{ trans('backend.label.id') }}</th>
    <th>{{ trans('backend.label.image') }}</th>
    <th>{{ trans('backend.label.title') }}</th>
    <th>{{ trans('backend.label.link') }}</th>
    <th>{{ trans('backend.label.position') }}</th>
    <th>{{ trans('backend.label.status') }}</th>
    <th>{{ trans('backend.label.action') }}</th>
    <th class="text_right">
        <div class="checkbox">
            <label>
                <input type="checkbox" value="" id="select_all">
                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            </label>
        </div>
    </th>
    </thead>
    <tbody>
    @foreach($datas as $key => $item)
        <tr>
            <td>
                <div class="text-more">
                    {!! $item->id or ''  !!}
                </div>
            </td>
            <td>
                <div class="text-more w-100">
                    <img class="img-file-upload" src="{!! $item->image ? url(config('filepath.'.$route.'_image') . $item->image) : url(config('filepath.no_image')) !!}"/>
                </div>
            </td>
            <td>
                @if($showPermission)
                    <a href="{!! URL::action($controller.'show', [$menu => $item->id]) !!}"
                       class="link">
                        <div class="text-more">
                            {!! $item->name or ''  !!}
                        </div>
                    </a>
                @else
                    <div class="text-more">
                        {!! $item->name or ''  !!}
                    </div>
                @endif
            </td>
            <td>
                <div class="text-more">
                    {!! $item->url or ''  !!}
                </div>
            </td>
            <td>
                <div class="text-more">
                    {!! $item->position or ''  !!}
                </div>
            </td>
            <td>
                @if($item->status)
                    <button {!! !$itemActionsPermission ? 'disabled="disabled"' : '' !!} type="button"
                            class="btn_status btn_status_success item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="{{ $menu }}" data-text="inactive" data-val="0"
                            data-id="{!! $item->id !!}">Active
                    </button>
                @else
                    <button {!! !$itemActionsPermission ? 'disabled="disabled"' : '' !!} type="button"
                            class="btn_status btn_status_false item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="{{ $menu }}" data-text="active" data-val="1"
                            data-id="{!! $item->id !!}">Inactive
                    </button>
                @endif
            </td>
            <td>
                <div class='btn-group'>
                    @if($showPermission)
                        <a href="{!! URL::action($controller.'show', [$menu => $item->id]) !!}"
                           class='btn btn-default btn-xs'>
                            <i class="glyphicon glyphicon-eye-open"></i>
                        </a>
                    @endif

                    @if($editPermission)
                        <a href="{!! URL::action($controller.'edit', [$menu => $item->id]) !!}"
                           class='btn btn-default btn-xs btn-edit'>
                            <i class="glyphicon glyphicon-edit"></i>
                        </a>
                    @endif

                    @if($itemActionsPermission)
                        <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-edit item_actions"
                           data-link="data-link-item-actions"
                           data-key="{{ strtolower($delete) }}" data-title="{{ $menu }}" data-text="{{ strtolower($delete) }}" data-val="none"
                           data-id="{!! $item->id !!}">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    @endif
                </div>
            </td>
            <td colspan="3" class="text_right">
                <div class="checkbox">
                    <label>
                        <input class="checkbox_item" type="checkbox" name="checkbox" value="{{$item->id}}">
                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                    </label>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div class="pagination-area">
    {!! $datas->appends(app('request')->all())->links() !!}
</div>
