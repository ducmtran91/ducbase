@extends('layouts.admin')

@section('content')
    @php
        $route = trans("backend.route.slider");
    @endphp
<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h2>{{ trans('backend.label.management.'.$route.'.edit') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-md-12">
                    @include('adminlte-templates::common.errors')
                </div>
                
                {!! Form::model($data, ['route' => ['admin.'.$route.'.update', $data->id], 'method' => 'patch', 'class' => 'form jsvalidation', 'novalidate' => 'novalidate', 'files' => true]) !!}
                    @include('admin.'.$route.'.fields')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection