@extends('layouts.admin')

@section('content')
@php
    $controller = "Backend\SliderController@";
    $route = trans("backend.route.slider");
    $params = [
        "route" => $route,
        "controller" => $controller
    ];
@endphp
<div class="content">
    <div class="box-primary">
        <div class="box-body">
            <div class="row col-sm-12">
                @include('admin.'.$route.'.show_fields', $params)
            </div>
        </div>
    </div>
</div>
@endsection