@extends('layouts.admin')

@section('content')
<div class="row">
    @php
        $controller = "Backend\SliderController@";
        $menu = trans("backend.menu.slider");
        $route = trans("backend.route.slider");
        $status = trans("backend.label.status");
        $delete = trans("backend.label.actions.delete");
        $params = [
            "menu" => $menu,
            "route" => $route,
            "status" => $status,
            "delete" => $delete,
            "controller" => $controller
        ];
    @endphp
    <div class="x_panel">
        <div class="x_title">
            <h2>{{ trans('backend.label.management.'.$route.'.create') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-md-12">
                    @include('adminlte-templates::common.errors')
                </div>
                
                {!! Form::open(['route' => 'admin.slider.store', 'class' => 'form jsvalidation', 'novalidate' => 'novalidate', 'files' => true]) !!}
                    @include('admin.slider.fields')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
