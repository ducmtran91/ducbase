@php
    $addPermission = Helper::checkUserPermission('admin.'.$route.'.create');
    $bulkActionsPermission = Helper::checkUserPermission('admin.'.$route.'.bulkActions');
@endphp

<div class="row col-xs-12 mg-bottom-20">
    <form action="" method="get" id="form_custom_list">
        <div class="wrap-filters x_title">
            <div class="row">
                <div class="col-xs-8 col-md-8 col-sm-8 box_search">
                    <div class="col-xs-4 col-md-4 col-sm-4">
                        <div class="input-group">
                            <span class="search-action input-group-addon"><i class="fa fa-search"></i></span>
                            <input type="text" class="form-control enter-submit search_name"
                                   name="keyword" placeholder="{{ trans('backend.label.search') }} {{ trans('backend.label.title') }}"
                                   value="{{$keyword or ''}}"/>
                        </div>
                    </div>
                    <div class="col-xs-3 col-md-3 col-sm-3">
                        <select name="filter_status" class="form-control filter_header">
                            <option value="none" {!! ($filterStatus === 'none') ? 'selected="selected"' : '' !!}>
                                {{ $status }}
                            </option>
                            @if(!empty($statuses))
                                @foreach($statuses as $key => $status)
                                    <option value="{{ $key }}" {!! ($filterStatus === $key) ? 'selected="selected"' : '' !!}>
                                        {{ $status }}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="col-xs-2 col-md-2 col-sm-2">
                        <a class="btn btn-danger"
                           href="{{ URL::action($controller.'index') }}">{{ $delete }}</a>
                    </div>
                </div>
                <div class="col-xs-4 col-md-4 col-sm-4 pd-right-0">
                    <ul class="nav navbar-right panel_toolbox">
                        @if($addPermission)
                            <li>
                                <a href="{!! URL::action($controller.'create') !!}"
                                   class="btn-toolbox success">
                                    <i class="fa fa-plus"></i>{{ trans('backend.label.actions.add') }}
                                </a>
                            </li>
                        @endif

                        @if($bulkActionsPermission)
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                    {{ trans("backend.label.actions.bulk") }}<span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li role="presentation">
                                        <a href="javascript:void(0);" value="1" class="bulk_action_change"
                                           data-link="data-link-bulk-actions" data-key="{{ $status }}" data-title="{{ $menu }}"
                                           data-text="active">Active</a>
                                    </li>
                                    <li role="presentation">
                                        <a href="javascript:void(0);" value="0" class="bulk_action_change"
                                           data-link="data-link-bulk-actions" data-key="{{ $status }}" data-title="{{ $menu }}"
                                           data-text="inactive">Inactive</a>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="row div_options mg-bottom-10"></div><!-- /.row -->
        <div class="div_option">
            <div class="col-xs-4 col-md-2 col-sm-2 count_record">
                <span class="viewing">{{$total . ' ' . ($total > 1 ? $menu : $menu)}} </span>
            </div>
            <div class="col-xs-8 col-md-10 col-sm-10 pd-right-0">
                <ul class="nav navbar-right panel_toolbox">
                    <li class="viewing">{{ trans('backend.label.filter.paging') }}</li>
                    <li class="dropdown">
                        <select class="form-control" name="limit" id="limit_pagination">
                            <option @if ($limit == 10)selected="selected" @endif value="10">
                                10
                            </option>
                            <option @if ($limit == 25)selected="selected" @endif value="25">
                                25
                            </option>
                            <option @if ($limit == 50)selected="selected" @endif value="50">
                                50
                            </option>
                            <option @if ($limit == 75)selected="selected" @endif value="75">
                                75
                            </option>
                            <option @if ($limit == 100)selected="selected" @endif value="100">
                                100
                            </option>
                        </select>
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
</div><!-- /.filter -->