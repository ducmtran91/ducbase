<div id="myTabContent" class="tab-content">
    @foreach($languages as $key => $item)
        <div role="tabpanel" class="tab-pane fade @if($key == 0) active in @endif" id="lang-{{ $item->id }}" aria-labelledby="lang-tab-{{ $item->id }}">
            <div class="full-width left">
                <div class="form-group col-sm-2">
                    {!! Form::label("name[{$item->id }]", 'Name') !!}
                    <span class="text-danger">*</span>
                </div>
                <div class="form-group col-sm-8">
                    {!! Form::text("name[{$item->id }]", $data ? $data->name : null, ['class' => 'form-control', 'maxlength' => 255, 'placeholder' => 'Name']) !!}
                </div>
            </div>


            <div class="full-width left">
                <div class="form-group col-sm-2">
                    {!! Form::label("description[{$item->id }]", 'Description') !!}
                    <span class="text-danger">*</span>
                </div>
                <div class="form-group col-sm-8">
                    {!! Form::textarea("description[{$item->id }]", null, ['class' => 'form-control','rows' => '4','style' => 'resize:none', 'placeholder' => 'Description']) !!}
                </div>
            </div>

            <div>
                <div class="form-group col-sm-2">
                    {!! Form::label("btn[{$item->id }]", 'Button text') !!}
                    <span class="text-danger">*</span>
                </div>
                <div class="form-group col-sm-8">
                    {!! Form::text("btn[{$item->id }]", null, ['class' => 'form-control', 'maxlength' => 255, 'placeholder' => 'Button text']) !!}
                </div>
            </div>
        </div>
    @endforeach
</div>