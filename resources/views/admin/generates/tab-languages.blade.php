<div class="full-width">
    <div class="form-group col-sm-10 col-xs-12">
        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            @foreach($languages as $key => $locale)
                <li role="presentation" class="@if($key == 0) active @endif">
                    <a href="#lang-{{ $key }}" id="lang-tab-{{ $key }}" role="tab" data-toggle="tab" aria-expanded="true">{{ $locale }}</a>
                </li>
            @endforeach
        </ul>
    </div>
</div>