<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('name', 'Name') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control', 'maxlength' => 255, 'placeholder' => 'Name']) !!}
    </div>
</div>

<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('description', 'Description') !!}
    </div>
    <div class="form-group col-sm-8">
        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
    </div>
</div>

<!-- Status Field -->
<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('status', 'Status') !!}
        <span class="text-danger">*</span>
    </div>
    <div class="form-group col-sm-8 col-xs-10">
        <label class="checkbox-inline checkbox-status">
            {!! Form::hidden('status', false) !!}
            {!! Form::checkbox('status', 1, null, ['class' => 'js-switch']) !!}
        </label>
    </div>
</div>

<div class="form-group row col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('status', 'Permissions') !!}
    </div>
    <div class="form-group col-sm-8">
        <!-- Permission List -->
        @php        
            $assignAll = array();
            $permissionsList = config("permission");

            if(old('permission', null) !== null) {
                $permissionAsigned = old('permission');
            } else {
                $permissionAsigned = isset($roles->permission) ? $roles->permission : null;
                $permissionAsigned = ($permissionAsigned != null) ? json_decode($permissionAsigned, true) : array();
            } 
        @endphp

        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-success tab-dropdown mg-bottom-10 collapsed" aria-expanded="false" data-toggle="collapse" data-target="#permission-area">Open to select permission</button>

                <div id="permission-area" class="collapse-custom collapse">                    
                    <div class="row">
                        <ul class="treeview">
                            @if(!empty($permissionsList))
                                <li>
                                    <input class="treeview-lv1" data-role="treeview-cb" type="checkbox" id="tree">
                                    <label for="tree" class="custom-unchecked">Select All</label>

                                    <ul>
                                        @php 
                                            $totalLevel2 = count($permissionsList);
                                            $count = 1;
                                        @endphp

                                        @foreach($permissionsList as $key => $pms)
                                            <li class="{!! ($count == $totalLevel2) ? 'last' : '' !!}">
                                                <input class="treeview-lv2" data-role="treeview-cb" type="checkbox" id="tree-{!! $count !!}">
                                                <label for="tree-{!! $count !!}" class="custom-unchecked">{!! $pms['name'] !!}</label>
                                                <ul>
                                                    @if(!empty($pms['actions']))
                                                        @php 
                                                            $totalLevel3 = count($pms['actions']);
                                                            $subCount = 1;
                                                        @endphp

                                                        @foreach($pms['actions'] as $subKey => $elem)
                                                            @php
                                                                $fieldData = explode('@', $elem['action']);
                                                                $parentKey = Helper::recursive_array_search($elem['action'], $permissionAsigned);
                                                                
                                                                if($parentKey !== false && array_key_exists($parentKey, $permissionAsigned)) {
                                                                    $cls = 'checked';
                                                                    $checkAttr = 'checked="checked"';
                                                                } else {
                                                                    $cls = 'unchecked';
                                                                    $checkAttr = '';
                                                                }                                                              
                                                            @endphp

                                                            <li class="treeview-half {!! ($subCount == $totalLevel3) ? 'last' : '' !!}">
                                                                <input class="treeview-lv3" 
                                                                       data-role="treeview-cb" 
                                                                       type="checkbox" 
                                                                       name="permission[{!! $fieldData[0] !!}][]" 
                                                                       id="tree-{!! $count !!}-{!! $subCount !!}" 
                                                                       value="{!! $elem['action'] !!}"
                                                                       {!! $checkAttr !!}
                                                                       />
                                                                <label for="tree-{!! $count !!}-{!! $subCount !!}" class="custom-{!! $cls !!}">{!! $elem['name'] !!}</label>
                                                            </li>
                                                            @php $subCount++; @endphp
                                                        @endforeach    
                                                    @endif                                                       
                                                </ul>
                                            </li>

                                            @php $count++; @endphp
                                        @endforeach  
                                    </ul>
                                </li>
                            @endif      
                        </ul>
                    </div>
                </div>                                    
            </div>
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group row col-sm-12 text-center">
    <a href="{!! route('admin.roles.index') !!}" class="btn btn-default">Cancel</a>
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
</div>
