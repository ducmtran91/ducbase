@php
    $editPermission = Helper::checkUserPermission('admin.roles.edit');    
    $itemActionsPermission = Helper::checkUserPermission('admin.roles.itemActions');
@endphp

<table class="table table-responsive" id="roles-table">
    <thead>
        <th>Name</th>
        <th>Description</th>    
        <th>Status</th>
        <th>Action</th>
        <th class="text_right">
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="" id="select_all">
                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                </label>
            </div>
        </th>
    </thead>
    <tbody>
        @foreach($roles as $key => $role)
        <tr>
            <td>
                @if($editPermission)
                    <a href="{!! URL::action('Backend\RolesController@edit', ['roles' => $role->id]) !!}" class="link">
                        <div class="text-more">
                            {!! $role->name or ''  !!}
                        </div>                    
                    </a> 
                @else
                    <div class="text-more">
                        {!! $role->name or ''  !!}
                    </div>                    
                @endif                  
            </td>
            <td>
                <div class="text-more">
                    {!! $role->description or ''  !!}
                </div>
            </td>           
            <td>
                @if($role->status)
                    <button {!! !$itemActionsPermission ? 'disabled="disabled"' : '' !!} type="button" class="btn_status btn_status_success item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="role" data-text="inactive" data-val="0"
                            data-id="{!! $role->id !!}">Active
                    </button>
                @else
                    <button {!! !$itemActionsPermission ? 'disabled="disabled"' : '' !!} type="button" class="btn_status btn_status_false item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="role" data-text="active" data-val="1"
                            data-id="{!! $role->id !!}">Inactive
                    </button>
                @endif
            </td>            
            <td>
                <div class='btn-group'>
                    @if($editPermission)
                        <a href="{!! URL::action('Backend\RolesController@edit', ['roles' => $role->id]) !!}" 
                            class='btn btn-default btn-xs btn-edit'>
                             <i class="glyphicon glyphicon-edit"></i>
                        </a> 
                    @endif                       
                    
                    @if($itemActionsPermission)
                        <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-edit item_actions"
                            data-link="data-link-item-actions"
                            data-key="delete" data-title="role" data-text="delete" data-val="none"
                            data-id="{!! $role->id !!}">
                             <i class="glyphicon glyphicon-trash"></i>
                         </a>
                    @endif                          
                </div>
            </td>
            <td colspan="3" class="text_right">
                <div class="checkbox">
                    <label>
                        <input class="checkbox_item" type="checkbox" name="checkbox" value="{{$role->id}}">
                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                    </label>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<div class="pagination-area">
    {!! $roles->appends(app('request')->all())->links() !!}
</div>