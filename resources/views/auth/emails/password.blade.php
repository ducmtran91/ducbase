
<p style="margin-bottom: 5px;margin-top: 0px;">Welcome to  Wize Pawz<b></b>!</p>
<p style="margin-bottom: 5px;margin-top: 0px;">We're glad to tell you that you have been assigned as Administrator. Now you can log in to the administration system with following credential:</p>
<p style="margin-bottom: 5px;margin-top: 0px;">Email: {{ $user->email }}</p>
<p style="margin-bottom: 5px;margin-top: 0px;">Link Resend Password:
    <a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}" style="display: inline-block;
                            font-size: 14px;
                            padding: 7px 24px;
                            border-radius: 4px;
                            color: #fff;
                            border: 1px solid #10a107;
                            background: #10a107;
                            font-weight: 400;">Change my password</a>
</p>
<p style="margin-bottom: 5px;margin-top: 0px;">You will need this every time you want to log in to the administration system, so keep it safe and don't share it with anyone.</p>
<p style="margin-bottom: 5px;margin-top: 0px;">Thanks and Best Regards, </p>
<p style="margin-top: 0px;margin-bottom: 20px;">Wize Pawz Team</p>

{{--Click here to reset your password: <a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a>--}}
