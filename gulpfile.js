process.env.DISABLE_NOTIFIER = true;
var elixir = require('laravel-elixir');
var gulp = require('gulp');
var concat = require('gulp-concat');
var spritesmith = require('gulp.spritesmith');

require('laravel-elixir-livereload');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    //===== combine vendor scripts =====
    // mix.scripts([
        
    // ], 'public/builds/js/vendor.js', 'public');

    //===== combine manual scripts =====
    // mix.scriptsIn('resources/assets/frontend/js', 'public/builds/js/main.js', 'resources/assets');

    //===== combine vendor style =====
    // mix.styles([
        
    // ], 'public/builds/css/vendor.css', 'public');

    //===== combine manual style =====
    // mix.sass('./resources/assets/frontend/sass/', 'public/builds/css/main.css');

    //===== combine vendor admin scripts =====
    mix.scripts([
        '/assets/jquery/dist/jquery.min.js',
        '/assets/bootstrap/dist/js/bootstrap.min.js',
        '/assets/jasny-bootstrap/dist/js/jasny-bootstrap.min.js',        
        '/plugins/sweetalert/sweetalert.js',
        '/plugins/moment/moment.min.js',
        '/plugins/daterangepicker/daterangepicker.js',
        '/plugins/jquery-ui/jquery-ui.min.js',
        '/plugins/readmore/readmore.min.js',
        '/plugins/masonry/masonry.pkgd.min.js',
        '/plugins/treeview-cb/script.js',
        '/assets/switchery/dist/switchery.js',
        '/assets/iCheck/icheck.min.js',
        '/assets/bootbox.js/bootbox.js',       
        '/assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
        '/assets/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
        '/assets/vex/dist/js/vex.combined.min.js',
        '/assets/select2/dist/js/select2.full.min.js',
        '/assets/taggle/src/taggle.js',
        '/assets/toastr/toastr.min.js',
        '/assets/jquery-loading/dist/jquery.loading.min.js',
        '/assets/dropzone/dist/min/dropzone.min.js',
        // '/assets/highcharts/highcharts.js',
        '/assets/bootstrap3-typeahead/bootstrap3-typeahead.min.js',
        '/themes/dashboard/js/main.js',
        '/themes/dashboard/js/notification.js'
    ], 'public/builds/js/vendor.admin.js', 'public');

    //===== combine vendor admin style =====
    mix.styles([
        '/assets/bootstrap/dist/css/bootstrap.min.css',
        '/assets/fontawesome/css/font-awesome.css',
        '/assets/jasny-bootstrap/dist/css/jasny-bootstrap.min.css',             
        '/assets/switchery/dist/switchery.min.css',        
        '/assets/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
        '/assets/vex/dist/css/vex.css',
        '/assets/vex/dist/css/vex-theme-os.css',
        '/assets/select2/dist/css/select2.min.css',
        '/assets/toastr/toastr.min.css',
        '/assets/jquery-loading/dist/jquery.loading.min.css',
        '/assets/dropzone/dist/min/dropzone.min.css',
        '/plugins/sweetalert/sweetalert.css',
        '/plugins/daterangepicker/daterangepicker.css',
        '/plugins/jquery-ui/jquery-ui.min.css',
        '/plugins/treeview-cb/style.css',
        '/themes/dashboard/css/style.css',
        '/themes/dashboard/css/custom.css',
        '/assets/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
    ], 'public/builds/css/vendor.admin.css', 'public');

    //===== combine manual admin scripts =====
    mix.scriptsIn('./resources/assets/backend/js', 'public/builds/js/main.admin.js', 'resources/assets');
    
    //===== combine manual admin style =====
    mix.sass('./resources/assets/backend/sass/', 'public/builds/css/main.admin.css');

    //==== livereload ====
    mix.livereload([
        // 'resources/assets/frontend/**/*',
        'resources/assets/backend/**/*',
        'resources/assets/backend/**/**/*.scss',
        'resources/views/**/*'
    ]);

    mix.sass('./resources/assets/sass/app.scss', 'public/builds/css');
    mix.scripts([
        './resources/assets/frontend/js/load-more.js'
        ], 'public/builds/js/vendor.frontend.js');
});

//---- fonts ----
gulp.task('fonts-fontawesome', function() {
    return gulp.src('./public/assets/fontawesome/fonts/*').pipe(gulp.dest('./public/builds/fonts/'));
});

gulp.task('fonts-bootstrap', function() {
    return gulp.src('./public/assets/bootstrap/dist/fonts/*').pipe(gulp.dest('./public/builds/fonts/'));
});

gulp.task('fonts', ['fonts-fontawesome', 'fonts-bootstrap']);
